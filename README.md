# InsituBalance
This is an EventLoop package for calibrating jets using a direct balance technique.
It is used in Z+jet, gamma+jet, and multijet events.

An example tutorial for running the MultijetBalance can be found on eos at /eos/atlas/atlascerngroupdisk/perf-jets/JESJER/ Recommendations/r21/MJB/MJBUpdateFinal_23Oct2018.pdf
This is a very useful document for learning the workflow of the InsituBalance code and understanding the various pieces!

## Broad description of analysis workflow and package purposes

`InsituBalanceAlgo` is the main code to run on DAODs and produces histogram output.
Because of the large number of uncertainties, TTrees are not regularly produced.
TTrees can be produced for nominal-only or nominal+systematic configurations, and should be used for the first iteration to allow for further cross-checking and debugging.
For the final production of calibrations, only the histogram output is necessary and recommended.
`InsituBalanceAlgo` relies on `Selections.h` and `JetCalibrations.h` to perform the dedicated jet calibrations and event selections.
`MultijetHists.cxx` defines the histograms to be output from DAODs.
`MiniTree.cxx` defines the additional branches for the TTree output, beyond the general ones defined in xAODAnaHelpers' `HelpTreeBase.cxx`.

`HistogramTree.cxx` is independent EventLoop code that can run on the output TTrees to make histograms similar to those from `MultijetHists.cxx`.
__It is not yet fully configured for Z+jet events, and should be considered a work in progress.__

The `scripts` directory includes python scripts for grid submission, response fitting, manipulating systematic uncertainties, and additional studies.

### Bootstrapping procedure
Systematic uncertainties can vary the number of events used in the response calculation, and systematic fluctuations may not always be statistically significant.
To find the statistical significance of systematic variations, the nominal fitting procedure is run over toy distributions, where each toy consists of
a subset of data or MC events that is selected randomly from a poisson distribution.
The procedure of generating and comparing these toy events is refered to as Bootstrapping.
This allows for the fitted response to be randomly varied within uncertainties, and the total shift of a systematic comparison to be compared against shifts due to random fluctuations.
This procedure does not affect the mean systematic uncertainty result from the nominal data and MC, but affects the binning at which those uncertainties are presented.

## Installation
Follow ATLAS cmake guidelines, the top directory should have 2 subdirectories, `build` and `source`.
All packages should be checked out into the `source` directory.

The InsituBalance package can be retrived from git with a command like:
```
git clone --recursive https://gitlab.cern.ch/atlas-jetetmiss-jesjer/Insitu/InsituBalance.git
```
The InsituBalance framework depends on several external packages, listed below, which are treated as submodules.
Using the --recursive option when cloning the package will check out all other packages in the `external_package` directory.
This removes the need to check them out one-by-one, and ensures that each commit of InsituBalance is tied to a specific version of each external packages.

### Dependencies
#### xAODAnaHelpers
The EventLoop portion of the package is built upon the xAODAnaHelpers (xAH) analysis framework.
General information on xAODAnaHelpers can be found on [ReadTheDocs](https://xaodanahelpers.readthedocs.io/en/latest/).
This lightweight framework handles the basic event-level selections as well as physics object-level calibrations and selections.
Each task is performed with an algorithm, which is a piece of EventLoop code, and each event will be processed through each algorithm sequentially.
The latest tested version of xAH was commit tag 8ff67a46eae1c, and a tag will be produced once the final 2017 multijet calibration is produced.

#### JES_ResponseFitter
Once histograms of the jet response is created, the Gaussian distributions are fit with the [JES_ResponseFitter](https://gitlab.cern.ch/atlas-jetetmiss-jesjer/tools/JES_ResponseFitter) package.
A central package exists so that all JES calibrations use similar fit functionality.
This package provides fitting machinery in both C++ and python, and the InsituBalance package uses the python functionality.
The latest version of JES_ResponseFitter is usually correct.

#### BootstrapGenerator
The [BoostrapGenerator](https://gitlab.cern.ch/cjmeyer/BootstrapGenerator) is the central tool for generating bootstrap objects,
the toy distributions derived from data or MC that reflect random statistical variations.
The latest version of BoostrapGenerator is usually correct.

#### SystTool
The [SystTool](https://gitlab.cern.ch/atlas-jetetmiss-jesjer/tools/SystTool) package implements the BootstrapGenerator within the JES/JER group, providing convenient functions for storing Bootstrap objects
and example scripts for determining the binning of systematic uncertainties that gives statistically significant results.
The latest version of SystTool is usually correct.

### Environment setup
This code is run within the normal AnalysisBase cmake framework.
It generally runs within the latest AnalysisBase, but the last confirmed working version was `21.2.43`.
You can find the latest AnalysisBase (21.2.X) release [here](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AnalysisBaseReleaseNotes21_2).


From within the `source` directory, the following will successfully setup the area after the relevant packages were downloaded.
```
setupATLAS
asetup AnalysisBase,21.2.43,here
cd ../build/
cmake ../source/
make
source ${AnalysisBase_PLATFORM}/setup.sh
cd ../source/
```
All commands should be run from the top directory.
If they do not work from the top directory, try the source directory.

## Instructions

### Config file basics
__When running InsituBalance, you should copy the default config file examples into your own configuration file.__
__This way your dedicated studies and edits will not be pushed to master accidentally, and changes to the defaults will not effect your work!__

Most changes to the workflow, such as EM/LC/Pflow specifics, new analysis selections, GRLs, etc., are handled by configuration files in `data`.
There are 4 top-level config files for each of the direct balance modes;
`config_MJB.py` is for the multijet balance calibration,
`config_Gjet.py` is for the gamma+jet calibration,
`config_Zee.py` is for the Z+jet calibration, where the Z decays leptonically to 2 eletrons, and
`config_Zmm.py` is for the Z+jet calibration, where the Z decays leptonically to 2 muons.

Within a config file are several algorithms that are set in the xAH_config object.
Algorithms are run in the order in which they are set, so first the BasicEventSelection, then the object calibrations, then the object selections, then the InsituBalance algorithm.
The attributes for the aglorithms relevant for InsituBalance are explained in the config file, and further details on each option can be found in the header of the respective algorithm code (either in `InsituBalance/InsituBalanceAlgo.h` or the appropriate xAH header).

Each config file builds upon either `BaseAlgos_mc16a.py` or `BaseAlgos_mc16c.py`, depending upon whether `do_mc16c` is set to True in the config file.
These only need to be edited if GRLs or iLumiCalc files have changed.

Both of these are in turn build upon `BaseAlgos.py`.
This defines which systematic configuration to use, and only `systCP` and `systInsitu` need to be set.
General JetCalibTools or JetUncertainties config files are also defined here.

### Local testing
The code should first be tested locally before being sent to the grid.
An example command for running locally is:
`xAH_run.py --config source/InsituBalance/data/config_Gjet.py --files path/to/DAOD.root --nevents 100 --force direct`

### Grid Submission
Grid submission is performed with `python InsituBalance/scripts/runGridSubmission.py`.
Run this file from within the source/ directory.
The following needs to be manually edited for each submission:
* `config_name`: which configuration file to use.
* `production_name`: Only set this if you're running with group production rights.
* `test`: Set to true to output each command that will be submitted, but to not actually submit.
* `samples`: Uncomment the relevant container names to send jobs.  The first option of the tuple will be the name appended to the job, before `extraTag`.  The second option is the wildcarded containers.
* `extraTag`: This string is added to the output container name.  The date of submission will be automatically added to this.  Use something clear to understand what jobs these correspond to.  (e.g. `calib_Gj_EM` for calibration of EM jets using Gamma+jet).  The container name is limited to a small number of characters, so keep this short!

### Download grid output
The grid output samples can be downloaded locally with the downloadAndMerge script.  The output files are TTrees, and can be downloaded with a command such as:
`python source/InsituBalance/scripts/downloadAndMerge.py --types tree,hist --container user.jdandoy.36*.Pythia_EM4_300517`

The output will be placed in the `gridOutput/tree/` or `gridOutput/hist/` directories.
The unmerged files will be stored in `gridOutput/rawDownload`, and may be deleted if all samples were succesfully downloaded.
The files for each slice will be merged automatically, but the various slices will remain separate.

### Special config options & inputs for Multijet balance
The MJB is performed after the eta-intercalibration stage and concurrently with the V+jet stages.
Several unique input files are therefore required that should be acquired from the relevant experts.
You should make sure to have JetCalibTools config files for the eta-intercalibration and for the Z/gamma+jet stages.
Use eta-intercalibration for the nominal `calibConfig` and Z/gamma+jet for `calibConfig_recoilJets`.
When running the 2nd iteration of the MJB, you should also have the output of the first iteration saved as `m_MJBCorrectionFile`, with the correct pt thresholds set for the V+jet calibrations (`m_subjetThreshold_Vjet`) and the MJB threshold (`m_subjetThreshold_MJB`).

## Fitting and Plotting

Fitting and plotting scripts are located in `scripts/plotting/`.
The main file to use is __`runMJBHists.py`__, which will:
* Create single data and MC files using `MJBSample.py`, scaling the MC JZ slices properly.
* Extract and fit the response distributions with `calcMJB.py`.  This will also handle fitting of all bootstrap toys, and determining the statistically significant rebinning.
* Derive final uncertainty histograms and TGraphs, and plot them, with `makeCalibrationFile.py`.

`runMJBHists.py` should be run from within the `scripts/plotting/` directory.
The input files for it should exist in some workarea.
Within the workarea directory should be a folder called `hist/`, with all the histograms in it from `downloadAndMerge.py`.
There may also be a folder called `SystToolOutput/`, with the boostrap objects in it.
Do not point the code to `hist/`, it should point to the workarea directory above it.
It will make a new folder called `files/`, which will contain individual root files for each sample type as defined in runMJBHists.py, which avoids needing to scale & hadd samples more than once.

Within `runMJBHists.py` the user should change several things according to their area & needs.
* `jet_type` to EM or PF, primarily used for easily switching between 2 directory paths you can provide, and for adding names to plots.  Nothing in the actual merging, fitting, or plotting code cares if you're using EM or PF, this is just for bookkeeping.
* `bootstrap = True` if you want to do bootstrapping.  Turn this off for quicker initial results.
* `args.workDir` is your work directory.  The directory must have `hist/` inside!
* `nominal_data`, `nominal_mc`, and `sys_mc` define the samples that are important (you can run over many data or MC types at once, to plot them together
* `sample_list` define the various samples you want to run over.

Add samples to sample list with lines such as:
`sample_list['Py8'] = MJBSample.MCSample(name="Py8", displayName="Pythia--8",        tags="QCD_Py8")`
Here Py8 defines the name of the sample for this code, displayName is what will be plotted, and tags is a wildcarded substring for grabbing input files.

There are various stages run sequentially, and flags exist to turn them on and off.
For your first time through, only keep one flag on at a time!
* `f_add_MCsys` :  Add nominal result of systematic MC to nominal MC sample, treating it as a regular systematic variation for the rest of the code.
* `f_derive_bs_binning` : Derive the booststrap rebinning from the toys.
* `f_fitting` : Perform the response fitting.
* `f_sys` : Make the systematic variations and plot them.

The code will finally print out a string for calling `plotHistograms.py`, which performs the plotting of all kinematic distributions.
You must run this yourself when you want it performed!


## Scripts for additional studies

### Trigger Efficiency Cutoff
Each trigger should be used only in a region of full efficiency with respect to recoil pt.
These values are set via the `m_triggerAndPt` config variable, as described above.
The cutoff files can be calculated using `scripts/binning/checkTrigger.py`.
This calculates the efficiency of a trigger wrt a reference supporting trigger, and find the point at which it's 99.5% efficient.

This code first creates histograms of the efficiencies using:
`python checkTrigger.py --calculate --data --file path/to/file.root`

* Use the `--mc` option if running on MC.
* Use the `--nevents` option if you'd only like to run on a subset of the events (10 million should be plenty).
* An optional output tag can be added with the `--outName` option.
* Use the `-dir` option to run over a directory with several files.

Before using this file, the first few lines of `checkTrigger()` should be edited with the triggers to use, in descending order.
This code will create an output directory called `triggerPlots` in the same directory as the input file.

After calculating the efficiencies with `--calculate`, the efficiencies may be plotted with:
`python checkTrigger --plot --file path/to/file.root`

Trigger are calculated using one of three methods.
Here the trigger in question is `higher`, and `lower` is the next lowest trigger.
Unprescaled method should be used only for the first unprescaled trigger, and uses the logic `(higher && lower) / lower`.
This should not be used for prescaled triggers, as there is no guarantee an event triggering `higher` will also trigger `lower`.
For the prescaled method, the logic is instead `higher / (higher || lower)`.
A third method, unbiased, is only to be used for MC when no trigger requirement is made on the TTree events.
The method is `higher/nevents`, calculating the number of all events that pass the `higher` trigger.
All methods are calculated by default.

### Nominal binning decision
The binning in recoil pt should be chosen so that enough statistics are in each bin.
In general, several thousand events should be used in each bin, though at higher recoil pt this will be lower (aim for >200 if possible).

First a histogram with fine binning (1 GeV) must be created.
This is done with:
`python getBinning.py --fineHist --file path/to/file.root`

This file is stored in a new directory, `binningPlots/`, stored in the same directory as the input file.
The binning can then be calculated using:
`python getBinning.py --calcBin --file path/to/file.root`

Here the beginning of `getBinning.py` should first be edited with the `eventThreshold`, `numRequiredBins`, and `calcBinEdges`.
`eventThreshold` is the number of required events in the bin, nominally set to 10,000.
`numRequiredBins` is the minimum width of the bins, in GeV.
Once not enough events are in a bin, this value is doubled permanently for all subsequent bins.
`calcBinEdges` is a list containing at least the first bin edge, as well as other bin edges the user is requiring.
New bins will only be calculated after the last entry in `calcBinEdges`.

The new bin edges will be printed to screen, and an output plot will be made as BinningCalculation.png.

### Iterative thresholds
The MJB is an iterative procedure, and at each stage events can only be used if the subleading jet pt is below a certain threshold.
This first threshold is set entirely by the reach of the input V+jet calibrations, but the subsequent thresholds
are set by the statistics.
The number of events in each bin after requring a specific subleading jet pt cut can be found with:
`python getBinning.py --iterHist --file path/to/file.root`

The beginning of this file should first be edited with the `iterativeEdges` and `iterativeCutoffs`.
Here `iterativeEdges` are the bin edges determined in the previous step,
and `iterativeCutoffs` are the subleading jet pt cutoffs to test.
These cutoffs should be identical to one of the bin edges, as it defines the point at which the previous calibration
can be used for the next iteartion.
These results are saved to IterativeBinningHist.root in the `binningPlots/` directory.

To plot the distributions after various pt cuts, use:
`python getBinning.py --plotIter --file path/to/file.root`

The statistics in each bin after a subleading jet pt cut can be seen, and the iterative cuts can be determined.

### Further studies  -- kTerm OOC
The InsituBalance was provided with a new class in: Root/KTermHists.cxx and the InsituBalance/KTermHists.h.
The histogram need for the kTerm Out-Of-Cone (OOC) studies are simple profile histograms which will be
filled with this class, the same way the MultiJetHists class does.
Therefore the class has an initialize and execute method called form the InsituBalanceAlgo.cxx.
Resuts are added within a folder to the standard histogram output of the InsituBalance package.

The profile histograms are  pt's density within annuli of given delta R around the jet's axis.



