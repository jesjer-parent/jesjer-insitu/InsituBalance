
//////////////////////////////// Selections //////////////////////////
bool InsituBalanceAlgo:: cut_LeadEta(){
  ANA_MSG_DEBUG("Cut: lead jet detector eta");

  if( m_jets->size() < m_numJets )
    return false;
  if( m_jets->size() == 0 ) //protection against accessing 0 element
    return true;

  static SG::AuxElement::ConstAccessor< float > DetectorEta ("DetectorEta");

  if( fabs( DetectorEta( *m_jets->at(0) ) ) > 0.8 )
      return false;
  else{
      return true;
    }
}

bool InsituBalanceAlgo:: cut_JetEta(){

  static SG::AuxElement::ConstAccessor< float > DetectorEta ("DetectorEta");
  for (unsigned int iJet = 0; iJet < m_jets->size(); ++iJet){
    if( fabs( DetectorEta( *m_jets->at(iJet) ) ) > m_jetEta ){
      m_jets->erase( m_jets->begin()+iJet );
      --iJet;
    }
  }// for m_jets


  if (m_jets->size() >= m_numJets)
    return true;
  else
    return false;

}

bool InsituBalanceAlgo:: cut_MCCleaning(){
  ANA_MSG_DEBUG("Cut: MC Cleaning");

  if(m_useMCPileupCheck && m_isMC){

    if( m_jets->size() <= 1 )
      return true;

    float pTAvg = ( m_jets->at(0)->pt() + m_jets->at(1)->pt() ) / 2.0;
    if( m_truthJets->size() == 0 || (pTAvg / m_truthJets->at(0)->pt() > 1.4) ){
      return false;
    }
  }

  return true;
}

bool InsituBalanceAlgo:: cut_SubPt(){
  ANA_MSG_DEBUG("Cut: Subleading jet pt");

  if( m_jets->size() < m_numJets )
    return false;
  if( m_jets->size() <= 1 )
    return true;


  float jet_etaInter_pt = m_jets->at(1)->auxdecor<float>("etaInter_pt");
  //If subleading jet is above the calibration range (MJB threshold if set, else Vjet threshold), then reject the event
  if( (m_subjetThreshold_MJB > 0 && jet_etaInter_pt > m_subjetThreshold_MJB) ||
      (m_subjetThreshold_MJB <= 0 && jet_etaInter_pt > m_subjetThreshold_Vjet) )
    return false;
  else
    return true;

}

bool InsituBalanceAlgo:: cut_JetPtThresh() {

  ANA_MSG_DEBUG("Cut: jet pt threshold");

  float thisPtCut = m_ptThresh;
  if( m_sysType.at(m_iSys) == CUTPt )
    thisPtCut += m_sysDetail.at(m_iSys);

  for (unsigned int iJet = 0; iJet < m_jets->size(); ++iJet){
    if( m_jets->at(iJet)->pt()/1e3 < thisPtCut ){ //Default 25 GeV
      if(iJet == 0){
        return false;  //Fail if removing leading jet
      }else{
        m_jets->erase(m_jets->begin()+iJet);
        --iJet;
      }
    }
  }

  if (m_jets->size() >= m_numJets)
    return true;
  else
    return false;

}

bool InsituBalanceAlgo:: cut_LeadJetPtThresh() {

  ANA_MSG_DEBUG("Cut: lead jet pt threshold");

  if( m_jets->size() < m_numJets )
    return false;
  if( m_jets->size() == 0 )
    return true;

  float thisLeadJetPtCut = m_leadJetPtThresh;
  if( m_jets->at(0)->pt()/1e3 >= thisLeadJetPtCut )
    return true;
  else
    return false;

}

bool InsituBalanceAlgo:: cut_JVT() {

  ANA_MSG_DEBUG("Cut: JVT");
  unsigned int startJet = 0;
  if(m_mode == MJB && m_largeR.compare("True")==0){startJet=1;}
  for(unsigned int iJet = startJet; iJet < m_jets->size(); ++iJet){
    m_jets->at(iJet)->auxdata< float >("Jvt") = m_JVTUpdateTool_handle->updateJvt( *(m_jets->at(iJet)) );

    if(  (m_sysType.at(m_iSys) == JVT && m_sysDetail.at(m_iSys) == -1 && !m_JetJVTEfficiencyTool_handle_down->passesJvtCut(*(m_jets->at(iJet))) ) 
	 || (m_sysType.at(m_iSys) == JVT && m_sysDetail.at(m_iSys) == 1 && !m_JetJVTEfficiencyTool_handle_up->passesJvtCut(*(m_jets->at(iJet))) )
	 || (m_sysType.at(m_iSys) != JVT && !m_JetJVTEfficiencyTool_handle->passesJvtCut( *(m_jets->at(iJet))) ) ){
      
      m_jets->erase(m_jets->begin()+iJet);  --iJet;  //Remove the jet & continue
    }//if fails JVT
  }
  
  if (m_jets->size() >= m_numJets)
    return true;
  else
    return false;

}

//// Ignore event if any of the jets are not clean ////
bool InsituBalanceAlgo:: cut_CleanJet(){
  ANA_MSG_DEBUG("Cut: Event-level jet cleaning decision");

  static SG::AuxElement::ConstAccessor<char> cleaning_decision("DFCommonJets_eventClean_LooseBad");
  if( !cleaning_decision.isAvailable( *m_eventInfo ) ){
//!!ANA_MSG_ERROR("ERROR: Cleaning decision \"eventClean_LooseBad\" cannot be found!  Will continue, but you should really update your samples!");
    return true;
  }

  //If event level cleaning fails
  if( !cleaning_decision( *m_eventInfo ))
    return false;

  return true;
}

bool InsituBalanceAlgo:: cut_TriggerEffRecoil(){
  ///// Trigger Efficiency and prescale /////

  m_prescale = 1.;
  bool passedTriggers = false;
  if (m_triggers.size() == 0)
    passedTriggers = true;

  static SG::AuxElement::Decorator< std::vector< std::string > >  passTrigs("passedTriggers");
  static SG::AuxElement::Decorator< std::vector< float > >        trigPrescales("triggerPrescales");
  std::vector< std::string > passingTriggers = passTrigs( *m_eventInfo );

  for( unsigned int iT=0; iT < m_triggers.size(); ++iT){
    if( m_recoilTLV.Pt() > m_triggerThresholds.at(iT)){
      auto trigIndex = std::find(passingTriggers.begin(), passingTriggers.end(), m_triggers.at(iT));
      if( trigIndex !=  passingTriggers.end() ){
        passedTriggers = true;
        m_prescale = trigPrescales( *m_eventInfo ).at( trigIndex-passingTriggers.begin() );
      }
      // only check 1 trigger in correct pt range unless threshold was -1
      // This allows for unique triggers in different pt ranges, or for several triggers over all ranges (for electrons and muons)
      if( m_triggerThresholds.at(iT) >= 0)
        break; // only check 1 trigger in correct pt range unless threshold was -1
    }//recoil Pt
  } // each Trigger

  if( passedTriggers ){
    return true;
  } else{
    return false;
  }

}

//Remove dijet events, i.e. events where subleading jet dominates the recoil jets
bool InsituBalanceAlgo:: cut_PtAsym(){
  ANA_MSG_DEBUG("Cut: Pt asymmetry");

  //Var cut changes with recoil object pt, while the minimum pt cut never changes
  float thisAsymVarCut = m_ptAsymVar;
  float thisAsymMinCut = m_ptAsymMin;

  if( m_sysType.at(m_iSys) == CUTAsym ){
    thisAsymVarCut += m_sysDetail.at(m_iSys);
    // Change min cut systematic variable manually
    if( m_sysDetail.at(m_iSys)  > 0 )
      thisAsymMinCut += 5.; //GeV
    else
      thisAsymMinCut -= 5.;
  }

  float maxPtCut = fmax( thisAsymMinCut*1e3, thisAsymVarCut * m_recoilTLV.Pt() );

  if( m_jets->size() >= 2){
    //    std::cout << maxPtCut << " from " << thisAsymMinCut*1e3 << " vs " << thisAsymVarCut * m_recoilTLV.Pt() << " against " <<  m_jets->at(1)->pt() << "(" << m_jets->at(0)->pt() << ")" << std::endl;
    m_eventInfo->auxdecor< float >( "ptAsym" ) = m_jets->at(1)->pt() / m_recoilTLV.Pt();
    if( m_jets->at(1)->pt() > maxPtCut )
      return false;
    else
      return true;
  } else {
    m_eventInfo->auxdecor< float >( "ptAsym" ) = 0.;
    return true;
  }

}

//Alpha is phi angle between leading jet and recoilJet system
bool InsituBalanceAlgo:: cut_Alpha(){
  ANA_MSG_DEBUG("Cut: Alpha (delta phi)");


  if( m_jets->size() < m_numJets )
    return false;
  if( m_jets->size() == 0 )
    return true;

  double alpha = fabs(DeltaPhi( m_jets->at(0)->phi(), m_recoilTLV.Phi() )) ;
  m_eventInfo->auxdecor< float >( "alpha" ) = alpha;

  float thisAlphaCut = m_alpha;
  if( m_sysType.at(m_iSys) == CUTAlpha )
    thisAlphaCut += m_sysDetail.at(m_iSys);

  if( (M_PI-alpha) > thisAlphaCut ){  //0.3 by default
    return false;
  }

  //Add dPhi between jet and TLV for every jet//
  for(unsigned int iJet=0; iJet < m_jets->size(); ++iJet){
    m_jets->at(iJet)->auxdecor< float >( "jetAlpha") = fabs(DeltaPhi( m_jets->at(iJet)->phi(), m_recoilTLV.Phi() )) ;
  }

  return true;
}

//Beta is phi angle between leading jet and each other passing jet
bool InsituBalanceAlgo:: cut_Beta(){
  ANA_MSG_DEBUG("Cut: Beta");

  if( m_jets->size() < m_numJets )
    return false;
  if( m_jets->size() <= 1 )
    return true;

  double smallestBeta=10., avgBeta = 0., jetBeta=0.;

  for(unsigned int iJet=1; iJet < m_jets->size(); ++iJet){
    jetBeta = DeltaPhi(m_jets->at(iJet)->phi(), m_jets->at(0)->phi() );
    m_jets->at(iJet)->auxdecor< float >( "beta") = jetBeta;
    avgBeta += jetBeta;

    if( (jetBeta < smallestBeta) && (m_jets->at(iJet)->pt() > m_jets->at(0)->pt()*m_betaPtVar) )
      smallestBeta = jetBeta;
  }
  avgBeta /= (m_jets->size()-1);
  m_eventInfo->auxdecor< float >( "avgBeta" ) = avgBeta;

  float thisBetaCut = m_beta;
  if( m_sysType.at(m_iSys) == CUTBeta )
    thisBetaCut += m_sysDetail.at(m_iSys);

  if( smallestBeta < thisBetaCut ){
    return false;
  }

  return true;
}

/// MPF code just ignores any subleading photons
bool InsituBalanceAlgo:: cut_ConvPhot(){
  // Remove Photons that are consistent with a converted photon
  ANA_MSG_DEBUG("Cut: Converted photon");

  if (xAOD::EgammaHelpers::conversionType(m_recoilPhoton) == 0)
    return true;

  double clusterEt = m_recoilPhoton->caloCluster()->e() / cosh(m_recoilPhoton->caloCluster()->eta());
  double trackPt  = xAOD::EgammaHelpers::momentumAtVertex(m_recoilPhoton).perp();
  if (xAOD::EgammaHelpers::conversionType(m_recoilPhoton) > 2){
    if (clusterEt / trackPt > 1.5 or clusterEt / trackPt < 0.5)
      return false;
  } else if (clusterEt / trackPt > 2.0){
    return false;
  }

  return true;
}

//ASG overlap standards: Removing x for x/y
//  j/p dR < 0.4 --> (We do this, but 0.2, which is a more strict EVENT-level selection)
//  j/e dR < 0.2
//  e/j dR < 0.4
//  j/m SPECIAL
//  m/j dR < 0.4

//Our OR: Always trust the lepton or photon, using smaller dR
//Only downside - allows jets (& pileup jets) to fake leptons and photons
//This method decouples the OR result from JVT decision & from # lepton / photon decisions

bool InsituBalanceAlgo:: cut_OverlapRemoval(){
  ANA_MSG_DEBUG("Cut: overlap removal");

  for(unsigned int iJet=0; iJet < m_jets->size(); ++iJet){
    for( unsigned int iP=0; iP < m_recoilParticles.size(); ++iP){
      if( m_jets->at(iJet)->p4().DeltaR(m_recoilParticles.at(iP)->p4()) < m_overlapDR ){
        m_jets->erase( m_jets->begin()+iJet );
        --iJet;
        iP = m_recoilParticles.size(); //break the loop
      }
    }//for iP
  }//for iJet

  if (m_jets->size() >= m_numJets)
    return true;
  else
    return false;
}

///Zmass peak & opposite-sign requirement
bool InsituBalanceAlgo:: cut_Zmass(){
  ANA_MSG_DEBUG("Cut: Z mass peak");
  if( m_recoilTLV.M() < 80*1e3 || m_recoilTLV.M() > 116*1e3 )
    return false;
  else
    return true;
}
