// EL include(s):
#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include "EventLoop/OutputStream.h"

// package include(s):
#include <xAODAnaHelpers/HelperFunctions.h>
#include <InsituBalance/HistogramTree.h>
#include <xAODAnaHelpers/tools/ReturnCheck.h>
#include "xAODAnaHelpers/HistogramManager.h"


// ROOT include(s):
#include "TFile.h"
#include "TTree.h"
#include "TTreeFormula.h"
#include "TSystem.h"
#include "TKey.h"

#include <sstream>

// this is needed to distribute the algorithm to the workers
ClassImp(HistogramTree)

HistogramTree :: HistogramTree (std::string className) :
  Algorithm(className),
  photon_pt(0),
  photon_eta(0),
  photon_phi(0),
  jet_pt(0),
  jet_eta(0),
  jet_phi(0),
  jet_E(0),
  triggerPrescales(0),
  jet_detEta(0),
  jet_TileCorrectedPt(0),
  jet_beta(0),
  jet_corr(0),
  jet_TileFrac(0),
  jet_Jvt(0),
  jet_PartonTruthLabelID(0)

{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
  Info("HistogramTree()", "Calling constructor");

  m_debug = false;
  m_isMC = true;


  //CP::CorrectionCode::enableFailure();
  //StatusCode::enableFailure();
}


EL::StatusCode HistogramTree :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  //
  Info("setupJob()", "Calling setupJob");


  return EL::StatusCode::SUCCESS;
}


EL::StatusCode HistogramTree :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  Info("histInitialize()", "Calling histInitialize");

  RETURN_CHECK("xAH::Algorithm::algInitialize()", xAH::Algorithm::algInitialize(), "");

  HistogramManager* HM = new HistogramManager("HistManager", "HistManager");

  numHistJets = 5;
  numHistPhotons = 3;

  float maxPt = 2000;
  int nPtBins = maxPt/10;

  //////Setup Binnings to use ///
  std::string binning = "85, 105, 125, 145, 170, 200, 250, 300, 400, 500, 600, 800, 1000, 1200, 1400, 1600, 2000";
  //std::string binning = "15, 20, 25, 30, 40, 55, 65, 75, 85, 105, 125, 145, 170, 200, 250, 300, 400, 500, 600, 800, 1000, 1200, 1400, 1600, 2000, 10000";
  Double_t binArray[100];
  std::stringstream ssb(binning);
  std::string thisBinStr;
  std::string::size_type sz;
  std::vector<double> vecBins;
  while (std::getline(ssb, thisBinStr, ',')) {
    vecBins.push_back( std::stof(thisBinStr, &sz) );
  }

  int numBins = vecBins.size()-1;

  for( int iBin=0; iBin < numBins+1; ++iBin ){
    binArray[iBin] = vecBins.at(iBin);
//    cout << "Bin " << iBin << " gets " << binArray[iBin] << endl;
  }

  // pt balance binnings
  Double_t ptBalBins[501];
  int numPtBalBins = 500;
  for(int i=0; i < numPtBalBins+1; ++i){
    ptBalBins[i] = i/100.;
  }

  h_recoilPt_PtBal = HM->book("Iteration0_Nominal/", "recoilPt_PtBal", "Recoil System p_{T} [GeV]", numBins, binArray, "p_{T} Balance", numPtBalBins, ptBalBins);

  h_NPV = HM->book("Iteration0_Nominal/", "h_NPV", "", 100, 0, 100);
  h_averageInteractionsPerCrossing = HM->book("Iteration0_Nominal/", "h_averageInteractionsPerCrossing", "", 100, 0, 100);
  h_correct_mu = HM->book("Iteration0_Nominal/", "h_correct_mu", "", 100, 0, 100);
  h_njet = HM->book("Iteration0_Nominal/", "h_njet", "n_{jet}", 20, 0, 20);

  h_ptAsym = HM->book("Iteration0_Nominal/", "h_ptAsym", "p_{T} Asymmetry", 100, 0., 1.0);
  h_alpha = HM->book("Iteration0_Nominal/", "h_alpha", "#alpha angle",100, TMath::Pi()-1, TMath::Pi());
  h_avgBeta = HM->book("Iteration0_Nominal/", "h_avgBeta", "Average #beta angle", 80, 0, 4.0);
  h_ptBal = HM->book("Iteration0_Nominal/", "h_ptBal", "p_{T} Balance", 80, 0., 4.);
  h_recoilPt = HM->book("Iteration0_Nominal/", "h_recoilPt", "Recoil system p_{T}", nPtBins, 0, maxPt);
  h_recoilEta = HM->book("Iteration0_Nominal/", "h_recoilEta", "Recoil system #eta", 80, -4, 4);
  h_recoilPhi = HM->book("Iteration0_Nominal/", "h_recoilPhi", "Recoil system #phi", 64, -TMath::Pi(), TMath::Pi());
  h_recoilM = HM->book("Iteration0_Nominal/", "h_recoilM", "Recoil system mass", nPtBins, 0, maxPt);
  h_recoilE = HM->book("Iteration0_Nominal/", "h_recoilE", "Recoil system energy", nPtBins, 0, maxPt);
  h_jet_pt_all = HM->book("Iteration0_Nominal/", "h_jet_pt_all", "Jet p_{T}", nPtBins, 0, maxPt);
  h_jet_eta_all = HM->book("Iteration0_Nominal/", "h_jet_eta_all", "Jet #eta", 80, -4, 4);
  h_jet_phi_all = HM->book("Iteration0_Nominal/", "h_jet_phi_all", "Jet #phi", 64, -TMath::Pi(), TMath::Pi() );
  h_jet_E_all = HM->book("Iteration0_Nominal/", "h_jet_E_all", "Jet energy", nPtBins, 0, maxPt);



  for(unsigned int iE=0; iE < numHistJets; ++iE){
    vh_jet_pt.push_back( HM->book( "Iteration0_Nominal/", ("h_jet_pt_"+std::to_string(iE)).c_str(), ("Jet p_{T} "+std::to_string(iE)).c_str(), nPtBins, 0, maxPt) );
    vh_jet_eta.push_back( HM->book( "Iteration0_Nominal/", ("h_jet_eta_"+std::to_string(iE)).c_str(), ("Jet #eta "+std::to_string(iE)).c_str(), 80, -4, 4) );
    vh_jet_phi.push_back( HM->book( "Iteration0_Nominal/", ("h_jet_phi_"+std::to_string(iE)).c_str(), ("Jet #phi "+std::to_string(iE)).c_str(), 64, -TMath::Pi(), TMath::Pi()) ) ;
    vh_jet_E.push_back( HM->book( "Iteration0_Nominal/", ("h_jet_E_"+std::to_string(iE)).c_str(), ("Jet energy"+std::to_string(iE)).c_str(), nPtBins, 0, maxPt) );
    vh_jet_detEta.push_back( HM->book( "Iteration0_Nominal/", ("h_jet_detEta_"+std::to_string(iE)).c_str(), ("Jet #eta_{det} "+std::to_string(iE)).c_str(), 80, -4, 4) );
    vh_jet_TileCorrectedPt.push_back( HM->book( "Iteration0_Nominal/", ("h_jet_TileCorrectedPt_"+std::to_string(iE)).c_str(), ("Jet p_{T}^{Tile} "+std::to_string(iE)).c_str(), nPtBins, 0, maxPt) );
    vh_jet_beta.push_back( HM->book( "Iteration0_Nominal/", ("h_jet_beta_"+std::to_string(iE)).c_str(), ("Jet #beta"+std::to_string(iE)).c_str(), 64, 0, 3.2) );
    vh_jet_corr.push_back( HM->book( "Iteration0_Nominal/", ("h_jet_corr_"+std::to_string(iE)).c_str(), ("Jet correction factor "+std::to_string(iE)).c_str(), 100, 0, 1.) );
    vh_jet_TileFrac.push_back( HM->book( "Iteration0_Nominal/", ("h_jet_TileFrac_"+std::to_string(iE)).c_str(), ("Jet Tile fraction "+std::to_string(iE)).c_str(), 100, 0, 1.) );
    vh_jet_Jvt.push_back( HM->book( "Iteration0_Nominal/", ("h_jet_Jvt_"+std::to_string(iE)).c_str(), ("Jet JVT "+std::to_string(iE)).c_str(), 100, 0, 1.) );
    vh_jet_PartonTruthLabelID.push_back( HM->book( "Iteration0_Nominal/", ("h_jet_PartonTruthLabelID_"+std::to_string(iE)).c_str(), ("Jet PartonTruthLabelID "+std::to_string(iE)).c_str(), 20, 0, 20) );
  }

  HM->record( wk() ); //Add all histograms to EventLoop output



  Info("histInitialize()", "Histograms initialized!");

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode HistogramTree :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed

  Info("fileExecute()", "Calling fileExecute");


  return EL::StatusCode::SUCCESS;

}

EL::StatusCode HistogramTree :: changeInput (bool /*firstFile*/)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.

  //// Get number of events ////
  TFile* inputFile = wk()->inputFile();
  std::string histName = "";

  TKey *key;
  TIter next(inputFile->GetListOfKeys() );
  while(( key = (TKey*)next() )){
    std::string thisHistName = key->GetName();
    if( thisHistName.find("cutflow") != std::string::npos && thisHistName.find("weight") == std::string::npos)
      histName = thisHistName;
  }
  TH1F* cutflow = (TH1F*) inputFile->Get(histName.c_str());
  m_totalNumEvents = cutflow->GetBinContent(3);  //sumOfWeights
  //m_totalNumEvents = cutflow->GetBinContent(1); //numberOfEvents
  Info("changeInput()", "In cutflow found initial number of events %f", m_totalNumEvents);
//  }// if MC
//

  std::string fileName = inputFile->GetName();

  if( (fileName.find("Data") != std::string::npos ) ||  (fileName.find("data") != std::string::npos) ){
    m_isMC = false;
  }



  ///// Connect branches ////
  TTree *tree = wk()->tree();
  tree->SetBranchStatus ("*", 1);

  tree->SetBranchAddress("jet_pt", &jet_pt);
  tree->SetBranchAddress("jet_eta", &jet_eta);
  tree->SetBranchAddress("jet_phi", &jet_phi);
  tree->SetBranchAddress("jet_E", &jet_E);
  tree->SetBranchAddress("triggerPrescales", &triggerPrescales);
  tree->SetBranchAddress("jet_detEta", &jet_detEta);
  tree->SetBranchAddress("jet_TileCorrectedPt", &jet_TileCorrectedPt);
  tree->SetBranchAddress("jet_beta", &jet_beta);
  tree->SetBranchAddress("jet_corr", &jet_corr);
  tree->SetBranchAddress("jet_TileFrac", &jet_TileFrac);
  tree->SetBranchAddress("jet_Jvt", &jet_Jvt);
  tree->SetBranchAddress("jet_PartonTruthLabelID", &jet_PartonTruthLabelID);
  tree->SetBranchAddress("runNumber", &runNumber);
  tree->SetBranchAddress("eventNumber", &eventNumber);
  tree->SetBranchAddress("mcChannelNumber", &mcChannelNumber);
  tree->SetBranchAddress("NPV", &NPV);
  tree->SetBranchAddress("averageInteractionsPerCrossing", &averageInteractionsPerCrossing);
  tree->SetBranchAddress("correct_mu", &correct_mu);
  tree->SetBranchAddress("weight", &weight);
  tree->SetBranchAddress("weight_pileup", &weight_pileup);
  tree->SetBranchAddress("mcEventWeight", &mcEventWeight);
  tree->SetBranchAddress("weight_xs", &weight_xs);
  tree->SetBranchAddress("weight_prescale", &weight_prescale);
  tree->SetBranchAddress("njet", &njet);
  tree->SetBranchAddress("ptAsym", &ptAsym);
  tree->SetBranchAddress("alpha", &alpha);
  tree->SetBranchAddress("avgBeta", &avgBeta);
  tree->SetBranchAddress("ptBal", &ptBal);
  tree->SetBranchAddress("recoilPt", &recoilPt);
  tree->SetBranchAddress("recoilEta", &recoilEta);
  tree->SetBranchAddress("recoilPhi", &recoilPhi);
  tree->SetBranchAddress("recoilM", &recoilM);
  tree->SetBranchAddress("recoilE", &recoilE);



  return EL::StatusCode::SUCCESS;
}



EL::StatusCode HistogramTree :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  Info("initialize()", "Initializing HistogramTree... ");

  m_eventCounter = -1;

  Info("initialize()", "HistogramTree succesfully initialized!");

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode HistogramTree :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  if( m_debug ) { Info("execute()", "Histogram B-L Tree"); }

  ++m_eventCounter;

  if( (m_eventCounter%1000) == 0) Info("execute()", "Event number %i", m_eventCounter);

  wk()->tree()->GetEntry (wk()->treeEntry());


  if (m_isMC){
    weight = weight*1000./m_totalNumEvents; // 1fb = 1pb*1000
  }else{
    weight = 1;
  }

  if( std::isnan(weight) ){
    std::cout << "Warning!!!!! it's nan!" << std::endl;
    return EL::StatusCode::SUCCESS;
  }


  if (m_debug)  Info("execute", "Beginning new selections");



  if( recoilPt/1e3 < 200. )
    return EL::StatusCode::SUCCESS;


  if (m_debug)  Info("execute", "Fill regular histograms");
  // !!FillHists!! //

  h_recoilPt_PtBal->Fill(recoilPt/1e3, ptBal, weight);
  h_NPV->Fill( NPV, weight );
  h_averageInteractionsPerCrossing->Fill( averageInteractionsPerCrossing, weight );
  h_correct_mu->Fill( correct_mu, weight );
  h_njet->Fill( njet, weight );
  h_ptAsym->Fill( ptAsym, weight );
  h_alpha->Fill( alpha, weight );
  h_avgBeta->Fill( avgBeta, weight );
  h_ptBal->Fill( ptBal, weight );
  h_recoilPt->Fill( recoilPt/1e3, weight );
  h_recoilEta->Fill( recoilEta, weight );
  h_recoilPhi->Fill( recoilPhi, weight );
  h_recoilM->Fill( recoilM, weight );
  h_recoilE->Fill( recoilE/1e3, weight );


  if (m_debug)  Info("execute", "Fill generic lepton histograms");

//!!  for(unsigned int iP=0; iP < photon_pt->size(); ++iP){
//!!    h_photon_pt_all->Fill( photon_pt->at(iP)/1e3, weight );
//!!    h_photon_eta_all->Fill( photon_eta->at(iP), weight );
//!!    h_photon_phi_all->Fill( photon_phi->at(iP), weight );
//!!
//!!    if( iP < numHistPhotons){
//!!      vh_photon_pt.at(iP)->Fill( photon_pt->at(iP)/1e3, weight );
//!!      vh_photon_eta.at(iP)->Fill( photon_eta->at(iP), weight );
//!!      vh_photon_phi.at(iP)->Fill( photon_phi->at(iP), weight );
//!!    }
//!!  }//end photons

  //jets
  for(unsigned int iJ=0; iJ < jet_pt->size(); ++iJ){
    h_jet_pt_all->Fill( jet_pt->at(iJ), weight );
    h_jet_eta_all->Fill( jet_eta->at(iJ), weight );
    h_jet_phi_all->Fill( jet_phi->at(iJ), weight );
    h_jet_E_all->Fill( jet_E->at(iJ), weight );

    if( iJ < numHistJets){
      vh_jet_pt.at(iJ)->Fill( jet_pt->at(iJ), weight );
      vh_jet_eta.at(iJ)->Fill( jet_eta->at(iJ), weight );
      vh_jet_phi.at(iJ)->Fill( jet_phi->at(iJ), weight );
      vh_jet_E.at(iJ)->Fill( jet_E->at(iJ), weight );
      vh_jet_detEta.at(iJ)->Fill( jet_detEta->at(iJ), weight );
      vh_jet_TileCorrectedPt.at(iJ)->Fill( jet_TileCorrectedPt->at(iJ), weight );
      vh_jet_beta.at(iJ)->Fill( jet_beta->at(iJ), weight );
      vh_jet_corr.at(iJ)->Fill( jet_corr->at(iJ), weight );
      vh_jet_TileFrac.at(iJ)->Fill( jet_TileFrac->at(iJ), weight );
      vh_jet_Jvt.at(iJ)->Fill( jet_Jvt->at(iJ), weight );
      vh_jet_PartonTruthLabelID.at(iJ)->Fill( jet_PartonTruthLabelID->at(iJ), weight );
    }
  }//end jets




  return EL::StatusCode::SUCCESS;
}

EL::StatusCode HistogramTree :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode HistogramTree :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  Info("finalize()", "Number of processed events \t= %i", m_eventCounter);


  return EL::StatusCode::SUCCESS;
}



EL::StatusCode HistogramTree :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.

  RETURN_CHECK("xAH::Algorithm::algFinalize()", xAH::Algorithm::algFinalize(), "");
  return EL::StatusCode::SUCCESS;
}

////Calculate DeltaR
//double HistogramTree::DeltaR(double eta1, double phi1,double eta2, double phi2){
//  phi1=TVector2::Phi_0_2pi(phi1);
//  phi2=TVector2::Phi_0_2pi(phi2);
//  double dphi=TVector2::Phi_0_2pi(phi1-phi2);
//  dphi = TMath::Min(dphi,(2.0*M_PI)-dphi);
//  double deta = eta1-eta2;
//  return sqrt(deta*deta+dphi*dphi);
//}
//
//std::vector< std::string  > HistogramTree::splitLine( std::string inLine ){
//  // Get vector of selections, splitting by commas
//  std::vector< std::string > lineVec;
//  std::istringstream ss(inLine);
//  std::string subStr = "";
//  while( std::getline(ss, subStr, ',') ){
//    lineVec.push_back( subStr );
//  }
//  return lineVec;
//}
//
