#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODEventInfo/EventInfo.h"

#include "InsituBalance/MiniTree.h"


MiniTree :: MiniTree(xAOD::TEvent * event, TTree* tree, TFile* file) :
  HelpTreeBase(event, tree, file, 1e3)
{
  if ( m_debug ) Info("MiniTree", "Creating output TTree %s", tree->GetName());
}

MiniTree :: ~MiniTree()
{
}

void MiniTree::AddEventUser(std::string detailStringMJB)
{
  // event variables
  m_tree->Branch("njet", &m_njet, "njet/I");

  if( detailStringMJB.find("emulateTrigger") != std::string::npos ){
    m_emulate_trigger = true;
    m_tree->Branch("max_HLT_jet_pt", &m_max_HLT_jet_pt, "max_HLT_jet_pt/F");
    m_tree->Branch("max_L1_jet_pt", &m_max_L1_jet_pt, "max_L1_jet_pt/F");
  }


  m_tree->Branch("weight",               &m_weight,               "weight/F");
  m_tree->Branch("weight_xs",            &m_weight_xs,            "weight_xs/F");
  m_tree->Branch("weight_mcEventWeight", &m_weight_mcEventWeight, "weight_mcEventWeight/F");
  m_tree->Branch("weight_prescale",      &m_weight_prescale,      "weight_prescale/F");
  m_tree->Branch("weight_pileup",        &m_weight_pileup,        "weight_pileup/F");

  m_tree->Branch("ptAsym",  &m_ptAsym,  "ptAsym/F");
  m_tree->Branch("alpha",   &m_alpha,   "alpha/F");
  m_tree->Branch("avgBeta", &m_avgBeta, "avgBeta/F");
  m_tree->Branch("ptBal",   &m_ptBal,   "ptBal/F");

  m_tree->Branch("recoilPt",  &m_recoilPt,  "recoilPt/F");
  m_tree->Branch("recoilEta", &m_recoilEta, "recoilEta/F");
  m_tree->Branch("recoilPhi", &m_recoilPhi, "recoilPhi/F");
  m_tree->Branch("recoilM",   &m_recoilM,   "recoilM/F");
  m_tree->Branch("recoilE",   &m_recoilE,   "recoilE/F");


}

void MiniTree::AddJetsUser(const std::string detailStr, const std::string jetName)
{

  std::cout << "AddJetsUser gives " << detailStr << std::endl;
  if (detailStr.find("MJBbTag_") != std::string::npos){
    std::string tmp = detailStr.substr(detailStr.find("MJBbTag_")+8, detailStr.size());
    tmp = tmp.substr(0, detailStr.find_last_of(' '));

    std::stringstream ssbtag(tmp);
    std::string thisSubStr;
    while (std::getline(ssbtag, thisSubStr, ',')) {
      m_jet_BTagNames.push_back( thisSubStr );
      std::vector<int> thisBTagBranch;
      m_jet_BTagBranches.push_back( thisBTagBranch );
      m_tree->Branch( ("jet_BTag_"+thisSubStr).c_str(), &m_jet_BTagBranches.at(m_jet_BTagBranches.size()-1) );
      std::vector<float> thisBTagSFBranch;
      m_jet_BTagSFBranches.push_back( thisBTagSFBranch );
      m_tree->Branch( ("jet_BTagSF_"+thisSubStr).c_str(), &m_jet_BTagSFBranches.at(m_jet_BTagSFBranches.size()-1) );

    }
  }

  // jet things
  m_tree->Branch("jet_detEta", &m_jet_detEta);
  m_tree->Branch("jet_alpha", &m_jet_alpha);
  m_tree->Branch("jet_TileCorrectedPt", &m_jet_TileCorrectedPt);
  m_tree->Branch("jet_beta", &m_jet_beta);
  m_tree->Branch("jet_corr", &m_jet_corr);

//  m_tree->Branch("jet_EMFrac", &m_jet_EMFrac);
//  m_tree->Branch("jet_HECFrac", &m_jet_HECFrac);
  m_tree->Branch("jet_TileFrac", &m_jet_TileFrac);
  m_tree->Branch("jet_Jvt", &m_jet_Jvt);

  m_tree->Branch("jet_PartonTruthLabelID", &m_jet_PartonTruthLabelID);

  if( detailStr.find("trackjet") != std::string::npos ){
    m_trackjet = true;
    m_tree->Branch("jet_track_pt",     &m_jet_track_pt);
    m_tree->Branch("jet_track_eta",    &m_jet_track_eta);
    m_tree->Branch("jet_track_phi",    &m_jet_track_phi);
    m_tree->Branch("jet_track_e",      &m_jet_track_e);
    m_tree->Branch("jet_track_respPt", &m_jet_track_respPt);
    m_tree->Branch("jet_track_respE",  &m_jet_track_respE);
  }

  if( detailStr.find("truthjet") != std::string::npos ){
    m_truthjet = true;
    m_tree->Branch("jet_truth_pt",             &m_jet_truth_pt);
    m_tree->Branch("jet_truth_eta",            &m_jet_truth_eta);
    m_tree->Branch("jet_truth_phi",            &m_jet_truth_phi);
    m_tree->Branch("jet_truth_e",              &m_jet_truth_e);
    m_tree->Branch("jet_truth_respPt",         &m_jet_truth_respPt);
    m_tree->Branch("jet_truth_respE",          &m_jet_truth_respE);
    m_tree->Branch("jet_truth_ghost_fraction", &m_jet_truth_ghost_fraction);
  }

}

void MiniTree::FillEventUser( const xAOD::EventInfo* eventInfo ) {

  m_weight = eventInfo->auxdecor< float >( "weight" );
  m_weight_xs = eventInfo->auxdecor< float >( "weight_xs" );
  m_weight_mcEventWeight = eventInfo->auxdecor< float >( "weight_mcEventWeight" );
  m_weight_prescale = eventInfo->auxdecor< float >( "weight_prescale" );
  if(eventInfo->isAvailable< float >("PileupWeight") ){
    m_weight_pileup = eventInfo->auxdecor< float >("PileupWeight");
  }else{
    m_weight_pileup = -999;
  }
  m_njet = eventInfo->auxdecor< int >( "njet" );

  m_ptAsym = eventInfo->auxdecor< float >( "ptAsym" );
  m_alpha = eventInfo->auxdecor< float >( "alpha" );
  m_avgBeta = eventInfo->auxdecor< float >( "avgBeta" );
  m_ptBal = eventInfo->auxdecor< float >( "ptBal" );

  if( eventInfo->isAvailable< float >("recoilPt") )
    m_recoilPt = eventInfo->auxdecor< float >( "recoilPt" )/1e3;
  else
    m_recoilPt = -999;

  m_recoilEta = eventInfo->auxdecor< float >( "recoilEta" );
  m_recoilPhi = eventInfo->auxdecor< float >( "recoilPhi" );
  m_recoilM = eventInfo->auxdecor< float >( "recoilM" )/1e3;
  m_recoilE = eventInfo->auxdecor< float >( "recoilE" )/1e3;

  if( m_emulate_trigger ){
    m_max_HLT_jet_pt = eventInfo->auxdecor< float > ( "max_HLT_jet_pt" );
    m_max_L1_jet_pt = eventInfo->auxdecor< float > ( "max_L1_jet_pt" );
  }

}

void MiniTree::FillJetsUser( const xAOD::Jet* jet, const std::string ) {

  if( jet->isAvailable< float >( "DetectorEta" ) ) {
    m_jet_detEta.push_back( jet->auxdata< float >("DetectorEta") );
  } else {
    m_jet_detEta.push_back( -999 );
  }
  if( jet->isAvailable< float >( "jetAlpha" ) ) {
    m_jet_alpha.push_back( jet->auxdata< float >("jetAlpha") );
  } else {
    m_jet_alpha.push_back( -999 );
  }
  if( jet->isAvailable< float >( "TileCorrectedPt" ) ) {
    m_jet_TileCorrectedPt.push_back( jet->auxdata< float >("TileCorrectedPt")/1e3 );
  } else {
    m_jet_TileCorrectedPt.push_back( -999 );
  }

  if (jet->isAvailable< float >( "beta" ) ){
    m_jet_beta.push_back( jet->auxdata< float >("beta") );
  }else{
    m_jet_beta.push_back( -999 );
  }
  if (jet->isAvailable< float >( "jetCorr" ) ){
    m_jet_corr.push_back( jet->auxdata< float >("jetCorr") );
  }else{
    m_jet_corr.push_back( -999 );
  }
//  if (jet->isAvailable< float >( "EMFrac" ) ){
//    m_jet_EMFrac.push_back( jet->auxdata< float >("EMFrac") );
//  }else{
//    m_jet_EMFrac.push_back( -999 );
//  }
//  if (jet->isAvailable< float >( "HECFrac" ) ){
//    m_jet_HECFrac.push_back( jet->auxdata< float >("HECFrac") );
//  }else{
//    m_jet_HECFrac.push_back( -999 );
//  }
  if (jet->isAvailable< float >( "TileFrac" ) ){
    m_jet_TileFrac.push_back( jet->auxdata< float >("TileFrac") );
  }else{
    m_jet_TileFrac.push_back( -999 );
  }
  if (jet->isAvailable< float >( "Jvt" ) ){
    m_jet_Jvt.push_back( jet->auxdata< float >("Jvt") );
  }else{
    m_jet_Jvt.push_back( -999 );
  }
  if (jet->isAvailable< int >( "PartonTruthLabelID" ) ){
    m_jet_PartonTruthLabelID.push_back( jet->auxdata< int >("PartonTruthLabelID") );
  }else{
    m_jet_PartonTruthLabelID.push_back( -999 );
  }

  if( m_trackjet ){
    jet->isAvailable<float>("jet_track_pt") ? m_jet_track_pt.push_back(jet->auxdata<float>("jet_track_pt")/1e3) : m_jet_track_pt.push_back(-999);
    jet->isAvailable<float>("jet_track_eta") ? m_jet_track_eta.push_back(jet->auxdata<float>("jet_track_eta")) : m_jet_track_eta.push_back(-999);
    jet->isAvailable<float>("jet_track_phi") ? m_jet_track_phi.push_back(jet->auxdata<float>("jet_track_phi")) : m_jet_track_phi.push_back(-999);
    jet->isAvailable<float>("jet_track_e") ? m_jet_track_e.push_back(jet->auxdata<float>("jet_track_e")/1e3) : m_jet_track_e.push_back(-999);
    jet->isAvailable<float>("jet_track_respPt") ? m_jet_track_respPt.push_back(jet->auxdata<float>("jet_track_respPt")) : m_jet_track_respPt.push_back(-999);
    jet->isAvailable<float>("jet_track_respE") ? m_jet_track_respE.push_back(jet->auxdata<float>("jet_track_respE")) : m_jet_track_respE.push_back(-999);
  }

  if( m_truthjet ){
    jet->isAvailable<float>("jet_truth_pt") ? m_jet_truth_pt.push_back(jet->auxdata<float>("jet_truth_pt")/1e3) : m_jet_truth_pt.push_back(-999);
    jet->isAvailable<float>("jet_truth_eta") ? m_jet_truth_eta.push_back(jet->auxdata<float>("jet_truth_eta")) : m_jet_truth_eta.push_back(-999);
    jet->isAvailable<float>("jet_truth_phi") ? m_jet_truth_phi.push_back(jet->auxdata<float>("jet_truth_phi")) : m_jet_truth_phi.push_back(-999);
    jet->isAvailable<float>("jet_truth_e") ? m_jet_truth_e.push_back(jet->auxdata<float>("jet_truth_e")/1e3) : m_jet_truth_e.push_back(-999);
    jet->isAvailable<float>("jet_truth_respPt") ? m_jet_truth_respPt.push_back(jet->auxdata<float>("jet_truth_respPt")) : m_jet_truth_respPt.push_back(-999);
    jet->isAvailable<float>("jet_truth_respE") ? m_jet_truth_respE.push_back(jet->auxdata<float>("jet_truth_respE")) : m_jet_truth_respE.push_back(-999);
    jet->isAvailable<float>("GhostTruthAssociationFraction") ? m_jet_truth_ghost_fraction.push_back(jet->auxdata<float>("GhostTruthAssociationFraction")) : m_jet_truth_ghost_fraction.push_back(-999);
  }

  for(unsigned int iB=0; iB < m_jet_BTagNames.size(); ++iB){
    std::string thisBTagName = m_jet_BTagNames.at(iB);


    if( jet->isAvailable< int >( ("BTag_"+thisBTagName+"Fixed").c_str() ) ){
      m_jet_BTagBranches.at(iB).push_back( jet->auxdata< int >( ("BTag_"+thisBTagName+"Fixed").c_str()) );
    }else{
      m_jet_BTagBranches.at(iB).push_back( -999 );
    }

    if( jet->isAvailable< float >( ("BTagSF_"+thisBTagName+"Fixed").c_str() ) ){
      m_jet_BTagSFBranches.at(iB).push_back( jet->auxdata< float >( ("BTagSF_"+thisBTagName+"Fixed").c_str()) );
    }else{
      m_jet_BTagSFBranches.at(iB).push_back( -999 );
    }
  }//for iB branches

}


void MiniTree::ClearEventUser() {
}

void MiniTree::ClearJetsUser(const std::string jetName ) {
  m_jet_detEta.clear();
  m_jet_alpha.clear();
  m_jet_TileCorrectedPt.clear();
  m_jet_beta.clear();
  m_jet_corr.clear();
//  m_jet_EMFrac.clear();
//  m_jet_HECFrac.clear();
  m_jet_TileFrac.clear();
  m_jet_Jvt.clear();
  m_jet_PartonTruthLabelID.clear();

  if( m_trackjet ){
    m_jet_track_pt.clear();
    m_jet_track_eta.clear();
    m_jet_track_phi.clear();
    m_jet_track_e.clear();
    m_jet_track_respPt.clear();
    m_jet_track_respE.clear();
  }

  if( m_truthjet ){
    m_jet_truth_pt.clear();
    m_jet_truth_eta.clear();
    m_jet_truth_phi.clear();
    m_jet_truth_e.clear();
    m_jet_truth_respPt.clear();
    m_jet_truth_respE.clear();
    m_jet_truth_ghost_fraction.clear();
  }

  for(unsigned int iB=0; iB < m_jet_BTagNames.size(); ++iB){
    m_jet_BTagBranches.at(iB).clear();
    m_jet_BTagSFBranches.at(iB).clear();
  }
}

