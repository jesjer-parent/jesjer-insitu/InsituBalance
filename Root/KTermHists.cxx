#include <InsituBalance/KTermHists.h>
#include <sstream>
#include <TLorentzVector.h>

using namespace std;


KTermHists :: KTermHists (std::string name, std::string detailStr) :
  JetHists(name, detailStr)
{

  m_debug = false;
}


StatusCode KTermHists::initialize() {

  cout << "initialized KTermHists" << endl;
  cout << "booking histogram" << endl;

  // initializing the histograms for each pt bin (see header)
  std::stringstream ss;
  for(unsigned int itr=0; itr < ptrefbins.size()-1; ++itr){
    ss << itr;
    // for the kTerm
    h_kTerm_ptvect.push_back( book("kTerm_histos/", ("h_kTerm_ptrefbin_"+ss.str()), "DeltaR",30,0.,3.,"density",0.,100000.) );
    // for the dR(track,J)
    h_kdR_ptvect.push_back( book("kTerm_histos/",  ("h_dR_ptrefbin_"+ss.str()), "DeltaR",90,0.,3.) ) ;
    ss.str("");
    ss.clear(); // Clear state flags.
  }

  return StatusCode::SUCCESS;
}


StatusCode KTermHists::execute( std::vector< xAOD::Jet* >* jets, const xAOD::EventInfo* eventInfo, std::vector< xAOD::TrackParticle_v1* >* tracks, TLorentzVector recoilTLV) {

  // Note: assumed the jets input is ordered per pt, descending (they are ordered in InsituBalanceAlgo.cxx)
  // Note: pt density calculation from Aliaksei's ZeeD package
  static SG::AuxElement::ConstAccessor<float> weight ("weight");
  float eventWeight = weight( *eventInfo );

  Double_t  leadingJetDeltaPhi    = jets->at(0)->p4().DeltaPhi( recoilTLV ) ;

  //Now reinforce the cuts just to be sure

  if ( jets->at(0)->eta() > 0.8  ) return StatusCode::SUCCESS; // cut on jets eta //
  // if ( jets->at(0)->eta() > 0.4  ) return StatusCode::SUCCESS; // cut on jets eta //

  if (jets->at(0)->pt() < 10*1e3) return StatusCode::SUCCESS; // 10 GeV cut on jet pt //
  Double_t maximum = std::max(15*1e3  , recoilTLV.Pt() );
  if(jets->size()>1){
    if (jets->at(1)->pt() > maximum ) return StatusCode::SUCCESS; // max cut on subleading jet //
  }
  if (leadingJetDeltaPhi < 2.8 )  return StatusCode::SUCCESS; // delta phi cut jet,Z //

  if (jets->at(0)->auxdata<float>("Jvt") < 0.59 and jets->at(0)->pt() < 60*1e3 ) return StatusCode::SUCCESS; // JVT cut for jets with pt < 60 GeV //

  // jets->at(0)->auxdata< float >("Jvt") = m_JVTUpdateTool_handle->updateJvt( *(jets->at(0)) );
  // if( !m_JetJVTEfficiencyTool_handle->passesJvtCut( *(jets->at(0))) and jets->at(0)->pt() < 60*1e3 ) return StatusCode::SUCCESS;

  // the pt of the reference object
  Double_t ptReferenceCosPhi     = recoilTLV.Pt() /1e3 * TMath::Abs( TMath::Cos(leadingJetDeltaPhi) );

  //initializing the pt sum and area
  Double_t *ptSum = new Double_t[ deltaRBins.size() ], *areas = new Double_t[ deltaRBins.size() ];

  // static SG::AuxElement::ConstAccessor< float > DetectorEta ("DetectorEta");

  for (UInt_t i = 0 ; i < deltaRBins.size() - 1 ; i++){
    ptSum[i] = 0;
    Double_t radiusInBin = deltaRBins.at(i+1);
    // get the areas
    areas[i] = truncatedArea( jets->at(0)->p4().Eta(), jets->at(0)->p4().Phi(), radiusInBin); // the 0th is the leading//
  }


  for(unsigned int iTrk = 0 ; iTrk<tracks->size();++iTrk){ // tracks loop
    // get delta R
    Double_t deltaR = tracks->at(iTrk)->p4().DeltaR( jets->at(0)->p4() );

    // loop on pt bin
    for(UInt_t j = 0 ; j < ptrefbins.size() -1; j++){

      if ( ptReferenceCosPhi >ptrefbins.at(j) && ptReferenceCosPhi < ptrefbins.at(j+1) )
        // fill dR histogram
        h_kdR_ptvect.at(j)->Fill(deltaR,eventWeight);
    }
    for (UInt_t i = 0 ; i < deltaRBins.size() - 1 ; i++){
      // if track is in dR, then add up the pT
      if ( deltaR >deltaRBins.at(i)  && deltaR < deltaRBins.at(i+1) )
        ptSum[i] +=  tracks->at(iTrk)->pt() / 1e3 ;
    }
  } // end tracks loop


  // now starts the density calculation
  Double_t areaM1 = 0;
  for (UInt_t i = 0 ; i < deltaRBins.size() - 1 ; i++){
    Double_t radiusInBin = deltaRBins.at(i) + (deltaRBins.at(i+1) - deltaRBins.at(i) )/2.;
    // get the annulus by subtraction
    Double_t areaI = areas[i] - areaM1;
    areaM1 += areaI;
    Double_t density = ptSum[i]/areaI;

    for(UInt_t j = 0 ; j < ptrefbins.size() -1; j++){
      if ( ptReferenceCosPhi >ptrefbins.at(j) && ptReferenceCosPhi < ptrefbins.at(j+1) ){
        h_kTerm_ptvect.at(j)->Fill(radiusInBin,density,eventWeight)  ;
      }
    }
  }


  return StatusCode::SUCCESS;
}


void KTermHists :: record(EL::Worker* wk){

  JetHists::record( wk );
  return;
}

// directly from Aliaksei's ZeeDHistManagerZjet.cxx
// to explain it simply: if the circle intersect the 2.5 (eta0) the area there has to be neglected (no tracks there)
Double_t KTermHists::truncatedArea(double etaJ, double phiJ, double R, double eta0)
/*****************************************************************/
{
  if( R > TMath::Pi() )
    Error("Zjet::truncatedArea()","R > Pi  circle will overlap itself\n");

  double deltaEta1 = fabs(etaJ-eta0);
  double deltaEta2 = fabs(etaJ+eta0);
  double deltaEta1Square = deltaEta1*deltaEta1;
  double deltaEta2Square = deltaEta2*deltaEta2;
  bool intersect1 = (R*R-deltaEta1Square>0 ? true : false);
  bool intersect2 = (R*R-deltaEta2Square>0 ? true : false);
  double phi11 = 0.;
  double phi12 = 0.;
  double phi21 = 0.;
  double phi22 = 0.;
  if((R<=deltaEta1 && intersect1) || (R<=deltaEta2 && intersect2))
    Error("Zjet::truncatedArea()"," Found intersections while it shouldn't");
  if((R>deltaEta1 && !intersect1) || (R>deltaEta2 && !intersect2))
    Error("Zjet::truncatedArea()"," Didn't found intersections while it should");

  if(intersect1){
      phi11 = phiJ + sqrt(R*R-deltaEta1Square);
      phi12 = phiJ - sqrt(R*R-deltaEta1Square);
  }
  if(intersect2){
      phi21 = phiJ + sqrt(R*R-deltaEta2Square);
      phi22 = phiJ - sqrt(R*R-deltaEta2Square);
  }

  double area = 0.;
  if(!intersect1 && !intersect2){
    area = M_PI*R*R;
  }else if(intersect1 && !intersect2){
      double deltaPhi = fabs(phi11-phi12);
      double alpha = 2.*asin(deltaPhi/(2.*R));
      double areaCircle = M_PI*R*R*(1.-alpha/(2.*M_PI));
      double areaTriangle = deltaEta1*deltaPhi/2.;
      area = areaCircle + areaTriangle;

  }else if(!intersect1 && intersect2){
      double deltaPhi = fabs(phi21-phi22);
      double alpha = 2.*asin(deltaPhi/(2.*R));
      double areaCircle = M_PI*R*R*(1.-alpha/(2.*M_PI));
      double areaTriangle = deltaEta2*deltaPhi/2.;
      area = areaCircle + areaTriangle;

  }else if(intersect1 && intersect2){
      double deltaPhi1 = fabs(phi11-phi12);
      double deltaPhi2 = fabs(phi21-phi22);
      double alpha1 = 2.*asin(deltaPhi1/(2.*R));
      double alpha2 = 2.*asin(deltaPhi2/(2.*R));
      double areaCircle = M_PI*R*R*(1.-(alpha1+alpha2)/(2.*M_PI));
      double areaTriangle = (deltaEta1*deltaPhi1 + deltaEta2*deltaPhi2)/2.;
      area = areaCircle + areaTriangle;
  }

  return area;
}


