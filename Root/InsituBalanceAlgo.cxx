// EL include(s):
#include <EventLoop/Job.h>
#include <EventLoop/Worker.h>
#include "EventLoop/OutputStream.h"

// EDM include(s):
#include "AthContainers/ConstDataVector.h"
#include "AthContainers/DataVector.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include <xAODJet/JetContainer.h>
#include "xAODEventInfo/EventInfo.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"

//For trigger jet matching
#include "xAODTrigger/JetRoIContainer.h"

#include "PathResolver/PathResolver.h"

// ROOT include(s):
#include "TFile.h"
#include "TEnv.h"
#include "TSystem.h"
#include "TKey.h"

// c++ includes(s):
#include <iostream>
#include <fstream>

// package include(s):
#include <InsituBalance/InsituBalanceAlgo.h>
#include <xAODAnaHelpers/HelperFunctions.h>
#include <InsituBalance/MultijetHists.h>
#include <InsituBalance/KTermHists.h>

// external tools include(s):
#include "JetCalibTools/JetCalibrationTool.h"
#include "JetUncertainties/JetUncertaintiesTool.h"

#include "JetMomentTools/JetVertexTaggerTool.h"
#include "JetJvtEfficiency/JetJvtEfficiency.h"

#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"
#include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"

#include "SystTool/SystContainer.h"

#include "JetTileCorrection/JetTileCorrectionTool.h"

#include "Selections.h"
#include "JetCalibrations.h"

//Track qualtity
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"


using namespace std;
using namespace xAH;
//using namespace std::placeholders;

// this is needed to distribute the algorithm to the workers
ClassImp(InsituBalanceAlgo)



InsituBalanceAlgo :: InsituBalanceAlgo () :
  Algorithm("InsituBalance")
{

}

//InsituBalanceAlgo ::~InsituBalanceAlgo(){
//}

EL::StatusCode  InsituBalanceAlgo :: configure (){
  Info("configure()", "Configuring InsituBalanceAlgo Interface.");

  if( m_inContainerName_jets.empty() ) {
    Error("configure()", "InputContainer is empty!");
    return EL::StatusCode::FAILURE;
  }

  if( m_modeStr.find("MJB") != std::string::npos)
    m_mode = MJB;
  else if( m_modeStr.find("Gjet") != std::string::npos)
    m_mode = GJET;
  else if( m_modeStr.find("Zeejet") != std::string::npos)
    m_mode = ZEEJET;
  else if( m_modeStr.find("Zmmjet") != std::string::npos)
    m_mode = ZMMJET;

  // Save binning to use
  std::stringstream ssb(m_binning);
  std::string thisSubStr;
  std::string::size_type sz;
  while (std::getline(ssb, thisSubStr, ',')) {
    m_bins.push_back( std::stof(thisSubStr, &sz) );
  }
  Info("configure()", "Setting binning to %s", m_binning.c_str());

  // Save b-tag WPs to use
  std::stringstream ssbtag(m_bTagWPsString);
  while (std::getline(ssbtag, thisSubStr, ',')) {
    m_bTagWPs.push_back( thisSubStr );
  }
  Info("configure()", "Setting b-tag WPs to %s", m_bTagWPsString.c_str());

  // Save triggers to use
  std::stringstream ss(m_triggerAndPt);
  while (std::getline(ss, thisSubStr, ',')) {
    m_triggers.push_back( thisSubStr.substr(0, thisSubStr.find_first_of(':')) );
    m_triggerThresholds.push_back( std::stof(thisSubStr.substr(thisSubStr.find_first_of(':')+1, thisSubStr.size()) , &sz) *1e3 );
  }


  //If using MCPileupCheck
  if( m_MCPileupCheckContainer.compare("None") == 0 )
    m_useMCPileupCheck = false;
  else
    m_useMCPileupCheck = true;

  if( m_writeNominalTree )
    m_writeTree = true;

  if(m_isMC){
    Info("configure()", "Running on MC input file. \n");
  }else{
    Info("configure()", "Running on data input file. \n");
  }

  Info("configure()", "InsituBalanceAlgo Interface succesfully configured! \n");

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode InsituBalanceAlgo :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  job.useXAOD();
  xAOD::Init( "InsituBalanceAlgo" ).ignore(); // call before opening first file

  EL::OutputStream outForTree("tree");
  job.outputAdd (outForTree);

  if( m_bootstrap )
    job.outputAdd(EL::OutputStream("SystToolOutput"));


  return EL::StatusCode::SUCCESS;
}



EL::StatusCode InsituBalanceAlgo :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  Info("histInitialize()", "Calling histInitialize \n");

  ANA_MSG_INFO( "Calling histInitialize");
  ANA_CHECK( xAH::Algorithm::algInitialize())


  return EL::StatusCode::SUCCESS;
}





EL::StatusCode InsituBalanceAlgo :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode InsituBalanceAlgo :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  (void)firstFile; //surpress unused param warning
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode InsituBalanceAlgo :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  m_event = wk()->xaodEvent();
  m_store = wk()->xaodStore();
  m_eventCounter = -1;

  const xAOD::EventInfo* eventInfo = 0;
  ANA_CHECK( HelperFunctions::retrieve(eventInfo, "EventInfo", m_event, m_store, msg()) );
  m_isMC = ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ) ? true : false;

  ANA_CHECK( this->configure() );

  ANA_CHECK(getSampleWeights(eventInfo) );

  // load all variations
  setupJetCalibrationStages();
  if(m_mode == MJB){
    ANA_CHECK(loadJetUncertaintyTool());
  }
  ANA_CHECK(loadSystematics());

  //load Calibration and systematics files
  ANA_CHECK(loadJetCalibrationTool());
  ANA_CHECK(loadJVTTool(m_jetDef)); 
  ANA_CHECK(loadBTagTools());
  ANA_CHECK(loadJetTileCorrectionTool());

  ANA_CHECK(loadMJBCalibration());

  if( m_bootstrap ){
    systTool = new SystContainer(m_sysName, m_bins, m_systTool_nToys);
  }

  //  the track selections
  if (m_makeKTerm){
    ANA_CHECK( ASG_MAKE_ANA_TOOL(m_trkSelTool_handle, InDet::InDetTrackSelectionTool) );
    ANA_CHECK( m_trkSelTool_handle.setProperty("CutLevel", "Loose") );
    ANA_CHECK( m_trkSelTool_handle.setProperty("OutputLevel", msg().level()) );
    ANA_CHECK( m_trkSelTool_handle.retrieve() );
  }

  //// Add the defined selections, in correct order /////
  std::vector< std::string > cutflowNames;
  if( m_mode == MJB ){

    std::function<bool(void)> func_MCCleaning = std::bind(&InsituBalanceAlgo::cut_MCCleaning, this);
    m_selections.push_back(func_MCCleaning);
    cutflowNames.push_back( "MCCleaning" );
    m_selType.push_back( PRE );

    std::function<bool(void)> func_CleanJet = std::bind(&InsituBalanceAlgo::cut_CleanJet, this);
    m_selections.push_back(func_CleanJet);
    cutflowNames.push_back( "CleanJet" );
    m_selType.push_back( PRE );

    //We pick the leading jet here
    std::function<bool(void)> func_LeadEta = std::bind(&InsituBalanceAlgo::cut_LeadEta, this);
    m_selections.push_back(func_LeadEta);
    cutflowNames.push_back( "LeadEta" );
    m_selType.push_back( PRE );

    std::function<bool(void)> func_JetEta = std::bind(&InsituBalanceAlgo::cut_JetEta, this);
    m_selections.push_back(func_JetEta);
    cutflowNames.push_back( "JetEta" );
    m_selType.push_back( PRE );

    //If 2nd jet is above Vjet (or MJB) calibration threshold
    std::function<bool(void)> func_SubPt = std::bind(&InsituBalanceAlgo::cut_SubPt, this);
    m_selections.push_back(func_SubPt);
    cutflowNames.push_back( "SubPt" );
    m_selType.push_back( PRE );

    std::function<bool(void)> func_JetPtThresh = std::bind(&InsituBalanceAlgo::cut_JetPtThresh, this);
    m_selections.push_back(func_JetPtThresh);
    cutflowNames.push_back( "JetPtThresh" );
    m_selType.push_back( SYST );

    //Lead jet can't fail due to pt requirement
    std::function<bool(void)> func_JVT = std::bind(&InsituBalanceAlgo::cut_JVT, this);
    m_selections.push_back(func_JVT);
    cutflowNames.push_back( "JVT" );
    m_selType.push_back( SYST );

    std::function<bool(void)> func_TriggerEffRecoil = std::bind(&InsituBalanceAlgo::cut_TriggerEffRecoil, this);
    m_selections.push_back(func_TriggerEffRecoil);
    cutflowNames.push_back( "TriggerEffRecoil" );
    m_selType.push_back( RECOIL );

    std::function<bool(void)> func_PtAsym = std::bind(&InsituBalanceAlgo::cut_PtAsym, this);
    m_selections.push_back(func_PtAsym);
    cutflowNames.push_back( "PtAsym" );
    m_selType.push_back( RECOIL );

    std::function<bool(void)> func_Alpha = std::bind(&InsituBalanceAlgo::cut_Alpha, this);
    m_selections.push_back(func_Alpha);
    cutflowNames.push_back( "Alpha" );
    m_selType.push_back( RECOIL );

    std::function<bool(void)> func_Beta = std::bind(&InsituBalanceAlgo::cut_Beta, this);
    m_selections.push_back(func_Beta);
    cutflowNames.push_back( "Beta" );
    m_selType.push_back( RECOIL );

  }

  if( m_mode == GJET ){

    std::function<bool(void)> func_MCCleaning = std::bind(&InsituBalanceAlgo::cut_MCCleaning, this);
    m_selections.push_back(func_MCCleaning);
    cutflowNames.push_back( "MCCleaning" );
    m_selType.push_back( PRE );

    std::function<bool(void)> func_CleanJet = std::bind(&InsituBalanceAlgo::cut_CleanJet, this);
    m_selections.push_back(func_CleanJet);
    cutflowNames.push_back( "CleanJet" );
    m_selType.push_back( PRE );

    std::function<bool(void)> func_JetEta = std::bind(&InsituBalanceAlgo::cut_JetEta, this);
    m_selections.push_back(func_JetEta);
    cutflowNames.push_back( "JetEta" );
    m_selType.push_back( PRE );

    // Leading photon trigger efficiency cut
    std::function<bool(void)> func_TriggerEffRecoil = std::bind(&InsituBalanceAlgo::cut_TriggerEffRecoil, this);
    m_selections.push_back(func_TriggerEffRecoil);
    cutflowNames.push_back( "TriggerEffRecoil" );
    m_selType.push_back( SYST );

    ///// Leading photon conversion veto
    std::function<bool(void)> func_ConvPhot = std::bind(&InsituBalanceAlgo::cut_ConvPhot, this);
    m_selections.push_back(func_ConvPhot);
    cutflowNames.push_back( "ConvPhot" );
    m_selType.push_back( SYST );

    // jet-photon overlap removal (only removes jets)
    std::function<bool(void)> func_OverlapRemoval = std::bind(&InsituBalanceAlgo::cut_OverlapRemoval, this);
    m_selections.push_back(func_OverlapRemoval);
    cutflowNames.push_back( "OverlapRemoval" );
    m_selType.push_back( SYST );

    //JVT (removes jets)
    std::function<bool(void)> func_JVT = std::bind(&InsituBalanceAlgo::cut_JVT, this);
    m_selections.push_back(func_JVT);
    cutflowNames.push_back( "JVT" );
    m_selType.push_back( SYST );

    //Leading jet eta selection must be after JVT, in case highest pt jet is pileup
    std::function<bool(void)> func_LeadEta = std::bind(&InsituBalanceAlgo::cut_LeadEta, this);
    m_selections.push_back(func_LeadEta);
    cutflowNames.push_back( "LeadEta" );
    m_selType.push_back( SYST );

    ////// Here we now have the leading jet /////
    std::function<bool(void)> func_LeadJetPtThresh = std::bind(&InsituBalanceAlgo::cut_LeadJetPtThresh, this);
    m_selections.push_back(func_LeadJetPtThresh);
    cutflowNames.push_back( "LeadJetPtThresh" );
    m_selType.push_back( SYST );

    std::function<bool(void)> func_PtAsym = std::bind(&InsituBalanceAlgo::cut_PtAsym, this);
    m_selections.push_back(func_PtAsym);
    cutflowNames.push_back( "PtAsym" );
    m_selType.push_back( RECOIL );

    //// This is delta phi ////////////
    std::function<bool(void)> func_Alpha = std::bind(&InsituBalanceAlgo::cut_Alpha, this);
    m_selections.push_back(func_Alpha);
    cutflowNames.push_back( "Alpha" );
    m_selType.push_back( RECOIL );

    // Just get beta info, not an actual selection.
    std::function<bool(void)> func_Beta = std::bind(&InsituBalanceAlgo::cut_Beta, this);
    m_selections.push_back(func_Beta);
    cutflowNames.push_back( "Beta" );
    m_selType.push_back( RECOIL );

  }

  if( m_mode == ZEEJET || m_mode == ZMMJET ){


    std::function<bool(void)> func_MCCleaning = std::bind(&InsituBalanceAlgo::cut_MCCleaning, this);
    m_selections.push_back(func_MCCleaning);
    cutflowNames.push_back( "MCCleaning" );
    m_selType.push_back( PRE );

    std::function<bool(void)> func_CleanJet = std::bind(&InsituBalanceAlgo::cut_CleanJet, this);
    m_selections.push_back(func_CleanJet);
    cutflowNames.push_back( "CleanJet" );
    m_selType.push_back( PRE );

    std::function<bool(void)> func_JetEta = std::bind(&InsituBalanceAlgo::cut_JetEta, this);
    m_selections.push_back(func_JetEta);
    cutflowNames.push_back( "JetEta" );
    m_selType.push_back( PRE );

    //ZMass & opposite-sign requirement
    std::function<bool(void)> func_Zmass = std::bind(&InsituBalanceAlgo::cut_Zmass, this);
    m_selections.push_back(func_Zmass);
    cutflowNames.push_back( "ZMass" );
    m_selType.push_back( SYST );

    std::function<bool(void)> func_TriggerEffRecoil = std::bind(&InsituBalanceAlgo::cut_TriggerEffRecoil, this);
    m_selections.push_back(func_TriggerEffRecoil);
    cutflowNames.push_back( "TriggerEffRecoil" );
    m_selType.push_back( SYST );

    // jet-el/mu overlap removal
    std::function<bool(void)> func_OverlapRemoval = std::bind(&InsituBalanceAlgo::cut_OverlapRemoval, this);
    m_selections.push_back(func_OverlapRemoval);
    cutflowNames.push_back( "OverlapRemoval" );
    m_selType.push_back( SYST );

    std::function<bool(void)> func_JVT = std::bind(&InsituBalanceAlgo::cut_JVT, this);
    m_selections.push_back(func_JVT);
    cutflowNames.push_back( "JVT" );
    m_selType.push_back( SYST );

    ////// Here we now have the leading jet /////
    std::function<bool(void)> func_LeadEta = std::bind(&InsituBalanceAlgo::cut_LeadEta, this);
    m_selections.push_back(func_LeadEta);
    cutflowNames.push_back( "LeadEta" );
    m_selType.push_back( SYST );

    std::function<bool(void)> func_LeadJetPtThresh = std::bind(&InsituBalanceAlgo::cut_LeadJetPtThresh, this);
    m_selections.push_back(func_LeadJetPtThresh);
    cutflowNames.push_back( "LeadJetPtThresh" );
    m_selType.push_back( SYST );

    std::function<bool(void)> func_PtAsym = std::bind(&InsituBalanceAlgo::cut_PtAsym, this);
    m_selections.push_back(func_PtAsym);
    cutflowNames.push_back( "PtAsym" );
    m_selType.push_back( RECOIL );

    //// This is delta phi ////////////
    std::function<bool(void)> func_Alpha = std::bind(&InsituBalanceAlgo::cut_Alpha, this);
    m_selections.push_back(func_Alpha);
    cutflowNames.push_back( "Alpha" );
    m_selType.push_back( RECOIL );

    // Just get beta info, not an actual selection.
    std::function<bool(void)> func_Beta = std::bind(&InsituBalanceAlgo::cut_Beta, this);
    m_selections.push_back(func_Beta);
    cutflowNames.push_back( "Beta" );
    m_selType.push_back( RECOIL );

  }

  for(unsigned int iVar=0; iVar < m_sysName.size(); ++iVar){
    Info("initialize", "Systematic var %i is %s with detail string (index for JES/JER) %f", iVar, m_sysName.at(iVar).c_str(), m_sysDetail.at(iVar) );
  }

  if(m_useCutFlow) {
    Info("initialize", "Setting Cutflow");

    //std::string newName;
    TFile *file = wk()->getOutputFile ("cutflow");
    TH1D* origCutflowHist = (TH1D*)file->Get("cutflow");
    TH1D* origCutflowHistW = (TH1D*)file->Get("cutflow_weighted");

    for(unsigned int iCut = 0; iCut < cutflowNames.size(); ++iCut){
      if( iCut == 0)
        m_cutflowFirst = origCutflowHist->GetXaxis()->FindBin( cutflowNames.at(iCut).c_str() );
      else
        origCutflowHist->GetXaxis()->FindBin( cutflowNames.at(iCut).c_str() );

      origCutflowHistW->GetXaxis()->FindBin( cutflowNames.at(iCut).c_str() );
    }
    //Add a cutflow for each variation
    for(unsigned int iVar=0; iVar < m_sysName.size(); ++iVar){
      m_cutflowHist.push_back( (TH1D*) origCutflowHist->Clone() );
      m_cutflowHistW.push_back( (TH1D*) origCutflowHistW->Clone() );
      m_cutflowHist.at(iVar)->SetName( ("cutflow_"+m_sysName.at(iVar)).c_str() );
      m_cutflowHistW.at(iVar)->SetName( ("cutflow_weighted_"+m_sysName.at(iVar)).c_str() );
      m_cutflowHist.at(iVar)->SetTitle( ("cutflow_"+m_sysName.at(iVar)).c_str() );
      m_cutflowHistW.at(iVar)->SetTitle( ("cutflow_weighted_"+m_sysName.at(iVar)).c_str() );
      m_cutflowHist.at(iVar)->SetDirectory( file );
      m_cutflowHistW.at(iVar)->SetDirectory( file );

      //Need to retroactively fill original bins of these histograms
      for(unsigned int iBin=1; iBin < m_cutflowFirst; ++iBin){
        m_cutflowHist.at(iVar)->SetBinContent(iBin, origCutflowHist->GetBinContent(iBin) );
        m_cutflowHistW.at(iVar)->SetBinContent(iBin, origCutflowHistW->GetBinContent(iBin) );
      }//for iBin
    }//for each m_sysName
  } //m_useCutflow

  //Add output hists for each variation
  if( m_subjetThreshold_MJB <= 0 && m_subjetThreshold_Vjet <= 0 )
    m_ss << "Validation";
  else
    m_ss << "MJB";
  for(unsigned int iVar=0; iVar < m_sysName.size(); ++iVar){
    std::string histOutputName =  m_ss.str()+"_"+m_sysName.at(iVar);

    MultijetHists* thisJetHists = new MultijetHists( histOutputName, (m_jetDetailStr+" "+m_MJBDetailStr).c_str() );
    if( m_mode == GJET )
      thisJetHists->SetPhoton( m_photonDetailStr );
    else if( m_mode == ZEEJET )
      thisJetHists->SetElectron( m_elDetailStr );
    else if( m_mode == ZMMJET )
      thisJetHists->SetMuon( m_muDetailStr );

    m_jetHists.push_back(thisJetHists);
    m_jetHists.at(iVar)->initialize(m_binning);
    m_jetHists.at(iVar)->record( wk() );
    std::cout << "multijethists name is " << histOutputName << std::endl;
  }
  m_ss.str("");

  if (m_makeKTerm){
    Info("initialize", "Setting KTerm Histograms");
    // now initializing the KTermHists, will fill them later
    m_KTermHist = new KTermHists( "KTermHists", "KTermHists" );
    m_KTermHist->initialize();
    m_KTermHist->record( wk() );
  }

  //Writing nominal tree only requies this sample to have the nominal output
  if( m_writeNominalTree && m_NominalIndex < 0){
    m_writeNominalTree = false;
  }

  if( m_writeTree){
    Info("initialize", "Setting TTrees");
    TFile * treeFile = wk()->getOutputFile ("tree");
    if( !treeFile ) {
      Error("initialize()","Failed to get file for output tree!");
      return EL::StatusCode::FAILURE;
    }
    for(int unsigned iVar=0; iVar < m_sysName.size(); ++iVar){
      if (m_writeNominalTree && (int) iVar != m_NominalIndex)
        continue;

      TTree * outTree = new TTree( ("outTree_"+m_sysName.at(iVar)).c_str(), ("outTree_"+m_sysName.at(iVar) ).c_str());
      if( !outTree ) {
        Error("initialize()","Failed to get output tree!");
        return EL::StatusCode::FAILURE;
      }
      outTree->SetDirectory( treeFile );
      MiniTree* thisMiniTree = new MiniTree(m_event, outTree, treeFile);
      m_treeList.push_back(thisMiniTree);
    }//for iVar

    for( unsigned int iTree=0; iTree < m_treeList.size(); ++iTree){
      m_treeList.at(iTree)->AddEvent(m_eventDetailStr);
      if (m_bTagWPsString.size() > 0){
        m_treeList.at(iTree)->AddJets( (m_jetDetailStr+" MJBbTag_"+m_bTagWPsString).c_str());
      }else{
        m_treeList.at(iTree)->AddJets( m_jetDetailStr );
      }
      if( m_mode == GJET )
        m_treeList.at(iTree)->AddPhotons( m_photonDetailStr );
      else if( m_mode == ZEEJET )
        m_treeList.at(iTree)->AddElectrons( m_elDetailStr );
      else if( m_mode == ZMMJET )
        m_treeList.at(iTree)->AddMuons( m_muDetailStr );

      m_treeList.at(iTree)->AddTrigger( m_trigDetailStr );
    }//for iTree

  }//if m_writeTree

  Info("initialize()", "Succesfully initialized output TTree! \n");

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode InsituBalanceAlgo :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  ++m_eventCounter;
  ANA_MSG_VERBOSE("Begin Execute " << m_eventCounter);
  if(m_eventCounter %100000 == 0)
    Info("execute()", "Event # %i", m_eventCounter);

  m_iCutflow = m_cutflowFirst; //for cutflow histogram automatic filling

  //----------------------------
  // Event information
  //---------------------------
  ///////////////////////////// Retrieve Containers /////////////////////////////////////////
  ANA_MSG_DEBUG("Retrieve Containers");

  m_eventInfo = 0;
  ANA_CHECK( HelperFunctions::retrieve(m_eventInfo, "EventInfo", m_event, m_store, msg()) );
  m_mcEventWeight = (m_isMC ? m_eventInfo->mcEventWeight() : 1.) ;

  const xAOD::VertexContainer* vertices = 0;
  ANA_CHECK( HelperFunctions::retrieve(vertices, "PrimaryVertices", m_event, m_store, msg()) );
  m_pvLocation = HelperFunctions::getPrimaryVertexLocation( vertices );  //Get primary vertex for JVF cut
  const xAOD::Vertex* recoVertex = vertices->at(m_pvLocation);

  const xAOD::JetContainer* inJets = 0;  
  const xAOD::JetContainer* inJetsRecoil = 0;
  ANA_CHECK( HelperFunctions::retrieve(inJets, m_inContainerName_jets, m_event, m_store, msg()) );
  if(m_largeR.compare("True")==0){ ANA_CHECK( HelperFunctions::retrieve(inJetsRecoil, m_inContainerName_ref, m_event, m_store, msg()) ); }
  else{ ANA_CHECK( HelperFunctions::retrieve(inJetsRecoil, m_inContainerName_jets, m_event, m_store, msg()) ); }

  m_truthJets = 0;
  if(m_useMCPileupCheck && m_isMC){
    ANA_CHECK( HelperFunctions::retrieve(m_truthJets, m_MCPileupCheckContainer, m_event, m_store, msg()) );
  }

  const xAOD::TrackParticleContainer* trkContainer = 0;
  ANA_CHECK( HelperFunctions::retrieve(trkContainer, "InDetTrackParticles", m_event, m_store, msg()) );
  if(trkContainer == 0){
    return StatusCode::SUCCESS;
  }

  ///// For jets and tracks, create an editable shallow copy container & vector where jets are removable //////
  std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > jetsSC = xAOD::shallowCopyContainer( *inJets );
  std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > jetsSCrecoil = xAOD::shallowCopyContainer( *inJetsRecoil );
  std::pair< xAOD::TrackParticleContainer*, xAOD::ShallowAuxContainer* > trkContainerSC = xAOD::shallowCopyContainer( *trkContainer );

  m_jets = new std::vector< xAOD::Jet* >();
  std::vector< xAOD::Jet* >* jets = new std::vector< xAOD::Jet* >();
  std::vector< xAOD::Jet* >* jetsRecoil = new std::vector< xAOD::Jet* >();
  for( auto thisJet : *(jetsSC.first) ) {
    jets->push_back( thisJet );
  }
  for( auto thisJet : *(jetsSCrecoil.first) ) {
    jetsRecoil->push_back( thisJet );
  }

  m_tracks = new std::vector< xAOD::TrackParticle_v1* >();
  for( auto thisTrack : *(trkContainerSC.first) ) {
     m_tracks->push_back( thisTrack );
   }

  ////////////////////////////////// Setup m_jets ///////////////////////////////////////////
  if(m_mode == MJB && m_largeR.compare("True")==0){
    if(jets->size()==0){
      // If there are no large-R jets, exit to next event //
      wk()->skipEvent();  
      return EL::StatusCode::SUCCESS;
    }

    //Calibrate large-R jets first and then find the leading jets index
    ANA_CHECK( applyLargeRjetCalibration( jets, jetsRecoil ) );
    reorderJets(jets); 
    reorderJets(jetsRecoil);

    m_jets->push_back( jets->at(0) );
    
    TLorentzVector largeRJet;
    largeRJet.SetPtEtaPhiM(jets->at(0)->pt(), jets->at(0)->eta(), jets->at(0)->phi(), jets->at(0)->m());
    for(uint ijet=0; ijet<jetsRecoil->size(); ++ijet){
      TLorentzVector jet;
      jet.SetPtEtaPhiM(jetsRecoil->at(ijet)->pt(), jetsRecoil->at(ijet)->eta(), jetsRecoil->at(ijet)->phi(), jetsRecoil->at(ijet)->m());
      float deltaR = largeRJet.DeltaR(jet);
      
      if(deltaR>1.0){
      	m_jets->push_back(jetsRecoil->at(ijet));
      }
    }
  }else{*m_jets = *jets;}

  ////////////////////////////////// Apply jet calibrations ////////////////////////////////////////
  if(m_largeR.compare("False")==0){
    ANA_MSG_DEBUG("Apply Jet Calibration Tool");
    ANA_CHECK( applyJetCalibrationTool( m_jets ) );
    reorderJets(m_jets);
  }

  ////// Do the event selections before looping over systematics  //////
  for(unsigned int iS = 0; iS < m_selections.size(); ++iS){
    if( m_selType.at(iS) != PRE ){ continue; }
    if( m_selections.at(iS)() ){ fillCutflowAll(iS); }
    else{
      // If it fails a preselection, exit to next event //
      delete jetsSC.first; delete jetsSC.second; delete m_jets; m_recoilParticles.clear();
      delete jetsSCrecoil.first; delete jetsSCrecoil.second; delete jets; delete jetsRecoil;
      wk()->skipEvent();  return EL::StatusCode::SUCCESS;
    }
  }// for preselections

  // MJB: A vector to save the calibrated P4 of jets
  std::vector<xAOD::JetFourMom_t> jets_calibratedP4;
  for(unsigned int iJet=0; iJet < m_jets->size(); ++iJet){
    xAOD::JetFourMom_t thisJet;
    thisJet.SetCoordinates(m_jets->at(iJet)->pt(), m_jets->at(iJet)->eta(), m_jets->at(iJet)->phi(), m_jets->at(iJet)->m());
    jets_calibratedP4.push_back(thisJet);
  }

  //// MJB: create new container to save the good jets up to this point ////
  std::vector< xAOD::Jet*>* savedJets = new std::vector< xAOD::Jet* >();
  *savedJets = *m_jets;

  const xAOD::PhotonContainer* inPhotons = 0;
  const xAOD::ElectronContainer* inElectrons = 0;
  const xAOD::MuonContainer* inMuons = 0;

  //Get list of systematics that passed selection for this event (for photons and leptons)
  std::vector<std::string>* sysNames_thisEvent(nullptr);
  if( m_mode == GJET || m_mode == ZEEJET || m_mode == ZMMJET){
    ANA_CHECK( HelperFunctions::retrieve(sysNames_thisEvent, m_inputAlgoSystNames_ref, 0, m_store, msg()) );
  }

  ////// Loop over systematic variations of recoil objects /////////////
  for(unsigned int iSys=0; iSys < m_sysName.size(); ++iSys){

    m_iSys = iSys;

    *m_jets = *savedJets;  // Reset the vector of pointers
    //// If MJB, need to reset 4-mom of jets and apply new systematic variations
    if( m_mode == MJB ){

      //Reset each jet pt to calibrated copy (including V+jet if applicable)
      for (unsigned int iJet = 0; iJet < m_jets->size(); ++iJet){

        if(m_sysType.at(iSys) == JCS){
          // A systematic variation that lets us use any jet calibration stage for all jets
          int iCalibStage = m_sysDetail.at(iSys);
          xAOD::JetFourMom_t jetCalibStageCopy = m_jets->at(iJet)->getAttribute<xAOD::JetFourMom_t>( m_JCSStrings.at(iCalibStage).c_str() );
          m_jets->at(iJet)->setJetP4( jetCalibStageCopy );
        } else {

          m_jets->at(iJet)->setJetP4( jets_calibratedP4.at(iJet) );

        }
      }// for each jet

      // Apply additional jet corrections based on systematic variation.
      // Will apply iterative MJB calibration or JES / JER uncertainties.
      applyJetSysVariation(m_jets, iSys);

      // Apply tile correction tool last
      applyJetTileCorrectionTool(m_jets, iSys);

      if(m_largeR.compare("False")==0){reorderJets(m_jets);}
    }//If MJB mode

    ////////// Rebuild the recoil object and get CP systematic varied particles if applicable //////////
    std::string contSuffix = "";
    if( m_sysType.at(iSys) == CP ){
      contSuffix = m_sysName.at(iSys);

      //If this systematic container is not in the list of systematics passing this event, then continue to next systematic
      if (std::find(sysNames_thisEvent->begin(), sysNames_thisEvent->end(), (m_inContainerName_ref+contSuffix).c_str()) != sysNames_thisEvent->end()){
        continue;
      }
    }

    if( m_mode == GJET ){
      ANA_CHECK( HelperFunctions::retrieve(inPhotons, m_inContainerName_ref+contSuffix, m_event, m_store, msg()).isSuccess() );
      m_recoilParticles.clear();
      m_recoilParticles.push_back( inPhotons->at(0) );
      m_recoilPhoton = inPhotons->at(0);
      m_recoilTLV.SetPtEtaPhiE(m_recoilPhoton->pt(), m_recoilPhoton->eta(), m_recoilPhoton->phi(), m_recoilPhoton->e() );

    } else if( m_mode == ZEEJET ){
      ANA_CHECK( HelperFunctions::retrieve(inElectrons, m_inContainerName_ref+contSuffix, m_event, m_store, msg()).isSuccess() );
      m_recoilParticles.clear();
      m_recoilParticles.push_back( inElectrons->at(0) );
      m_recoilParticles.push_back( inElectrons->at(1) );
      TLorentzVector e1;
      e1.SetPtEtaPhiM(inElectrons->at(0)->pt(), inElectrons->at(0)->eta(), inElectrons->at(0)->phi(), 0.510998 );
      TLorentzVector e2;
      e2.SetPtEtaPhiM(inElectrons->at(1)->pt(), inElectrons->at(1)->eta(), inElectrons->at(1)->phi(), 0.510998 );

      //If not opposite sign, set TLV to empty so ZMass fails
      if( inElectrons->at(0)->charge() + inElectrons->at(1)->charge() == 0 )
        m_recoilTLV = e1 + e2;
      else
        m_recoilTLV.SetPtEtaPhiE(0,0,0,0);

    } else if( m_mode == ZMMJET ){
      ANA_CHECK( HelperFunctions::retrieve(inMuons, m_inContainerName_ref+contSuffix, m_event, m_store, msg()) );

      m_recoilParticles.clear();
      m_recoilParticles.push_back( inMuons->at(0) );
      m_recoilParticles.push_back( inMuons->at(1) );
      TLorentzVector m1;
      m1.SetPtEtaPhiM(inMuons->at(0)->pt(), inMuons->at(0)->eta(), inMuons->at(0)->phi(), 105.65837 );
      TLorentzVector m2;
      m2.SetPtEtaPhiM(inMuons->at(1)->pt(), inMuons->at(1)->eta(), inMuons->at(1)->phi(), 105.65837 );

      //If not opposite sign, set TLV to empty so ZMass fails
      if( inMuons->at(0)->charge() + inMuons->at(1)->charge() == 0 )
        m_recoilTLV = m1 + m2;
      else
        m_recoilTLV.SetPtEtaPhiE(0,0,0,0);
    }

    ////// Do the event selections for each systematic before the recoil object is built //////
    bool hasFailedSyst = false;
    for(unsigned int iS = 0; iS < m_selections.size(); ++iS){
      if( m_selType.at(iS) != SYST ){ continue; }
      if( m_selections.at(iS)() ){ fillCutflow(iS, iSys); }
      else{
        hasFailedSyst = true;
    	break;
      }
    }

    /// If it failed a syst selection, continue to the next systematic variation without filling output ///
    if( hasFailedSyst )
      continue;
    ////////// Build the recoil object if MJB //////////
    if( m_mode == MJB ){
      //Create recoilJets object from all nonleading, passing jets
      m_recoilTLV.SetPtEtaPhiM(0,0,0,0);
      for (unsigned int iJet = 1; iJet < m_jets->size(); ++iJet){
        TLorentzVector tmpJet;
        tmpJet.SetPtEtaPhiE(m_jets->at(iJet)->pt(), m_jets->at(iJet)->eta(), m_jets->at(iJet)->phi(), m_jets->at(iJet)->e());
        m_recoilTLV += tmpJet;
      }

    }

    ////// Do the event selections for each systematic after the recoiling object is built //////
    bool hasFailed = false;
    for(unsigned int iS = 0; iS < m_selections.size(); ++iS){
      if( m_selType.at(iS) != RECOIL ){ continue; }
      if( m_selections.at(iS)() ){ fillCutflow(iS, iSys); }
      else{
	hasFailed = true;
        break;
      }
    }
    /// If it failed a syst selection, continue to the next systematic variation without filling output ///
    if( hasFailed ){ continue; }
    //%%%%%%%%%%%%%%%%%%%%%%%%%%% End Selections %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

    ////////////////////// Add Extra Variables //////////////////////////////
    m_eventInfo->auxdecor< int >("njet") = m_jets->size();
    m_eventInfo->auxdecor< float >( "recoilPt" ) = m_recoilTLV.Pt();
    m_eventInfo->auxdecor< float >( "recoilEta" ) = m_recoilTLV.Eta();
    m_eventInfo->auxdecor< float >( "recoilPhi" ) = m_recoilTLV.Phi();
    m_eventInfo->auxdecor< float >( "recoilM" ) = m_recoilTLV.M();
    m_eventInfo->auxdecor< float >( "recoilE" ) = m_recoilTLV.E();
    m_eventInfo->auxdecor< float >( "ptBal" ) = m_jets->at(0)->pt() / m_recoilTLV.Pt();
    unsigned int startJet = 0;
    if(m_largeR.compare("True")==0){startJet=1;}
    for(unsigned int iJet=startJet; iJet < m_jets->size(); ++iJet){

      vector<float> thisEPerSamp = m_jets->at(iJet)->auxdata< vector< float> >("EnergyPerSampling");
      float TotalE = 0., TileE = 0.;
      for( int iLayer=0; iLayer < 24; ++iLayer){
        TotalE += thisEPerSamp.at(iLayer);
      }
      
      TileE += thisEPerSamp.at(12);
      TileE += thisEPerSamp.at(13);
      TileE += thisEPerSamp.at(14);

      m_jets->at(iJet)->auxdecor< float >( "TileFrac" ) = TileE / TotalE;

    }

    if( m_match_track_jets )
      matchTrackJets(m_jets);
    if( m_match_truth_jets && m_isMC)
      matchTruthJets(m_jets);
    
    m_eventInfo->auxdecor< float >("weight_mcEventWeight") = m_mcEventWeight;
    m_eventInfo->auxdecor< float >("weight_prescale") = m_prescale;
    m_eventInfo->auxdecor< float >("weight_xs") = m_weight_xs * m_weight_kfactor;
    float weight_pileup = 1.;
    if(m_eventInfo->isAvailable< float >("PileupWeight") ){
      weight_pileup = m_eventInfo->auxdecor< float >("PileupWeight");
    }
 
    if(m_isMC)
      m_eventInfo->auxdecor< float >("weight") = m_mcEventWeight*m_weight_xs*m_weight_kfactor*weight_pileup;
    else
      m_eventInfo->auxdecor< float >("weight") = m_prescale;

    /////////////// Output Plots ////////////////////////////////
    ANA_MSG_DEBUG("Begin Hist output for " << m_sysName.at(iSys) );
    if( m_mode == GJET ){m_jetHists.at(iSys)->execute( m_jets, m_eventInfo, inPhotons);}
    else if( m_mode == ZEEJET ){m_jetHists.at(iSys)->execute( m_jets, m_eventInfo, inElectrons);}
    else if( m_mode == ZMMJET ){m_jetHists.at(iSys)->execute( m_jets, m_eventInfo, inMuons);}
    else{m_jetHists.at(iSys)->execute( m_jets, m_eventInfo);}

    if (m_makeKTerm){
      if (iSys == 0 and m_mode == ZEEJET){

        //// loop over tracks after selection
        for(unsigned int iTrk = 0 ; iTrk < m_tracks->size();++iTrk){
          // make quality and PV selection on tracks
          if( !m_trkSelTool_handle->accept(*m_tracks->at(iTrk),recoVertex) ) {
            m_tracks->erase(m_tracks->begin()+iTrk); --iTrk;
            continue;
          }
          if( m_tracks->at(iTrk)->vertex() != recoVertex ) {// if not in PV vertex fit
            if( m_tracks->at(iTrk)->vertex() != 0 ){
              m_tracks->erase(m_tracks->begin()+iTrk); --iTrk;
              continue;
            } // make sure in no vertex fits
            if( fabs((m_tracks->at(iTrk)->z0()+m_tracks->at(iTrk)->vz()-recoVertex->z())*sin(m_tracks->at(iTrk)->theta())) > 3.0 ) {
              m_tracks->erase(m_tracks->begin()+iTrk); --iTrk;
              continue;
            } // make sure close to PV in z
          }

          bool trk_close_electron = false;
          // add here loop over m_recoilParticles; if dR (m_tracks->at(iTrk),m_recoilParticle) < 0.35, continue; // this will remove the electrons from the tracks.
          for(unsigned int iPart = 0; iPart < m_recoilParticles.size(); ++iPart){
            if (m_recoilParticles.at(iPart)->p4().DeltaR(m_tracks->at(iTrk)->p4() )  <  0.35 ){
              trk_close_electron = true;
            }
          }
          if (trk_close_electron) {
            m_tracks->erase(m_tracks->begin()+iTrk); --iTrk;
            continue; // cut this track
          }
          if(m_tracks->at(iTrk)->pt() < 1e3 ) {
            m_tracks->erase(m_tracks->begin()+iTrk); --iTrk;
            continue; // 1GeV cut on track
          }

        }

        m_KTermHist->execute(m_jets,m_eventInfo,m_tracks,m_recoilTLV); // execute the KTermHists
      }
    }//if m_makeKTerm

    //If doing trigger studies
    if( m_emulate_trigger )
      matchTriggerJets();

    ANA_MSG_DEBUG("Begin TTree output for " << m_sysName.at(iSys) );
    ///////////////// Optional MiniTree Output for Nominal Only //////////////////////////
    if( m_writeTree ) {
      if(!m_writeNominalTree ||  m_NominalIndex == (int) iSys) {
      //The following is a bit slow!
        //std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > originalSignalJetsSC = xAOD::shallowCopyContainer( *inJets );
        xAOD::JetContainer* plottingJets = new xAOD::JetContainer();
        xAOD::JetAuxContainer* plottingJetsAux = new xAOD::JetAuxContainer();
        plottingJets->setStore( plottingJetsAux );
        for(unsigned int iJet=0; iJet < m_jets->size(); ++iJet){
          xAOD::Jet* newJet = new xAOD::Jet();
          newJet->makePrivateStore( *(m_jets->at(iJet)) );
          plottingJets->push_back( newJet );
        }

        int iTree = iSys;
        if( m_writeNominalTree)
          iTree = 0;
        if(m_eventInfo)   m_treeList.at(iTree)->FillEvent( m_eventInfo    );
        if(m_jets)  m_treeList.at(iTree)->FillJets(  plottingJets);
        if( m_mode == GJET )
          m_treeList.at(iTree)->FillPhotons( inPhotons );
        else if( m_mode == ZEEJET )
          m_treeList.at(iTree)->FillElectrons( inElectrons, vertices->at(m_pvLocation) );
        else if( m_mode == ZMMJET )
          m_treeList.at(iTree)->FillMuons( inMuons, vertices->at(m_pvLocation) );
        m_treeList.at(iTree)->FillTrigger( m_eventInfo );
        m_treeList.at(iTree)->Fill();

        delete plottingJets;
        delete plottingJetsAux;
      }//If it's not m_writeNominalTree or else we're on the nominal sample
    }//if m_writeTree


    /////////////////////////////////////// SystTool ////////////////////////////////////////
    if( m_bootstrap ){
      systTool->fillSyst(m_sysName.at(iSys), m_eventInfo->runNumber(), m_eventInfo->eventNumber(), m_recoilTLV.Pt()/1e3, (m_jets->at(0)->pt()/m_recoilTLV.Pt()), m_eventInfo->auxdecor< float >("weight") );
    }

  } // loop over systematic variations of recoil objects

  delete jetsSC.first; delete jetsSC.second; delete m_jets; delete savedJets; m_recoilParticles.clear();
  delete jetsSCrecoil.first; delete jetsSCrecoil.second; delete jets; delete jetsRecoil;

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode InsituBalanceAlgo :: postExecute ()
{
  ANA_MSG_VERBOSE("postExecute()");
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode InsituBalanceAlgo :: finalize ()
{
  ANA_MSG_INFO("finalize()");
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  for(unsigned int iVar=0; iVar < m_sysName.size(); ++iVar){
    m_jetHists.at(iVar)->finalize();
  }

  if (m_makeKTerm)
    m_KTermHist->finalize();

  ANA_MSG_INFO("Done with jes hists");

  if( m_bootstrap ){
    systTool->writeToFile(wk()->getOutputFile("SystToolOutput"));
    //delete systTool;  systTool = nullptr;
  }


  //Need to retroactively fill original bins of these histograms
  if(m_useCutFlow) {
    TFile *file = wk()->getOutputFile ("cutflow");
    TH1D* origCutflowHist = (TH1D*)file->Get("cutflow");
    TH1D* origCutflowHistW = (TH1D*)file->Get("cutflow_weighted");

    for(unsigned int iVar=0; iVar < m_sysName.size(); ++iVar){
      for(unsigned int iBin=1; iBin < m_cutflowFirst; ++iBin){
        m_cutflowHist.at(iVar)->SetBinContent(iBin, origCutflowHist->GetBinContent(iBin) );
        m_cutflowHistW.at(iVar)->SetBinContent(iBin, origCutflowHistW->GetBinContent(iBin) );
      }//for iBin
    }//for each m_sysName

    //Add one cutflow histogram to output for number of initial events
    std::string thisName;
    if(m_isMC)
      m_ss << m_mcChannelNumber;
    else
      m_ss << m_runNumber;

    // Get Nominal Cutflow if it's available
    TH1D *histCutflow, *histCutflowW;
    if( m_NominalIndex >= 0){
      histCutflow = (TH1D*) m_cutflowHist.at(m_NominalIndex)->Clone();
      histCutflowW = (TH1D*) m_cutflowHistW.at(m_NominalIndex)->Clone();
    } else {
      histCutflow = (TH1D*) m_cutflowHist.at(0)->Clone();
      histCutflowW = (TH1D*) m_cutflowHistW.at(0)->Clone();
    }
    histCutflow->SetName( ("cutflow_"+m_ss.str()).c_str() );
    histCutflowW->SetName( ("cutflow_weighted_"+m_ss.str()).c_str() );

    wk()->addOutput(histCutflow);
    wk()->addOutput(histCutflowW);
  }//m_useCutFlow
  // Get MetaData_EventCount histogram and add to hist file
  TFile* metaDataFile = wk()->getOutputFile( "metadata" );
  TH1D* metaDataHist = (TH1D*) metaDataFile->Get("MetaData_EventCount");
  wk()->addOutput(metaDataHist);

  ANA_MSG_INFO("Done with cutflow hists");
  //Only if Nominal is available
  if( m_writeTree && m_NominalIndex >= 0) {
    TH1D *treeCutflow, *treeCutflowW;
    if( m_NominalIndex >= 0){
      treeCutflow = (TH1D*) m_cutflowHist.at(m_NominalIndex)->Clone();
      treeCutflowW = (TH1D*) m_cutflowHistW.at(m_NominalIndex)->Clone();
    }else{
      treeCutflow = (TH1D*) m_cutflowHist.at(m_NominalIndex)->Clone();
      treeCutflowW = (TH1D*) m_cutflowHistW.at(m_NominalIndex)->Clone();
    }

    treeCutflow->SetName( ("cutflow_"+m_ss.str()).c_str() );
    treeCutflowW->SetName( ("cutflow_weighted_"+m_ss.str()).c_str() );

    TFile * treeFile = wk()->getOutputFile ("tree");
    treeCutflow->SetDirectory( treeFile );
    treeCutflowW->SetDirectory( treeFile );

    TH1D* thisMetaDataHist = (TH1D*) metaDataHist->Clone();
    thisMetaDataHist->SetDirectory( treeFile );

  }

  m_ss.str( std::string() );
  ANA_MSG_INFO("Done finalize");

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode InsituBalanceAlgo :: histFinalize ()
{
  ANA_MSG_INFO("histFinalize()");
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.


  ANA_CHECK( xAH::Algorithm::algFinalize());
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode InsituBalanceAlgo::fillCutflow(int iSel, int iVar){
  if(m_useCutFlow) {
    ANA_MSG_DEBUG("fillCutflow(): passing cut " << iSel << " of selection " << iVar);
    m_cutflowHist.at(iVar)->Fill(iSel+m_cutflowFirst, 1);
    m_cutflowHistW.at(iVar)->Fill(iSel+m_cutflowFirst, m_mcEventWeight);
  }

return EL::StatusCode::SUCCESS;
}


EL::StatusCode InsituBalanceAlgo::fillCutflowAll(int iSel){
  if(m_useCutFlow) {
    ANA_MSG_DEBUG("fillCutflowAll(): passing cut " << iSel);
    for(unsigned int iVar=0; iVar < m_sysName.size(); ++iVar){
      m_cutflowHist.at(iVar)->Fill(iSel+m_cutflowFirst, 1);
      m_cutflowHistW.at(iVar)->Fill(iSel+m_cutflowFirst, m_mcEventWeight);
    }
    m_iCutflow++;
  }

return EL::StatusCode::SUCCESS;
}

//This grabs luminosity, acceptace, and eventNumber information from the respective text file
EL::StatusCode InsituBalanceAlgo::getSampleWeights(const xAOD::EventInfo* eventInfo) {
  ANA_MSG_INFO("getSampleWeights()");

  float weight_xs = 1.0, weight_kfactor = 1.0, weight_eff = 1.0;
  if(!m_isMC){
    m_runNumber = eventInfo->runNumber();
  }else{
    m_mcChannelNumber = eventInfo->mcChannelNumber();
    ifstream fileIn(  PathResolverFindCalibFile( m_XSFile.c_str() ) );

    // Search the input file for the correct MCChannelNumber, and find the XS and kfactor numbers
    std::string mcChanStr = std::to_string( m_mcChannelNumber );
    bool foundXS = false;
    std::string line;
    std::string subStr;
    while (getline(fileIn, line)){
      istringstream iss(line);
      // DSID should be first item in the line, so put it into subStr and see if mcChanStr matches
      iss >> subStr;
      if (subStr.find(mcChanStr) != std::string::npos){
        iss >> subStr; // Get Name
        iss >> subStr; // Get XS (ipb)
        sscanf(subStr.c_str(), "%e", &weight_xs);
        iss >> subStr; // Get kfactor
        sscanf(subStr.c_str(), "%e", &weight_kfactor);
        iss >> subStr; // Get Efficiency
        sscanf(subStr.c_str(), "%e", &weight_eff);
        //iss >> subStr; // Get relative uncertainty
        Info("getSampleWeights", "Setting xs=%f, efficiency=%f, acceptance=%f", weight_xs, weight_eff, weight_kfactor);
        foundXS = true;
        break;
      }
    }
    if( !foundXS){
      cerr << "ERROR: Could not find proper file information for MC sample " << mcChanStr << endl;
      return EL::StatusCode::FAILURE;
    }

    // Decorate eventInfo with information //
    m_weight_xs = weight_xs*weight_eff;
    m_weight_kfactor = weight_kfactor;
  }

  return EL::StatusCode::SUCCESS;
}

//Calculate DeltaPhi
double InsituBalanceAlgo::DeltaPhi(double phi1, double phi2){
  phi1=TVector2::Phi_0_2pi(phi1);
  phi2=TVector2::Phi_0_2pi(phi2);
  return fabs(TVector2::Phi_mpi_pi(phi1-phi2));
}

//Calculate DeltaR
double InsituBalanceAlgo::DeltaR(double eta1, double phi1,double eta2, double phi2){
  phi1=TVector2::Phi_0_2pi(phi1);
  phi2=TVector2::Phi_0_2pi(phi2);
  double dphi = DeltaPhi( phi1, phi2);
  double deta = eta1-eta2;
  return sqrt(deta*deta+dphi*dphi);
}

EL::StatusCode InsituBalanceAlgo :: loadSystematics (){
  ANA_MSG_INFO("loadSystematics() on systematics set " << m_sysVariations);

  // Define GRID submission set of systematics
  if( m_sysVariations.find("GRID") != std::string::npos){
    if(m_isMC || m_mode != MJB){
        Error( "loadSystematics()", "GRID systematics is only for data in the MJB.");
    }

    if( m_sysVariations.find("GRID1") != std::string::npos ){
      m_sysVariations = "Nominal-JGROUP1";
    }else if( m_sysVariations.find("GRID2") != std::string::npos ){
      m_sysVariations = "Nominal-JGROUP2";
    }else if( m_sysVariations.find("GRID3") != std::string::npos ){
      m_sysVariations = "Nominal-JGROUP3";
    }else if( m_sysVariations.find("GRID4") != std::string::npos ){
      m_sysVariations = "Nominal-JGROUP4";
    }
  }

  // Define the All systematic //
  if( m_sysVariations.find("All") != std::string::npos){
    if(m_isMC){
      m_sysVariations = "Nominal-EvSel-JVT";
      if (m_mode != MJB)
        m_sysVariations += "-CP";
    }else{
      m_sysVariations = "Nominal-EvSel-JVT";
      if (m_mode == MJB)
        m_sysVariations += "-JES";
    }
  }
  ANA_MSG_INFO("Systematics set transformed into: " << m_sysVariations);

  m_NominalIndex = -1; //The index of the nominal

  // Turn into a vector of all systematics names
  std::vector< std::string> varVector;
  size_t pos = 0;
  std::string tmpSyst = m_sysVariations;
  while ((pos = tmpSyst.find("-")) != std::string::npos){
    varVector.push_back( tmpSyst.substr(0, pos) );
    tmpSyst.erase(0, pos+1);
  }
  varVector.push_back( tmpSyst ); //append final one

  for( unsigned int iVar = 0; iVar < varVector.size(); ++iVar ){

    /////////////////////////////// Nominal ///////////////////////////////
    if( varVector.at(iVar).compare("Nominal") == 0 ){
      m_sysName.push_back( "Nominal" ); m_sysType.push_back( NOMINAL ); m_sysDetail.push_back( -1 );
      m_sysSet.push_back( CP::SystematicSet() ); m_sysSet.back().insert( CP::SystematicVariation("") );
      m_NominalIndex = m_sysName.size()-1;

    }else if( varVector.at(iVar).compare("MCType") == 0 ){
      m_sysName.push_back( "MCType" ); m_sysType.push_back( NOMINAL ); m_sysDetail.push_back( -1 );
      m_sysSet.push_back( CP::SystematicSet() ); m_sysSet.back().insert( CP::SystematicVariation("") );
      m_NominalIndex = m_sysName.size()-1;

    /////////////////// Every Jet Calibration Stage ////////////////
    }else if( varVector.at(iVar).compare("JCS") == 0 ){
      if( m_JCSTokens.size() <= 0){
        Error( "loadSystematics()", "JetCalibSequence is empty.  This will not be added to the systematics");
      }
      Info( "loadSystematics()", "Adding JetCalibSequence");
      for( unsigned int iJCS = 0; iJCS < m_JCSTokens.size(); ++iJCS){
        //Name - JetCalibTool - Variation Number - sign
        m_sysName.push_back("JCS_"+m_JCSTokens.at(iJCS) ); m_sysType.push_back( JCS ); m_sysDetail.push_back( iJCS );
        m_sysSet.push_back( CP::SystematicSet() ); m_sysSet.back().insert( CP::SystematicVariation("") );
      }

    ////////////////////// Special set of JES for GRID submission /////////////////////////////
    } else if( varVector.at(iVar).find("JGROUP") != std::string::npos ){
      const CP::SystematicSet recSysts = m_JetUncertaintiesTool_handle->recommendedSystematics();
      std::vector<CP::SystematicSet> JESSysList = HelperFunctions::getListofSystematics( recSysts, "All", 1, msg() ); //All sys at +-1 sigma

      string iGroup_str = varVector.at(iVar);
      iGroup_str.erase(0,6);
      int iGroup = std::stoi(iGroup_str);
      int this_JES_start = ( ((iGroup-1)*(JESSysList.size()+3)/4)-3 );
      if( this_JES_start <= 0 )
        this_JES_start = 1;
      unsigned int this_JES_end   = ( (iGroup*(JESSysList.size()+3)/4)-3 );
      std::cout << "iGroup " << iGroup << " start " << this_JES_start << " and end " << this_JES_end << std::endl;
      for(unsigned int i=this_JES_start; i < this_JES_end; ++i){
        m_sysName.push_back( JESSysList.at(i).name() );   m_sysType.push_back( JES ); m_sysDetail.push_back( i ); m_sysSet.push_back( JESSysList.at(i) );
      }

    ////////////////////////////////// JES Uncertainties /////////////////////////////////////////
    } else if( varVector.at(iVar).find("JES") != std::string::npos ){
      const CP::SystematicSet recSysts = m_JetUncertaintiesTool_handle->recommendedSystematics();
      std::vector<CP::SystematicSet> JESSysList = HelperFunctions::getListofSystematics( recSysts, "All", 1, msg() ); //All sys at +-1 sigma
      for(unsigned int i=1; i < JESSysList.size(); ++i){
        m_sysName.push_back( JESSysList.at(i).name() );   m_sysType.push_back( JES ); m_sysDetail.push_back( i ); m_sysSet.push_back( JESSysList.at(i) );
      }


    ////////// Special per-jet flavor uncertainties ////////////////
    } else if( varVector.at(iVar).find("Flavor") != std::string::npos ){
      const CP::SystematicSet recSysts = m_JetUncertaintiesTool_handle->recommendedSystematics();
      std::vector<CP::SystematicSet> JESSysList = HelperFunctions::getListofSystematics( recSysts, "All", 1, msg() ); //All sys at +-1 sigma
      for(unsigned int i=1; i < JESSysList.size(); ++i){

        //Special uncertainty that will only apply to a single jet at a time (first 3 subleading jets)
        size_t pos_flavor_string = JESSysList.at(i).name().find("Flavor_Composition");
        if( pos_flavor_string != std::string::npos ){
          for( unsigned int iJet=1; iJet < 4; ++iJet ){
            std::string newName = JESSysList.at(i).name().replace(pos_flavor_string, std::string("Flavor_Composition").length(), ("Flavor_Composition_jet"+std::to_string(iJet)));
            m_sysName.push_back( newName );   m_sysType.push_back( PERJETFLAVOR ); m_sysDetail.push_back( iJet ); m_sysSet.push_back( JESSysList.at(i) );
          }//For first 3 subleading jets
        }// If this is the JES Flavor_Composition Uncertainty
      }

    } else if( varVector.at(iVar).find("JVT") != std::string::npos ){
      m_sysName.push_back( "JVT__1down" );   m_sysType.push_back( JVT ); m_sysDetail.push_back( -1 );
      m_sysSet.push_back( CP::SystematicSet() ); m_sysSet.back().insert( CP::SystematicVariation("") );

      m_sysName.push_back( "JVT__1up" );   m_sysType.push_back( JVT ); m_sysDetail.push_back( 1 );
      m_sysSet.push_back( CP::SystematicSet() ); m_sysSet.back().insert( CP::SystematicVariation("") );

    //////////////////////////////////////// Event Selection  /////////////////////////////////////////
    } else if( varVector.at(iVar).compare("EvSel") == 0 ){
      //Name - EvSel Variation - EvSel Value - sign

      //Alpha systematics are +-.1  (*100)
      m_sysName.push_back("EvSel_Alpha"+to_string(int(round(m_alpha*100))-10)+"__1down" );   m_sysType.push_back( CUTAlpha ); m_sysDetail.push_back( -0.1 );
      m_sysSet.push_back( CP::SystematicSet() ); m_sysSet.back().insert( CP::SystematicVariation("") );
      m_sysName.push_back("EvSel_Alpha"+to_string(int(round(m_alpha*100))+10)+"__1up" );   m_sysType.push_back( CUTAlpha ); m_sysDetail.push_back( 0.1 );
      m_sysSet.push_back( CP::SystematicSet() ); m_sysSet.back().insert( CP::SystematicVariation("") );

      //Beta systematics are +-.5 (*10)
      if( m_beta > -1 ){
        m_sysName.push_back("EvSel_Beta"+to_string(int(round(m_beta*10))-5)+"__1down" );   m_sysType.push_back( CUTBeta ); m_sysDetail.push_back( -0.5 );
        m_sysSet.push_back( CP::SystematicSet() ); m_sysSet.back().insert( CP::SystematicVariation("") );
        m_sysName.push_back("EvSel_Beta"+to_string(int(round(m_beta*10))+5)+"__1up" );     m_sysType.push_back( CUTBeta ); m_sysDetail.push_back( 0.5 );
        m_sysSet.push_back( CP::SystematicSet() ); m_sysSet.back().insert( CP::SystematicVariation("") );
      }

      //pt Asymmetry systematics are +-.1 (*100)
      if( m_mode == MJB ){
        m_sysName.push_back("EvSel_Asym"+to_string(int(round(m_ptAsymVar*100))-10)+"__1down" );   m_sysType.push_back( CUTAsym ); m_sysDetail.push_back( -0.1 );
        m_sysSet.push_back( CP::SystematicSet() ); m_sysSet.back().insert( CP::SystematicVariation("") );
        m_sysName.push_back("EvSel_Asym"+to_string(int(round(m_ptAsymVar*100))+10)+"__1up" );     m_sysType.push_back( CUTAsym ); m_sysDetail.push_back( 0.1 );
        m_sysSet.push_back( CP::SystematicSet() ); m_sysSet.back().insert( CP::SystematicVariation("") );
      } else {
        m_sysName.push_back("EvSel_Asym"+to_string(int(round(m_ptAsymVar*100))-5)+"__1down" );   m_sysType.push_back( CUTAsym ); m_sysDetail.push_back( -0.05 );
        m_sysSet.push_back( CP::SystematicSet() ); m_sysSet.back().insert( CP::SystematicVariation("") );
        m_sysName.push_back("EvSel_Asym"+to_string(int(round(m_ptAsymVar*100))+5)+"__1up" );     m_sysType.push_back( CUTAsym ); m_sysDetail.push_back( 0.05 );
        m_sysSet.push_back( CP::SystematicSet() ); m_sysSet.back().insert( CP::SystematicVariation("") );
      }

      //pt threshold systematics are +- 5
      if( m_mode == MJB ){
        m_sysName.push_back("EvSel_Threshold"+to_string(int(round(m_ptThresh))-5)+"__1down" );   m_sysType.push_back( CUTPt ); m_sysDetail.push_back( -5 );
        m_sysSet.push_back( CP::SystematicSet() ); m_sysSet.back().insert( CP::SystematicVariation("") );
        m_sysName.push_back("EvSel_Threshold"+to_string(int(round(m_ptThresh))+5)+"__1up" );     m_sysType.push_back( CUTPt ); m_sysDetail.push_back( 5 );
        m_sysSet.push_back( CP::SystematicSet() ); m_sysSet.back().insert( CP::SystematicVariation("") );
      }

    } else if( varVector.at(iVar).compare("CP") == 0 ){
      std::vector<std::string>* sysNames(nullptr);
      m_store->print();
      ANA_CHECK( HelperFunctions::retrieve(sysNames, m_CPSystNames, 0, m_store, msg()) );
      for(unsigned int i=1; i < sysNames->size(); ++i){
        m_sysName.push_back( sysNames->at(i) );   m_sysType.push_back( CP ); m_sysDetail.push_back( i );
        m_sysSet.push_back( CP::SystematicSet() ); m_sysSet.back().insert( CP::SystematicVariation("") );
      }
    }

  }//for varVector
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode InsituBalanceAlgo :: setupJetCalibrationStages() {

  ANA_MSG_INFO("setupJetCalibrationStages()");
  // Setup calibration stages tools //
  // Create a map from the CalibSequence string components to the xAOD aux data
  std::map <std::string, std::string> JCSMap;
  if (m_inContainerName_jets.find("EMTopo") != std::string::npos || m_inContainerName_jets.find("EMPFlow") != std::string::npos)
    JCSMap["RAW"] = "JetEMScaleMomentum";
  else if( m_inContainerName_jets.find("LCTopo") != std::string::npos )
    JCSMap["RAW"] = "JetConstitScaleMomentum";
  else{
    Error( "setupJetCalibrationStages()", " Input jets are not EMScale, EMPFlow or LCTopo.  Exiting.");
    return EL::StatusCode::FAILURE;
  }
  JCSMap["JetArea"] = "JetPileupScaleMomentum";
  JCSMap["Origin"] = "JetOriginConstitScaleMomentum";
  JCSMap["EtaJES"] = "JetEtaJESScaleMomentum";
  JCSMap["GSC"] = "JetGSCScaleMomentum";
  JCSMap["Insitu"] = "JetInsituScaleMomentum";


  //// Now break up the Jet Calib string into the components
  // m_JCSTokens will be a vector of fields in the m_jetCalibSequence
  // m_JCSStrings will be a vector of the Jet momentum States
  size_t pos = 0;
  std::string JCSstring = m_jetCalibSequence;
  std::string token;
  m_JCSTokens.push_back( "RAW" );  //The original xAOD value
  m_JCSStrings.push_back( JCSMap["RAW"] );
  while( JCSstring.size() > 0){
    pos = JCSstring.find("_");
    if (pos != std::string::npos){
      token = JCSstring.substr(0, pos);
      JCSstring.erase(0, pos+1);
    }else{
      token = JCSstring;
      JCSstring.erase();
    }
    //Skip the Residual one, it seems JetArea_Residual == Pileup
    if (token.find("Residual") == std::string::npos){
      m_JCSTokens.push_back( token );
      m_JCSStrings.push_back( JCSMap[token] );
    }
  }

  return EL::StatusCode::SUCCESS;
}

// initialize and configure B-tagging efficiency tools
EL::StatusCode InsituBalanceAlgo :: loadBTagTools(){

  if (! m_bTag){
    return EL::StatusCode::SUCCESS;
  }

  for(unsigned int iB=0; iB < m_bTagWPs.size(); ++iB){
    std::string thisBTagOP = "FixedCutBEff_"+m_bTagWPs.at(iB);

    // Initialize & Configure the BJetSelectionTool
    asg::AnaToolHandle<IBTaggingSelectionTool> this_BTaggingSelectionTool_handle{"BTaggingSelectionTool_"+m_bTagWPs.at(iB)};
    ANA_CHECK( ASG_MAKE_ANA_TOOL( this_BTaggingSelectionTool_handle, BTaggingSelectionTool ) );
    ANA_CHECK( this_BTaggingSelectionTool_handle.setProperty("MaxEta",2.5) );
    ANA_CHECK( this_BTaggingSelectionTool_handle.setProperty("MinPt",20000.) );
    ANA_CHECK( this_BTaggingSelectionTool_handle.setProperty("FlvTagCutDefinitionsFileName",m_bTagFileName.c_str()) );
    ANA_CHECK( this_BTaggingSelectionTool_handle.setProperty("TaggerName",          m_bTagVar) );
    ANA_CHECK( this_BTaggingSelectionTool_handle.setProperty("OperatingPoint",      thisBTagOP) );
    ANA_CHECK( this_BTaggingSelectionTool_handle.setProperty("JetAuthor",           (m_jetDef+"Jets").c_str()) );
    ANA_CHECK( this_BTaggingSelectionTool_handle.retrieve() );
    ANA_MSG_INFO("loadBTagTools(): bTaggingSelectionTool initialized : " << this_BTaggingSelectionTool_handle.name() );

    m_AllBTaggingSelectionTool_handles.push_back( this_BTaggingSelectionTool_handle );

    // Initialize & Configure the BJetEfficiencyCorrectionTool
    if( m_isMC) {
      asg::AnaToolHandle<IBTaggingEfficiencyTool> this_BTaggingEfficiencyTool_handle{"BTaggingEfficiencyTool/BTaggingEfficiencyTool_"+m_bTagWPs.at(iB)};
      ANA_CHECK( ASG_MAKE_ANA_TOOL( this_BTaggingEfficiencyTool_handle, BTaggingEfficiencyTool ) );
      ANA_CHECK( this_BTaggingEfficiencyTool_handle.setProperty("TaggerName",          m_bTagVar) );
      ANA_CHECK( this_BTaggingEfficiencyTool_handle.setProperty("OperatingPoint",      thisBTagOP) );
      ANA_CHECK( this_BTaggingEfficiencyTool_handle.setProperty("JetAuthor",           (m_jetDef+"Jets").c_str()) );
      ANA_CHECK( this_BTaggingEfficiencyTool_handle.setProperty("ScaleFactorFileName", m_bTagFileName.c_str()) );
      ANA_CHECK( this_BTaggingEfficiencyTool_handle.setProperty("UseDevelopmentFile",  m_useDevelopmentFile) );
      ANA_CHECK( this_BTaggingEfficiencyTool_handle.setProperty("ConeFlavourLabel",    m_useConeFlavourLabel) );
      ANA_CHECK( this_BTaggingEfficiencyTool_handle.retrieve() );
      ANA_MSG_INFO("loadBTagTools(): bTaggingEfficiencyTool initialized : " << this_BTaggingEfficiencyTool_handle.name() );

      m_AllBTaggingEfficiencyTool_handles.push_back( this_BTaggingEfficiencyTool_handle );
    }
  }//for iB working points

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode InsituBalanceAlgo :: matchTriggerJets() {

  // Grab highest pt L1 and HLT jet //

  //---- Grab L1 and HLT trigger jets ----//
  // L1 Jet
  const xAOD::JetRoIContainer * L1JetContainer = 0;
  ANA_CHECK( HelperFunctions::retrieve(L1JetContainer, "LVL1JetRoIs", m_event, m_store, msg()) );
  float max_L1_jet_pt = 0;
  for( auto thisJet : *(L1JetContainer) ) {
    max_L1_jet_pt = fmax( max_L1_jet_pt, thisJet->et8x8());
  }
  m_eventInfo->auxdecor< float >("max_L1_jet_pt") = max_L1_jet_pt/1e3;

  // HLT Jet
  const xAOD::JetContainer * HLTJetContainer = 0;
  ANA_CHECK( HelperFunctions::retrieve(HLTJetContainer, m_HLT_trigger_jet_container.c_str(), m_event, m_store, msg()) );
  float max_HLT_jet_pt = 0;
  for( auto thisJet : *(HLTJetContainer) ) {
    max_HLT_jet_pt = fmax( max_HLT_jet_pt, thisJet->pt());
  }
  m_eventInfo->auxdecor< float >("max_HLT_jet_pt") = max_HLT_jet_pt/1e3;
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode InsituBalanceAlgo :: matchTrackJets(std::vector< xAOD::Jet* >* jets) {

  const xAOD::JetContainer* track_jets = 0;
  ANA_CHECK( HelperFunctions::retrieve(track_jets, "AntiKt4PV0TrackJets", m_event, m_store, msg()) );

  float dR_trackjet = 0.3; //dR requirement for matching track jets

  //loop over reco jets
  for( auto jet : *jets ){
    const xAOD::Jet* matchedTrackJet = nullptr;

    for( auto thisTrackJet : *track_jets ){

      if(! matchedTrackJet )
        matchedTrackJet = thisTrackJet;
      else if( jet->p4().DeltaR( thisTrackJet->p4() ) < jet->p4().DeltaR( matchedTrackJet->p4() ) )
        matchedTrackJet = thisTrackJet;

    }// for track jet

    // track matching dR requirement //
    ANA_MSG_DEBUG("Check dR track jet matching requirement");
    if( jet->p4().DeltaR( matchedTrackJet->p4() ) > dR_trackjet){
      ANA_MSG_DEBUG("No matched track jet");
      jet->auxdecor< float >("jet_track_pt")     = -999.*1e3;
      jet->auxdecor< float >("jet_track_eta")    = -999.;
      jet->auxdecor< float >("jet_track_phi")    = -999.;
      jet->auxdecor< float >("jet_track_e")      = -999.*1e3;
      jet->auxdecor< float >("jet_track_respPt") = -999.;
      jet->auxdecor< float >("jet_track_respE")  = -999.;
    } else {
      ANA_MSG_DEBUG("Matched a track jet");
      jet->auxdecor< float >("jet_track_pt")     = matchedTrackJet->pt();
      jet->auxdecor< float >("jet_track_eta")    = matchedTrackJet->eta();
      jet->auxdecor< float >("jet_track_phi")    = matchedTrackJet->phi();
      jet->auxdecor< float >("jet_track_e")      = matchedTrackJet->e();
      jet->auxdecor< float >("jet_track_respPt") = jet->pt()/matchedTrackJet->pt();
      jet->auxdecor< float >("jet_track_respE")  = jet->e()/matchedTrackJet->e();
    }

  }//for reco jet

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode InsituBalanceAlgo :: matchTruthJets(std::vector< xAOD::Jet* >* jets) {

  const xAOD::JetContainer* truth_jets = 0;
  ANA_CHECK( HelperFunctions::retrieve(truth_jets, "AntiKt4TruthJets", m_event, m_store, msg()) );

  //loop over reco jets
  for( auto jet : *jets ){
    const xAOD::Jet* matchedTruthJet = nullptr;

    // If not associated truth jet
    if( !jet->isAvailable< ElementLink<xAOD::JetContainer> >("GhostTruthAssociationLink") ||
        !jet->auxdata< ElementLink<xAOD::JetContainer> >("GhostTruthAssociationLink").isValid() ){
      ANA_MSG_DEBUG("No matched truth jet");
      jet->auxdecor< float >("jet_truth_pt")     = -999.*1e3;
      jet->auxdecor< float >("jet_truth_eta")    = -999.;
      jet->auxdecor< float >("jet_truth_phi")    = -999.;
      jet->auxdecor< float >("jet_truth_e")      = -999.*1e3;
      jet->auxdecor< float >("jet_truth_respPt") = -999.;
      jet->auxdecor< float >("jet_truth_respE")  = -999.;
    } else {
      ANA_MSG_DEBUG("Matched truth jet");
      matchedTruthJet = *jet->auxdata< ElementLink<xAOD::JetContainer> >("GhostTruthAssociationLink");
      //ghostFraction = jet->auxdata<float>("GhostTruthAssociationFraction");
      jet->auxdecor< float >("jet_truth_pt")     = matchedTruthJet->pt();
      jet->auxdecor< float >("jet_truth_eta")    = matchedTruthJet->eta();
      jet->auxdecor< float >("jet_truth_phi")    = matchedTruthJet->phi();
      jet->auxdecor< float >("jet_truth_e")      = matchedTruthJet->e();
      jet->auxdecor< float >("jet_truth_respPt") = jet->pt()/matchedTruthJet->pt();
      jet->auxdecor< float >("jet_truth_respE")  = jet->e()/matchedTruthJet->e();
    }

  }//for reco jet

  return EL::StatusCode::SUCCESS;
}
