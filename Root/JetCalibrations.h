/// This file holds the InsituBalance framework's functions for calibrating jets and calculating their systematics

// initialize and configure the JVT correction tool
EL::StatusCode InsituBalanceAlgo :: loadJVTTool( std::string jetDefinition ){
  ANA_MSG_INFO("loadJVTTool()");

  // Get correction JVT SF file name for this jet type
  std::string SFFile;
  if(m_jetDef.find("PFlow") != std::string::npos){
    ANA_MSG_INFO("Using PFlow JVT file");
    SFFile = "JetJvtEfficiency/Moriond2018/JvtSFFile_EMPFlow.root";
    m_isPFlow = true;
  }else if(m_jetDef.find("LC") != std::string::npos){
    ANA_MSG_INFO("Using LC JVT file");
    SFFile = "JetJvtEfficiency/Moriond2018/JvtSFFile_LC.root";
  }else if(m_jetDef.find("EMTopo") != std::string::npos){
    ANA_MSG_INFO("Using EMTopoJets JVT file");
    SFFile = "JetJvtEfficiency/Moriond2018/JvtSFFile_EMTopoJets.root";
  }
  
  //// Set up tagger tool to update JVT value after calibration
  ANA_CHECK( ASG_MAKE_ANA_TOOL(m_JVTUpdateTool_handle, JetVertexTaggerTool) );
  //ANA_CHECK( m_JVTUpdateTool_handle.setProperty("JVTFileName","JetMomentTools/JVTlikelihood_20140805.root") );
  ANA_CHECK( m_JVTUpdateTool_handle.setProperty("JVFCorrName", m_JVTVar) );
  ANA_CHECK( m_JVTUpdateTool_handle.setProperty("OutputLevel", msg().level() ) );
  ANA_CHECK( m_JVTUpdateTool_handle.retrieve() );

  ANA_CHECK( ASG_MAKE_ANA_TOOL(m_JetJVTEfficiencyTool_handle, CP::JetJvtEfficiency) );
  ANA_CHECK( m_JetJVTEfficiencyTool_handle.setProperty("WorkingPoint", m_JVTWP) );
  ANA_CHECK( m_JetJVTEfficiencyTool_handle.setProperty("SFFile", SFFile) );
  ANA_CHECK( m_JetJVTEfficiencyTool_handle.setProperty("OutputLevel", msg().level() ) );
  ANA_CHECK( m_JetJVTEfficiencyTool_handle.retrieve() );

  //// For Tight and Loose systematic variations, create a new tool handle
  if(m_sysVariations.find("JVT") != std::string::npos){
    ANA_CHECK( ASG_MAKE_ANA_TOOL(m_JetJVTEfficiencyTool_handle_up, CP::JetJvtEfficiency) );
    ANA_CHECK( m_JetJVTEfficiencyTool_handle_up.setProperty("WorkingPoint", "Tight") );
    ANA_CHECK( m_JetJVTEfficiencyTool_handle_up.setProperty("SFFile", SFFile) );
    ANA_CHECK( m_JetJVTEfficiencyTool_handle_up.setProperty("OutputLevel", msg().level() ) );
    ANA_CHECK( m_JetJVTEfficiencyTool_handle_up.retrieve() );

    ANA_CHECK( ASG_MAKE_ANA_TOOL(m_JetJVTEfficiencyTool_handle_down, CP::JetJvtEfficiency) );
    //No loose WP for PFlow, so just set to nominal value
    if(!m_isPFlow){
      ANA_CHECK( m_JetJVTEfficiencyTool_handle_down.setProperty("WorkingPoint", m_JVTWP) );
    }
    ANA_CHECK( m_JetJVTEfficiencyTool_handle_down.setProperty("SFFile", SFFile) );
    ANA_CHECK( m_JetJVTEfficiencyTool_handle_down.setProperty("OutputLevel", msg().level() ) );
    ANA_CHECK( m_JetJVTEfficiencyTool_handle_down.retrieve() );
  }


  return EL::StatusCode::SUCCESS;
}

// initialize and configure the JetCalibTool
EL::StatusCode InsituBalanceAlgo :: loadJetCalibrationTool(){

  ///// Jet calib and uncertainty Tool Config parameters /////
  ANA_MSG_INFO("loadJetCalibrationTool()");

  //Configure defaults if none is given
  if( m_jetCalibSequence.size() <= 0 ){
    if ( m_jetDef.find("AntiKt4") == std::string::npos ){
      Error("configure()", "No m_jetCalibSequence set in config yet this is not an AntiKt4 jet.  Unsure which sequence to use, exiting.");
      return EL::StatusCode::FAILURE;
    }
    if (m_isMC)
      m_jetCalibSequence = "JetArea_Residual_EtaJES_GSC_Smear";
    else
      m_jetCalibSequence = "JetArea_Residual_EtaJES_GSC_Insitu";
  }

  //Various warnings for non-standard configurations
  if ( !m_isMC && m_jetCalibSequence.find("Insitu") == std::string::npos)
      Warning("configure()", "Running on data WITHOUT _Insitu in jet calibration sequence.  Are you sure you want this?");
  else if( m_isMC && m_jetCalibSequence.find("Insitu") != std::string::npos)
      Warning("configure()", "Running on MC WITH _Insitu in jet calibration sequence.  Are you sure you want this?");
  if ( m_isMC && m_jetCalibSequence.find("Smear") == std::string::npos)
      Warning("configure()", "Running on MC WITHOUT _Smear in jet calibration sequence.  Are you sure you want this?");
  else if( !m_isMC && m_jetCalibSequence.find("Smear") != std::string::npos)
      Warning("configure()", "Running on data WITH _Smear in jet calibration sequence.  Are you sure you want this?");

  //Configure the nominal JetCalibTool
  Info("configure()", "JetCalibTool: %s, %s", m_jetCalibConfig.c_str(), m_jetCalibSequence.c_str() );
  ANA_CHECK( ASG_MAKE_ANA_TOOL(m_JetCalibrationTool_handle, JetCalibrationTool) );
  ANA_CHECK( m_JetCalibrationTool_handle.setProperty("JetCollection", m_jetDef) );
  ANA_CHECK( m_JetCalibrationTool_handle.setProperty("ConfigFile", m_jetCalibConfig) );
  ANA_CHECK( m_JetCalibrationTool_handle.setProperty("CalibSequence", m_jetCalibSequence) );
  ANA_CHECK( m_JetCalibrationTool_handle.setProperty("IsData", !m_isMC) );
  if( m_CalibArea.size() > 0 )
    ANA_CHECK( m_JetCalibrationTool_handle.setProperty("CalibArea", m_CalibArea) );
  ANA_CHECK( m_JetCalibrationTool_handle.setProperty("OutputLevel", msg().level()) );
  ANA_CHECK( m_JetCalibrationTool_handle.retrieve() );

  //The 2nd calibration tool is only for MJB
  if( m_mode != MJB )
    return EL::StatusCode::SUCCESS;

  //If lead jet calibration info not provided, use info for other jets
  if( m_jetDef_recoilJets.empty() ){
    Info("configure()", "No leading jet calibration specified, defaulting to regular jet calibration");
    m_jetDef_recoilJets = m_jetDef;
    m_jetCalibSequence_recoilJets = m_jetCalibSequence;
    m_jetCalibConfig_recoilJets = m_jetCalibConfig;
  }

  //Configure defaults if none is given
  if( m_jetCalibSequence_recoilJets.size() <= 0 ){
    if ( m_jetDef_recoilJets.find("AntiKt4") == std::string::npos ){
      Error("configure()", "No m_jetCalibSequence_recoilJets set in config yet this is not an AntiKt4 jet.  Unsure which sequence to use, exiting.");
      return EL::StatusCode::FAILURE;
    }
    if (m_isMC)
      m_jetCalibSequence_recoilJets = "JetArea_Residual_EtaJES_GSC_Smear";
    else
      m_jetCalibSequence_recoilJets = "JetArea_Residual_EtaJES_GSC_Insitu";
  }

  //Various warnings for non-standard configurations
  if ( !m_isMC && m_jetCalibSequence_recoilJets.find("Insitu") == std::string::npos)
      Warning("configure()", "Running on data WITHOUT _Insitu in jet calibration sequence.  Are you sure you want this?");
  else if( m_isMC && m_jetCalibSequence_recoilJets.find("Insitu") != std::string::npos)
      Warning("configure()", "Running on MC WITH _Insitu in jet calibration sequence.  Are you sure you want this?");
  if ( m_isMC && m_jetCalibSequence_recoilJets.find("Smear") == std::string::npos)
      Warning("configure()", "Running on MC WITHOUT _Smear in jet calibration sequence.  Are you sure you want this?");
  else if( !m_isMC && m_jetCalibSequence_recoilJets.find("Smear") != std::string::npos)
      Warning("configure()", "Running on data WITH _Smear in jet calibration sequence.  Are you sure you want this?");

  //Configure the recoil JetCalibTool
  Info("configure()", "JetCalibTool: %s, %s", m_jetCalibConfig.c_str(), m_jetCalibSequence.c_str() );
  ANA_CHECK( ASG_MAKE_ANA_TOOL(m_JetCalibrationTool_handle_recoilJets, JetCalibrationTool) );
  ANA_CHECK( m_JetCalibrationTool_handle_recoilJets.setProperty("JetCollection", m_jetDef_recoilJets) );
  ANA_CHECK( m_JetCalibrationTool_handle_recoilJets.setProperty("ConfigFile", m_jetCalibConfig_recoilJets) );
  ANA_CHECK( m_JetCalibrationTool_handle_recoilJets.setProperty("CalibSequence", m_jetCalibSequence_recoilJets) );
  ANA_CHECK( m_JetCalibrationTool_handle_recoilJets.setProperty("IsData", !m_isMC) );
  if( m_CalibArea_recoilJets.size() > 0 )
    ANA_CHECK( m_JetCalibrationTool_handle_recoilJets.setProperty("CalibArea", m_CalibArea_recoilJets) );
  ANA_CHECK( m_JetCalibrationTool_handle_recoilJets.setProperty("OutputLevel", msg().level()) );
  ANA_CHECK( m_JetCalibrationTool_handle_recoilJets.retrieve() );

  return EL::StatusCode::SUCCESS;
}

// initialize and configure the JetUncertainty Tool
EL::StatusCode InsituBalanceAlgo :: loadJetUncertaintyTool(){
  ANA_MSG_INFO("loadJetUncertaintyTool()");

  ANA_CHECK( ASG_MAKE_ANA_TOOL(m_JetUncertaintiesTool_handle, JetUncertaintiesTool) );
  ANA_CHECK( m_JetUncertaintiesTool_handle.setProperty("ConfigFile", m_jetUncertaintyConfig ) );
  if( m_jetUncertainty_CalibArea.size() > 0 )
    ANA_CHECK( m_JetUncertaintiesTool_handle.setProperty("CalibArea", m_jetUncertainty_CalibArea) );

  if(m_largeR.compare("True")!=0){
    ANA_CHECK( m_JetUncertaintiesTool_handle.setProperty("JetDefinition", m_jetDef) );
    if( m_isAFII ){
      ANA_CHECK( m_JetUncertaintiesTool_handle.setProperty("MCType", "AFII") );
    }else{
      ANA_CHECK( m_JetUncertaintiesTool_handle.setProperty("MCType", m_mcType) );
    }
  }else{
    ANA_CHECK( m_JetUncertaintiesTool_handle.setProperty("JetDefinition", m_jetDef_recoilJets) );
    if( m_isAFII ){
      ANA_CHECK( m_JetUncertaintiesTool_handle.setProperty("MCType", "AFII") );
    }else{
      ANA_CHECK( m_JetUncertaintiesTool_handle.setProperty("MCType", m_mcType_recoilJets) );
    }
  }

  ANA_CHECK( m_JetUncertaintiesTool_handle.setProperty("OutputLevel", msg().level() ) );
  ANA_CHECK( m_JetUncertaintiesTool_handle.retrieve() );

  return EL::StatusCode::SUCCESS;
}

/* //initialize and configure the JER Tool */
/* EL::StatusCode InsituBalanceAlgo :: loadJetResolutionTool(){ */
/*   ANA_MSG_INFO("loadJetResolutionTool()"); */

/*   // Instantiate the JER Uncertainty tool */
/*   ANA_CHECK( ASG_MAKE_ANA_TOOL(m_JERTool_handle, JERTool) ); */
/*   ANA_CHECK( m_JERTool_handle.setProperty("PlotFileName", m_JERUncertaintyConfig.c_str()) ); */
/*   ANA_CHECK( m_JERTool_handle.setProperty("CollectionName", m_jetDef) ); */
/*   ANA_CHECK( m_JERTool_handle.setProperty("OutputLevel", msg().level() ) ); */
/*   ANA_CHECK( m_JERTool_handle.retrieve() ); */

/*   // Instantiate the JER Smearing tool */
/*   ANA_CHECK( ASG_MAKE_ANA_TOOL(m_JERSmearingTool_handle, JERSmearingTool) ); */
/*   ANA_CHECK( m_JERSmearingTool_handle.setProperty("JERTool", m_JERTool_handle.getHandle()) ); */
/*   ANA_CHECK( m_JERSmearingTool_handle.setProperty("isMC", m_isMC) ); */
/*   ANA_CHECK( m_JERSmearingTool_handle.setProperty("ApplyNominalSmearing", m_JERApplySmearing) ); */
/*   ANA_CHECK( m_JERSmearingTool_handle.setProperty("SystematicMode", m_JERSystematicMode) ); */
/*   ANA_CHECK( m_JERSmearingTool_handle.setProperty("OutputLevel", msg().level() ) ); */
/*   ANA_CHECK( m_JERSmearingTool_handle.retrieve() ); */

/*   return EL::StatusCode::SUCCESS; */
/* } */

// initialize and configure the JetTileCorrectionTool
EL::StatusCode InsituBalanceAlgo :: loadJetTileCorrectionTool(){
  ANA_MSG_INFO("loadJetTileCorrectionTool()");

  ANA_CHECK( ASG_MAKE_ANA_TOOL(m_JetTileCorrectionTool_handle, CP::JetTileCorrectionTool) );
  ANA_CHECK( m_JetTileCorrectionTool_handle.setProperty("OutputLevel", msg().level() ) );
  ANA_CHECK( m_JetTileCorrectionTool_handle.retrieve() );

  return EL::StatusCode::SUCCESS;
}

//Setup Previous MJB calibration and systematics files
EL::StatusCode InsituBalanceAlgo :: loadMJBCalibration(){
  ANA_MSG_INFO("loadMJBCalibration()");

  if(m_subjetThreshold_MJB <= 0 && !m_closureTest)
    return EL::StatusCode::SUCCESS;

  // Load the file.  If m_closureTest, then apply the current iteration
  TFile* MJBFile = TFile::Open( PathResolverFindCalibFile(m_MJBCorrectionFile).c_str() , "READ" );
  if( MJBFile->IsZombie() ){
    Error("loadMJBCalibration()", "Can't find MJB function %s. Exiting...", m_MJBCorrectionFile.c_str() );
    return EL::StatusCode::FAILURE;
  }
  ANA_MSG_INFO("Loaded MJB input file");

  // Get the MJB correction histograms in the input file
  TKey *key;
  TIter next(MJBFile->GetListOfKeys());
  while ((key = (TKey*) next() )){
    std::string graph_name = key->GetName();
    if (graph_name.find( "graph" ) != std::string::npos) { //If it's a calibration TGraph
      TGraphAsymmErrors* MJB_graph = (TGraphAsymmErrors*) MJBFile->Get( (graph_name).c_str() );
      //MJB_graph->SetDirectory(0);
      m_MJB_graphs.push_back(MJB_graph);
    }
  }

  // Fix the systematics mappings to match the input MJB corrections
  std::vector<std::string> new_sysName;
  std::vector<int> new_sysType;
  std::vector<float> new_sysDetail;
  std::vector<CP::SystematicSet> new_sysSet;

  // Loop over each input MJB systematic histogram
  for(int i=0; i < (int) m_MJB_graphs.size(); ++i){
    bool foundMatch = false;
    std::string graph_name = m_MJB_graphs.at(i)->GetName();

    // Loop over the loaded systematics and find the match
    for(unsigned int iSys=0; iSys < m_sysName.size(); ++iSys){
      if( graph_name.find( m_sysName.at(iSys) ) != std::string::npos ){
        std::cout << "Match: " << graph_name << " to " <<  m_sysName.at(iSys) << std::endl;
        new_sysName.push_back( m_sysName.at(iSys) );
        new_sysType.push_back( m_sysType.at(iSys) );
        new_sysDetail.push_back( m_sysDetail.at(iSys) );
        new_sysSet.push_back( m_sysSet.at(iSys) );
        foundMatch = true;
        break;
      }
    }//for m_sysName
    if( foundMatch == false){
      Info("loadMJBCalibration()", "Can't find Systematic Variation corresponding to MJB Correction %s. Skipping it...", graph_name.c_str() );
      m_MJB_graphs.erase(m_MJB_graphs.begin()+i); //Remove unused variations to ensure iSys matches up with m_MJB_graphs
      i--;
    }
  }//for m_MJB_graphs

  //Fix the m_NominalIndex
  for(unsigned int i=0; i < new_sysName.size(); ++i){
    if( new_sysName.at(i).find("Nominal") != std::string::npos){
      m_NominalIndex = i;
      break;
    }
  }//for m_MJB_graphs

  //Replace systematic vectors with their new versions.  Now we are sure iSys will match up with m_MJB_graphs
  m_sysName    =  new_sysName;
  m_sysType   =  new_sysType;
  m_sysDetail =  new_sysDetail;
  m_sysSet = new_sysSet;

  Info("loadMJBCalibration()", "Succesfully loaded MJB calibration file");

  return EL::StatusCode::SUCCESS;
}

//Apply leading jet calibration (this should be at eta-intercalibration stage)
EL::StatusCode InsituBalanceAlgo :: applyJetCalibrationTool( std::vector< xAOD::Jet*>* jets ){

  ANA_MSG_DEBUG("applyJetCalibrationTool()");

  if( jets->size() <= 0 )
    return EL::StatusCode::SUCCESS;

  //First apply eta-intercalibration scale to all jets, to find their scale
  for(unsigned int iJet=0; iJet < jets->size(); ++iJet){
    if ( m_JetCalibrationTool_handle->applyCorrection( *(jets->at(iJet)) ) == CP::CorrectionCode::Error ) {
      Error("execute()", "JetCalibrationTool for lead jet reported a CP::CorrectionCode::Error");
      return StatusCode::FAILURE;
    }
    jets->at(iJet)->auxdecor<float>("etaInter_pt") = jets->at(iJet)->pt();
  }

  //For MC or non-MJB calibrations, this will be final pt scale
  if( (m_mode != MJB) || m_isMC )
    return EL::StatusCode::SUCCESS;

  //Loop over each jet again and decide if it should be a "subleading" jet, calibrated at fully (V+jet) in situ scale
  //This is for MJB only
  for(unsigned int iJet=0; iJet < jets->size(); ++iJet){
    //If validation mode (m_subjetThreshold_Vjet<=0),
    //Or if not leading jet and it's below Vjet threshold, calibrate the jet
    if( (m_subjetThreshold_Vjet <= 0) || (iJet != 0 && jets->at(iJet)->pt() <= m_subjetThreshold_Vjet) ){

      if ( m_JetCalibrationTool_handle_recoilJets->applyCorrection( *(jets->at(iJet)) ) == CP::CorrectionCode::Error ) {
        Error("execute()", "JetCalibrationTool for subleading jets reported a CP::CorrectionCode::Error");
        return StatusCode::FAILURE;
      }
    }//if calibrate with V+jet
  }// for each jet
  return EL::StatusCode::SUCCESS;
}

EL::StatusCode InsituBalanceAlgo :: applyJetUncertaintyTool( xAOD::Jet* jet , unsigned int iSys, unsigned int iJet ){
  ANA_MSG_DEBUG("applyJetUncertaintyTool with systematic " << m_sysName.at(iSys) );

   if( m_sysType.at(iSys) != JES && m_sysType.at(iSys) != PERJETFLAVOR) //If not a JetUncertaintyTool sys variation
    return EL::StatusCode::SUCCESS;

  //PERJETFLAVOR only occurs for a single jet in each event, so find out if we're at that jet
  if( m_sysType.at(iSys) == PERJETFLAVOR && m_sysDetail.at(iSys) != iJet ){
    return EL::StatusCode::SUCCESS;
  }

  if ( m_JetUncertaintiesTool_handle->applySystematicVariation( m_sysSet.at(iSys) ) != CP::SystematicCode::Ok ) {
    Error("execute()", "Cannot configure JetUncertaintiesTool for systematic %s", m_sysName.at(iSys).c_str());
    return EL::StatusCode::FAILURE;
  }

  if ( m_JetUncertaintiesTool_handle->applyCorrection( *jet ) == CP::CorrectionCode::Error ) {
    Error("execute()", "JetUncertaintiesTool reported a CP::CorrectionCode::Error");
    return EL::StatusCode::FAILURE;
  }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode InsituBalanceAlgo :: applyMJBCalibration( xAOD::Jet* jet , unsigned int iSys ){
  ANA_MSG_DEBUG("applyMJBCalibration()");

  if(m_isMC)
    return EL::StatusCode::SUCCESS;

  // Get calibration
  float this_pt = jet->pt()/1e3;
  int last_point = m_MJB_graphs.at(iSys)->GetN()-1;
  float max_graph_pt = m_MJB_graphs.at(iSys)->GetX()[last_point];

  //If jet pt is last point of TGraph, set the pt to the last TGraph point
  //Because we don't want to extrapolate.  For this jet, this_pt == etaInter_pt
  if( this_pt > max_graph_pt ){
    this_pt = max_graph_pt;
  }

  float thisCalibration = m_MJB_graphs.at(iSys)->Eval( this_pt );

  //Transform relative uncertainty into absolute calibration for systematic variations
  if( (int) iSys != m_NominalIndex ){
    float thisNominal = m_MJB_graphs.at(m_NominalIndex)->Eval( this_pt );
    thisCalibration = (thisCalibration*thisNominal) + thisNominal;
  }

  thisCalibration = 1. / thisCalibration;

  xAOD::JetFourMom_t thisJet;
  thisJet.SetCoordinates( jet->pt(), jet->eta(), jet->phi(), jet->m() );
  thisJet *= thisCalibration;
  jet->setJetP4( thisJet );

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode InsituBalanceAlgo::applyJetSysVariation(std::vector< xAOD::Jet*>* theseJets, unsigned int iSys ){
  ANA_MSG_DEBUG("applyJetSysVariation()");

  // No further corrections for earlier jet calibration stages
  if(m_sysType.at(iSys) == JCS)
    return EL::StatusCode::SUCCESS;

  // Apply additional calibrations when appropriate
  for(unsigned int iJet = 0; iJet < theseJets->size(); ++iJet){
    xAOD::Jet* jet = theseJets->at(iJet);

    //Don't calibrate leading jet if not closure mode
    if( iJet == 0 && !m_closureTest )
      continue;

    //JetUncertaintiesTool has issues below 16 GeV
    if( jet->pt() <= 16.*1e3)
      continue;

    float jet_etaInter_pt = jet->auxdecor<float>("etaInter_pt");
    //Regular uncertainties below Vjet threshold
    if( jet_etaInter_pt <= m_subjetThreshold_Vjet ){
      if(m_largeR.compare("True")==0 && iJet!=0){
	ANA_CHECK( applyJetUncertaintyTool( jet , iSys, iJet ) );
	//ANA_CHECK( applyJetResolutionTool(  jet , iSys ) );
      }else{
	ANA_CHECK( applyJetUncertaintyTool( jet , iSys, iJet ) );
	//ANA_CHECK( applyJetResolutionTool(  jet , iSys ) );
      }
      continue; //this skip the next MJB if statement
    }
    
    //Otherwise, apply MJB if below threshold
    if( (m_subjetThreshold_MJB > 0) && (jet_etaInter_pt >  m_subjetThreshold_Vjet) && (jet_etaInter_pt <= m_subjetThreshold_MJB) ){
      ANA_CHECK( applyMJBCalibration( jet , iSys ) );
    }

  }//For each jet

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode InsituBalanceAlgo :: applyJetTileCorrectionTool( std::vector< xAOD::Jet*>* jets, unsigned int iSys  ){
  ANA_MSG_DEBUG("applyJetTileCorrectionTool()");

  if( m_isMC )
    return EL::StatusCode::SUCCESS;

  // No further corrections for earlier jet calibration stages
  if(m_sysType.at(iSys) == JCS)
    return EL::StatusCode::SUCCESS;

  for(unsigned int iJet=0; iJet < jets->size(); ++iJet){

    xAOD::Jet* jet = jets->at(iJet);
    xAOD::JetFourMom_t previous_P4;
    previous_P4.SetCoordinates( jet->pt(), jet->eta(), jet->phi(), jet->m() );

    if ( m_JetTileCorrectionTool_handle->applyCorrection( *jet ) == CP::CorrectionCode::Error ) {
      Error("execute()", "JetTileCorrectionTool reported a CP::CorrectionCode::Error");
      return StatusCode::FAILURE;
    }

    jet->auxdata< float >("TileCorrectedPt") = jet->pt();

    if(!m_TileCorrection)
      jet->setJetP4( previous_P4 );
  }//for jet


  return EL::StatusCode::SUCCESS;
}

//Apply leading jet calibration (this should be at eta-intercalibration stage)
EL::StatusCode InsituBalanceAlgo :: applyLargeRjetCalibration( std::vector< xAOD::Jet*>* largeRjets, std::vector< xAOD::Jet*>* smallRjets ){

  ANA_MSG_DEBUG("applyLargeRjetCalibration()");

  if( largeRjets->size() <= 0 || largeRjets->size() <= 0 )
    return EL::StatusCode::SUCCESS;

  //Apply large-R jet calibration to large-R jets
  for(unsigned int iJet=0; iJet < largeRjets->size(); ++iJet){
    if ( m_JetCalibrationTool_handle->applyCorrection( *(largeRjets->at(iJet)) ) == CP::CorrectionCode::Error ) {
      Error("execute()", "JetCalibrationTool for large-R jet reported a CP::CorrectionCode::Error");
      return StatusCode::FAILURE;
    }
    largeRjets->at(iJet)->auxdecor<float>("etaInter_pt") = largeRjets->at(iJet)->pt();
  }

  //Apply small-R jet calibration to small-R jets
  for(unsigned int iJet=0; iJet < smallRjets->size(); ++iJet){
    if ( m_JetCalibrationTool_handle_recoilJets->applyCorrection( *(smallRjets->at(iJet)) ) == CP::CorrectionCode::Error ) {
      Error("execute()", "JetCalibrationTool for small-R jet reported a CP::CorrectionCode::Error");
      return StatusCode::FAILURE;
    }
    smallRjets->at(iJet)->auxdecor<float>("etaInter_pt") = smallRjets->at(iJet)->pt();
  }
  return EL::StatusCode::SUCCESS;
}
