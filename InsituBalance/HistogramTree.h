/**
 * @file   HistogramTree.h
 * @author Jeff Dandoy <jeff.dandoy@cern.ch>
 *
 */

#ifndef InsituBalance_HistogramTree_H
#define InsituBalance_HistogramTree_H

// ROOT include(s):
#include "TH1D.h"
#include "TH2D.h"
#include "TProfile.h"
// algorithm wrapper
#include "xAODAnaHelpers/Algorithm.h"


class HistogramTree : public xAH::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
  public:
    // variables read in through configuration file

  private:


    /** @brief Event counter */
    int m_eventCounter;     //!
    /** @brief Initial number of events in the AOD for weighting purposes.  Taken from metadta histogram */
    float m_totalNumEvents;  //!
    /** @brief If sample is MC */
    bool m_isMC;      //!


    // variables that don't get filled at submission time should be
    // protected from being send from the submission node to the worker
    // node (done by the //!)
  public:
    // Tree *myTree; //!
    // TH1 *myHist; //!
    //

    // this is a standard constructor
    HistogramTree (std::string className = "HistogramTree");

    // these are the functions inherited from Algorithm
    virtual EL::StatusCode setupJob (EL::Job& job);
    virtual EL::StatusCode fileExecute ();
    virtual EL::StatusCode histInitialize ();
    virtual EL::StatusCode changeInput (bool firstFile);
    virtual EL::StatusCode initialize ();
    virtual EL::StatusCode execute ();
    virtual EL::StatusCode postExecute ();
    virtual EL::StatusCode finalize ();
    virtual EL::StatusCode histFinalize ();

    /** @brief Function to calculation the deltaR between two physics objects */
    double DeltaR(double eta1, double phi1, double eta2, double phi2);

    /// @cond
    // this is needed to distribute the algorithm to the workers
    ClassDef(HistogramTree, 1);
    /// @endcond

  private:



    int      runNumber;    //!
    Long64_t eventNumber;  //!
    int mcChannelNumber; //!
    int NPV;    //!
    float averageInteractionsPerCrossing;    //!
    float correct_mu; //!
    float weight;   //!
    float weight_pileup;    //!
    float mcEventWeight;    //!
    float weight_xs; //!
    float weight_prescale; //!

    int njet; //!

    float ptAsym; //!
    float alpha; //!
    float avgBeta; //!
    float ptBal; //!
    float recoilPt; //!
    float recoilEta; //!
    float recoilPhi; //!
    float recoilM; //!
    float recoilE; //!

    std::vector<float> *photon_pt; //!
    std::vector<float> *photon_eta; //!
    std::vector<float> *photon_phi; //!
    std::vector<float> *jet_pt; //!
    std::vector<float> *jet_eta; //!
    std::vector<float> *jet_phi; //!
    std::vector<float> *jet_E; //!

    std::vector<std::string> *passedTriggers; //!
    std::vector<float> *triggerPrescales; //!

    std::vector<float> *jet_detEta; //!
    std::vector<float> *jet_TileCorrectedPt; //!
    std::vector<float> *jet_beta; //!
    std::vector<float> *jet_corr; //!
    std::vector<float> *jet_TileFrac; //!
    std::vector<float> *jet_Jvt; //!
    std::vector<float> *jet_PartonTruthLabelID; //!

    TH2F* h_recoilPt_PtBal; //!
    TH1F* h_NPV;    //!
    TH1F* h_averageInteractionsPerCrossing;    //!
    TH1F* h_correct_mu;    //!
    TH1F* h_njet;    //!

    TH1F* h_ptAsym; //!
    TH1F* h_alpha; //!
    TH1F* h_avgBeta; //!
    TH1F* h_ptBal; //!
    TH1F* h_recoilPt; //!
    TH1F* h_recoilEta; //!
    TH1F* h_recoilPhi; //!
    TH1F* h_recoilM; //!
    TH1F* h_recoilE; //!

    unsigned int numHistJets; //!
    unsigned int numHistPhotons; //!

    std::vector<TH1F*> vh_photon_pt; //!
    std::vector<TH1F*> vh_photon_eta; //!
    std::vector<TH1F*> vh_photon_phi; //!
    std::vector<TH1F*> vh_jet_pt; //!
    std::vector<TH1F*> vh_jet_eta; //!
    std::vector<TH1F*> vh_jet_phi; //!
    std::vector<TH1F*> vh_jet_E; //!
    std::vector<TH1F*> vh_jet_detEta; //!
    std::vector<TH1F*> vh_jet_TileCorrectedPt; //!
    std::vector<TH1F*> vh_jet_beta; //!
    std::vector<TH1F*> vh_jet_corr; //!
    std::vector<TH1F*> vh_jet_TileFrac; //!
    std::vector<TH1F*> vh_jet_Jvt; //!
    std::vector<TH1F*> vh_jet_PartonTruthLabelID; //!

    TH1F* h_jet_pt_all; //!
    TH1F* h_jet_eta_all; //!
    TH1F* h_jet_phi_all; //!
    TH1F* h_jet_E_all; //!
    TH1F* h_photon_pt_all; //!
    TH1F* h_photon_eta_all; //!
    TH1F* h_photon_phi_all; //!
    TH1F* h_photon_E_all; //!


};

#endif
