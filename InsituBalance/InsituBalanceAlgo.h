#ifndef InsituBalance_InsituBalanceAlgo_H
#define InsituBalance_InsituBalanceAlgo_H

/** @file InsituBalanceAlgo.h
 *  @brief Run the Multijet Balance Selection
 *  @author Jeff Dandoy
 */

// algorithm wrapper
#include "xAODAnaHelpers/Algorithm.h"

//#include <EventLoop/StatusCode.h>
//#include <EventLoop/Algorithm.h>

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

// ROOT include(s):
#include "TH1D.h"
#include "TGraphAsymmErrors.h"

// inlude the parent class header for tree
#ifndef __MAKECINT__
#include "InsituBalance/MiniTree.h"
#endif

#include <sstream>


#include "AsgTools/AnaToolHandle.h"

//#include "JetInterface/IJetSelector.h" //JetCleaningTool
#include "JetCalibTools/IJetCalibrationTool.h"
#include "JetCPInterfaces/ICPJetUncertaintiesTool.h"
#include "JetInterface/IJetUpdateJvt.h"
#include "JetAnalysisInterfaces/IJetJvtEfficiency.h"
#include "FTagAnalysisInterfaces/IBTaggingEfficiencyTool.h"
#include "FTagAnalysisInterfaces/IBTaggingSelectionTool.h"
#include "JetCPInterfaces/IJetTileCorrectionTool.h"

#include <functional>

#include "InDetTrackSelectionTool/IInDetTrackSelectionTool.h"
#include "TrackVertexAssociationTool/ITrackVertexAssociationTool.h"

class MultijetHists;
class KTermHists;
class IJetCalibrationTool;
class ICPJetUncertaintiesTool;
class IJERTool;
class IJERSmearingTool;
class IJetSelector;
class IJetUpdateJvt;
class IBTaggingEfficiencyTool;
class IBTaggingSelectionTool;

class SystContainer;
class IJetTileCorrectionTool;





/**
    @brief Event selection of the Multijet Balance
 */

// variables that don't get filled at submission time should be
// protected from being send from the submission node to the worker
// node (done by the //!)
class InsituBalanceAlgo : public xAH::Algorithm
//class InsituBalanceAlgo : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
  public:

//    MSG::Level m_msgLevel = MSG::INFO;

    std::string m_modeStr = "MJB";


    /** @brief TEvent object */
    xAOD::TEvent *m_event;  //!
    /** @brief TStore object for variable storage*/
    xAOD::TStore *m_store;  //!
    /** @brief Count of the current event*/
    int m_eventCounter;     //!
    /** @brief Global name to give to the algorithm*/
    std::string m_name;


////// configuration variables set by user //////
    /** @brief Input container name for jet and reference (photon/muon/electron) collections*/
    std::string m_inContainerName_jets = "";
    std::string m_inContainerName_ref = "";

    /// @brief Name of all possible CP systematic variations (for photon, e, and mu systematics)
    std::string m_CPSystNames = "";

    /** @brief Per-event list of systematics that passes */
    std::string m_inputAlgoSystNames_ref = "";

    /** @brief String consisting of triggers and their recoil \f$p_{T}\f$ thresholds
     *  @note Each trigger / recoil \f$p_{T}\f$ combination is separated by a colon ":",
     *  and each combination is separated by a comma ",".
     *  Use -1 for no pt requirement, and this trigger will not be skipped on failure.
     * */
    std::string m_triggerAndPt = "";
    /** @brief The iteration of the current MJB
     * @note MJB is an iterative procedure, with each iteration using outputs from the previous.
     * The procedure starts at a m_MJBIteration value of 0, and is increased by 1 for each iteration.
     * This value corresponds directly to the entries in InsituBalanceAlgo#m_MJBIterationThreshold.
     * Set to -1 for validation mode, applying full insitu calibration to all jets.
     * */
    int m_MJBIteration = 0;               // Number of previous MJB iterations
    /** @brief A comma separated list of subleading jet \f$p_{T}\f$ thresholds
     * @note The comma separated list is translated into a vector of values.
     * Each vector entry corresponds to a potential MJB iteration, and the value used is chosen
     * by InsituBalanceAlgo#m_MJBIteration.
     * */
    std::string m_MJBIterationThreshold = "999999";
    /** @brief The location of the file containing previous MJB calibrations
     * @note The file corresponds to previous MJB calibrations and will not be used for a InsituBalanceAlgo#m_MJBIteration of zero.
     * The naming convention of the calibration histograms should agree with the iteration.
     * */
    std::string m_MJBCorrectionFile = "";

    /** @brief Dash "-" separated list of systematic variations to use
     * @note Systematic variations are chosen according to their name in the MJB or their respective calibration tools.
     * For example including "EtaIntercalibration" will grab the JetUncertainties \f$\eta\f$-intercalibration systematics.
     * Short-cut strings are also accepted, including:
     *   - Nominal : Include the nominal result
     *   - AllSystematic : Include every defined systematic
     *   - MJB : Include all MJB systematics (defined in InsituBalanceAlgo#loadSystematics)
     *   - AllZjet : Include all Z+jet \a in-situ calibration systematics
     *   - AllGjet : Include all \f$\gamma\f$+jet \a in-situ calibration systematics
     *   - AlleXX  : Include all JetUncertainties systematics include the substring "XXX"
     *   - Special : Include all JetUncertainties systematics including EtaIntercalibration, Pileup, Flavor, or PunchThrough
     *   - JetCalibSequence : Include each stage of the jet calibration procedure (origin, etaJES, GSC, etc.)
     * */
    std::string m_sysVariations = "Nominal";

    /** @brief Apply previous MJB calibration to the leading jet, performing a closure test*/
    bool m_closureTest = false;
    /** @brief Run in Bootstrap mode (See Instructions)*/
    bool m_bootstrap = false;
    /** @brief Run in KTerm mode*/
    bool m_makeKTerm = false;

    /** @brief Selection for the minimum number of jets in the event (Default of 3)*/
    unsigned int m_numJets = 3;
    /** @brief Maximum eta for jets to consider (Default of 2.8 - use 4.49 for V+jet)*/
    float m_jetEta = 2.8;
    /** @brief Selection for the relative \f$p_{T}\f$ asymmetry of the subleading jet compared to the recoil object*/
    float m_ptAsymVar = 0.8;
    /** @brief Selection for the minimum \f$p_{T}\f$ rejection of the subleading jet */
    float m_ptAsymMin = 0;
    /** @brief Selection for the \f$\alpha\f$ of the event (Default 0.3)*/
    float m_alpha = 0.3;
    /** @brief \f$p_{T}\f$ threshold for each jet to be included (default 25 GeV)*/
    float m_ptThresh = 25.;
    /** @brief \f$p_{T}\f$ threshold for leading jet*/
    float m_leadJetPtThresh = 0.;
    /** @brief Selection for the \f$\beta\f$ of any jet in the event (Default 1.0)*/
    float m_beta = 1.;
    /** @brief Relative pt threshold of each jet (compared to leading jet) to be considered in the InsituBalanceAlgo#m_beta selection,
     * i.e. only jets with \f$p_{T}\f$ > m_betaPtVar*100% of the leading jet \f$p_{T}\f$ */
    float m_betaPtVar = 0;
    /** @brief Maximum deltaR value between a jet and a reference object.  Jets that fail are removed from consideration */
    float m_overlapDR = 0.;

    /** @brief Apply the GSC-stage calibration to the leading jet when calibrating.  This should only be used
     * if a special eta-intercalibration stage insitu file is not available, and is a close approximate.
     * @note It is an exact approximation if the leading jet detEta cut is changed from 1.2 to 0.8 */
    bool m_leadingGSC = false;
    /** @brief True value will allow TTree output */
    bool m_writeTree = false;
    /** @brief True value will write out only the Nominal TTree */
    bool m_writeNominalTree = false;
    /** @brief Control substrings for creating MJB histograms */
    std::string m_MJBDetailStr = "";
    /** @brief Control substrings for creating various event or particle histograms */
    std::string m_eventDetailStr = "";
    std::string m_jetDetailStr = "";
    std::string m_photonDetailStr = "";
    std::string m_elDetailStr = "";
    std::string m_muDetailStr = "";
    std::string m_trigDetailStr = "";

    /** @brief Name of the truth xAOD container for MC Pileup Check, set to "None" if not used */
    std::string m_MCPileupCheckContainer = "AntiKt4TruthJets";
    /** @brief Setting for ATLAS Fastsim production samples */
    bool m_isAFII = false;
    /** @brief Option to output the cutflow histograms */
    bool m_useCutFlow = true;
    /** @brief File containing the MC sample cross-section values */
    std::string m_XSFile= "InsituBalance/susy_crosssections_13TeV.txt";
    /** @brief Apply the Tile Correction to data */
    bool m_TileCorrection = false;

    /** @brief Number of toys used by the bootstrapping procedure.  Recommendation is 100*/
    int m_systTool_nToys = 100;
    /** @brief Comma separated list of bin edges for recoil \f$p_{T}\f$*/
    std::string m_binning = "";

    /** @brief Boolean for performing b-tagging on jets*/
    bool m_bTag = true;
    /** @brief Comma separated list of of b-tag working point efficiency percentages*/
    std::string m_bTagWPsString = "";
    /** @brief Path to b-tagging CDI file*/
    std::string m_bTagFileName = "$ROOTCOREBIN/data/xAODAnaHelpers/2015-PreRecomm-13TeV-MC12-CDI-October23_v1.root";
    /** @brief Variable for b-tagging (i.e. MV2c20) */
    std::string m_bTagVar = "MV2c10";
    /** @brief Propagated option from b-tagging tool*/
    bool m_useDevelopmentFile = true;
    /** @brief Propagated option from b-tagging tool*/
    bool m_useConeFlavourLabel = true;

    /** @brief Container name for HLT trigger jets */
    std::string m_HLT_trigger_jet_container = "";

    /** @brief Boolean to save trigger emulation information */
    bool m_emulate_trigger = false;

    /** @brief Boolean to match track-jets to reco-jets */
    bool m_match_track_jets = false;
    /** @brief Boolean to match truth-jets to reco-jets */
    bool m_match_truth_jets = false;

////// configuration variables set automatically //////
    /** @brief If input is MC, as automatically determined from xAOD::EventInfo::IS_SIMULATION*/
    bool m_isMC;
    /** @brief Jet pt threshold for V+jet calibration (from JetCalibTools), used for first iteration & uncertainties from tools.
        Set to -1 to calibrate and use all subleading jets (i.e. for validation studies). */
    double m_subjetThreshold_Vjet = -1;
    /** @brief Jet pt threshold for previus iteration of MJB, used for 2nd+ MJB iterations. */
    double m_subjetThreshold_MJB  = -1;

    /** @brief Vector of b-tag working point efficiency percentages, filled automatically from InsituBalanceAlgo#m_bTag*/
    std::vector<std::string> m_bTagWPs;
    /** @brief Set to true automatically if InsituBalanceAlgo#m_MCPileupCheckContainer is not "None"*/
    bool m_useMCPileupCheck;
    /** @brief Set to true for iterations beyond the first, as bootstrap mode propagation is handled differently */
    bool m_iterateBootstrap;
    /** @brief Vector of triggers, filled automatically from InsituBalanceAlgo#m_triggerAndPt*/
    std::vector<std::string> m_triggers;
    /** @brief Vector of trigger thresholds, filled automatically from InsituBalanceAlgo#m_triggerAndPt*/
    std::vector<float> m_triggerThresholds;
    /** @brief Vector of bins, filled automatically from InsituBalanceAlgo#m_binning*/
    std::vector<double> m_bins;

////// config for Probe Jet Tools //////
    /** @brief Jet definition (e.g. AntiKt4EMTopo). Propagated option for Jet Tools */
    std::string m_jetDef = "";
    std::string m_jetDef_recoilJets = "";
    /** @brief Calibration sequence for JetCalibTools */
    std::string m_mcType = "";
    std::string m_mcType_recoilJets = "";
////// Common configs for Probe and Reference Jet Tools //////
    std::string m_jetCalibSequence = "";
    std::string m_jetCalibSequence_recoilJets = "";
    /** @brief Configuration file for JetCalibTools*/
    std::string m_jetCalibConfig = "";
    std::string m_jetCalibConfig_recoilJets = "";
    std::string m_CalibArea = "";
    std::string m_CalibArea_recoilJets = "";

    /** @brief JVT working point to apply*/
    std::string m_JVTWP = "Medium";
    /** @brief JVT variable name in DAOD*/
    std::string m_JVTVar = "JVFCorr";
    /** @brief Is this for large-R jets */
    std::string m_largeR = "";
    /** @brief Configuration file for JetUncertainties */
    std::string m_jetUncertaintyConfig = "";
    std::string m_jetUncertainty_CalibArea = "";

    /** @brief SystTool object for Boostrap mode*/
    SystContainer  *systTool; //!

    /** @brief needed for track selection in KTerm calculation*/
    asg::AnaToolHandle<InDet::IInDetTrackSelectionTool> m_trkSelTool_handle{"InDetTrackSelectionTool"}; //!

  private:

    // Index of current systematic uncertainty
    unsigned int m_iSys;

    bool m_isPFlow = false;

    /** @brief Location of primary vertex within xAOD::VertexContainer*/
    int m_pvLocation; //!

    /** @brief Event weight from the MC generation*/
    float m_mcEventWeight; //!
    /** @brief AMI cross-section weight, grabbed from file TODO*/
    float m_weight_xs; //!
    /** @brief AMI acceptance weight, grabbed from file TODO*/
    float m_weight_kfactor; //!
    /** @brief Channel number assigned to the MC sample*/
    int m_mcChannelNumber; //!
    /** @brief Run number assigned to the data*/
    int m_runNumber; //!
    /** @brief A stringstream object for various uses*/
    std::stringstream m_ss; //!

    /** @brief Vector of each individual systematic variation name*/
    std::vector<std::string> m_sysName; //!
    /** @brief Integer corresponding to the type of systematic used.
     * @note Value is  -1 for Nominal,
     * 0 for JetUncertainties, 1 for JER, 10 for JetCalibSequence, 2 for MJB alpha, 3 for MJB beta, 4 for MJB \f$p_{T}\f$ asymmetry,
     * 5 for MJB \f$p_{T}\f$ threshold, 6 for MJB statistical uncertainty. */
    std::vector<int> m_sysType; //!
    /** @brief Detail related to the systematic uncertainty.
     * @note For example, this would be the selection value for MJB systematics, or the index of a JES systematic uncertainty in the JetUncertainties tool*/
    std::vector<float> m_sysDetail; //!
    /** @brief CP::SystematicSet for each systematic */
    std::vector<CP::SystematicSet> m_sysSet; //!
    /** @brief Position of the Nominal result within m_sysVar */
    int m_NominalIndex; //!

    /** @brief MJB \a in-situ calibration TGraphs from previous iterations taken from InsituBalanceAlgo#m_MJBCorrectionFile*/
    std::vector< TGraphAsymmErrors* > m_MJB_graphs; //!
//    std::map<int, int> m_VjetMap; //!
//    std::map<int, int> m_JESMap; //!
    /** @brief Substring name given to each JES calibration stage in InsituBalanceAlgo#m_jetCalibSequence,
     * used for JetCalibSequence option of InsituBalanceAlgo#m_sysVariations */
    std::vector<std::string> m_JCSTokens; //!
    /** @brief Full name of each JES calibration stage (i.e. JetGSCScaleMomentum).
     * Has a direct correspondence with InsituBalanceAlgo#m_JCSTokens*/
    std::vector<std::string> m_JCSStrings; //!

    /** @brief Update every cutflow for this selection
        @note The current selection is determined by the variable m_iCutflow, which automatically updates with each use
    */
    EL::StatusCode fillCutflowAll(int iSel);
    /** @brief Update cutflow iVar for this selection
        @note The current selection is determined by the variable m_iCutflow, which automatically updates with each use
        @param iVar  The index of the current systematic variation whose cutflow is to be filled
    */
    EL::StatusCode fillCutflow(int iSel, int iVar);

  public:
    /** @brief Vector of cutflows, for each systematic variation, showing the integer number of events passing each selection */
    std::vector<TH1D*> m_cutflowHist;    //!
    /** @brief Vector of weighted cutflows, for each systematic variation, showing the weighted number of events passing each selection */
    std::vector<TH1D*> m_cutflowHistW;   //!
    /** @brief  Bin index corresponding to the first new selection
     * @note  The cutflow histograms are grabbed from previous xAH algorithms and already include entries.
     * The first location of the new MJB entr is stored here.
     * */
    unsigned int m_cutflowFirst;     //!
    /** @brief Automatically updating index of the current selection, used by passCutAll() and passCut() */
    unsigned int m_iCutflow;     //!

  private:
    /** @brief Calculate the \f$\Delta\phi\f$ between two objects*/
    double DeltaPhi(double phi1, double phi2);
    /** @brief Calculate the \f$\Delta R\f$ between two objects*/
    double DeltaR(double eta1, double phi1,double eta2, double phi2);

    /** @brief Function to match trigger jets to reco jets, and save their L1 Et and HLT Pt */
    EL::StatusCode matchTriggerJets();

    /** @brief Function to match track jets to reco jets, and decorate their kinematics to the reco jet */
    EL::StatusCode matchTrackJets(std::vector< xAOD::Jet* >* jets);
    /** @brief Function to match truth jets to reco jets, and decorate their kinematics to the reco jet */
    EL::StatusCode matchTruthJets(std::vector< xAOD::Jet* >* jets);

//    double DeltaR(double eta1, double phi1,double eta2, double phi2);

    #ifndef __MAKECINT__

    /** @brief Vector of MiniTree objects to output, each corresponding to a different systematic*/
    std::vector<MiniTree*> m_treeList; //!
    /** @brief Retrieve event info from the xAOD::EventInfo object and the file TODO
     * @note: Fills in values for InsituBalanceAlgo#m_runNumber, InsituBalanceAlgo#m_mcChannelNumber, InsituBalanceAlgo#m_weight_xs, InsituBalanceAlgo#m_weight_kfactor */
    EL::StatusCode getSampleWeights(const xAOD::EventInfo* eventInfo);
    /** @brief Vector of MultijetHists objects to output, each corresponding to a different systematic*/
    std::vector<MultijetHists*> m_jetHists; //!
    /** @brief KTermHists object used to fill KTerm Histogram */
    KTermHists* m_KTermHist; //!


    #endif

    /** @brief Load all the systematic variations from InsituBalanceAlgo#m_sysVariations */
    EL::StatusCode loadSystematics();
    /** @brief Load the JVT correction Tool*/
    EL::StatusCode loadJVTTool( std::string jetDefinition );
    /** @brief Load the JetCalibTool*/
    EL::StatusCode loadJetCalibrationTool();
    /** @brief Find and connect the jet calibration stages for InsituBalanceAlgo#m_JCSTokens and InsituBalanceAlgo#m_JCSStrings*/
    EL::StatusCode setupJetCalibrationStages();
    /** @brief Load the JetUncertainties*/
    EL::StatusCode loadJetUncertaintyTool();
    /** @brief Load the JetTileCorrectionTool*/
    EL::StatusCode loadJetTileCorrectionTool();
    /** @brief Load the previous MJB calibrations from InsituBalanceAlgo#m_MJBCorrectionFile*/
    EL::StatusCode loadMJBCalibration();
    /** @brief Load the b-tagging tools*/
    EL::StatusCode loadBTagTools();

    #ifndef __MAKECINT__
    /** @brief Apply the JetCalibTool*/
    EL::StatusCode applyJetCalibrationTool( std::vector< xAOD::Jet*>* jets );
     /** @brief Apply the intermediate V+jet \a in-situ calibrations*/
     EL::StatusCode applyVjetCalibration( std::vector< xAOD::Jet*>* jets );
    /** @brief For jets below subleading pt threshold, call (optionally) applyVjetCalibration and applyJetUncertaintyTool */
    EL::StatusCode applyJetSysVariation(std::vector< xAOD::Jet*>* jets, unsigned int iSys );
    /** @brief Apply the JetTileCorrectionTool*/
     EL::StatusCode applyJetTileCorrectionTool( std::vector< xAOD::Jet*>* jets, unsigned int iSys );


     EL::StatusCode applyLargeRjetCalibration( std::vector< xAOD::Jet*>* largeRjets, std::vector< xAOD::Jet*>* smallRjets );


    /** @brief Apply the Jet Uncertainty Tool*/
     EL::StatusCode applyJetUncertaintyTool( xAOD::Jet* jet , unsigned int iSys, unsigned int iJet );
    /** @brief Apply the MJB calibration from previous iterations */
     EL::StatusCode applyMJBCalibration( xAOD::Jet* jet , unsigned int iSys );
    /** @brief Order the jets in a collection to descend in \f$p_{T}\f$*/
//     EL::StatusCode reorderJets(std::vector< xAOD::Jet*>* signalJets);

     template<typename T> EL::StatusCode reorderJets( std::vector<T>* theseJets ){

       ANA_MSG_VERBOSE("reorderJets()");
       T tmpJet = nullptr;
       for(unsigned int iJet = 0; iJet < theseJets->size(); ++iJet){
         for(unsigned int jJet = iJet+1; jJet < theseJets->size(); ++jJet){
           if( theseJets->at(iJet)->pt() < theseJets->at(jJet)->pt() ){
             tmpJet = theseJets->at(iJet);
             theseJets->at(iJet) = theseJets->at(jJet);
             theseJets->at(jJet) = tmpJet;
           }
         }//jJet
       }//iJet

       return EL::StatusCode::SUCCESS;
     }


    #endif

public:
    const xAOD::JetContainer* m_truthJets; //!
    const xAOD::EventInfo* m_eventInfo; //!
    TLorentzVector m_recoilTLV; //!
    std::vector<const xAOD::IParticle*> m_recoilParticles; //!
    const xAOD::Photon* m_recoilPhoton; //!

    float m_prescale; //!

    enum Mode {MJB, GJET, ZEEJET, ZMMJET};

    Mode m_mode = MJB;

    /** @brief enum defining selection */
    enum SelType {PRE, SYST, RECOIL};

    /** @brief enum defining systematics */
    enum SysType {NOMINAL, JES, JER, JVT, SCALERES, OOC, PHOTONPURITY, JCS, CUTAsym, CUTAlpha, CUTBeta, CUTPt, CP, PERJETFLAVOR};

    bool cut_LeadEta(); //!
    bool cut_JetEta(); //!
    bool cut_MCCleaning(); //!
    bool cut_SubPt(); //!
    bool cut_JetPtThresh(); //!
    bool cut_LeadJetPtThresh(); //!
    bool cut_JVT(); //!
    bool cut_CleanJet(); //!
    bool cut_TriggerEffRecoil(); //!
    bool cut_PtAsym(); //!
    bool cut_Alpha(); //!
    bool cut_Beta(); //!
    bool cut_ConvPhot(); //!
    bool cut_OverlapRemoval(); //!
    bool cut_Zmass(); //!
    bool cut_OS(); //!

    std::vector< xAOD::Jet*>* m_jets; //!
    /** @brief vector of track particles for the KTerm histograms*/
    std::vector< xAOD::TrackParticle_v1*>* m_tracks; //!

    std::vector< std::function<bool(void)> > m_preSelections;  //!
    std::vector< std::function<bool(void)> > m_systSelections;  //!

    std::vector< SelType > m_selType;
    std::vector< std::function<bool(void)> > m_selections;  //!


  private:

    /** @brief AnaToolHandle for each CP tool*/
    asg::AnaToolHandle<IJetCalibrationTool> m_JetCalibrationTool_handle{"JetCalibrationTool/JetCalib_regular"}; //!
    asg::AnaToolHandle<IJetCalibrationTool> m_JetCalibrationTool_handle_recoilJets{"JetCalibrationTool/JetCalib_recoilJets"}; //!
    asg::AnaToolHandle<ICPJetUncertaintiesTool> m_JetUncertaintiesTool_handle{"JetUncertaintiesTool"}; //!

    asg::AnaToolHandle<CP::IJetTileCorrectionTool>  m_JetTileCorrectionTool_handle{"JetTileCorrectionTool"}; //!

    /** @brief JVT handles, including systematic variations*/
    asg::AnaToolHandle<IJetUpdateJvt> m_JVTUpdateTool_handle{"JetVertexTaggerTool"}; //!
    asg::AnaToolHandle<CP::IJetJvtEfficiency> m_JetJVTEfficiencyTool_handle{"JetJVTEfficiencyTool/JVT_nominal"}; //!
    asg::AnaToolHandle<CP::IJetJvtEfficiency> m_JetJVTEfficiencyTool_handle_up{"JetJVTEfficiencyTool/JVT_up"}; //!
    asg::AnaToolHandle<CP::IJetJvtEfficiency> m_JetJVTEfficiencyTool_handle_down{"JetJVTEfficiencyTool/JVT_down"}; //!

    /** @brief Vector of b-tagging handles, for each WP*/
    std::vector< asg::AnaToolHandle<IBTaggingSelectionTool> >  m_AllBTaggingSelectionTool_handles; //!
    std::vector< asg::AnaToolHandle<IBTaggingEfficiencyTool> > m_AllBTaggingEfficiencyTool_handles; //!



  public:
    /** @brief Standard constructor*/
    InsituBalanceAlgo ();
//    /** @brief Standard destructor*/
//    ~InsituBalanceAlgo ();

    /** @brief Setup the job (inherits from Algorithm)*/
    virtual EL::StatusCode setupJob (EL::Job& job);
    /** @brief Execute the file (inherits from Algorithm)*/
    virtual EL::StatusCode fileExecute ();
    /** @brief Initialize the output histograms before any input file is attached (inherits from Algorithm)*/
    virtual EL::StatusCode histInitialize ();
    /** @brief Change to the next input file (inherits from Algorithm)*/
    virtual EL::StatusCode changeInput (bool firstFile);
    /** @brief Initialize the input file (inherits from Algorithm)*/
    virtual EL::StatusCode initialize ();
    /** @brief Execute each event of the input file (inherits from Algorithm)*/
    virtual EL::StatusCode execute ();
    /** @brief End the event execution (inherits from Algorithm)*/
    virtual EL::StatusCode postExecute ();
    /** @brief Finalize the input file (inherits from Algorithm)*/
    virtual EL::StatusCode finalize ();
    /** @brief Finalize the histograms after all files (inherits from Algorithm)*/
    virtual EL::StatusCode histFinalize ();

  // these are the functions not inherited from Algorithm
    /** @brief Configure the variables once before running*/
    virtual EL::StatusCode configure ();

    /** @brief Used to distribute the algorithm to the workers*/
    ClassDef(InsituBalanceAlgo, 1);
};

#endif
