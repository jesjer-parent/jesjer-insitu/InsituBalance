#ifndef MJB_KTermHists_H
#define MJB_KTermHists_H
/** @file KTermHists.h
 *  @brief Manage the KTerm histograms, based on MultijetHists
 *  @author Fabrizio: fabrizio.napolitano@cern.ch
 *  @bug ...
 */

#include <EventLoop/Worker.h>
#include "xAODAnaHelpers/JetHists.h"
#include "xAODAnaHelpers/HelperClasses.h"
#include <xAODJet/JetContainer.h>
#include <xAODJet/Jet.h>
#include "xAODTracking/TrackParticleContainer.h"
#include <InsituBalance/InsituBalanceAlgo.h>




class KTermHists : public JetHists
{
  public:

    /** @brief Verbose mode*/
    bool m_debug;

    asg::AnaToolHandle<IJetUpdateJvt> m_JVTUpdateTool_handle{"JetVertexTaggerTool"}; //!
    asg::AnaToolHandle<CP::IJetJvtEfficiency> m_JetJVTEfficiencyTool_handle{"JetJVTEfficiencyTool"}; //!

    /** @brief Standrad constructor
     * @param name        The name of the KTermHists isntance
     * @param detailStr   A string consists of substring details which determine which sets of histograms to include*/
    KTermHists(std::string name, std::string detailStr);
    /** @brief Standard destructor*/
    ~KTermHists() {};

    using JetHists::execute;
    using JetHists::initialize;
    using JetHists::record;

    /** @brief Initialize all histograms */
    StatusCode initialize( );
    /** @brief Execute function for only the MJB-defined histograms
     * @param jets        A vector of xAOD::Jet pointers
     * @param eventInfo   The xAOD::EventInfo of the event*/
    StatusCode execute( std::vector< xAOD::Jet* >* jets, const xAOD::EventInfo* eventInfo, std::vector< xAOD::TrackParticle_v1* >* tracks, TLorentzVector recoilTLV );


    /** @brief Execute function inherited from JetHists, filling non-MJB jet histograms
     * @param jets        An xAODLLJetContainer of the jets
     * @param eventWeight The weight to be applied to each event*/
    // StatusCode execute( const xAOD::JetContainer* jets, float eventWeight ) { return JetHists::execute( jets, eventWeight); };

    void record(EL::Worker* wk);

  private:

    Double_t truncatedArea(double etaJ, double phiJ, double R, double eta0 = 2.5 );
    /** @brief Number of bins in the recoil system \f$p_{T}\f$ histogram */

    int m_numBins;

    std::vector<Double_t> deltaRBins  { // Array of track delta R bin edges
	  0 , 0.1 , 0.2 , 0.3 , 0.4 , 0.5 , 0.6 , 0.7 , 0.8 , 0.9 ,
	  1 , 1.1 , 1.2 , 1.3 , 1.4 , 1.5 , 1.6 , 1.7 , 1.8 , 1.9 ,
	  2 , 2.1 , 2.2 , 2.3 , 2.4 , 2.5 , 2.6 , 2.7 , 2.8 , 2.9 , 3
	  };
    std::vector<Double_t> ptrefbins { // Array of jet pt  bin edges
	  17., 20., 25., 30., 35., 45.,
	  60., 80., 110., 160., 210.,
	  260., 350., 500., 1000., 5000.
	  };
    TProfile* h_kTerm_tmp; //!
    TH1F* h_ptref; //!
    TH1F* h_dR; //!
    std::vector< TProfile* > h_kTerm_ptvect;       //!
    std::vector< TH1F* > h_kdR_ptvect;       //!

};

#endif
