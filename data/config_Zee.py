import ROOT
import sys,os
sys.path.insert(0, os.environ['WorkDir_DIR']+"/data/InsituBalance/")
from xAH_config import xAH_config
c = xAH_config()

#--- Choose which type of MC or data you are using ---#
inputType = "2017"
#jetCont = "EMTopo"
#jetCont = "EMPFlow"
jetCont = "EMTopoLowPt"
jetRadius = "4"

if not inputType in ["mc16a", "mc16d", "2015", "2016", "2017"]:
  print("Error, inputType is not a valid format, but is ", inputType)
  exit(1)

from BaseAlgos import *

#--- General information on the jet type ---#

if "EMTopo" in jetCont:
  calibConfig         = "JES_data2017_2016_2015_Consolidated_EMTopo_2018_Rel21.config"
elif jetCont == "EMPFlow":
  calibConfig         = "JES_data2017_2016_2015_Consolidated_PFlow_2018_Rel21.config"

#Base_InsituBalanceAlgo.update({ "m_CalibArea"         : "00-04-82" })

#-- JetUncertaintiesTool --#
Base_InsituBalanceAlgo.update({ "m_jetUncertaintyConfig" : "InsituBalance/Uncertainties_Fall2018/R4_JESNuisanceParametersForMJB.config"  })

if inputType in ["mc16a", "2015", "2016"]:
  Base_BasicEventSelection.update({ "m_triggerSelection" : "HLT_2e17_lhvloose_nod0" })
  Base_InsituBalanceAlgo.update({ "m_triggerAndPt" : "HLT_2e17_lhvloose_nod0:-1" })
elif inputType in ["2017", "mc16d"]:
  Base_BasicEventSelection.update({ "m_triggerSelection" : "HLT_2e17_lhvloose_nod0_L12EM15VHI" })
  Base_InsituBalanceAlgo.update({ "m_triggerAndPt" : "HLT_2e17_lhvloose_nod0_L12EM15VHI:-1" })

#--- Configure BaseEventSelection and InsituBalanceAlgo ---#
Base_BasicEventSelection.update({ "m_derivationName"  : "JETM3Kernel" })
c.algorithm("BasicEventSelection", Base_BasicEventSelection)


c.algorithm("ElectronCalibrator", {
    "m_name"                : "ElectronCalibAlgo",
    "m_msgLevel"            : "info",
    "m_inContainerName"     : "Electrons",
    "m_inputAlgoSystNames"  : "",
    "m_outContainerName"    : "Electrons_Calib",
    "m_outputAlgoSystNames" : "Electrons_Calib_Syst",
    "m_esModel"             : "es2017_R21_v1",
    "m_decorrelationModel"  : "1NP_v1",
    "m_systName"            : systCP,
    "m_systVal"             : 1.0,
    } )

c.algorithm("ElectronSelector", {
    "m_name"                      : "ElectronSelectAlgo",
    "m_msgLevel"                  : "info",
    "m_inContainerName"           : "Electrons_Calib",
    "m_outContainerName"          : "Electrons_Selected",
    "m_inputAlgoSystNames"        : "Electrons_Calib_Syst",
    "m_outputAlgoSystNames"       : "Electrons_Selected_Syst",
    "m_createSelectedContainer"   : True,
    "m_decorateSelectedObjects"   : True,
    "m_pass_min"                  : 2,
    "m_pass_max"                  : 2,
    "m_pT_min"                    : 20e3,
    "m_eta_max"                   : 2.47,
    "m_vetoCrack"                 : True,
    "m_d0sig_max"                 : 5.0,
    "m_z0sintheta_max"            : 0.5,
    "m_doAuthorCut"               : False,
    "m_doOQCut"                   : True,
    "m_doLHPIDcut"                : True,
    "m_LHOperatingPoint"          : "Medium",
    "m_doCutBasedPID"             : False,
    "m_readIDFlagsFromDerivation" : True,
#    "m_MinIsoWPCut"               : "Loose",
#    "m_IsoWPList"                 : "Loose,GradientLoose,Gradient,FixedCutLoose,FixedCutTight,FixedCutTightTrackOnly",
    "m_MinIsoWPCut"               : "Gradient",
    "m_IsoWPList"                 : "Gradient", #Is necessary
#    "m_singleElTrigChains"        : "HLT_2e15_lhvloose_nod0_L12EM13VH,HLT_2e17_lhvloose_nod0",  #For trigger matching
    } )

#### Gjet collection and observables ####
Base_InsituBalanceAlgo.update({ "m_name" : "ZeeJetBalance" })
Base_InsituBalanceAlgo.update({ "m_modeStr" : "Zeejet" })
Base_InsituBalanceAlgo.update({ "m_inContainerName_ref" : "Electrons_Selected" })
Base_InsituBalanceAlgo.update({ "m_inputAlgoSystNames_ref" : "Electrons_Selected_Syst" })
Base_InsituBalanceAlgo.update({ "m_CPSystNames" : "ele_SystElectronCalibAlgo" })
Base_InsituBalanceAlgo.update({ "m_elDetailStr" : "NLeading3 kinematic" })

Base_InsituBalanceAlgo.update({ "m_binning" : "17,20,25,30,35,40,45,50,60,70,80,100,120,140,160,200,250,300,500,700,1000" })

Base_InsituBalanceAlgo.update({ "m_alpha" : 0.35 })
Base_InsituBalanceAlgo.update({ "m_ptAsymVar" : 0.1 })
Base_InsituBalanceAlgo.update({ "m_ptAsymMin" : 15 })
Base_InsituBalanceAlgo.update({ "m_leadJetPtThresh" : 10 })
Base_InsituBalanceAlgo.update({ "m_ptThresh" : 8 })
Base_InsituBalanceAlgo.update({ "m_overlapDR" : 0.2 })
Base_InsituBalanceAlgo.update({ "m_beta" : -1 })
Base_InsituBalanceAlgo.update({ "m_numJets" : 1 })

Base_InsituBalanceAlgo.update({ "m_inContainerName_jets" : "AntiKt"+jetRadius+jetCont+"Jets" })
Base_InsituBalanceAlgo.update({ "m_jetDef" : "AntiKt"+jetRadius+jetCont.replace('LowPt','') })
Base_InsituBalanceAlgo.update({ "m_jetCalibConfig" : calibConfig })

Base_InsituBalanceAlgo.update({ "m_sysVariations" : systInsitu })

# Flag to output KTerm histograms
#Base_InsituBalanceAlgo.update({ "m_makeKTerm" : True })
Base_InsituBalanceAlgo.update({ "m_match_track_jets" : True })
Base_InsituBalanceAlgo.update({ "m_match_truth_jets" : True })

c.algorithm("InsituBalanceAlgo", Base_InsituBalanceAlgo)

