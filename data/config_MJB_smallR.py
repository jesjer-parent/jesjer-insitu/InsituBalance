
import ROOT, sys,os
from xAODAnaHelpers import Config as xAH_config
c = xAH_config()
sys.path.insert(0, os.environ['WorkDir_DIR']+"/data/InsituBalance/")

#--- Choose which type of MC or data you are using ---#
inputType = "2017"
jetCont = "EMTopo"
#jetCont = "EMPFlow"

if not inputType in ["mc16a", "mc16d", "mc16e", "2015", "2016", "2017", "2018"]:
  print("Error, inputType is not a valid format, but is ", inputType)
  exit(1)

from BaseAlgos import *

#--- General information on the jet type ---#
if jetCont == "EMTopo":
  if inputType in ["mc16a", "mc16d", "mc16e"]:
    # calibConfig_recoilJets         = "JES_data2017_2016_2015_Recommendation_Feb2018_rel21.config" #MC only
    # calibConfig = "JES_data2017_2016_2015_Recommendation_Feb2018_rel21.config" #MC only
    calibConfig_recoilJets         = "JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config"
    calibConfig = "JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config"
  elif inputType in ["2015", "2016"]:
    calibConfig_recoilJets         = "JES_data2017_2016_2015_VJES_EMTopo_Oct2018_rel21.config" #V+jet
    calibConfig = "JES_MC16Recommendation_EtaInterCalibrationOnly_20Dec2017.config" #eta-intercalibration only
  elif inputType in ["2017"]:
    # calibConfig_recoilJets         = "JES_data2017_2016_2015_VJES_EMTopo_Oct2018_rel21.config" #V+jet
    # calibConfig = "JES_2017data_4EM_EtaInterCalibrationOnly_08Feb2018.config" #eta-intercalibration only
    calibConfig_recoilJets         = "JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config"
    calibConfig = "JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config"
  elif inputType in ["2018"]:
    # calibConfig_recoilJets         = "JES_data2017_2016_2015_VJES_EMTopo_Oct2018_rel21.config" #V+jet
    # calibConfig = "JES_2017data_4EM_EtaInterCalibrationOnly_08Feb2018.config" #eta-intercalibration only
    calibConfig_recoilJets         = "JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config"
    calibConfig = "JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config"
elif jetCont == "EMPFlow":
  if inputType in ["mc16a", "mc16d", "mc16e"]:
    # calibConfig_recoilJets         = "JES_data2017_2016_2015_Recommendation_PFlow_Feb2018_rel21.config" #MC only
    # calibConfig = "JES_data2017_2016_2015_Recommendation_PFlow_Feb2018_rel21.config" #MC only
    calibConfig_recoilJets         = "JES_MC16Recommendation_Consolidated_PFlow_Apr2019_Rel21.config"
    calibConfig = "JES_MC16Recommendation_Consolidated_PFlow_Apr2019_Rel21.config"
  elif inputType in ["2015", "2016"]:
    calibConfig_recoilJets         = "JES_data2017_2016_2015_VJES_PFlow_Oct2018_rel21.config" #V+jet
    calibConfig = "JES_MC16Recommendation_PFlow_EtaInterCalibrationOnly_20Dec2017.config" #eta-intercalibration only
  elif inputType in ["2017"]:
    # calibConfig_recoilJets         = "JES_data2017_2016_2015_VJES_PFlow_Oct2018_rel21.config" #V+jet
    # calibConfig = "JES_2017data_PFlow_EtaInterCalibrationOnly_08Feb2018.config" #eta-intercalibration only
    calibConfig_recoilJets         = "JES_MC16Recommendation_Consolidated_PFlow_Apr2019_Rel21.config"
    calibConfig = "JES_MC16Recommendation_Consolidated_PFlow_Apr2019_Rel21.config"
  elif inputType in ["2018"]:
    # calibConfig_recoilJets         = "JES_data2017_2016_2015_VJES_PFlow_Oct2018_rel21.config" #V+jet
    # calibConfig = "JES_2017data_PFlow_EtaInterCalibrationOnly_08Feb2018.config" #eta-intercalibration only
    calibConfig_recoilJets         = "JES_MC16Recommendation_Consolidated_PFlow_Apr2019_Rel21.config"
    calibConfig = "JES_MC16Recommendation_Consolidated_PFlow_Apr2019_Rel21.config"

Base_InsituBalanceAlgo.update({ "m_inContainerName_jets" : "AntiKt4"+jetCont+"Jets" })
Base_InsituBalanceAlgo.update({ "m_jetDef" : "AntiKt4"+jetCont })
Base_InsituBalanceAlgo.update({ "m_jetCalibConfig" : calibConfig })

Base_InsituBalanceAlgo.update({ "m_jetDef_recoilJets" : "AntiKt4"+jetCont })
Base_InsituBalanceAlgo.update({ "m_jetCalibConfig_recoilJets" : calibConfig_recoilJets })

Base_InsituBalanceAlgo.update({ "m_CalibArea_recoilJets" : "00-04-82" })
Base_InsituBalanceAlgo.update({ "m_CalibArea"         : "00-04-82" })

Base_InsituBalanceAlgo.update({ "m_largeR" : "False" })
Base_InsituBalanceAlgo.update({ "m_mcType" : "MC16" })
Base_InsituBalanceAlgo.update({ "m_mcType_recoilJets" : "MC16" })

#-- JetUncertaintiesTool --#
#Base_InsituBalanceAlgo.update({ "m_jetUncertaintyConfig" : "InsituBalance/Uncertainties_Fall2018/R4_JESNuisanceParametersForMJB.config"  })
#Base_InsituBalanceAlgo.update({ "m_jetUncertaintyConfig" : "dev/Fall2018/R4_JESNuisanceParametersForMJB.config"  })
Base_InsituBalanceAlgo.update({ "m_jetUncertainty_CalibArea" : "CalibArea-07" })
Base_InsituBalanceAlgo.update({ "m_jetUncertaintyConfig" : "rel21/Fall2018/R4_AllNuisanceParameters_AllJERNP.config"  })

#--- Configure BaseEventSelection and InsituBalanceAlgo ---#
Base_BasicEventSelection.update({ "m_derivationName"  : "JETM1Kernel" })

if inputType in ["mc16a", "2015"]:
  Base_BasicEventSelection.update({ "m_triggerSelection" : "HLT_j360|HLT_j260|HLT_j175|HLT_j110" })
  Base_InsituBalanceAlgo.update({ "m_triggerAndPt" : "HLT_j360:500,HLT_j260:400,HLT_j175:300,HLT_j110:200" })
#  Base_InsituBalanceAlgo.update({ "m_HLT_trigger_jet_container" : "HLT_xAOD__JetContainer_a4tcemsubjesFS" })
elif inputType in ["2016"]:
  Base_BasicEventSelection.update({ "m_triggerSelection" : "HLT_j380|HLT_j260|HLT_j175|HLT_j110" })
  Base_InsituBalanceAlgo.update({ "m_triggerAndPt" : "HLT_j380:500,HLT_j260:400,HLT_j175:300,HLT_j110:200" })
#  Base_InsituBalanceAlgo.update({ "m_HLT_trigger_jet_container" : "HLT_xAOD__JetContainer_a4tcemsubjesFS" })
elif inputType in ["2017", "mc16d"]:
  Base_BasicEventSelection.update({ "m_triggerSelection" : "HLT_j420|HLT_j360|HLT_j260|HLT_j175|HLT_j110" })
  Base_InsituBalanceAlgo.update({ "m_triggerAndPt" : "HLT_j420:550,HLT_j360:500,HLT_j260:400,HLT_j175:300,HLT_j110:200" })
#  Base_InsituBalanceAlgo.update({ "m_HLT_trigger_jet_container" : "HLT_xAOD__JetContainer_a4tcemsubjesISFS" })
elif inputType in ["2018", "mc16e"]:
  # Base_BasicEventSelection.update({ "m_triggerSelection" : "HLT_j420|HLT_j360|HLT_j260|HLT_j175|HLT_j110|HLT_j85|L1_J100|L1_J75|L1_J50|L1_J30|L1_J20" })
  Base_BasicEventSelection.update({ "m_triggerSelection" : "HLT_j420|HLT_j360|HLT_j260|HLT_j175|HLT_j110" })
  Base_InsituBalanceAlgo.update({ "m_triggerAndPt" : "HLT_j420:550,HLT_j360:500,HLT_j260:400,HLT_j175:300,HLT_j110:200" })
  # Base_InsituBalanceAlgo.update({ "m_HLT_trigger_jet_container" : "HLT_xAOD__JetContainer_a4tcemsubjesISFS" })

#--- MJB version of InsituBalanceAlgo ---#
Base_InsituBalanceAlgo.update({ "m_name" : "MultiJetBalance" })
Base_InsituBalanceAlgo.update({ "m_modeStr" : "MJB" })

#Base_InsituBalanceAlgo.update({ "m_binning" : "200,250,300,350,400,450,500,550,600,650,700,750,800,850,900,950,1000,1050,1100,1150,1200,1300,1500,1700,2000,2300,2600,3000,3500" })
#Base_InsituBalanceAlgo.update({ "m_binning" : "200,250,300,350,400,450,500,550,600,650,700,750,800,850,900,950,1000,1050,1100,1150,1200,1250,1300,1350,1400,1450,1500,1600,1700,1800,1900,2000,2250,2500,2750,3000" })
Base_InsituBalanceAlgo.update({ "m_binning" : "200,250,300,350,400,450,500,550,600,650,700,750,800,850,900,950,1000,1050,1100,1150,1200,1300,1450,1600,1900,2200,2500,3000" })

Base_InsituBalanceAlgo.update({ "m_alpha" : 0.3 })
Base_InsituBalanceAlgo.update({ "m_beta" : 1.0 })
Base_InsituBalanceAlgo.update({ "m_betaPtVar" : 0.05 })

Base_InsituBalanceAlgo.update({ "m_ptAsymVar" : 0.8 })
Base_InsituBalanceAlgo.update({ "m_ptAsymMin" : 0 })
Base_InsituBalanceAlgo.update({ "m_ptThresh" : 25 })
Base_InsituBalanceAlgo.update({ "m_jetEta" : 2.8 })
Base_InsituBalanceAlgo.update({ "m_numJets" : 3 })


Base_InsituBalanceAlgo.update({ "m_sysVariations" : systInsitu })

Base_InsituBalanceAlgo.update({ "m_MJBIteration" : 2 })
Base_InsituBalanceAlgo.update({ "m_emulate_trigger" : False })

Base_InsituBalanceAlgo.update({ "m_subjetThreshold_Vjet" : 1200*1e9})
Base_InsituBalanceAlgo.update({ "m_subjetThreshold_MJB" : -1})
Base_InsituBalanceAlgo.update({ "m_MJBCorrectionFile" : "" })

## Example for running 2nd iteration of MJB
#Base_InsituBalanceAlgo.update({ "m_subjetThreshold_Vjet" : 1200*1e3})
#Base_InsituBalanceAlgo.update({ "m_subjetThreshold_MJB" : 1800*1e3})
#Base_InsituBalanceAlgo.update({ "m_MJBCorrectionFile" : "InsituBalance/EM_DoubleRatio_Data_Py8_graphs.root" })

#--- Load algorithms in correct order ---#
c.algorithm("BasicEventSelection", Base_BasicEventSelection)
c.algorithm("InsituBalanceAlgo", Base_InsituBalanceAlgo)
