#!/usr/bin/env python

### Define systematics to use ###
# MC should have systInstu "All" and systCP "", while data has "All" for both
#systCP = "" #"All"
#systInsitu  = "All" #""

#Split systInsitu for data
#systInsitu = "All"
systInsitu = "Nominal"
#systInsitu = "Nominal-EvSel"
#systInsitu = "Nominal-EvSel-JVT"
#systInsitu  = "Nominal-JCS-JES-JER"
#systInsitu = "GRID1"
#systInsitu = "GRID2"
#systInsitu = "GRID3"
#systInsitu = "GRID4"

GRLs = [
        "GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml",
        "GoodRunsLists/data16_13TeV/20180129/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml",
        "GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml",
        "GoodRunsLists/data18_13TeV/20190318/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"
        ]

iLumis = [
          "GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root",
          "GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root",
          "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root",
          "GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root"
          ]


GRLlist = ','.join(GRLs)
Lumilist = ','.join(iLumis)

Base_BasicEventSelection = {
  "m_name"                       : "BaseEventSelectionAlgo",
  "m_applyGRLCut"                : True,
  "m_msgLevel"                   : "info",
  "m_useMetaData"                : True,
  "m_storePassHLT"               : True,
  "m_storeTrigDecisions"         : True,
  "m_applyTriggerCut"            : True,
  "m_checkDuplicatesData"        : False,
  "m_applyEventCleaningCut"      : True,
  "m_applyPrimaryVertexCut"      : True,
  "m_doPUreweighting"            : True,
  "m_autoconfigPRW"              : True,
  "m_calcBCIDInfo"               : False,
  "m_GRLxml"                     : GRLlist,
  "m_lumiCalcFileNames"          : Lumilist,
  #Can override mcCampaign if needed
  #"m_mcCampaign" : "mc16d",
  }

### MJB Configuration  ###
Base_InsituBalanceAlgo = {

  "m_name"                : "PileupStudies",
  "m_msgLevel"                   : "info",

  #------ Event Selections ------#
  "m_numJets"  : 1,
  "m_jetEta"    : 4.49,
  "m_beta"      : -1,

  #------ Bootstrap Mode ------#
  #If MJB, don't forget to set m_MJBIteration!
  "m_bootstrap" : True, #Set to True do the full run
  "m_systTool_nToys" : 200,

  #------ B-tag Mode : Not yet implemented ------#
  #"m_bTagWPsString" : "77,85",
  #"m_bTagFileName" : "$ROOTCOREBIN/data/xAODAnaHelpers/2016-Winter-13TeV-MC15-CDI-February5_prov.root",
  #"m_bTagVar"  : "MV2c20",
  #"m_bTagLeadJetWP" : "",
  #"m_leadingInsitu" : True,
  #"m_noLimitJESPt" :  True,

  #------ Closure Test Mode ------#
  #Apply MJB correction to the leading jet:
  #"m_closureTest" : True,

  ### Plotting Options ###
  "m_writeTree" : False, #Best to keep False, it is only good for debugging specific systematics (would suggest keeping this false and setting the up sys to what you want to check)
  "m_writeNominalTree" : True,
  #"m_MJBDetailStr"  : "trackjet truthjet",
  #"m_MJBDetailStr" : "extraMJB bTag85 bTag77 emulateTrigger",
  #"m_jetDetailStr" : "kinematic truth truth_details flavorTag sfFTagVL sfFTagL sfFTagM sfFTagT",

  # "m_eventDetailStr" : "",
  # "m_jetDetailStr" : "kinematic jvt trackjet truthjet",
  # "m_trigDetailStr" : "basic passTriggers",

  "m_eventDetailStr" : "pileup shapeEM shapeLC bcidInfo",
  "m_jetDetailStr" : "kinematic layer trackAll energy",
  "m_trigDetailStr" : "basic passTriggers",

  ###--- Tigger Study Settings ---###
  # "m_eventDetailStr" : "pileup shapeEM shapeLC bcidInfo emulateTrigger",
  # "m_jetDetailStr" : "kinematic",
  # "m_trigDetailStr" : "basic passTriggers prescales passTrigBits",
  # "m_MJBDetailStr"  : "emulateTrigger",

  ### Extra Options ###
  ## Remove problematic Pileup events from low pt MC slices:
  "m_MCPileupCheckContainer" : "AntiKt4TruthJets",

  #"m_isAFII"     : True,
  #"m_isDAOD"     : True,
  #"m_useCutFlow" : False,

  #---------------------------------------------------------------------------------------------
  #### Tool Configurations ####

  #-- JVT --#
  "m_JVTWP" : "Medium", # 2016

  #-- TileCorrectionTool --#
  "m_TileCorrection"      : False

  }

