import ROOT, sys,os
from xAODAnaHelpers import Config as xAH_config
c = xAH_config()
sys.path.insert(0, os.environ['WorkDir_DIR']+"/data/InsituBalance/")

do_mc16c = True
if do_mc16c:
  from BaseAlgos_mc16c import *
else:
  from BaseAlgos_mc16a import *

jetCont = "EMTopo"
#jetCont = "LCTopo"
#jetCont = "EMPFlow"
#Eta-intercalibration only config
calibConfig = "JES_MC16Recommendation_EtaInterCalibrationOnly_20Dec2017.config"
#calibConfig = "JES_MC16Recommendation_PFlow_EtaInterCalibrationOnly_20Dec2017.config"
#Unceratinty config
uncertConfig = "rel21/Moriond2018/R4_AllNuisanceParameters.config"
mcCamp = "MC16"

Base_BasicEventSelection.update({ "m_derivationName"  : "JETM4Kernel" })
Base_BasicEventSelection.update({ "m_triggerSelection" : "HLT_g140_loose|HLT_g120_loose|HLT_g100_loose|HLT_g80_loose|HLT_g70_loose|HLT_g60_loose|HLT_g50_loose_L1EM15|HLT_g45_loose_L1EM15|HLT_g40_loose_L1EM15|HLT_g35_loose_L1EM15|HLT_g25_loose_L1EM15|HLT_g20_loose_L1EM12" })
c.algorithm("BasicEventSelection", Base_BasicEventSelection)

c.algorithm("PhotonCalibrator", {
    "m_name"                : "PhotonCalibAlgo",
    "m_msgLevel"            : "info",
    "m_inContainerName"     : "Photons",
    "m_inputAlgoSystNames"  : "",
    "m_outContainerName"    : "Photons_Calib",
    "m_outputAlgoSystNames" : "Photons_Calib_Syst",
    "m_esModel"             : "es2017_R21_PRE",
    "m_decorrelationModel"  : "1NP_v1",
    "m_systName"            : systCP,
    "m_systVal"             : 1.0,
    } )

c.algorithm("PhotonSelector", {
    "m_name"                      : "PhotonSelectAlgo",
    "m_msgLevel"                  : "info",
    "m_inContainerName"           : "Photons_Calib",
    "m_outContainerName"          : "Photons_Selected",
    "m_inputAlgoSystNames"        : "Photons_Calib_Syst",
    "m_outputAlgoSystNames"       : "Photons_Selected_Syst",
    "m_createSelectedContainer"   : True,
    "m_decorateSelectedObjects"   : True,
    "m_pT_min"                    : 25e3,
    "m_eta_max"                   : 1.37, #2.37?
    "m_pass_min"                  : 1,
    "m_pass_max"                  : 1,
    "m_vetoCrack"                 : True,
    "m_doAuthorCut"               : True,
    "m_doOQCut"                   : True,
    "m_photonIdCut"               : "Tight",
    "m_IsoWPList"                 : "FixedCutTight",
    } )


#### Gjet collection and observables ####
Base_InsituBalanceAlgo.update({ "m_name" : "GammaJetBalance" })
Base_InsituBalanceAlgo.update({ "m_modeStr" : "Gjet" })
Base_InsituBalanceAlgo.update({ "m_inContainerName_ref" : "Photons_Selected" })
Base_InsituBalanceAlgo.update({ "m_inputAlgoSystNames_ref" : "Photons_Selected_Syst" }) #All systematics passing an event
Base_InsituBalanceAlgo.update({ "m_CPSystNames" : "photons_SystPhotonCalibAlgo" })  #All possible systematics
Base_InsituBalanceAlgo.update({ "m_photonDetailStr" : "NLeading3 kinematic" })

Base_InsituBalanceAlgo.update({ "m_triggerAndPt" : "HLT_g140_loose:150,HLT_g120_loose:130,HLT_g100_loose:110,HLT_g80_loose:90,HLT_g70_loose:80,HLT_g60_loose:70,HLT_g50_loose_L1EM15:60,HLT_g45_loose_L1EM15:55,HLT_g40_loose_L1EM15:50,HLT_g35_loose_L1EM15:45,HLT_g25_loose_L1EM15:35,HLT_g20_loose_L1EM12:30" })
Base_InsituBalanceAlgo.update({ "m_binning" : "30,35,40,45,50,55,60,70,80,90,100,110,120,130,140,150,160,170,180,190,200,220,240,260,280,300,350,400,450,500,600,800,1000,2500" })


Base_InsituBalanceAlgo.update({ "m_alpha" : 0.35 })
Base_InsituBalanceAlgo.update({ "m_ptAsymVar" : 0.1 })
Base_InsituBalanceAlgo.update({ "m_ptAsymMin" : 20 })
Base_InsituBalanceAlgo.update({ "m_leadJetPtThresh" : 20 })
Base_InsituBalanceAlgo.update({ "m_ptThresh" : 10 })
Base_InsituBalanceAlgo.update({ "m_overlapDR" : 0.2 }) #ASG rec is 0.4, but 0.2 is a more strict EVENT-level selection
Base_InsituBalanceAlgo.update({ "m_beta" : -1 })
Base_InsituBalanceAlgo.update({ "m_numJets" : 1 })


Base_InsituBalanceAlgo.update({ "m_inContainerName_jets" : "AntiKt4"+jetCont+"Jets" })
Base_InsituBalanceAlgo.update({ "m_jetDef" : "AntiKt4"+jetCont })
Base_InsituBalanceAlgo.update({ "m_jetCalibConfig" : calibConfig })
Base_InsituBalanceAlgo.update({ "m_jetUncertaintyConfig" : uncertConfig })
Base_InsituBalanceAlgo.update({ "m_mcType" : mcCamp })

Base_InsituBalanceAlgo.update({ "m_sysVariations" : systInsitu })

c.algorithm("InsituBalanceAlgo", Base_InsituBalanceAlgo)

#  "m_triggerAndPt" : "", #Leave this empty for efficiency studies!
  #Jamie's binning
  #"m_binning"   : "25, 45, 65, 85, 105, 125, 160, 210, 260, 310, 400, 500, 600, 800, 1000, 1200, 1400, 1600, 2000, 10000",
  #First attempt binning
  #"m_triggerAndPt" : "HLT_g140_loose:145,HLT_g120_loose:125,HLT_g100_loose:105,HLT_g80_loose:85,HLT_g70_loose:75,HLT_g60_loose:65,HLT_g50_loose:55,HLT_g35_loose:40,HLT_g25_loose:30,HLT_g20_loose:25,HLT_g15_loose:20,HLT_g10_loose:15",
  #"m_binning"   : "15, 20, 25, 30, 40, 55, 65, 75, 85, 105, 125, 145, 170, 200, 250, 300, 400, 500, 600, 800, 1000, 1200, 1400, 1600, 2000, 10000",

#  "m_triggerAndPt" : "HLT_g140_loose:150,HLT_g120_loose:130,HLT_g100_loose:110,HLT_g80_loose:90,HLT_g70_loose:80,HLT_g60_loose:70,HLT_g50_loose_L1EM15:60,HLT_g45_loose_L1EM15:55,HLT_g40_loose_L1EM15:50,HLT_g35_loose_L1EM15:45,HLT_g25_loose_L1EM15:35,HLT_g20_loose_L1EM12:30",
#  "m_binning"   : "30,35,40,45,50,55,60,70,80,90,100,110,120,130,140,150,160,170,180,190,200,220,240,260,280,300,350,400,450,500,600,800,1000,2500",

  #"m_triggerAndPt" : "HLT_g140_loose:145,HLT_g120_loose:125,HLT_g100_loose:105,HLT_g80_loose:85,HLT_g70_loose:75,HLT_g60_loose:65,HLT_g50_loose_L1EM15:55,HLT_g45_loose_L1EM15:50,HLT_g40_loose_L1EM15:45,HLT_g35_loose_L1EM15:40,HLT_g25_loose_L1EM15:30,HLT_g20_loose_L1EM12:25",
  #"m_binning"   : "25,30,35,40,45,50,55,65,75,85,95,105,125,145,170,200,250,300,400,500,600,800,1000,2500",
  #"m_binning"   : "25,30,35,40,45,50,55,65,75,85,95,105,125,145,170,200,250,300,400,500,600,800,1000,1200,1400,1600,2000,2500",

