 
import ROOT, sys,os
from xAODAnaHelpers import Config as xAH_config
c = xAH_config()
sys.path.insert(0, os.environ['WorkDir_DIR']+"/data/InsituBalance/")

#--- Choose which type of MC or data you are using ---#
inputType = "2015"

############### LARGE-R JET INFORMATION ###############
largeRJetCont = "LCTopoTrimmedPtFrac5SmallR20"
#largeRJetCont = "LCTopoCSSKSoftDropBeta100Zcut10"

jetContainerName_large = "AntiKt10"+largeRJetCont+"Jets"
jetDef_large = "AntiKt10"+largeRJetCont
calibArea_large = "00-04-82"

if largeRJetCont == "LCTopoTrimmedPtFrac5SmallR20":
  if inputType in ['2015', '2016', '2017']:
    calibSeq_large = "EtaJES_JMS_Insitu_InsituCombinedMass"
    calibConfig_large = "JES_MC16recommendation_FatJet_Trimmed_JMS_comb_3April2019.config"
  else:
    calibSeq_large = "EtaJES_JMS"
    calibConfig_large = "JES_MC16recommendation_FatJet_Trimmed_JMS_calo_12Oct2018.config"
elif largeRJetCont == "LCTopoCSSKSoftDropBeta100Zcut10":
  calibSeq_large = "EtaJES_JMS"
  calibConfig_large = "JES_MC16recommendation_FatJet_SoftDrop_JMS_calo_12Oct2018.config"

if inputType in ["mc16d", "2017"]:
  mcType_large = "MC16d"
elif inputType in ["mc16a", "2015", "2016"]:
  mcType_large = "MC16a"
#######################################################

############### SMALL-R JET INFORMATION ###############
smallRJetCont = "EMTopo"
#smallRJetCont = "EMPFlow"

jetContainerName_small = "AntiKt4"+smallRJetCont+"Jets"
jetDef_small = "AntiKt4"+smallRJetCont
uncertArea_small = "CalibArea-06"
uncertConfig = "rel21/Fall2018/R4_AllNuisanceParameters_AllJERNP.config"

calibArea_small = "00-04-82"
if smallRJetCont == "EMTopo":
  calibConfig_small = "JES_data2017_2016_2015_Consolidated_EMTopo_2018_Rel21.config"
elif smallRJetCont == "EMPFlow":
  calibConfig_small = "JES_data2017_2016_2015_Consolidated_PFlow_2018_Rel21.config"

if inputType in ["mc16a", "mc16d"]:
  calibSeq_small = "JetArea_Residual_EtaJES_GSC_Smear"
elif inputType in ["2015", "2016", "2017"]:
  calibSeq_small = "JetArea_Residual_EtaJES_GSC_Insitu"

mcType_small = "MC16"
#######################################################

if not inputType in ["mc16a", "mc16d", "2015", "2016", "2017"]:
  print("Error, inputType is not a valid format, but is ", inputType)
  exit(1)

from BaseAlgos import *

Base_InsituBalanceAlgo.update({ "m_largeR" : "True" })

#Set Leading Jet Info
Base_InsituBalanceAlgo.update({ "m_inContainerName_jets" : jetContainerName_large })
Base_InsituBalanceAlgo.update({ "m_jetDef" : jetDef_large })
Base_InsituBalanceAlgo.update({ "m_CalibArea" : calibArea_large })
Base_InsituBalanceAlgo.update({ "m_jetCalibSequence" : calibSeq_large })
Base_InsituBalanceAlgo.update({ "m_jetCalibConfig" : calibConfig_large })
Base_InsituBalanceAlgo.update({ "m_mcType" : mcType_large })

#Set Recoil Jets Info
Base_InsituBalanceAlgo.update({ "m_inContainerName_ref" : jetContainerName_small })
Base_InsituBalanceAlgo.update({ "m_jetDef_recoilJets" : jetDef_small })
Base_InsituBalanceAlgo.update({ "m_CalibArea_recoilJets" : calibArea_small })
Base_InsituBalanceAlgo.update({ "m_jetUncertainty_CalibArea" : uncertArea_small })
Base_InsituBalanceAlgo.update({ "m_jetUncertaintyConfig" : uncertConfig })
Base_InsituBalanceAlgo.update({ "m_mcType_recoilJets" : mcType_small })
Base_InsituBalanceAlgo.update({ "m_jetCalibConfig_recoilJets" : calibConfig_small })
Base_InsituBalanceAlgo.update({ "m_jetCalibSequence_recoilJets" : calibSeq_small })
    
#--- Configure BaseEventSelection and InsituBalanceAlgo ---#
Base_BasicEventSelection.update({ "m_derivationName"  : "JETM1Kernel" })

if inputType in ["mc16a", "2015"]:
  Base_BasicEventSelection.update({ "m_triggerSelection" : "HLT_j360|HLT_j260|HLT_j175|HLT_j110" })
  Base_InsituBalanceAlgo.update({ "m_triggerAndPt" : "HLT_j360:500,HLT_j260:400,HLT_j175:300,HLT_j110:200" })
#  Base_InsituBalanceAlgo.update({ "m_HLT_trigger_jet_container" : "HLT_xAOD__JetContainer_a4tcemsubjesFS" })
elif inputType in ["2016"]:
  Base_BasicEventSelection.update({ "m_triggerSelection" : "HLT_j380|HLT_j260|HLT_j175|HLT_j110" })
  Base_InsituBalanceAlgo.update({ "m_triggerAndPt" : "HLT_j380:500,HLT_j260:400,HLT_j175:300,HLT_j110:200" })
#  Base_InsituBalanceAlgo.update({ "m_HLT_trigger_jet_container" : "HLT_xAOD__JetContainer_a4tcemsubjesFS" })
elif inputType in ["2017", "mc16d"]:
  Base_BasicEventSelection.update({ "m_triggerSelection" : "HLT_j420|HLT_j360|HLT_j260|HLT_j175|HLT_j110" })
  Base_InsituBalanceAlgo.update({ "m_triggerAndPt" : "HLT_j420:550,HLT_j360:500,HLT_j260:400,HLT_j175:300,HLT_j110:200" })
#  Base_InsituBalanceAlgo.update({ "m_HLT_trigger_jet_container" : "HLT_xAOD__JetContainer_a4tcemsubjesISFS" })

#--- MJB version of InsituBalanceAlgo ---#
Base_InsituBalanceAlgo.update({ "m_name" : "MultiJetBalance" })
Base_InsituBalanceAlgo.update({ "m_modeStr" : "MJB" })

Base_InsituBalanceAlgo.update({ "m_binning" : "200,250,300,350,400,450,500,550,600,650,700,750,800,850,900,950,1000,1050,1100,1150,1200,1300,1500,1800,2100,2400,3000,3500" })

Base_InsituBalanceAlgo.update({ "m_alpha" : 0.3 })
Base_InsituBalanceAlgo.update({ "m_beta" : 1.5 })
Base_InsituBalanceAlgo.update({ "m_betaPtVar" : 0.1 })

Base_InsituBalanceAlgo.update({ "m_ptAsymVar" : 0.8 })
Base_InsituBalanceAlgo.update({ "m_ptAsymMin" : 0 })
Base_InsituBalanceAlgo.update({ "m_ptThresh" : 25 })
Base_InsituBalanceAlgo.update({ "m_jetEta" : 2.8 })
Base_InsituBalanceAlgo.update({ "m_numJets" : 3 })

Base_InsituBalanceAlgo.update({ "m_sysVariations" : systInsitu })

#Base_InsituBalanceAlgo.update({ "m_emulate_trigger" : True })

Base_InsituBalanceAlgo.update({ "m_subjetThreshold_Vjet" : 1200*1e9})
Base_InsituBalanceAlgo.update({ "m_subjetThreshold_MJB" : -1})
Base_InsituBalanceAlgo.update({ "m_MJBCorrectionFile" : "" })

#--- Load algorithms in correct order ---#
c.algorithm("BasicEventSelection", Base_BasicEventSelection)
c.algorithm("InsituBalanceAlgo", Base_InsituBalanceAlgo)
