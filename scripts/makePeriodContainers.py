# MakePeriodContainer.py Janury 2018 C.Clement - a small script to generate period containers ...
import sys
import pyAMI.client
import pyAMI.atlas.api as AtlasAPI

#
# see pyAMI documentation at https://ami.in2p3.fr/pyAMI/ and https://ami.in2p3.fr/pyAMI/pyAMI5_atlas_api.html
#
# list of existing fields for datasets
#
# [`ami_status`, `ami_tags`, `atlas_release`, `beam`, `completion`, `conditions_tag`, `cross_section`, `dataset_number`, `ecm_energy`, `events`, `generator_filter_efficienty`, `generator_name`, `generator_tune`, `geometry`, `in_container`, `job_config`, `ldn`, `modified`, `nfiles`, `pdf`, `period`, `physics_comment`, `physics_short`, `prodsys_status`, `production_step`, `project`, `requested_by`, `responsible`, `run_number`, `stream`, `total_size`, `transformation_package`, `trash_annotation`, `trash_date`, `trash_trigger`, `trigger_config`, `type`]

client = pyAMI.client.Client('atlas')

AtlasAPI.init()

# ####################
# ### User Inputs ####
# ####################
WantedYear = 15  # should be an integer ! 15 or 16 or 17 or 18 for 2015, 2016, 2017, 2018
Derivation = "JETM3"

WantedRecoTagList = {}
WantedRecoTagList["JETM4_15"] = [ "r9264_p3083_p3372", "r9412_p3083_p3372" ] # one needs to know / figure out this first
WantedRecoTagList["JETM4_16"] = [ "r9264_p3083_p3372"]
WantedRecoTagList["JETM4_17"] = [ "f822_m1799_p3372","f836_m1824_p3372","f837_m1824_p3372","f838_m1824_p3372","f839_m1824_p3372","f841_m1824_p3372","f843_m1824_p3372","f846_m1839_p3372","f847_m1839_p3372","f848_m1844_p3372","f851_m1850_p3372","f854_m1850_p3372","f857_m1855_p3372","f859_m1860_p3372","f867_m1860_p3372","f868_m1870_p3372","f868_m1879_p3372","f871_m1879_p3372","f873_m1879_p3372","f873_m1885_p3372","f877_m1885_p3372","f877_m1885_p3402","f877_m1892_p3402","f877_m1897_p3402","f886_m1885_p3372","f887_m1897_p3402","f889_m1902_p3402","f889_m1907_p3402","f894_m1902_p3402","f894_m1907_p3402"]

WantedRecoTagList["JETM3_15"] = [ "r9264_p3083_p3372","r9412_p3083_p3372" ]
WantedRecoTagList["JETM3_16"] = [ "r9264_p3083_p3372"]
WantedRecoTagList["JETM3_17"] = [ "f877_m1885_p3402","f877_m1892_p3402","f877_m1897_p3402","f887_m1897_p3402","f889_m1902_p3402","f889_m1907_p3402","f894_m1902_p3402","f894_m1907_p3402","f822_m1799_p3372","f836_m1824_p3372","f837_m1824_p3372","f838_m1824_p3372","f839_m1824_p3372","f841_m1824_p3372","f843_m1824_p3372","f846_m1839_p3372","f847_m1839_p3372","f848_m1844_p3372","f851_m1850_p3372","f854_m1850_p3372","f857_m1855_p3372","f859_m1860_p3372","f867_m1860_p3372","f868_m1870_p3372","f868_m1879_p3372","f871_m1879_p3372","f873_m1879_p3372","f873_m1885_p3372","f877_m1885_p3372","f886_m1885_p3372"]

WantedRecoTags = WantedRecoTagList[Derivation+'_'+str(WantedYear)]
WantedDerivation = "DAOD_"+Derivation

Version = "v01" # this will give a version number to the generated dataset

RunForReal = True # Set to False to just test the outputs and to True if you really want to create the DS after checking the outputs of this script

# ############## Done setting up user inputs  ###################

WantedPeriodList = [ ]

WantedProject = "data"+str(WantedYear)+"_13TeV"

# ### Extract list of run periods for the wanted year ###
WantedPeriodListDic = pyAMI.atlas.api.list_dataperiods(client, 2, year=WantedYear)

for period in WantedPeriodListDic :
    if period['projectName'] == WantedProject :
        if not period['period'] == 'VdM' : # ignore Van Der Meer
            WantedPeriodList.append(period['period'])

# Print summary of user inputs
print "=============== SUMMARY OF USER INPUTS ================"
print " "
print " Requested year       = ", 2000+WantedYear
print " Requested AMITags    = ", WantedRecoTags
print " Requested Derivation = ", WantedDerivation
print " Requested project    = ", WantedProject
print " Found following data periods :  "
for WantedPeriod in WantedPeriodList:
    print "   ", WantedPeriod

# ### Generate list of dataset dictionary "YearDatasetList" with WantedPattern and in WantedProject and with WantedDerivation  ###

WantedPattern = WantedProject+'%physics_Main%'

YearDatasetList = AtlasAPI.list_datasets(client, patterns = [ WantedPattern ] , fields = ['ldn', 'geometry', 'run_number', 'project', 'period', 'ami_status', 'ami_tags', 'atlas_release' ], type = WantedDerivation, project = WantedProject )

# ### Generate dataset lists for each run period  ###
WantedDSList = { }

# if want to do / overun the WantedPeriodList
WantedPeriodList = ["J"]

for WantedPeriod in WantedPeriodList :
    WantedDSList[WantedPeriod] = []

    for DSindex in range(1,len(YearDatasetList)):
        # print YearDatasetList[DSindex]
        currentDSname = YearDatasetList[DSindex]['ldn']
        currentDSproject = YearDatasetList[DSindex]['project']
        currentDSperiod = YearDatasetList[DSindex]['period']
        currentDSami_tags = YearDatasetList[DSindex]['ami_tags']
        currentDSrun_number = YearDatasetList[DSindex]['run_number']

        #print " "
        #print currentDSname
        #print currentDSproject
        #print currentDSperiod
        #print currentDSami_tags
        #print currentDSrun_number

        if currentDSproject == WantedProject :
            print "Found project   = ", WantedProject
            print "currentDSperiod = ", currentDSperiod
            print "WantedPeriod    = ", WantedPeriod
            if WantedPeriod in currentDSperiod :
                print "Found run in period ", WantedPeriod
                if currentDSami_tags in WantedRecoTags:
                    print "Found run ami tags ", currentDSami_tags
                    #WantedDSList.append(currentDSname)
                    print " currentDSname = ", currentDSname
                    WantedDSList[WantedPeriod].append(currentDSname)


print "DONE looping over dataset dictionary"

print "=========== LIST OF IDENTIFIED DS :"

# ### Loop over the wanted period and create the dataset ###
for WantedPeriod in WantedPeriodList:
    print ""
    print "===== WantedPeriod    ==>  ", WantedPeriod
    print "===== Nbr of Datasets ==>  ", len(WantedDSList[WantedPeriod])
    print "List of DS in period : ",WantedPeriod
    firstDS=True
    for DS in WantedDSList[WantedPeriod]:
        print DS
        if firstDS :
            ListOfDSWithComma=DS
            firstDS = False
        else :
            ListOfDSWithComma= ListOfDSWithComma+","+DS

    print ""
    print "List of comma separated DS: "
    print ""
    print ListOfDSWithComma

    # #######################################################################################################
    # To actually create the period container see
    # https://twiki.cern.ch/twiki/bin/view/AtlasProtected/CreateContainerAPI
    # and  /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/pyAmi/pyAMI-5.0.7/lib/pyAMI/atlas/api.py
    # #######################################################################################################

    CreationComment = WantedProject+" period"+WantedPeriod+" "+WantedDerivation+" "+Version
    SuperTagPart1 = "period"+WantedPeriod
    SuperTagPart2 = "grp"+str(WantedYear)+"_"+Version+"_"+WantedRecoTags[0][len(WantedRecoTags[0])-5:len(WantedRecoTags[0])]
    SuperTag = SuperTagPart1+","+SuperTagPart2

    print ""
    print "CreationComment = ", CreationComment
    print "SuperTag        = ", SuperTag

    COMMAND = "COMAPopulateSuperProductionDataset -rucioRegistration=\"yes\" -creationComment=\""+CreationComment+"\" -selectionType=\"run_config\" -superTag=\""+SuperTag+"\" -containedDatasets=\""+ ListOfDSWithComma +"\"  -separator=\",\""


    print COMMAND

    if RunForReal :
        output_of_creation_command = client.execute(COMMAND)
        print output_of_creation_command

    FinalContainerName = WantedProject+":"+WantedProject+"."+"period"+WantedPeriod+".physics_Main.PhysCont."+WantedDerivation+"."+SuperTagPart2

    print ""
    print "Created Data period container: ", FinalContainerName

    print ""
    print "check content of container with rucio list-content PeriodContainerName"

    print ""
    print "Did we run for real ? ",RunForReal
