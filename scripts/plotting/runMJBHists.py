###################################################################
# runMJBHists.py                                                  #
# Author Jeff Dandoy, UPenn                                       #
#                                                                 #
# This scripts run the full fitting & plotting machinery,         #
# manipulating the histograms created by InsituBalance and        #
# retrieved with downloadAndMerge.py.                             #
# Sequentially it will run MJBSamply.py, calcMJB.py, and          #
# makeCalibrationFile.py, and will create commands for            #
# plotHistograms.py.  Please refer to the README for instructions # 
#                                                                 #
# The stages can be turned off & on with f_add_MCsys,             #
# f_derive_bs_binning, f_fitting, and f_sys.  For your first time #
# you should set these to True one by one!                        # 
###################################################################

import glob, os, sys
import argparse, itertools
import array

import MJBSample, calcMJB, makeCalibrationFile
import ROOT

def main():
  ROOT.gErrorIgnoreLevel=ROOT.kWarning
  
  #!!!!!!!!!!!!!! Things for the user to change !!!!!!!!!!!!!!!!!#
  jet_type = "EM" #"PF"  #Easy type for switching between PF and EM jets
  lead_jet_type = "Trimmed" #"SoftDrop"
  mcType = ""
  
  #Do you have boostrap objects (and the time to run over them?)
  #Set to false if you just want to do straight fits
  bootstrap = True

  # Pt ranges for bootstrapping & making final results
  min_pt=200
  max_pt=3000

  # Set your directory structure here, if not providing it as an argument
  if len(args.workDir) == 0:
    args.workDir = "./gridOutput"

  #Set Name of nominal & systematic samples for actual results
  nominal_data = 'Data'
  nominal_mc = 'Py8'
  sys_mc = 'Shp'

  # Create sample objects & append them to the sample list
  # Tags are substrings that will be used to wildcard for your samples.  Make sure your samples names are sensible! 
  # You can use comma-separated tags for selecting more than one type of file 
  sample_list = {}
  sample_list['Py8'] = MJBSample.MCSample(name="Py8"+mcType, displayName="Pythia--8"+mcType,        tags="Py8"+mcType)
  sample_list['Py8'].isJZW = True #Important for Pythia8 JZW samples!
  sample_list['PP8'] = MJBSample.MCSample(name="PP8"+mcType, displayName="Powheg+Pythia--8"+mcType, tags="PP8"+mcType )
  sample_list['Shp'] = MJBSample.MCSample(name="Shp"+mcType, displayName="Sherpa--2.1"+mcType,      tags="Shp"+mcType)
  sample_list['Hw8'] = MJBSample.MCSample(name="Hw8"+mcType, displayName="Herwig--8"+mcType,        tags="Hw8"+mcType)
  #sample_list['D15'] = MJBSample.DataSample(name="Data15", displayName="Data15",         tags="Data15")
  #sample_list['D16'] = MJBSample.DataSample(name="Data16", displayName="Data16",         tags="Data16")
  #sample_list['Data17'] = MJBSample.DataSample(name="Data17", displayName="Data17",         tags="Data17")
  sample_list['Data'] = MJBSample.DataSample(name="Data", displayName="Data",         tags="Data")
  #sample_list['D151617'] = MJBSample.DataSample(name="Data15+16+17", displayName="Data15+16+17",         tags="Data")

  #Here is an example of defining samples if you want to do mc16d vs mc16a!
  # sample_list['Py8_d'] = MJBSample.MCSample(name="Py8_d", displayName="Pythia--8--MC16d",        tags="QCD_Py8_mc16d")
  # sample_list['Py8_d'].isJZW = True
  # sample_list['PP8_d'] = MJBSample.MCSample(name="PP8_d", displayName="Powheg+Pythia--8--MC16d", tags="QCD_PP8_mc16d" )
  # sample_list['S21_d'] = MJBSample.MCSample(name="S21_d", displayName="Sherpa--2.1--MC16d",      tags="QCD_Shp_mc16d")
  # sample_list['Hw7_d'] = MJBSample.MCSample(name="Hw7_d", displayName="Herwig--7--MC16d",        tags="QCD_Hw8_mc16d")
  # sample_list['Py8_a'] = MJBSample.MCSample(name="Py8_a", displayName="Pythia--8--MC16a",        tags="QCD_Py8_mc16a")
  # sample_list['Py8_a'].isJZW = True
  # sample_list['PP8_a'] = MJBSample.MCSample(name="PP8_a", displayName="Powheg+Pythia--8--MC16a", tags="QCD_PP8_mc16a" )
  # sample_list['S21_a'] = MJBSample.MCSample(name="S21_a", displayName="Sherpa--2.1--MC16a",      tags="QCD_Shp_mc16a")
  # sample_list['Hw7_a'] = MJBSample.MCSample(name="Hw7_a", displayName="Herwig--7--MC16a",        tags="QCD_Hw8_mc16a")
  # sample_list['D17'] = MJBSample.DataSample(name="Data17", displayName="Data--2017", tags="QCD_Data17")
  # sample_list['D16'] = MJBSample.DataSample(name="Data16", displayName="Data--2016", tags="QCD_Data16")
  # sample_list['D15'] = MJBSample.DataSample(name="Data15", displayName="Data--2015", tags="QCD_Data15")


  #Make directory for placing intermediary files
  if not os.path.exists(args.workDir+'/files'):
    os.makedirs(args.workDir+'/files')

  ### Gather input files for each sample ###
  #If redo is set to False for this sample, it will not redo the scale / hadd if the file exists
  for name, sample in sample_list.items():
    sample.setInputDir( args.workDir )
    sample.redo = False
    sample.gatherInput() #Makes 1 file from many
    if( bootstrap ):
      sample.gatherInput(bootstrap=True) #Makes 1 bootstrap from many

  # Add the nominal result from the systematic MC sample to the nominal MC sample
  # Then everything after this can treat it as a regular systematic variation!
  f_add_MCsys = True
  if( f_add_MCsys and nominal_mc and sys_mc):
    #Add the systematic MC to the nominal, to treat it as a systematic
    sample_list[nominal_mc].addMCSys( sample_list[sys_mc] )


  # Derive the significant binning if we're doing boostrapping
  # This runs many jobs in parallel and can take up to 2 hours (for 6 cores). Make sure your machine can run 6 jobs at once!
  f_derive_bs_binning = True
  if( f_derive_bs_binning and bootstrap):
    calcMJB.derive_rebinning(sample_data=sample_list[nominal_data], sample_mc=sample_list[nominal_mc], min_pt=min_pt, max_pt=max_pt, n_cores=6)

    # This version of the function will turn rebinning off, and will save the result of every toys fit
    # You probably don't need this unless you want to plot how all the toys behave, or compare the non-rebinned significance/RMS against the rebinned significance/RMS
    #calcMJB.derive_rebinning(sample_data=sample_list[nominal_data], sample_mc=sample_list[nominal_mc], min_pt=min_pt, max_pt=max_pt, save_all_toys=True)

  #Defien where the rebinning file is for the nominal samples
  if( bootstrap and nominal_data and nominal_mc):
    sample_list[nominal_data].rebin_file = args.workDir+'/files/rebinning_file.root'
    sample_list[nominal_mc].rebin_file = args.workDir+'/files/rebinning_file.root'


  #Do the fitting!
  f_fitting = True
  if( f_fitting ):

    for name, sample in sample_list.items():
      if( bootstrap ):
        calcMJB.derive_nominal_uncertainty(sample) #calculate nominal uncertainty from RMS of toys
        calcMJB.derive_balance(sample, rebin=False, nominal_only=False, toy_uncert=True) #for fitBalance for plotting
        if name == nominal_data or name == nominal_mc:
          calcMJB.derive_balance(sample, rebin=True, nominal_only=False, toy_uncert=True) #for fitBalance_rebin for final results

      else:
        calcMJB.derive_balance(sample, rebin=False, nominal_only=False, toy_uncert=False) #If no bootstrapping was done

  #Manipulate the results into systematic variations
  f_sys = True
  if( f_sys ):
    final_file_name =  os.path.dirname(sample_list[nominal_data].histFile)+'/DoubleRatio_'+sample_list[nominal_data].name+'_'+sample_list[nominal_mc].name+'.root'

    #Makes systematic ratios, and created pt-remapped TGraphs for the final result
    makeCalibrationFile.makeRatios(sample_list[nominal_data], sample_list[nominal_mc], final_file_name,  min_pt=min_pt, max_pt=max_pt, sys_mc_name=sys_mc, do_bootstrap=bootstrap)
   
    #Plot the systematic variations.  First plot them 1 by 1 by type 
    #sys_types = "Gjet,Zjet,Pileup,EvSel,PunchThrough,EtaIntercalibration,MCType,Flavor".split(',')
    sys_types = "Gjet,Zjet,MJB,Pileup,JVT,PunchThrough,EtaIntercalibration,MCType,Flavor".split(',')
    for this_sys in sys_types:
      makeCalibrationFile.plotUncertainties( final_file_name, systematics=this_sys, out_tag=this_sys, jetType=jet_type)
    sys_types = ','.join(sys_types)+',Statistical,All'

    #Plot all systematic types together, and create official (1-sided) systematic plots using remapped pt edges
    final_tgraph_file = final_file_name.replace('.root','_graphs.root')
    makeCalibrationFile.plotUncertainties( final_file_name, systematics=sys_types, out_tag='Final', jetType=jet_type, do_final_plot=True, tgraph_file=final_tgraph_file)


  #Build the full kinematic distribution plotting command for use with plotHistograms.py
  #This will not actually run the code, you must run it yourself!
  executable_path = os.path.dirname(sys.argv[0])
  if len(executable_path) > 0:
    executable_path += '/'
  plotCommand = 'python '+executable_path+'plotHistograms.py -b '
  plotCommand += " --inDir "+args.workDir+"/files/"
  plotCommand += " --outDir "+args.workDir+"/plots/"
  plotCommand += ' --bkgType '
  for name, sample in sample_list.items():
    if sample.isMC:
      fileName = os.path.basename(sample.histFile).replace('.root','')
      plotCommand += sample.displayName+':'+fileName+','
  plotCommand = plotCommand.rstrip(',')
  plotCommand += ' --dataType '
  for name, sample in sample_list.items():
    if not sample.isMC:
      fileName = os.path.basename(sample.histFile).replace('.root','')
      plotCommand += sample.displayName+':'+fileName+','
  plotCommand = plotCommand.rstrip(',')
  plotCommand += ' --plotDir MJB_Nominal '
  plotCommand += ' --plotRatio --ratioWRT data --lumi 80.5 --normToData --rebin 2 --balanceType=\"MJB\" --jetType '+jet_type
  plotCommand += ' --addOverflow --toyErrors '

  print( plotCommand )
  os.system(plotCommand)

#Command will look like:
#python plotHistograms.py -b  --inDir /home/jdandoy/disk/MJB/MJB_R21/workarea_PF//files/ --outDir /home/jdandoy/disk/MJB/MJB_R21/workarea_PF//plots/ --bkgType Pythia--8:Py8.all,Herwig--7:Hw7.all,Powheg+Pythia--8:PP8.all,Sherpa--2.1:S21.all --dataType Data:Data.all --plotDir MJB_Nominal  --plotRatio --ratioWRT data --lumi 80.5 --normToData --rebin 2 --balanceType="MJB" --addOverflow --toyErrors


#First thing that gets called, grabbing any arguments then running the main code
if __name__ == "__main__":
  parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot")
  parser.add_argument("-p", dest='printOnly', action='store_true', default=False, help="Do not run commands, but only print them to screen.")
  parser.add_argument("--dir", dest='workDir', default='', help="Path to work area.  Within this directory should be hist/.")
  args = parser.parse_args()

  main()
  print("Finished runMJBHists.py")
