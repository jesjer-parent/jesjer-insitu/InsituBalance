#!/usr/bin/env python

###################################################################
# makeCalibraitonFiles.py                                         #
# Author Jeff Dandoy, UPenn                                       #
#                                                                 #
# Code to make final results and systematic variations for MJB.   #
#                                                                 #
# makeRatios will make the data/MC nominal result and the sys.    #
# uncertianty histograms.  Can also make the final pt remapped    #
# TGraphs.                                                        #
# plotUncertainties plots uncertainties with dedicated format.    #
###################################################################

#Use python 3 printing
from __future__ import print_function

import os,sys,math
import MJBSample, ROOT
import AtlasStyle
import array

AtlasStyle.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

#For Plotting: Mapping from systematic short name to plot display name    
name_conversion = { 
  "All" : "Total uncertainty",
  "MCType" : "MC generator",
  "EvSel"  :  "Event selection",
  "Zjet"   :  "Z+jet",
  "Gjet"   :  "#gamma+jet",
  "EtaIntercalibration" : "#eta-intercalibration",
  "PunchThrough"  : "Punch-through"
}

#Make the systematic variation histograms from the fitted response histograms.
#
def makeRatios( data_sample, mc_sample, out_file_name,  min_pt=0, max_pt=9999, sys_mc_name="", do_bootstrap = True ):

  #If needed for studies, to remove any uncertainty variations that should not be in the final version
  #sys_to_ignore = ["JET_JER_SINGLE_NP__1up", "Flavor_Composition_jet"]
  sys_to_ignore = []

  #Rename parts of each systematic name, to match what is expected by the insitu combination code
  sys_to_rename = [ ('Asym90','localMJB_Asym80'), ('Asym70','localMJB_Asym80'), ('Beta20','localMJB_Beta15'), ('Beta10','localMJB_Beta15'), ('Alpha40','localMJB_Alpha30'), ('Alpha20','localMJB_Alpha30'), ('Threshold30','localMJB_Threshold25'), ('Threshold20','localMJB_Threshold25'), (sys_mc_name, 'MJB_MCType')]

  #Get rebinned fits for bootstrap, else the regular fits
  if( do_bootstrap ):
    fit_name = "fitBalance_rebin"
  else:
    fit_name = "fitBalance"

  in_file_data = ROOT.TFile.Open(data_sample.histFile, "READ")
  in_file_mc = ROOT.TFile.Open(mc_sample.histFile, "READ")
  out_file = ROOT.TFile.Open(out_file_name, "RECREATE")

  #Get all variations for data and MC
  sys_list_data = [key.GetName() for key in in_file_data.GetListOfKeys()]
  sys_list_mc = [key.GetName() for key in in_file_mc.GetListOfKeys()]

  #Get unique list of all variations
  sys_list = list(set(sys_list_data+sys_list_mc))

  #Only accept things with MJB in the name, and not the Nominal result
  sys_list = [sys for sys in sys_list if "MJB" in sys and not "Nominal" in sys]
  
  #Run over Nominal first, we need this for each systematic variation
  nominal_dir_name = "MJB_Nominal"
  nominal_dir_data = in_file_data.Get( nominal_dir_name )
  nominal_dir_mc   = in_file_mc.Get  ( nominal_dir_name )
  nominal_mc = nominal_dir_mc.Get( fit_name )
  nominal_hist = nominal_dir_data.Get( fit_name )
  
  fine_pt_name = "recoilPt_center" #This is the histogram needed for remapping from recoil pt to leading jet pt
  #Save information we need for pt remapping from recoil pt -> lead jet pt 
  #if( do_bootstrap ): - evandewa
  nominal_response_hist = nominal_hist.Clone( "Nominal_data_response_hist" )
  nominal_pt_distribution = nominal_dir_data.Get( fine_pt_name ) #Event distribution w/in recoil pt bins

  final_hists = [] #List of output histograms

  #This is the MC/data ratio!
  nominal_hist.Divide( nominal_mc )
  nominal_hist.SetName( "localMJB_Nominal_AntiKt10LCTopoTrimmedPtFrac5SmallR20" )
  nominal_hist.Write( nominal_hist.GetName(), ROOT.TObject.kOverwrite )
  final_hists.append( nominal_hist )


  #Loop over the systematic variations and get their relative difference to the nominal
  for iSys, sys_name in enumerate(sys_list):

    if any( ignore_sys in sys_name for ignore_sys in sys_to_ignore ):
      print("Skipping systematic", sys_name)
      continue
    print("Getting final histogram for", sys_name )
    sys_dir_data = in_file_data.Get( sys_name )
    sys_dir_mc   = in_file_mc.Get  ( sys_name )

    #If this is a data-only systematic, use the nominal result from MC
    if not sys_dir_mc:
      sys_dir_mc = in_file_mc.Get( nominal_dir_name )

    #If this is a MC-only systematic, get the nominal result from data
    if not sys_dir_data:
      sys_dir_data = in_file_data.Get( nominal_dir_name )

    #Get histogram of fits
    sys_mc = sys_dir_mc.Get( fit_name )
    sys_hist = sys_dir_data.Get( fit_name )
     
    #If we rebinned this systematic variations, we also need the nominal result that is rebinned 
    if( do_bootstrap ):
      nominal_mc = sys_dir_mc.Get( fit_name+'_nominal' )
      nominal_data = sys_dir_data.Get( fit_name+'_nominal' )
      nominal_hist = nominal_data.Clone("tmp")
      nominal_hist.Divide( nominal_mc ) #data/mc
    else:
      nominal_hist = final_hists[0].Clone( "tmp_"+sys_name )

    #Make the systematic uncertainty!
    sys_hist.Divide( sys_mc ) #data/mc
    sys_hist.Add( nominal_hist, -1 )
    sys_hist.Divide( nominal_hist )

    #Figure out the output systenatmic name
    split_name =  sys_name.split('_')
    new_name = ''

    if split_name[1]=='JET':
      new_name = '_'.join( split_name[2:len(split_name)-2] )
    elif split_name[1]=='EvSel':
      new_name = split_name[2]
    else:
      new_name = 'localMJB_'+split_name[1]

    if('up' in split_name[-1]):
      new_name+='_up'
    elif('down' in split_name[-1]):
      new_name+='_down'

    #Make name replacements if necessary
    for change_name in sys_to_rename:
      if change_name[0] in new_name:
        print(change_name[0], change_name[1], new_name)
        new_name = new_name.replace( change_name[0], change_name[1] )
    new_name+='_AntiKt10LCTopoTrimmedPtFrac5SmallR20'
    sys_hist.SetName( new_name )

    #Save it to the list
    final_hists.append( sys_hist )

  #Sort all histograms so output is less messy
  final_hists.sort(key=lambda x: x.GetName() )
  for sys_hist in final_hists:
    sys_hist.Write( sys_hist.GetName(), ROOT.TObject.kOverwrite )

  #Get remapped TGraphs and save them
  #if( do_bootstrap ): - evandewa
  graphs = convert_hist_to_graph(nominal_response_hist, nominal_pt_distribution, final_hists, min_pt=min_pt, max_pt=max_pt )
  out_file.Close()
  out_file_graph = ROOT.TFile.Open(out_file_name.replace('.root','_graphs.root'), "RECREATE" )
  for graph in graphs:
    graph.Write(graph.GetName(), ROOT.TObject.kOverwrite )
    
  out_file_graph.Close()
  # else: - evandewa
  #   out_file.Close()

  in_file_data.Close()
  in_file_mc.Close()

  return out_file_name

# Function to create TGraph objects from histograms for the final uncertainty.
# Bin centers are corrected based on recoil_pt event distribution and on lead jet pt / recoil system pt ratio
# All distributions (including sys_hists) get corrected to the nominal data distribution
def convert_hist_to_graph( nominal_response_hist, nominal_pt_distribution, final_hists, min_pt=0, max_pt=9999 ):

  graphs = []

  x_centers = []
  x_errors_low = []
  x_errors_high = []
  x_errors_symm = []

  #If there is not enough events to use the mean of the distribution, then just use the bin center
  is_low_stat_bin = []

  #Get all bins from nominal histogram
  bin_range = []
  for iBin in range(1, nominal_response_hist.GetNbinsX()+1):
    if( nominal_response_hist.GetXaxis().GetBinLowEdge(iBin) < min_pt or nominal_response_hist.GetXaxis().GetBinUpEdge(iBin) > max_pt ):
      continue
    else:
      bin_range.append(iBin)

  ##First get x-axis bin centers based on event distribution within recoil pt bins
  for iBin in bin_range:
    bin_center = nominal_response_hist.GetXaxis().GetBinCenter(iBin)
    x_errors_low.append( bin_center - nominal_response_hist.GetXaxis().GetBinLowEdge(iBin) )
    x_errors_high.append( nominal_response_hist.GetXaxis().GetBinUpEdge(iBin) - bin_center )
    x_errors_symm.append( nominal_response_hist.GetXaxis().GetBinUpEdge(iBin) - bin_center )

    nominal_pt_distribution.GetXaxis().SetRangeUser( nominal_response_hist.GetXaxis().GetBinLowEdge(iBin), nominal_response_hist.GetXaxis().GetBinUpEdge(iBin) )

    mean = nominal_pt_distribution.GetMean()

    #Get centers for each TGraph point
    if nominal_pt_distribution.Integral() < 30: #If not enough entries, just set bin center
      is_low_stat_bin.append( True )
      x_centers.append( bin_center )
    else:
      is_low_stat_bin.append( False )
      x_centers.append( mean )
  
  ##Then remap x-axis bin centers & bin edges according to nominal data response distribution (leading pt / recoil pt)
  for iList, iBin in enumerate(bin_range):
    response = nominal_response_hist.GetBinContent( iBin )
  
    #Only remap w/ response if we have reasonable statistics
    if( not is_low_stat_bin[iList] ):
      x_centers[iList] *= response
      x_errors_low[iList] *= response
      x_errors_high[iList] *= response
      x_errors_symm[iList] *= response

  x_centers_array = array.array('d', x_centers)
  x_errors_low_array = array.array('d', x_errors_low)
  x_errors_high_array = array.array('d', x_errors_high)
  x_errors_symm_array = array.array('d', x_errors_symm)

  #Graph y-axis values from each systematic variation and make a new TGraphErrors
  for final_hist in final_hists:

    y_values = []
    y_errors = []
    #for iBin in range(1, final_hist.GetNbinsX()+1):
    for iBin in bin_range:
      y_values.append( final_hist.GetBinContent(iBin) )
      y_errors.append( final_hist.GetBinError(iBin) )
    y_values_array = array.array('d', y_values)
    y_errors_array = array.array('d', y_errors) #y_errors are symmetric

    #evandewa - Use to be TGraphAsymErrors
    #new_graph = ROOT.TGraphAsymmErrors( len(bin_range), x_centers_array, y_values_array, x_errors_low_array, x_errors_high_array, y_errors_array, y_errors_array )
    new_graph = ROOT.TGraphErrors( len(bin_range), x_centers_array, y_values_array, x_errors_symm_array, y_errors_array )
    new_graph.SetName( final_hist.GetName() )
    new_graph.SetTitle( "Graph "+final_hist.GetName() )
    new_graph.SetMarkerStyle(28)
    new_graph.SetMarkerSize(1)

    new_graph.GetXaxis().SetTitle( final_hist.GetXaxis().GetTitle() )
    new_graph.GetYaxis().SetTitle( final_hist.GetYaxis().GetTitle() )
   
    graphs.append( new_graph ) 

    #Make statistical variation for nominal result
    if "Nominal" in final_hist.GetName():

      #Get empty array for errors 
      y_empty_array = array.array('d', [0 for iBin in bin_range])

      #stat_graph = ROOT.TGraphAsymmErrors( len(bin_range), x_centers_array, y_errors_array, x_errors_low_array, x_errors_high_array, y_empty_array, y_empty_array )
      stat_graph = ROOT.TGraphErrors( len(bin_range), x_centers_array, y_errors_array, x_errors_symm_array, y_empty_array )
      stat_graph.SetName( "localMJB_Statistical_AntiKt10LCTopoTrimmedPtFrac5SmallR20" ) #evandewa - use to be "graph_Stat"
      stat_graph.SetTitle( "Graph Stat" )
      stat_graph.SetMarkerStyle(28)
      stat_graph.SetMarkerSize(1)
      stat_graph.GetXaxis().SetTitle( final_hist.GetXaxis().GetTitle() )
      stat_graph.GetYaxis().SetTitle( "Statistical Uncertainty" )

      graphs.append( stat_graph ) 

  return graphs
 
#Function to plot the uncertainties with a dedicated formatting 
def plotUncertainties( in_file_name, systematics = "Zjet,Gjet,Flavor,EtaIntercalibration,PunchThrough,Pileup,MCType,EvSel,JVT,Statistical,All", out_tag = "", jetType="EM", do_final_plot = False, tgraph_file=""):

  in_file = ROOT.TFile.Open(in_file_name, "READ")
  sys_name_list = [sys.GetName() for sys in in_file.GetListOfKeys() if not "Nominal" in sys.GetName()]
  sys_name_list.sort()

  sys_types_to_use = systematics.split(',')
  #sys_types_to_use = ["Zjet", "Gjet", "Flavor", "EtaIntercalibration", "PunchThrough", "Pileup", "MCType", "EvSel", "JVT", "Statistical", "All"]
  # "Other" is also allowed

  #For detailed plot
  c_detailed = ROOT.TCanvas()
  c_detailed.SetRightMargin(0.25)
  leg_detailed = ROOT.TLegend(0.75, 0.15, 0.99, 0.98)
  canvases = [c_detailed]
  legends = [leg_detailed]

  #For final formatted (1-sided) purple plot
  if( do_final_plot ):
    c_final = ROOT.TCanvas("Final")
    c_final.SetRightMargin(0.05)
    leg_final = ROOT.TLegend(0.48, 0.6, 0.93, 0.93)
    leg_final.SetNColumns(2)
    leg_final.SetFillStyle(0)
    canvases.append( c_final )
    legends.append( leg_final )

    #Get x-axis edges from the remapped TGraph
    if len(tgraph_file) > 0:
      f_tgraph = ROOT.TFile.Open(tgraph_file)
      tgraph = f_tgraph.Get('localMJB_Nominal_AntiKt10LCTopoTrimmedPtFrac5SmallR20') #evandewa - use to be graph_Nominal
      x_centers = list(tgraph.GetX())
      x_errors = []
      for iP in range(0, len(x_centers)):
        x_errors.append( tgraph.GetErrorXlow(iP))
      x_errors.append( tgraph.GetErrorXhigh(len(x_centers)-1))

      x_edges = []
      for iP, x_center in enumerate(x_centers):
        x_edges.append( x_centers[iP] - x_errors[iP])
      x_edges.append( x_centers[-1] + x_errors[-1] )



  #Only use sys_types that are actually available
  for sys_type in sys_types_to_use:
    if sys_type == "All" or sys_type == "Statistical" or sys_type == "Other":
      continue
    if not any(sys_type in sys_name for sys_name in sys_name_list):
      print("Error, sys type", sys_type, "is not available.  IT should not be included in sys_types_to_use")
      exit(1)

  sys_list = [in_file.Get(sys) for sys in sys_name_list] 

  #Add Statistical Uncertainty
  nom_hist = in_file.Get("localMJB_Nominal_AntiKt10LCTopoTrimmedPtFrac5SmallR20")
  stat_up = nom_hist.Clone("localMJB_Statistical_up_AntiKt10LCTopoTrimmedPtFrac5SmallR20")
  stat_down = nom_hist.Clone("localMJB_Statistical_down_AntiKt10LCTopoTrimmedPtFrac5SmallR20")
  for iBin in range(1, nom_hist.GetNbinsX()+1):
    stat_up.SetBinContent(iBin, nom_hist.GetBinError(iBin) )
    stat_up.SetBinError(iBin, 0 )
    stat_down.SetBinContent(iBin, -1.*nom_hist.GetBinError(iBin) )
    stat_down.SetBinError(iBin, 0 )

  sys_list.append( stat_up )
  sys_list.append( stat_down )

  print( "Using", sys_types_to_use)
  if len(sys_types_to_use) == 1:
    sys_types_to_use = [sys for sys in sys_name_list if sys_types_to_use[0] in sys]
  if "All" in sys_types_to_use:
    color_offset = 240./(len(sys_types_to_use)-1)
  else:
    color_offset = 240./len(sys_types_to_use)

  marker_styles = [3, 20, 21, 23, 22]

  #Loop over each top level systematic variation and create it from the sub-systematics
  sys_ups = []
  sys_downs = []
  for iTopSys, sys_name in enumerate(sys_types_to_use):

    if 'All' in sys_name:
      sub_sys_list = sys_list
      color = ROOT.kBlack
    elif 'Other' in sys_name:
      other_sys_list = [sys_type for sys_type in sys_types_to_use if not "Other" in sys_type or "All" in sys_type]
      sub_sys_list = [sys for sys in sys_list if not any(other_sys_name in sys.GetName() for other_sys_name in other_sys_list)]
      color = ROOT.gStyle.GetColorPalette(int(color_offset*(iTopSys+1)))
    else:
      sub_sys_list = [sys for sys in sys_list if sys_name in sys.GetName()]
      if(sys_name=='MJB'):
        sub_sys_list = [sys for sys in sub_sys_list if not 'local' in sys.GetName()]
      color = ROOT.gStyle.GetColorPalette(int(color_offset*(iTopSys+1)))

    #Get the up and down plots from all the sub-systematics
    sys_up, sys_down = get_combined_sys(sub_sys_list, sys_name)
    sys_ups.append( sys_up )
    sys_downs.append( sys_down )

    #Formatting
    sys_up.SetLineColor(color)
    sys_up.SetMarkerColor(color)
    sys_down.SetLineColor(color)
    sys_down.SetMarkerColor(color)
    sys_up.SetMarkerSize(0.5)
    sys_down.SetMarkerSize(0.5)
    sys_up.SetMarkerStyle( marker_styles[iTopSys%len(marker_styles)] )
    sys_down.SetMarkerStyle( marker_styles[iTopSys%len(marker_styles)] )


  #Order legend according to maximum size of uncertainty
  maxes = []
  for iTopSys, sys_name in enumerate(sys_types_to_use):
    this_max = max( sys_ups[iTopSys].GetMaximum(), abs(sys_downs[iTopSys].GetMinimum() ) )
    maxes.append(this_max)
 
  maxes, sys_types_to_use, sys_downs, sys_ups = zip(*sorted(zip(maxes, sys_types_to_use, sys_downs, sys_ups)))
  sys_types_to_use = list(sys_types_to_use)
  sys_downs = list(sys_downs)
  sys_ups = list(sys_ups)
  sys_types_to_use.reverse()
  sys_downs.reverse()
  sys_ups.reverse()

  #Get maximum and minimum values of all systematics
  if 'All' in sys_name:
    max_sys = 0.028
    min_sys = -0.02
  else:
    max_sys = 0
    min_sys = 0
    for sys_up in sys_ups:
      max_sys = max( max_sys, sys_up.GetMaximum() )
    for sys_down in sys_downs:
      min_sys = min( min_sys, sys_down.GetMinimum() )
    max_sys *= 1.6
    min_sys *= 1.2

  #Loop over each systematic uncertainty and plot it
  sys_averages = []
  for iSys, sys_up in enumerate(sys_ups):
    sys_down = sys_downs[iSys]
    sys_up.SetMaximum(max_sys)
    sys_up.SetMinimum(min_sys)
    sys_down.SetMaximum(max_sys)
    sys_down.SetMinimum(min_sys)

    #First plots will want marker style of 1, but save current marker style for final plots
    this_marker_style = sys_up.GetMarkerStyle()
    sys_up.SetMarkerStyle(20)
    sys_down.SetMarkerStyle(20)

    sys_down.GetXaxis().SetRangeUser(200, 3000)
    sys_up.GetXaxis().SetRangeUser(200, 3000)

    #Plot the detailed canvas of up and down variations 
    c_detailed.cd()

    if( iSys == 0 ):
      sys_up.GetXaxis().SetTitle("Recoil p_{T} (GeV)")
      sys_up.GetYaxis().SetTitle("Relative Uncertainty")
      sys_up.Draw("hist lp")
    else:
      sys_up.Draw("same hist lp")
    sys_down.Draw("same hist lp")

    #Convert the name for legend
    leg_name = sys_types_to_use[iSys]
    if leg_name in name_conversion:
      leg_name = name_conversion[leg_name]

    leg_detailed.AddEntry(sys_up, leg_name, 'lp')


    #Make averaged version for final plot (1-sided)
    if( do_final_plot ):
      c_final.cd()
      
      if len(tgraph_file) == 0:
        sys_average = sys_up.Clone(sys_up.GetName()+'_avg')
      else:
        sys_average = ROOT.TH1F( sys_up.GetName()+'_avg', sys_up.GetName()+'_avg', len(x_edges)-1, array.array('f', x_edges) )
        sys_average.SetMarkerColor(sys_up.GetMarkerColor())

      sys_average.SetMarkerSize(0.8)
      sys_average.SetMarkerStyle(this_marker_style)
      sys_averages.append( sys_average ) #Must save histogram or it will disappear

      #Get average of up and down variations for each bin
      for iBin in range(1, sys_average.GetNbinsX()+1):
        iVarBin = sys_up.FindBin( sys_average.GetBinCenter(iBin))
        average_content = 100*(abs(sys_up.GetBinContent(iVarBin)) + abs(sys_down.GetBinContent(iVarBin))) / 2.
        sys_average.SetBinContent(iBin,  average_content)

      
      if "All" in sys_types_to_use[iSys]:
        draw_string = 'hist F'
        leg_string = 'f'
        sys_average.SetFillColor( ROOT.TColor.GetColor("#ccccff") );
      else:
        draw_string = 'p'
        leg_string = 'p'

      sys_average.SetMinimum(0)
      sys_average.SetMaximum(5)

      sys_average.GetXaxis().SetRangeUser(500, 2500)

      if( iSys == 0 ):
        if len(tgraph_file) == 0:
          sys_average.GetXaxis().SetTitle("Recoil p_{T} (GeV)")
        else:
          sys_average.GetXaxis().SetTitle("Leading jet p_{T} (GeV)")
        sys_average.GetYaxis().SetTitle("Relative JES Uncertainty [%]")
        sys_average.DrawCopy(draw_string)
      else:
        sys_average.DrawCopy("same "+draw_string)

      leg_final.AddEntry(sys_average, leg_name, leg_string)

  #Draw the canvas and legend and save output images
  for iC, canv in enumerate(canvases):
    canv.cd()
    legends[iC].Draw("same")
    
    ### Draw text to plot ###
    textSize = 0.92
    yTop = 0.89
    if iC == 1:
      xLeft = 0.20
      AtlasStyle.ATLAS_LABEL(xLeft,yTop, textSize, "Internal")
      AtlasStyle.myText(xLeft,yTop-0.05, textSize, "Multijet Balance")
      AtlasStyle.myText(xLeft,yTop-0.10, textSize, "#sqrt{s}=13 TeV, 80 fb^{-1}")
      AtlasStyle.myText(xLeft,yTop-0.15, textSize, "anti-#it{k_{t} R}=1.0")
      AtlasStyle.myText(xLeft,yTop-0.20, textSize, "LCW+JES+JMS")
      AtlasStyle.myText(xLeft,yTop-0.25, textSize, "|#eta^{leading}| < 0.8")
    else:
      xLeft = 0.25
      AtlasStyle.ATLAS_LABEL(xLeft,yTop, textSize, "Internal, Multijet Balance")
      AtlasStyle.myText(xLeft,yTop-0.10, textSize, "#sqrt{s}=13 TeV, 80 fb^{-1}")
      AtlasStyle.myText(xLeft,yTop-0.10, textSize, "anti-#it{k_{t} R}=1.0, LCW+JES+JMS, |#eta^{leading}| < 0.8")

    #Make folder for output plots and save them in several formats
    out_path = os.path.dirname(in_file_name)+'/../systematics_plots/'
    if not os.path.exists(out_path+'format/'):
      os.makedirs(out_path+'format/')
    out_name = os.path.basename(in_file_name).replace('.root', out_tag )
   
    if iC == 1:
      out_name += '_final' 
    canv.SaveAs(out_path+out_name+'.png')
    canv.SaveAs(out_path+'format/'+out_name+'.pdf')
    canv.SaveAs(out_path+'format/'+out_name+'.eps')
    canv.SaveAs(out_path+'format/'+out_name+'.C')

  in_file.Close()

#Function to take all the systematics in sub_sys_list and combine them into a single up and down variation
def get_combined_sys( sub_sys_list, sys_name ):

  sys_down = sub_sys_list[0].Clone(sys_name+'_down')
  sys_up   = sub_sys_list[0].Clone(sys_name+'_up')

  #Clear histograms
  for iBin in range( 0, sys_up.GetNbinsX()+2 ):
    sys_up.SetBinContent(iBin, 0)
    sys_down.SetBinContent(iBin, 0)
    sys_up.SetBinError(iBin, 0)
    sys_down.SetBinError(iBin, 0)

  #Get squared combination of each systematic
  for sys in sub_sys_list:
    for iBin in range(1, sys.GetNbinsX()+1):
      bin_content = sys.GetBinContent(iBin)
      if bin_content < 0:
        sys_down.SetBinContent(iBin, sys_down.GetBinContent(iBin) + (bin_content)**2)
      else:
        sys_up.SetBinContent(iBin, sys_up.GetBinContent(iBin) + bin_content**2)

  #Get sqrt of combined value, and make negative for sdown
  for iBin in range(1, sys_up.GetNbinsX()+1):
    sys_up.SetBinContent( iBin, math.sqrt( sys_up.GetBinContent(iBin)) )
    sys_down.SetBinContent( iBin, -1.*math.sqrt( sys_down.GetBinContent(iBin)) )

  sys_down.GetXaxis().SetRangeUser(200, 3000)
  sys_up.GetXaxis().SetRangeUser(200, 3000)
  return sys_up, sys_down
