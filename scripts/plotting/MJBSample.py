#!/usr/bin/env python

###################################################################
# MJBSample.py                                                    #
# A MJB python class                                              #
# Author Jeff Dandoy, UPenn                                       #
#                                                                 #
# Define Sample objects correspond to one data or MC sample.      #
# Samples are associated to a set of input histograms, and        #
# new TFiles are created for scaled and combined histograms.      #
#                                                                 #
# MCSample and DataSample define individual samples.              #
# addMCSys will add the MC systematic to the nominal MC file      #
# scaleHist will scale MC samples by total event yield            # 
#                                                                 #
###################################################################

#Use python 3 printing
from __future__ import print_function

import os, argparse, ROOT, glob

# Bold text output
class color:
   BOLD = '\033[1m'
   END = '\033[0m'


### Define a generic sample type, will be inherited by data and MC samples
class Sample():
  def __init__(self, name, tags="", displayName=None):
    self.name = name
    self.tags = tags.split(',') #Will combine any samples that include any tag (comma separated)
    if displayName == None:
      self.displayName = name
    else:
      self.displayName = displayName

    self.verbose = True #Print each command as it runs
    self.inputDir = None #Input directory for raw histograms
    self.redo = False #Recreate intermediary files

    self.histFile = ""  #File with the histograms
    self.bootstrapFile = ""  #File with the histograms
    self.rebin_file = "" #File with final binning, from bootstrap

  def setInputDir(self, inputDir):
    self.inputDir = inputDir

### A sample extension for data ###
class DataSample(Sample):
  def __init__(self, name, tags="", displayName=None):
    Sample.__init__(self, name=name, tags=tags, displayName=displayName )
    self.isMC = False

  #Function to find all input files and hadd them
  def gatherInput(self, bootstrap=False):

    if bootstrap:
      out_tag = "bootstrap"
      type_tag = "SystToolOutput"
    else:
      out_tag = "all"
      type_tag = "hist"

    ### Do not recreate combined file if it already exists and redo is false
    combinedFileName = self.inputDir+'/files/'+self.name+'.'+out_tag+'.root'
    if bootstrap:
      self.bootstrapFile = combinedFileName
    else:
      self.histFile = combinedFileName

    if os.path.isfile(combinedFileName):
      if self.redo:
        print(color.BOLD+combinedFileName, "exists, but we WILL recreate it.",color.END)
      else:
        print(color.BOLD+combinedFileName, "exists, will NOT recreate it.", color.END)
        return

    ### Hadd all data files together
    command = 'hadd -f '+combinedFileName+' '
    for tag in self.tags:
      command += self.inputDir+'/'+type_tag+'/*'+tag+'*_'+type_tag+'.root '
    if(self.verbose):  print(command)
    os.system(command)



### A sample extension for MC ###
class MCSample(Sample):
  def __init__(self, name, tags="", displayName=None):
    Sample.__init__(self, name=name, tags=tags, displayName=displayName )
    self.isMC = True
    self.isJZW = False

  #Function to find all input files, scale them, and hadd them
  def gatherInput(self, bootstrap=False):

    if bootstrap:
      out_tag = "bootstrap"
      type_tag = "SystToolOutput"
    else:
      out_tag = "all"
      type_tag = "hist"

    ### Do not recreate combined file if it already exists and redo is false
    combinedFileName = self.inputDir+'/files/'+self.name+'.'+out_tag+'.root'
    if bootstrap:
      self.bootstrapFile = combinedFileName
    else:
      self.histFile = combinedFileName

    if os.path.isfile(combinedFileName):
      if self.redo:
        print(color.BOLD+combinedFileName, "exists, but we WILL recreate it.",color.END)
      else:
        print(color.BOLD+combinedFileName, "exists, will NOT recreate it.", color.END)
        return

    ### Scale MC files by correct normalization
    raw_files = []
    for tag in self.tags:
      raw_files+= glob.glob(self.inputDir+'/'+type_tag+'/*'+tag+'*_'+type_tag+'.root')
    for raw_file in raw_files:
      if(self.verbose):  print("scalehist on ", raw_file)
      scaleHist(raw_file, self.isJZW, isBootstrap=bootstrap)

    ### Hadd all MC files together
    command = 'hadd -f '+combinedFileName+' '
    for tag in self.tags:
      command += self.inputDir+'/files/*'+tag+'*_'+type_tag+'.scaled.root '
    if(self.verbose):  print(command)
    os.system(command)

    # Remove intermediary files
    for tag in self.tags:
      command = "rm "+self.inputDir+'/files/*'+tag+'*_'+type_tag+'.scaled.root '
      if(self.verbose):  print(command)
      os.system(command)

  ### Add MC systematic variation to this sample, corresponding to the nominal result of a different MC sample ###
  def addMCSys( self, sysSample ):
    if self.verbose:  print("addMCSys on ",self.name)

    ### Check if there is a 'Nominal' hist directory in the systematic file, and exit if not ###
    sysFile = ROOT.TFile.Open(sysSample.histFile, "READ")
    sysDir = [sysFile.Get(key.GetName()) for key in sysFile.GetListOfKeys() if "Nominal" in key.GetName() and "MJB" in key.GetName()]
    if len(sysDir) != 1:
      print("Error, MC systematic sample", sysSample.name, "does not have only 1 Nominal file, but contains the following.  Exiting..")
      print(sysDir)
      exit(1)
    sysDir = sysDir[0]

    ### Create new directory in nominal file ###
    nomFile = ROOT.TFile.Open(self.histFile, "UPDATE")
    newDirName = sysDir.GetName().replace('Nominal','MCType')

    ## If directory already exists, do not recreate ##
    if nomFile.Get( newDirName ) and not self.redo:
      if self.verbose:  print("nom file", self.name, " already has dir for MC syst file", sysSample.name, ", we will not recreate")
    else:

      nomFile.mkdir( newDirName )
      nomDir = nomFile.Get( newDirName )
      nomDir.cd()

      ### Loop over each histogram in the systematic sample directory, and add it to the nominal MC file ###
      for key in sysDir.GetListOfKeys():
        obj = key.ReadObj()
        obj.SetDirectory(nomDir)
        obj.Write( obj.GetName(), ROOT.TObject.kOverwrite )

    nomFile.Close()
    sysFile.Close()

    ## Now do boostrap object
    ### Check if there is a 'Nominal' hist directory in the systematic file, and exit if not ###
    sysFile = ROOT.TFile.Open(sysSample.bootstrapFile, "READ")
    bootstrap_file = [sysFile.Get(key.GetName()) for key in sysFile.GetListOfKeys() if "Nominal" in key.GetName() and "bootstrap" in key.GetName()]
    if len(bootstrap_file) != 1:
      print("Error, MC systematic sample", sysSample.name, "does not have only 1 Nominal bootstrap file, but contains the following.  Exiting..")
      print(bootstrap_file)
      exit(1)
    bootstrap_file = bootstrap_file[0]

    ### Create new directory in nominal file ###
    nomFile = ROOT.TFile.Open(self.bootstrapFile, "UPDATE")
    newBootName = bootstrap_file.GetName().replace('Nominal','MCType')#sysSample.name)

    ## If bootstrap file already exists, do not recreate ##
    if nomFile.Get( newBootName ) and not self.redo:
      print("nom file", self.name, " already has bootstrap for MC syst file", sysSample.name, ", we will not recreate")
    else:
      bootstrap_file.Write( newBootName, ROOT.TObject.kOverwrite )

    nomFile.Close()
    sysFile.Close()


### A function to scale a single MC input file ###
### A new output file is created with the same name, but ending in 'scaled.root' ###
def scaleHist(file, isJZW=False, isBootstrap=False):

  ### First get scale factor for this file.  Might not come from same input file is we're running over SystToolOutput!
  scaleFileName = file.replace('SystToolOutput','hist')
  scaleFile = ROOT.TFile.Open(scaleFileName, "READ")

  #Get total event number for normalization
  thisCutflow = scaleFile.Get("MetaData_EventCount")
  if(isJZW):
    numEvents = thisCutflow.GetBinContent(1)  #numberOfEvents, Pythia8 JZW only
  else:
    numEvents = thisCutflow.GetBinContent(3)  #sumOfWeights
  scaleFactor = 1./numEvents

  scaleFile.Close()


  ### Create input and output files ###
  inFile = ROOT.TFile.Open(file, "READ");
  outFileName = file.replace('.root', '.scaled.root').replace('/hist/','/files/').replace('/SystToolOutput/','/files/')
  outFile = ROOT.TFile.Open(outFileName, "RECREATE");

  #Get list of all objects in file
  keyList = [key.GetName() for key in inFile.GetListOfKeys()] #List of top level objects
  
  #Can't SetDirectory w/ Bootstrap objects, so save to list
  #If we don't keep track of this, we will get memory leaks
  bootstrap_objects = [] 
  if(isBootstrap):
    bootstrapList = [key for key in keyList if "bootstrap" in key] #List of all bootstrap objects

    for bootstrapName in bootstrapList:
      thisBootstrap = inFile.Get(bootstrapName)
      print(thisBootstrap)
      thisBootstrap.Scale( scaleFactor )
      thisBootstrap.Write(bootstrapName) #Write bootstrap to new file - originally i
      bootstrap_objects.append( thisBootstrap )

  else:
    #Keys will be many TDirectories
    dirList = [key for key in keyList if "MJB" in key or "Iteration0" in key] #List of all directories

    for dir in dirList:
      dirName = dir
      #Temporary fix, everything should be MJB moving forward
      if "Iteration0" in dir:
        dirName = dir.replace("Iteration0","MJB")

      outFile.mkdir( dirName )
      newDir = outFile.Get( dirName )
      oldDir = inFile.Get( dir )

      ### Save all histograms to new directory ###
      histList = [key.GetName() for key in oldDir.GetListOfKeys()]
      for histName in histList:
        thisHist = oldDir.Get(histName)
        thisHist.SetDirectory( newDir )
        thisHist.Scale( scaleFactor )

  outFile.Write()
  outFile.Close()
  inFile.Close()

  #Delete bootstrap objects, which we could not associate to a directory
  for thisBootstrap in bootstrap_objects:
    thisBootstrap.Delete()

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot")
  parser.add_argument("--isJZW", dest='isJZW', action='store_true', default=False, help="Is Pythia JZW file, with special normalization.")
  parser.add_argument("--file", dest='file', default="submitDir/hist-data12_8TeV.root",
           help="Input file name")
  args = parser.parse_args()

  scaleHist(args.file, args.isJZW)

