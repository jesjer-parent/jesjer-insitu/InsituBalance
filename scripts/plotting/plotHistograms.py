#!/usr/bin/env python

##############################################################
# plotHistograms.py                                          #
##############################################################
# Code for plotting histograms.     #
# Edited for use with MJB.                                   #
##############################################################
# Jeff.Dandoy and Nedaa.Asbah                                #
##############################################################


import os, sys, glob, copy, subprocess
import time
import argparse
import AtlasStyle
from collections import defaultdict
from math import sqrt, log, isnan, isinf, fabs, exp, ceil
#from enum import Enum
import ROOT

#
#put argparse before ROOT call.  This allows for argparse help options to be printed properly (otherwise pyroot hijacks --help) and allows -b option to be forwarded to pyroot
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-b", dest='b', action='store_true', default=False, help="Batch mode for PyRoot")
parser.add_argument("-v", dest='v', action='store_true', default=False, help="Verbose mode for debugging")
parser.add_argument("--test", dest='test', action='store_true', default=False, help="Test mode will only output pair1 mass plot")

##### Input / Output #####
parser.add_argument("--inDir", dest='inDir', default="./", help="Default directory to find all histograms")
parser.add_argument("--dataDir", dest='dataDir', default=None, help="Directory to find data histograms, defaults to --inDir")
parser.add_argument("--bkgDir", dest='bkgDir', default=None, help="Directory to find bkg histograms, defaults to --inDir")
parser.add_argument("--plotDir", dest='plotDir', default="", help="TDirectory to search for plots in ROOT file. This is used for the various regions. By default no directory")
parser.add_argument("--dataType", dest='dataType', default='', help="Name of data type to be used in plotting. Seperate inputs by a comma, tags by a +")
parser.add_argument("--bkgType", dest='bkgType', default='', help="Name of mc type to be used in plotting.  Seperate inputs by a comma, tags by a +")
parser.add_argument("--outDir", dest='outDir', default="./plots", help="Name directory to store plots")
parser.add_argument("--outputTag", dest='outputTag', default="", help="Output Tag Name for plots")
parser.add_argument("--outputVersion", dest='outputVersion', default="", help="Add a version number to the end of the plots.")

parser.add_argument("--histName", dest='histName', default="", help="A hist name to plot.")
parser.add_argument("--xRangeMin", dest='xRangeMin', type=int, default=-1, help="Additional factor to xRangeMin all histograms by")
parser.add_argument("--xRangeMax", dest='xRangeMax', type=int, default=-1, help="Additional factor to xRangeMax all histograms by")

###### Scaling options ####
#TODO check these
parser.add_argument("--lumi", dest='lumi', type=float, default=1, help="Scale by Luminosity, in /fb")
parser.add_argument("--normToData", dest='normToData', action='store_true', default=False, help="normalize all files to the first data file")
parser.add_argument("--normToBkg", dest='normToBkg', action='store_true', default=False, help="Normalize all files to the first background File")
parser.add_argument("--unitNormalize", dest='unitNormalize', action='store_true', default=False, help="Draw unit normalization")
parser.add_argument("--differential", dest='differential', action='store_true', default=False, help="Draw differential, scaling by bin width")
parser.add_argument("--printNormOnly", dest='printNormOnly', action='store_true', default=False, help="Print the normalization factor in the legend, but don't actually normalize")

parser.add_argument("--drawRatioError", dest='drawRatioError', action='store_true', default=False, help="Draw error bars on the ratio histogram")

#### Common formatting options #####
parser.add_argument("--plotRatio", dest='plotRatio', action='store_true', default=False, help="Add ratio plot below 1D plots")
parser.add_argument("--ratioWRT", dest='ratioWRT', default='data', help="Do ratio wrt data or bkg")

#### Extra Commands ####
parser.add_argument("--writeMerged", dest='writeMerged', action='store_true', default=False, help="Write out combined histograms for each sample.  Requires --outMergeDir")
parser.add_argument("--outMergeDir", dest='outMergeDir', default='', help="Name of output directory to put merged histograms")
parser.add_argument("--addOverflow", dest='addOverflow', action='store_true', default=True, help="Add overflow to last bin for each histogram")
parser.add_argument("--plotText", dest='plotText', default="", help="Additional text to be written to the plots")
parser.add_argument("--rebin", dest='rebin', type=int, default=1, help="Additional factor to rebin all histograms by")
parser.add_argument("--ratioMax", dest='ratioMax', type=float, default=1.0, help="Symmetric range for the ratio plot")

parser.add_argument("--balanceType", dest='balanceType', default="", help="String labeling which type of balance is being performed, for use in labeling and x-axis ranges. Can be MJB, Gjet, Zee, or Zmm.")
parser.add_argument("--jetType", dest='jetType', default="EM", help="String labeling which type of jet is being performed, for use in labeling.  Should be EM or PF.")
parser.add_argument("--toyErrors", dest='toyErrors', action='store_true', default=False, help="Use errors from toys loaded in histogram")
#TODO logXList


#TODO --scaleHists "histName"

#    use scalefactors as derived from this histName
#------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------

def enum(**enums):
  return type('Enum', (), enums)

m_numSignals = 0
m_excludeFromRebin = ["weight", "Balance","njet","NPV","Weight","averageInteractionsPerCrossing", "recoilPt"]

args = parser.parse_args()
if len(args.outputVersion) > 0:
  args.outputVersion = "_" + args.outputVersion
if not os.path.exists(args.outDir):
  os.mkdir(args.outDir)
if not os.path.exists(args.outDir+'/formats'):
  os.mkdir(args.outDir+'/formats')

AtlasStyle.SetAtlasStyle()


ROOT.gErrorIgnoreLevel = ROOT.kWarning

#Use same colors for stop search
m_colorNames = ["Z+jets", "single-top", "t#bar{t}", "W+jets", "diboson", "tt+V"]
#original and good! m_colors = [ROOT.kRed-3, ROOT.kBlue-3, ROOT.kMagenta-3, ROOT.kGreen-3, ROOT.kCyan-3, ROOT.kYellow, ROOT.kAzure, ROOT.kPink, ROOT.kTeal, ROOT.kSpring, ROOT.kViolet]
m_colors = [ROOT.kOrange-3, ROOT.kAzure-7, ROOT.kGreen-5, ROOT.kMagenta-5, ROOT.kGray+1, ROOT.kCyan-3, ROOT.kYellow, ROOT.kAzure, ROOT.kPink, ROOT.kTeal, ROOT.kSpring, ROOT.kViolet]
bkgTypes = args.bkgType.split(',')
for thisBkg in bkgTypes:
  thisBkgName = thisBkg.split(':')[0]
  if thisBkgName in m_colorNames:
    iC = m_colorNames.index(thisBkgName)
    iB = bkgTypes.index(thisBkg)
    #Swap colors to match their background
    m_colors[iB], m_colors[iC] = m_colors[iC], m_colors[iB]
    m_colorNames[iB], m_colorNames[iC] = m_colorNames[iC], m_colorNames[iB]


balanceEnum = enum(MJB=1, Gjet=2, Zee=3, Zmm=4)
if args.balanceType == "MJB":
  balanceType = balanceEnum.MJB
elif args.balanceType == "Gjet":
  balanceType = balanceEnum.Gjet
elif args.balanceType == "Zee":
  balanceType = balanceEnum.Zee
elif args.balanceType == "Zmm":
  balanceType = balanceEnum.Zmm
else:
  print("Error, could not figure out which type of insitu balance you are performing (MJB, Gjet, Zee, or Zmm).  Received ", args.balanceType)
  exit(1)



SampleEnum = enum(data=1, bkg=2, previous=3)
if( args.ratioWRT == 'data'):
  args.ratioWRT = SampleEnum.data
elif( args.ratioWRT == 'bkg'):
  args.ratioWRT = SampleEnum.bkg
elif( args.ratioWRT == 'previous'):
  args.ratioWRT = SampleEnum.previous

########################## Histogram Retrieval Code #########################################
def getPlotList():
  print("getPlotList")

  if not args.dataDir:  args.dataDir = args.inDir
  if not args.bkgDir:  args.bkgDir = args.inDir

  ########## Get all fileNames to use ##########
  SampleNames, SampleTypes, SampleFiles = [], [], []
  ## Get All Background Files ##
  if len(args.bkgType) > 0:
    args.bkgType = args.bkgType.split(',')
    for thisBkgType in args.bkgType:
      if ':' in thisBkgType:
        thisBkgName = thisBkgType.split(':')[0].replace('--',' ')
        thisBkgType = thisBkgType.split(':')[1]
      else:
        thisBkgName = thisBkgType
      SampleNames.append( thisBkgName )
      SampleTypes.append( SampleEnum.bkg )
      SampleFiles.append( getFileNames( args.bkgDir, thisBkgType.split('+') ) )

  #### Keep Data Last!! ####
  if len(args.dataType) > 0:
    args.dataType = list(args.dataType.split(','))
    if args.v: print(args.dataType)
    for thisDataType in args.dataType:
      if ':' in thisDataType:
        thisDataName = thisDataType.split(':')[0].replace('--', ' ')
        thisDataType = thisDataType.split(':')[1]
      else:
        thisDataName = thisDataType
      SampleNames.append( thisDataName )
      SampleTypes.append( SampleEnum.data )
      SampleFiles.append( getFileNames( args.dataDir, thisDataType.split('+') ) )

  ##### Get histogram list from first file #####
  HistNames = getHistNames( SampleFiles )

  ##### Get all histograms #####
  Hists = []
  for iS, fileList in enumerate(SampleFiles):
    Hists.append( getHists(SampleNames[iS], fileList, HistNames) )

  #Use errors as RMS of toys if requested
  if( args.toyErrors):
    error_hist_name = "Nominal_RMS"
    for iH, histname in enumerate(HistNames):
      if histname.endswith("fitBalance") or histname.endswith("fitBalance_rebin"):
        print(fileList)
        for iS, fileList in enumerate(SampleFiles):
          for iF, fileName in enumerate(fileList):
            print("Updating fitBalance errors for file", fileName)
            this_file = ROOT.TFile.Open(fileName, "READ")
            error_hist = this_file.Get( error_hist_name )
            if not error_hist:
              continue

            if( error_hist.GetNbinsX() != Hists[iF][iH].GetNbinsX()):
              print("Warning, fitBalance histogram's binning does not match that from the error uncertainty file!")
              exit(1)
            for iBin in range(1, error_hist.GetNbinsX()+1):
              Hists[iS][iH].SetBinError( iBin, error_hist.GetBinContent(iBin) )

  return SampleNames, SampleTypes, HistNames, Hists

############## Get File List #############################
def getFileNames( histDir, histFileTags ):

  thisHistDir = histDir
  thisHistFileTags = histFileTags
  thisHistFileTags.append(".root") #must be root files
  thisHistDir += '/'

  ## Get List of Files that match the tag ##
  histFileNames = []
  for f in os.listdir(histDir):
    if not os.path.isfile( os.path.join(histDir,f) ): continue

    passed = True
    for tag in histFileTags:
      if '=' in tag:
        if not any(splitTag in f for splitTag in list(tag.split('='))):
          passed = False
      else:
        if not tag in f:
          passed = False

    if passed:
      histFileNames.append( os.path.join(histDir,f) )

  histFileNames.sort()
  if len(histFileNames)<1:
    print( "ERROR:  There are no histogram files of requested type", histFileTags)
    exit(1)

  elif args.v: print( histFileNames)

  return histFileNames

################# Get Histogram List ################################
def getHistNames( inFileNames ):

  ## Get names of all histograms in all files ##
  ## Histogram list will be all those common to all files ##

  allFileNames = sum( inFileNames, [])
  allHistNames = []
  for inFileName in allFileNames:
    allHistNames.append([])
    inFile = ROOT.TFile(inFileName, 'READ')
    if len(args.plotDir) > 0:
      plotDir = inFile.Get(args.plotDir)
      if not plotDir:
        print( "ERROR, Couldn't find TDirectory", args.plotDir, "in", inFileName)
        exit(1)
    else:
      plotDir = inFile
    keys = plotDir.GetListOfKeys()
    for key in keys:
      if ("2D" in key.GetName()) or ( not args.histName in key.GetName()):
        continue
      allHistNames[-1].append(args.plotDir+'/'+key.GetName())

    inFile.Close()

  ## Hist Names must be common to all files ##
  histNames = []
  for histName in allHistNames[0]:
    if all( histName in histNameList for histNameList in allHistNames):
      histNames.append(histName)
    else:
      print("Did not find", histName, "in all hist list")

  print( "HistNames are", histNames)
#  histNames = [hist for hist in histNames if "jetPt_" in hist]

  return histNames

################# Combine all samples ##############################
def getHists( sampleName, fileList, histNames ):

  CombinedHists = []

  for iF, fileName in enumerate(fileList):
    file = ROOT.TFile.Open(fileName, "READ")

    for iH, histName in enumerate(histNames):
      thisHist = file.Get(histName)
      if iF == 0:
        #print sampleName, "and", histName
        CombinedHists.append( thisHist.Clone( sampleName+'_'+thisHist.GetName() ) ) #Unique name for each type
        CombinedHists[-1].SetDirectory( 0 ) # just in case...
      else:
        CombinedHists[iH].Add( thisHist )

    file.Close()


  ### If add overflow ###
  if args.addOverflow:
    for iH, combinedHist in enumerate(CombinedHists):
      nbinX = combinedHist.GetNbinsX()
      if combinedHist.GetBinContent( nbinX + 1 ) > 0:
        combinedHist.SetBinContent( nbinX, combinedHist.GetBinContent( nbinX ) + combinedHist.GetBinContent( nbinX + 1 ) )
        newError = sqrt( combinedHist.GetBinError( nbinX )*combinedHist.GetBinError( nbinX ) + combinedHist.GetBinError( nbinX + 1 )*combinedHist.GetBinError( nbinX + 1) )
        combinedHist.SetBinError( nbinX, newError )


  return CombinedHists

##################################### Plotting Code ###########################################
def plotAll( SampleNames, SampleTypes, HistNames, Hists):
  print( "plotAll")

  ### Align histograms and reorder so histName is first dimension ###
  #Hists are [fileType][histName]
  newHistList = []  #Will be [histName][fileType]
  for iHist, thisHistName in enumerate(HistNames):
    newHistList.append([])

    for iSample, thisSample in enumerate(SampleTypes):
      newHistList[-1].append( Hists[iSample][iHist] )

  Hists = newHistList

  for iHist, histName in enumerate(HistNames):
    print("Doing "+histName)
    theseHists = Hists[iHist]
    theseTypes = SampleTypes
    theseNames = SampleNames
    formatHists(theseTypes, theseHists)
    normFactors = scaleHists(theseTypes, theseHists)
    if ( type(theseHists[0]) == ROOT.TH1D or type(theseHists[0]) == ROOT.TH1F or type(theseHists[0]) == ROOT.THStack ):
      if("jetRapidity" in theseHists[0].GetName()):
        continue
      plot1D( theseHists, theseTypes, theseNames, normFactors )
      plot1D_logy( theseHists, theseTypes, theseNames, normFactors )

def getStackNormFactors(theseTypes, normFactors):

  newNormFactors = [ 0. ]
  for iType, thisType in enumerate(theseTypes):
    if thisType != SampleEnum.bkg :
      newNormFactors.append( normFactors[iType] )
    else:
      newNormFactors[0] = normFactors[ iType ]

  return newNormFactors

######### Rescale the histograms ###########
def scaleHists( histTypes, hists ):

# Signals are only scaled to luminosity
  normFactors = []

  ## If a balance histogram, don't normalize by luminosity or to data
  if "Balance" in hists[0].GetName():
    for iHist, hist in enumerate(hists):
      normFactors.append( None )
    return normFactors

  ## First, Scale all bkg and sig by lumi factor
  if args.lumi != 0 :
    for iHist, hist in enumerate(hists):
      if histTypes[iHist] == SampleEnum.bkg:
        hist.Scale( args.lumi )


  ## Normalize to first data ##
  if args.normToData:
    dataIntegral = hists[ histTypes.index(SampleEnum.data) ].Integral()

    ## Scale everything to first data separately
    for iHist, hist in enumerate(hists):

      if hist.Integral() > 0:
        if not args.printNormOnly:
          normFactors.append( dataIntegral/hist.Integral() )
        hist.Scale( dataIntegral/hist.Integral() )
      else:
        normFactors.append( None )

  #Normalize to first bkg
  elif args.normToBkg:
    bkgIntegral = hists[histTypes.index(SampleEnum.bkg)].Integral()
    for iHist, hist in enumerate(hists):
      if hist.Integral() > 0:
        normFactors.append( bkgIntegral/hist.Integral() )
        if not args.printNormOnly:
          hist.Scale( bkgIntegral/hist.Integral() )
      else:
        normFactors.append( None )


  elif args.unitNormalize or args.differential:
    for iHist, hist in enumerate(hists):
      if hist.Integral() > 0:
        if args.unitNormalize:
          normFactors.append( 1.0/hist.Integral() )
          if not args.printNormOnly:
            hist.Scale( 1.0/hist.Integral() )
        elif args.differential:
          normFactors.append( None )
          if not args.printNormOnly:
            hist.Scale(1.0/hist.Integral(),"width")
      else:
        normFactors.append( None )


  if (len(normFactors) > 0) and  (len(normFactors) != len( hists )) :
    print( "Error, something wrong with norm factors!")
    exit(1)
  return normFactors



def plot1D_logy( Hists, SampleTypes, SampleNames, normFactors = [] ):
  print (Hists[0].GetName())
  if 'jetBeta_jet0' in Hists[0].GetName():
    return
  plot1D( Hists, SampleTypes, SampleNames, normFactors, True )

#### Plot 1D Histograms ####
def plot1D( Hists, SampleTypes, SampleNames, normFactors = [], is_logy=False ):

  histName = '_'.join(Hists[0].GetName().split('_')[1:] )
  plotRatio = args.plotRatio

  ## Setup Canvas ##
  c0 = ROOT.TCanvas(histName)

#  if( "Balance" in histName or "recoilPt" in histName):
#    logx = True
#  else:
#    logx = False
  logx = False

  setMaximum( Hists, is_logy )
  setMinimum( Hists, is_logy )

  if plotRatio:
    pad1, pad2, zeroLine, oneLine = getRatioObjects(c0, logx, is_logy)
    pad1.cd()
  else:
    c0.SetTopMargin(0.01)
    c0.SetRightMargin(0.03)
    c0.SetLeftMargin(0.13)
    c0.SetBottomMargin(.15)
    if is_logy:
      c0.SetLogy()

  leg = configureLegend(SampleTypes, Hists, SampleNames, normFactors)

  ### Configure draw string ###
  for iH, hist in enumerate(Hists):
    drawString = ""
    if iH != 0:
      drawString += 'same'
    if SampleEnum.data == SampleTypes[iH]:
      drawString += 'epX0'
    elif SampleEnum.bkg == SampleTypes[iH]:
      drawString += 'histF'

#    ROOT.gStyle.SetHatchesLineWidth(4)
#    ROOT.gStyle.SetHatchesSpacing(4)
    Hists[iH].Draw( drawString )

    if( SampleTypes[iH] == SampleEnum.bkg ):
      errorHist = getErrorHist( Hists[iH] )
      errorHist.DrawCopy("same e2")


#  ## Draw Arrow
#  arrow = ROOT.TArrow(0.1,0.1,0.1,0.7)
#  line = ROOT.TLine(0.1,0.1,0.1,0.7)
#  arrow.Draw("same")

  ## Setup Ratio Plots
  if plotRatio:
    ratioHists = [] #Ratio histograms
    lowerErrors = []
    for iHist, hist in enumerate(Hists):
      if args.ratioWRT == SampleEnum.data or args.ratioWRT == SampleEnum.bkg:
        iRatioHist = SampleTypes.index(args.ratioWRT)
      elif args.ratioWRT == SampleEnum.previous:
        if(iHist%2) == 0:
          continue
        else:
          iRatioHist = iHist-1

      if iHist == iRatioHist: continue

      ## get relevant hists ##
      tmpHist = Hists[iRatioHist].Clone( hist.GetName()+'_denom' )
      tmpRatioHist = hist.Clone( hist.GetName()+'_ratio' )

      ## create ratio ##
      tmpRatioHist.Divide( tmpHist )


      if args.ratioWRT == SampleEnum.previous:
        configureRatioHist(tmpRatioHist, tmpRatioHist)
      else:
        configureRatioHist(tmpHist, tmpRatioHist)
      ratioHists.append( tmpRatioHist )

  ## Draw Ratio Plots
    pad2.cd()
    for iHist, ratioHist in enumerate(ratioHists):
      ratioHists[iHist].SetStats(0)
      if args.drawRatioError:
        #ratioHists[iHist].DrawCopy("p")
        #ratioHists[iHist].SetMarkerSize(0)
        ratioHists[iHist].SetFillStyle( 3005 )
        ratioHists[iHist].SetFillColor( ROOT.kBlack )
        if iHist == 0:
          ratioHists[iHist].DrawCopy("e2")
        else:
          ratioHists[iHist].DrawCopy("same e2")
        ratioHists[iHist].DrawCopy("same p")

      else:
        ratioHists[iHist].Draw( "same hist" )
        #ratioHists[iHist].Draw( "same hist" )
        errorHist = getErrorHist( ratioHists[iHist] )
        errorHist.SetLineColor( ratioHists[iHist].GetLineColor() )
        errorHist.DrawCopy("same e2")
    oneLine.Draw("same")

    c0.cd()

  leg.Draw("same")

  ### Draw text to plot ###
  textSize = 0.95
  yTop = 0.94 #0.94
  AtlasStyle.ATLAS_LABEL(0.17,yTop, textSize, "Internal")
  sqrtSLumiText = getSqrtSLumiText( args.lumi )
  AtlasStyle.myText(0.17,yTop-0.05, textSize, sqrtSLumiText)

  #Draw info for this DB method
  if balanceType == balanceEnum.Gjet:
    AtlasStyle.myText(0.17,yTop-0.10, textSize, "Direct Balance with #gamma+jet")
  elif balanceType == balanceEnum.Zee:
    AtlasStyle.myText(0.17,yTop-0.10, textSize, "Direct Balance with #it{Z}(ee)+jet")
  elif balanceType == balanceEnum.Zmm:
    AtlasStyle.myText(0.17,yTop-0.10, textSize, "Direct Balance with #it{Z}(#mu#mu)+jet")
  elif balanceType == balanceEnum.MJB:
    AtlasStyle.myText(0.17,yTop-0.10, textSize, "Multijet Balance")
  AtlasStyle.myText(0.17,yTop-0.15, textSize, "anti-#it{k_{t} R}=1.0, "+args.jetType+"+JES")
  AtlasStyle.myText(0.17,yTop-0.20, textSize, "|#eta^{jet}| < 0.8")

  ### Save TCanvas ###
  printString = "/" + args.outputTag + "_" + histName + args.outputVersion
  if (is_logy):
    printString += "_logY"
  print( args.outDir+printString+".png")
  c0.Print( args.outDir+printString+".png", "png" )
  c0.Print( args.outDir+'/formats/'+printString+".eps", "eps" )
  c0.Print( args.outDir+'/formats/'+printString+".pdf", "pdf" )
  c0.SaveAs( args.outDir+'/formats/'+printString+".C")

  if plotRatio:
    pad1.Clear()
    pad2.Clear()
  c0.Clear()

  return

def configureRatioHist(originalHist, ratioHist):

  ratioHist.GetYaxis().SetTitle("AU")
  ratioHist.GetYaxis().SetTitleSize( 0.14 )
  ratioHist.GetYaxis().SetLabelSize( 0.11 )
  ratioHist.GetYaxis().SetTitleOffset( 0.4 )
  ratioHist.GetXaxis().SetLabelSize( 0.13 )
  ratioHist.GetXaxis().SetTitleSize( 0.2 )
  ratioHist.GetXaxis().SetTitleOffset( 1 )
  ratioHist.GetYaxis().SetNdivisions(5)
  if(args.ratioWRT == SampleEnum.previous):
    ratioHist.GetYaxis().SetTitle("#splitline{Ratio w.r.t}{previous}")
  else:
    ratioHist.GetYaxis().SetTitle("#splitline{Sample/}{Data}")
#  ratioHist.SetMinimum(-args.ratioMax)
#  ratioHist.SetMaximum(args.ratioMax)


  if("Balance" in ratioHist.GetName()):
    if balanceType == balanceEnum.Gjet:
      ratioHist.SetMinimum(0.95)
      ratioHist.SetMaximum(1.0299)
    else:
      ratioHist.SetMinimum(0.95)
      ratioHist.SetMaximum(1.05)
      #ratioHist.SetMaximum(1.04)
  elif("njet" in ratioHist.GetName()):
    ratioHist.SetMinimum(0.5)
    ratioHist.SetMaximum(1.25)
  else:
    ratioHist.SetMinimum(0.5)
    ratioHist.SetMaximum(2.0)
  return

def configureLegend(SampleTypes, Hists, SampleNames, normFactors):
  leg = ROOT.TLegend(0.50, 0.77, 0.92, 0.96,"")
  leg.SetTextFont(62)
  leg.SetFillStyle(0)
  leg.SetEntrySeparation(0.0001)
  leg.SetTextSize(0.036)
  #leg.SetNColumns(2)

  global m_numSignals
  ## Draw all input histograms ##
  for iHist, hist in enumerate(Hists):
    legendString = ""
    if SampleEnum.data == SampleTypes[iHist]:
      legendString += 'ep'
    elif SampleEnum.bkg == SampleTypes[iHist] and m_numSignals == 0:
      legendString += 'l'
    else:
      legendString += 'f'

    leg.AddEntry( hist, SampleNames[iHist], legendString)

  return leg


############ Format the histograms ##################
#Hists is a list of a specific histogram (e.g. jet pt) for each Sample Type
def formatHists( SampleTypes, Hists ):

  thisHistName = Hists[0].GetName()

  ## Set colors ##
  global m_colors


  #ROOT.gStyle.SetPalette(ROOT.kLightTemperature)
  ROOT.gStyle.SetPalette(ROOT.kThermometer)
  DataColors = [ROOT.kBlack, ROOT.kRed, ROOT.kCyan-3]
  DataStyle = [20, 22, 33]

  BkgColors = []
  BkgColors = m_colors
  #if m_numSignals > 0:
  #  BkgColors = [ROOT.kRed, ROOT.kCyan, ROOT.kMagenta, ROOT.kGreen, ROOT.kBlue, ROOT.kYellow, ROOT.kAzure, ROOT.kPink, ROOT.kTeal, ROOT.kSpring, ROOT.kViolet]
  #else:
  #  BkgColors = [ROOT.kRed-3, ROOT.kBlue-3, ROOT.kMagenta-3, ROOT.kGreen-3, ROOT.kCyan, ROOT.kYellow, ROOT.kAzure, ROOT.kPink, ROOT.kTeal, ROOT.kSpring, ROOT.kViolet]




  iBkg, iData, iSig = 0, 0, 0
  for iS, sampleType in enumerate(SampleTypes):
    if SampleEnum.data == sampleType:
      Hists[iS].SetMarkerColor(DataColors[iData])
      Hists[iS].SetLineColor(DataColors[iData])
      Hists[iS].SetMarkerStyle(DataStyle[iData])
      Hists[iS].SetMarkerSize(0.6)
      iData += 1
    else:
      Hists[iS].SetMarkerSize(0.0)
#!!      Hists[iS].SetFillColor(BkgColors[iBkg]) #!!
      #Hists[iS].SetFillStyle(3005) #!!
      Hists[iS].SetLineColor(BkgColors[iBkg])
#      Hists[iS].SetFillColor(BkgColors[iBkg])
      Hists[iS].SetLineStyle(1)
      #!!!Hists[iS].SetLineStyle(2)
      #if m_numSignals > 0:
      #  Hists[iS].SetFillColor(BkgColors[iBkg])
      #Hists[iS].SetFillStyle(3001)  ## This is the dot style
#      Hists[iS].SetFillStyle(1001)

      iBkg += 1


  ##### Rebin #####
  for iS, sampleType in enumerate(SampleTypes):
    if (args.rebin > 1):
      if ( Hists[iS].InheritsFrom("TH2") ):
        continue
      #if "jetPt" in thisHistName:
      ##if "jetPt" in thisHistName or "recoilPt" in thisHistName:
      #  if "jet2" in thisHistName:
      #    Hists[iS].Rebin(6)
      #  elif "jet3" in thisHistName or "jet4" in thisHistName or "jet5" in thisHistName:
      #    Hists[iS].Rebin(2)
      #  else:
      #    Hists[iS].Rebin(10)
      if not any( excludeName in thisHistName for excludeName in m_excludeFromRebin ):
        Hists[iS].Rebin(args.rebin)

  #Find xMin and xMax of all samples for this histogram, ensuring consistency
  xMin, xMax = -1, -1
  for iS, sampleType in enumerate(SampleTypes):
    thisMax = Hists[iS].GetXaxis().GetBinUpEdge(Hists[iS].FindLastBinAbove(0.1))
    thisMin = Hists[iS].GetXaxis().GetBinLowEdge(Hists[iS].FindFirstBinAbove(0.))
    xMax = max(xMax, thisMax) if xMax != -1 else thisMax
    xMin = max(xMin, thisMin) if xMin != -1 else thisMin
  print( "xMax is", xMax)
  if( 'recoilPt_center' in thisHistName ):
    xMin = 10

  #Apply x-axis range
  for iS, sampleType in enumerate(SampleTypes):
    if "jetPt" in thisHistName or "recoilPt" in thisHistName or "Balance" in thisHistName:
      #if "jet2" in thisHistName:
      #  xMax = 1200
      #elif "jet3" in thisHistName or "jet4" in thisHistName or "jet5" in thisHistName:
      #  xMax = 600
      #else:
      #  xMax = 3000

      if "Balance" in thisHistName:
        Hists[iS].GetXaxis().SetMoreLogLabels()
        if balanceType == balanceEnum.Gjet:
          #xMax = 2500
          #xMin = 25
          Hists[iS].GetXaxis().SetTitle("#it{p}_{T}^{#gamma}")
          Hists[iS].GetYaxis().SetTitle("#LT #it{p}_{T}^{jet}/#it{p}_{T}^{#gamma} #GT")
        elif balanceType == balanceEnum.Zee or balanceType == balanceEnum.Zmm:
          #xMax = 800
          #xMin = 50
          Hists[iS].GetXaxis().SetTitle("#it{p}_{T}^{#it{Z}}")
          Hists[iS].GetYaxis().SetTitle("#LT #it{p}_{T}^{jet}/#it{p}_{T}^{#it{Z}} #GT")
        elif balanceType == balanceEnum.MJB:
          Hists[iS].GetYaxis().SetTitle("#LT #it{p}_{T}^{jet}/#it{p}_{T}^{ref} #GT")
          Hists[iS].GetXaxis().SetTitle("Recoil p_{T}")
          xMin=200
          xMax=3000 #evandewa

      Hists[iS].GetXaxis().SetRangeUser(xMin, xMax)
      if( args.xRangeMin != -1 and args.xRangeMax != -1) :
        Hists[iS].GetXaxis().SetRangeUser(args.xRangeMin, args.xRangeMax)


  ## Set Axis Titles
  for iS, sampleType in enumerate(SampleTypes):

    if args.differential and not args.printNormOnly:
      xaxisName = Hists[iS].GetXaxis().GetTitle()
      Hists[iS].GetYaxis().SetTitle("(1/N)dN/d"+xaxisName)
    elif "fitBalance" in thisHistName:
      Hists[iS].GetYaxis().SetTitle("#LT p_{T}^{lead jet}/p_{T}^{recoil} #GT") 
    elif args.unitNormalize and not args.printNormOnly:
      Hists[iS].GetYaxis().SetTitle("A.U.")
    elif args.normToData and not args.printNormOnly:
      Hists[iS].GetYaxis().SetTitle("Normalized to data")
      #Hists[iS].GetYaxis().SetTitle("Norm. to 1st Data")
    elif args.normToBkg and not args.printNormOnly:
      Hists[iS].GetYaxis().SetTitle("Norm. to 1st Bkg")
    else:
      xtitle = Hists[iS].GetXaxis().GetTitle()
      if "GeV" in xtitle:
        xAxisWidth = Hists[iS].GetXaxis().GetBinUpEdge(1)-Hists[iS].GetXaxis().GetBinLowEdge(1)
        #xAxisWidth *= pow(2,args.rebin)
        if xAxisWidth.is_integer() :
          Hists[iS].GetYaxis().SetTitle("Events / "+str(int(xAxisWidth))+" GeV" )
        else:
          Hists[iS].GetYaxis().SetTitle("Events / "+str(int(ceil(xAxisWidth)))+" GeV" )
      else:
        Hists[iS].GetYaxis().SetTitle("Events")


    Hists[iS].GetYaxis().SetTitleSize( 0.07 )
    Hists[iS].GetYaxis().SetTitleOffset( 0.8 )
    Hists[iS].GetYaxis().SetLabelSize( 0.06 )




########## Get Lumi Difference #########
## Lumi difference between all data histograms and all bkg histograms
def getLumiDifference( SampleTypes, hists ):
  dataInteg = 0.
  bkgInteg = 0.
  for iHist, hist in enumerate(hists):
    if SampleTypes[iHist] == SampleEnum.data:
      dataInteg += hist.Integral()
    elif SampleTypes[iHist] == SampleEnum.bkg:
      bkgInteg += hist.Integral()
  if bkgInteg > 0:
    lumiRatio = dataInteg/bkgInteg
  else:
    lumiRatio = None

  return lumiRatio

#### Set Maximum of all hists ####
def setMaximum(Hists, is_logy=False):


  for iHist, hist in enumerate(Hists):
    if iHist == 0:
      yMax = hist.GetMaximum()
    else:
      yMax = max(yMax, hist.GetMaximum() )

  if is_logy:
    if yMax > 1:
      yMax *= yMax
    else:
      yMax = 10

  else:    yMax *= 1.8

  if( "Balance" in Hists[0].GetName()):
    if balanceType == balanceEnum.Zee or balanceType == balanceEnum.Zmm:
      yMax = 1.35
    else:
      yMax = 1.0799

  for iHist, hist in enumerate(Hists):
#    hist.SetMaximum( 90000 )
    hist.SetMaximum( yMax )

  return

#### Set Minimum of all hists ####
def setMinimum(Hists, is_logy=False):

  if is_logy:
    yMin = 0.1001
  else:
    yMin = 0.0001

  if( "Balance" in Hists[0].GetName()):
    if balanceType == balanceEnum.Zee or balanceType == balanceEnum.Zmm:
      yMin = 0.75
    else:
      yMin = 0.9301

  for iHist, hist in enumerate(Hists):
    hist.SetMinimum( yMin )

  return

#### Get Ratio Objects ####
def getRatioObjects(c0, logX, logY):
  pad1 = ROOT.TPad("pad1","pad1",0,0.33,1,1)
  pad2 = ROOT.TPad("pad2","pad2",0,0.01,1,0.32)
  pad1.Draw()
  pad2.Draw()
  pad1.SetBottomMargin(0)
  pad1.SetTopMargin(0.01)
  pad2.SetTopMargin(0.03)
  pad1.SetRightMargin(0.03)
  pad2.SetRightMargin(0.03)
  pad1.SetLeftMargin(0.13)
  pad2.SetLeftMargin(0.13)
  pad2.SetBottomMargin(.45)
  zeroLine = ROOT.TF1("zl0", "0", -50000, 50000 )
  zeroLine.SetTitle("")
  zeroLine.SetLineWidth(1)
  zeroLine.SetLineStyle(7)
  zeroLine.SetLineColor(ROOT.kBlack)
  oneLine = ROOT.TF1("ol0", "1", -50000, 50000 )
  oneLine.SetTitle("")
  oneLine.SetLineWidth(1)
  oneLine.SetLineStyle(7)
  oneLine.SetLineColor(ROOT.kBlack)

  if logX:
    pad1.SetLogx()
    pad2.SetLogx()
  if logY:
    pad1.SetLogy()

  return pad1, pad2, zeroLine, oneLine

#####################################################
##  END      FUNCTIONS TO CALL PLOTNTUPLE.PY      ##
####################################################

def getSqrtSLumiText( lumi ):
  sqrtSLumiText = "#sqrt{s}=13 TeV"

  if not (args.unitNormalize or args.differential):
    sqrtSLumiText += ", "+str(lumi)+" fb^{-1}"
  return sqrtSLumiText

def getErrorHist( thisHist):

  newHist = thisHist.Clone( "Error_"+thisHist.GetName() )

  newHist.SetFillStyle( (3354) )
  newHist.SetFillColor( thisHist.GetLineColor() )
  newHist.SetLineWidth(2)
  newHist.SetMarkerSize(0)

  return newHist

if __name__ == "__main__":
  if len(args.outputTag) > 0:
    args.outputTag = args.outputTag+'_'+args.plotDir
  else:
    args.outputTag = args.plotDir
  SampleNames, SampleTypes, HistNames, Hists = getPlotList()
  plotAll( SampleNames, SampleTypes, HistNames, Hists )

