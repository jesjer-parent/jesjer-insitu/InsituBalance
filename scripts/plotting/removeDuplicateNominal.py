import glob
import ROOT

#Quick script for removing the nominal results from files in case things had to be split up on the grid.

def remove_duplicate_nominal(file_name):

  in_file = ROOT.TFile.Open(file_name, "UPDATE")
  in_file.Delete("MJB_Nominal;1")
  in_file.Delete("bootstrap_Nominal;1")
  in_file.Close()


if __name__ == "__main__":

  file_tags = ["GRID1", "GRID2", "GRID4"]
  path = "/Data2017/MJB_R21/secondIteration/workarea_EM/hist/"

  for file_tag in file_tags:
    file_names = glob.glob(path+'/*'+file_tag+'*.root')
    for file_name in file_names:
      print( "Removing nominal from file", file_name)
      remove_duplicate_nominal(file_name)
