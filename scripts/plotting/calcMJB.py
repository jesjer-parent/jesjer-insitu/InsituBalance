#!/usr/bin/env python

###################################################################
# calcMJB.py                                                      #
# A MJB second stage python script                                #
# Author Jeff Dandoy, UPenn                                       #
#                                                                 #
# This script performs fits on MJB Samples and handles bootstrap. # 

#                                                                 #
###################################################################

#Use python 3 printing
from __future__ import print_function
import os,sys,math

import MJBSample, ROOT
sys.path.insert(0, os.path.dirname(os.path.realpath(__file__))+'/../../external_packages/JES_ResponseFitter/scripts/')
import JES_BalanceFitter, AtlasStyle
import array

import numpy, datetime
import multiprocessing, time

#Set plot style
AtlasStyle.SetAtlasStyle()
f_plot_fits = True
ROOT.gROOT.SetBatch(True)

#Wait until only n_jobs processes are running (for multiprocessing)
def wait_for_process(process_list, n_jobs):
  while len(process_list) >= n_jobs:
    time.sleep(2.0)
    sys.stdout.write('.')
    sys.stdout.flush()
    for process in process_list:
      if not process.is_alive():
        process.join()
        process_list.remove(process)
  return

#Convenient function for getting fit range and performing the fit
#Only fit Gaussian distribution within bins w/ 1/8 of max bin content
def MJB_fit(myFitter, h_response_distribution, low_threshold=1/8., high_threshold=1/8.):
  maxContent = h_response_distribution.GetBinContent(h_response_distribution.GetMaximumBin())
  fitLow = h_response_distribution.GetXaxis().GetBinLowEdge( h_response_distribution.FindFirstBinAbove(maxContent*low_threshold)-1 )
  fitHigh = h_response_distribution.GetXaxis().GetBinUpEdge( h_response_distribution.FindLastBinAbove(maxContent*high_threshold)+1 )
  fit_result = myFitter.Fit(h_response_distribution, fitLow, fitHigh )
  return fit_result

#Function to get data/MC ratio for all toys, and print warning if there are a large percentage of bad fits
def get_data_mc_ratios(nominal_means_data, nominal_means_mc):
  nominal_ratios = {}
  for iToy, data_toy in enumerate(nominal_means_data):
    #Ignore obviously bad fits
    if( data_toy <= 0.01 or nominal_means_mc[iToy] <= 0.01 or data_toy > 4 or nominal_means_mc[iToy] > 4):
      nominal_ratios[iToy] = None
    else:
      nominal_ratios[iToy] = data_toy / nominal_means_mc[iToy]

  n_badToys = sum(1 for toy in nominal_ratios.values() if toy == None)
  if n_badToys / len(nominal_ratios) > 0.1:
    print( (n_badToys / len(nominal_ratios)*100)+"% of toys were bad (>10%), will assume this bin is not statistically significant and will combine it")

  return nominal_ratios

#Get bin grouping when rebinning from original_edges to a rebin histogram
def get_bin_grouping(original_edges, rebin_hist):
  bins_to_merge = []
  rebin_edges = list(rebin_hist.GetXaxis().GetXbins())

  #Rebinned histogram may not included highest & lowest pt bins, so add these (just for future plotting purposes)
  low_edges = [edge for edge in original_edges if edge < rebin_edges[0] ]
  high_edges = [edge for edge in original_edges if edge > rebin_edges[-1] ]
  rebin_edges = low_edges + rebin_edges + high_edges

  # Need to find which bins should be merged for fits, and collect them into groupings
  bins_to_merge.append( [] )
  iRebin = 1
  for iOrig in range(1, len(original_edges)):
    if original_edges[iOrig] < rebin_edges[iRebin]:
      bins_to_merge[-1].append(iOrig)
      iOrig += 1
    elif original_edges[iOrig] == rebin_edges[iRebin]:
      bins_to_merge[-1].append(iOrig)
      iOrig += 1
      iRebin += 1
      if( iOrig < len(original_edges)): #If not at end
        bins_to_merge.append( [] )  #Start new bin grouping
  return bins_to_merge

#Main function to derive statistically significant rebinning for all systematic variations from toys
def derive_rebinning(sample_data, sample_mc, nominal_only=False, n_sigma=2, min_pt=0, max_pt=999999, n_cores=6, save_all_toys=False):

  hist_to_fit = "recoilPt_PtBal" #Name of histogram to fit

  #Get input bootstrap files
  in_file_data = ROOT.TFile.Open( sample_data.bootstrapFile, "READ" )
  in_file_mc = ROOT.TFile.Open( sample_mc.bootstrapFile, "READ" )

  #Get list of all bootstrap objects
  bootstrap_list_data = [key.GetName() for key in in_file_data.GetListOfKeys() if "bootstrap" in key.GetName()]
  bootstrap_list_mc = [key.GetName() for key in in_file_mc.GetListOfKeys() if "bootstrap" in key.GetName()]

  #Remove the nominal bootstrap from the list, this will be treated separately.
  bootstrap_list_data = [boot for boot in bootstrap_list_data if not "bootstrap_Nominal" in boot ]
  bootstrap_list_mc = [boot for boot in bootstrap_list_mc if not "bootstrap_Nominal" in boot ]
  bootstrap_list = list(set(bootstrap_list_data+bootstrap_list_mc))

  in_file_data.Close()
  in_file_mc.Close()

  #Run fits on nominal data & MC toys
  nominal_rms_hist, nominal_sig_hist, nominal_data_fits, nominal_mc_fits = derive_nominal_significance(sample_data.bootstrapFile, sample_mc.bootstrapFile, min_pt=min_pt, max_pt=max_pt, save_all_toys=save_all_toys)


  #Extract results
  result_queue = multiprocessing.Queue() #Thread-safe objecdt for storing results
  sys_hists = []
  rms_hists = []
  sig_hists = []

  ## Submit jobs for all bootstrap ##
  process_list = []
  for iBoot, boot_name in enumerate(bootstrap_list):

    #If this is a data-only systematic, get nominal mc and treat as systematic (and vice-versa)
    if not boot_name in bootstrap_list_mc:
      mc_boot_name = "bootstrap_Nominal"
    else:
      mc_boot_name = boot_name
    if not boot_name in bootstrap_list_data:
      data_boot_name = "bootstrap_Nominal"
    else:
      data_boot_name = boot_name

    #Submit a multithreaded process for this systematic variations
    process_thread = multiprocessing.Process(target=derive_sys_binning, args=(sample_data.bootstrapFile, sample_mc.bootstrapFile, data_boot_name, mc_boot_name, min_pt, max_pt, n_sigma, nominal_data_fits, nominal_mc_fits, save_all_toys, result_queue))
    process_list.append( process_thread )
    process_thread.start() #Start running it
    time.sleep(1.0)
    wait_for_process(process_list, n_cores) #Wait for a process to finish if # processes > n_cores

    #Try to pop a result to keep this object smaller?
    if not( result_queue.empty() ):
      this_result = result_queue.get()
      sys_hists.append( this_result[0] )
      rms_hists.append( this_result[1] )
      sig_hists.append( this_result[2] )

  wait_for_process(process_list, 1) #Wait for all processes to finish
  print("Done running over all toys")

  #Get all remaining results
  while( not result_queue.empty() ):
    this_result = result_queue.get()
    sys_hists.append( this_result[0] )
    rms_hists.append( this_result[1] )
    sig_hists.append( this_result[2] )

  #Save results to file
  out_file_name = os.path.dirname(sample_data.bootstrapFile)+'/rebinning_file.root'
  if(save_all_toys):
    out_file_name = out_file_name.replace('.root','_NoRebin.root')
  rebin_file = ROOT.TFile.Open( out_file_name, "RECREATE" )
  nominal_rms_hist.Write()
  nominal_sig_hist.Write()
  for iBoot, boot_name in enumerate(bootstrap_list):
    print(boot_name)
    sys_hists[iBoot].Write()
    rms_hists[iBoot].Write()
    sig_hists[iBoot].Write()
  rebin_file.Close()

#Function for deriving rebinning histogram (sys, RMS, and sig) for a single systematic.
#Meant to be run in parallel using multiprocessing, and save the output to the result_queue
def derive_sys_binning(in_file_data_name, in_file_mc_name, sys_name, mc_sys_name, min_pt=0, max_pt=99999999, sig_threshold=2, prefitted_nominal_data=[], prefitted_nominal_mc=[], save_all_toys=False, result_queue=None):

  start_time = datetime.datetime.now()
  out_sys_name = sys_name
  if "Nominal" in sys_name and not "Nominal" in mc_sys_name:
    out_sys_name = mc_sys_name

  print("\nStarting on toys of systematic "+out_sys_name)

  in_file_data = ROOT.TFile.Open( in_file_data_name, "READ" )
  in_file_mc = ROOT.TFile.Open( in_file_mc_name, "READ" )

  #Create new fitter
  myFitter = JES_BalanceFitter.JES_BalanceFitter(1.6)
  myFitter.SetGaus()
  myFitter.SetFitColor(ROOT.kRed)
  myFitter.rebin = False

  #Get bootstrap object
  boot_data = in_file_data.Get(sys_name)
  boot_mc = in_file_mc.Get(mc_sys_name)

  nominal_boot_name = "bootstrap_Nominal"
  nominal_boot_data = in_file_data.Get(nominal_boot_name)
  nominal_boot_mc = in_file_mc.Get(nominal_boot_name)

  #Get number of replicas.  Should be the same for everyone, but we can use lowest number also
  nR_data = boot_data.GetNReplica()
  nR_mc = boot_mc.GetNReplica()
  nR_data_nominal = nominal_boot_data.GetNReplica()
  nR_mc_nominal = nominal_boot_mc.GetNReplica()
  if( nR_data != nR_mc or nR_data != nR_data_nominal or nR_data != nR_mc_nominal ):
    print( "Warning, number of bootstrap replicas does not agree.  Continuing with lowest number available. (sys data:",nR_data, ", sys MC:", nR_mc, ", nominal data:", nR_data_nominal, ", nominal mc:", nR_mc_nominal)
    nReplica = min(nR_data, nR_mc, nR_data_nominal, nR_mc_nominal)
  else:
    nReplica = nR_data

  #Get all data and MC replicas (TH1Fs)
  hists_data = [] #systematic data
  hists_data.append( boot_data.GetNominal() )
  for iToy in range(0, nReplica):
    hists_data.append( boot_data.GetReplica(iToy) )
  hists_mc = [] #systematic MC
  hists_mc.append( boot_mc.GetNominal() )
  for iToy in range(0, nReplica):
    hists_mc.append( boot_mc.GetReplica(iToy) )
  nominal_hists_data = [] #nominal data
  nominal_hists_data.append( nominal_boot_data.GetNominal() )
  for iToy in range(0, nReplica):
    nominal_hists_data.append( nominal_boot_data.GetReplica(iToy) )
  nominal_hists_mc = [] #nominal MC
  nominal_hists_mc.append( nominal_boot_mc.GetNominal() )
  for iToy in range(0, nReplica):
    nominal_hists_mc.append( nominal_boot_mc.GetReplica(iToy) )

  #Get original binning.  The loop (and bin combination) will start from the highest pt bin!
  orig_binning = range(1,hists_data[0].GetNbinsX()+1)
  orig_binning = [iBin for iBin in orig_binning if hists_data[0].GetXaxis().GetBinUpEdge(iBin) <= max_pt]
  orig_binning = [iBin for iBin in orig_binning if hists_data[0].GetXaxis().GetBinLowEdge(iBin) >= min_pt]
  orig_binning.reverse()

  #Groupings of bins to save, and results to save
  bins_to_combine = []
  bins_to_combine.append( [] )
  bin_sigs = []
  bin_RMS = []
  bin_sysVar = []

  if( save_all_toys ):
    all_response_means = []
    for iToy in range(0, nReplica+1): #One list of bins per toy
      all_response_means.append( [] )

  #For each pt bin
  bin_count = -1
  for iBin in orig_binning:

    #bin_count += 1
    #if(bin_count%10 == 1 ):
    #  time_diff = (datetime.datetime.now()-start_time).seconds / 60.
    #  print( sys_name+' bin '+str(bin_count)+' '+str((time_diff/bin_count)*(len(orig_binning)-bin_count))+" more minutes   " )

    #If current bin grouping is not empty, use it to find the first pt bin of the fit
    firstBin = iBin
    if len(bins_to_combine[-1]) > 0:
      lastBin = bins_to_combine[-1][0]
    else:
      lastBin = iBin


    ##### Nominal fits #####
    #Whennever running over nominal toy fits w/ a single bin, use the saved result for that bin, instead of repeated fitting!
    if( firstBin == lastBin and iBin in prefitted_nominal_data ):
      nominal_means_data = prefitted_nominal_data[iBin]
      nominal_means_mc = prefitted_nominal_mc[iBin]
    else:
      #Fit all nominal MC distributions
      nominal_means_mc = []
      for iToy in range(len(nominal_hists_mc)):
        h_response_distribution = nominal_hists_mc[iToy].ProjectionY( "h_proj_mc_"+str(iToy)+"_"+str(iBin), firstBin, lastBin, "ed");
        fit_result = MJB_fit(myFitter, h_response_distribution)
        nominal_means_mc.append( myFitter.GetMean() )

      #Fit all nominal data distributions
      nominal_means_data = []
      for iToy in range(len(nominal_hists_data)):
        h_response_distribution = nominal_hists_data[iToy].ProjectionY( "h_proj_data_"+str(iToy)+"_"+str(iBin), firstBin, lastBin, "ed");
        fit_result = MJB_fit(myFitter, h_response_distribution)
        nominal_means_data.append( myFitter.GetMean() )

    #Get all nominal+toy data/MC response ratios
    nominal_ratios = get_data_mc_ratios(nominal_means_data, nominal_means_mc)

    ##### Systematic fits #####
    sys_means_mc = []
    #If we're doing a data systematic with no MC version, then mc systematic is same as mc nominal
    #Just grab the results of the mc nominal fits
    if( mc_sys_name == nominal_boot_name ):
      sys_means_mc = nominal_means_mc
    else:
      for iToy in range(len(hists_mc)):
        h_response_distribution = hists_mc[iToy].ProjectionY( "h_proj_mc_"+str(iToy)+"_"+str(iBin), firstBin, lastBin, "ed");
        fit_result = MJB_fit(myFitter, h_response_distribution)
        sys_means_mc.append( myFitter.GetMean() )

    #Fit all sys data distributions
    sys_means_data = []
    for iToy in range(len(hists_data)):
      h_response_distribution = hists_data[iToy].ProjectionY( "h_proj_data_"+str(iToy)+"_"+str(iBin), firstBin, lastBin, "ed");
      fit_result = MJB_fit(myFitter, h_response_distribution)
      sys_means_data.append( myFitter.GetMean() )

    #Get all systematic+toy data/MC response ratios
    sys_ratios = get_data_mc_ratios(sys_means_data, sys_means_mc)
    if( save_all_toys ):
      for iToy, sys_ratio in sys_ratios.items():
        all_response_means[iToy].append( sys_ratios[iToy] )

    ##### Convert ratios to systematic variations #####
    sys_toy_variations = []
    sys_variation = -1.
    for iToy, sys_ratio in sys_ratios.items():
      if (iToy == 0):
        sys_variation = (sys_ratio / nominal_ratios[0]) - 1.
      else:
        if( sys_ratio != None and nominal_ratios[iToy] != None ):
          sys_toy_variations.append( (sys_ratio / nominal_ratios[iToy]) - 1.)

    #Calculate RMS & significance
    sys_rms = numpy.std(sys_toy_variations)
    if( sys_variation != 0 and sys_rms != 0):
      mu = sys_variation / sys_rms / sys_rms
      sig = 1.0 / sys_rms / sys_rms
      final_significance = abs(mu)/math.sqrt(sig)
      #print(out_sys_name+' bin '+str(iBin)+' gives variation:'+str(sys_variation)+' mu:'+str(mu)+' rms:'+str(sys_rms)+' sig:'+str(final_significance))
    else:
      print(out_sys_name+' bin '+str(iBin)+' has a zero! sys_variation:'+str(sys_variation)+', sys_rms:'+str(sys_rms) )
      mu = 0
      sig = 0
      final_significance = 0

    #Add this bin to latest grouping regardless
    bins_to_combine[-1].append( iBin )

    #If significant, save the sig & rms, and make a new grouping
    if( (final_significance > sig_threshold) or (iBin == orig_binning[-1]) or (save_all_toys==True) or (len(bins_to_combine[-1]) >= 5) ):
      bin_sigs.append( final_significance )
      bin_RMS.append( sys_rms )
      bin_sysVar.append( sys_variation )

      #Don't add new grouping if we're at last ibn
      if(iBin != orig_binning[-1]):
        bins_to_combine.append( [] )

  # Calculate new histogram edges based on how bins are combined
  bins_to_combine.reverse()
  bin_sigs.reverse()
  bin_RMS.reverse()
  bin_sysVar.reverse()

  new_hist_edges = []
  for bin_grouping in bins_to_combine:
    bin_grouping.sort()
    new_hist_edges.append( hists_data[0].GetXaxis().GetBinLowEdge( bin_grouping[0]) )
  new_hist_edges.append( hists_data[0].GetXaxis().GetBinUpEdge(bins_to_combine[-1][-1] ) )


  if( save_all_toys ):
    orig_hist_edges = list(hists_data[0].GetXaxis().GetXbins())
    orig_hist_edges = [edge for edge in orig_hist_edges if edge <= max_pt] #Edges below a pre-determined pt threshold
    orig_hist_edges = [edge for edge in orig_hist_edges if edge >= min_pt] #Edges above a pre-determined pt threshold
    orig_hist_edges_array = array.array('f', orig_hist_edges)

  in_file_data.Close()
  in_file_mc.Close()

  #Make RMS & significance histograms
  new_hist_edges_array = array.array('f', new_hist_edges)
  sysVar_name = out_sys_name.replace('bootstrap','sys')
  sysVar_hist = ROOT.TH1F(sysVar_name, sysVar_name, len(new_hist_edges)-1, new_hist_edges_array)
  for iBin in range(1, sysVar_hist.GetNbinsX()+1):
    sysVar_hist.SetBinContent(iBin, bin_sysVar[iBin-1])
  rms_name = out_sys_name.replace('bootstrap','RMS')
  rms_hist = ROOT.TH1F(rms_name, rms_name, len(new_hist_edges)-1, new_hist_edges_array)
  for iBin in range(1, rms_hist.GetNbinsX()+1):
    rms_hist.SetBinContent(iBin, bin_RMS[iBin-1])
  sig_name = out_sys_name.replace('bootstrap','sig')
  sig_hist = ROOT.TH1F(sig_name, sig_name, len(new_hist_edges)-1, new_hist_edges_array)
  for iBin in range(1, sig_hist.GetNbinsX()+1):
    sig_hist.SetBinContent(iBin, bin_sigs[iBin-1])

  #If saving all toys, but all their pt distributions into a single file in AllToyFits/
  if( save_all_toys ):
    out_dir = os.path.dirname(in_file_data_name)
    if not os.path.exists(out_dir+'/AllToyFits'):
      os.makedirs(out_dir+'/AllToyFits')
    out_file_toys = ROOT.TFile.Open(out_dir+"/AllToyFits/"+out_sys_name+".root", "RECREATE")
    badToys = [iToy for iToy in range(0, nReplica+1) if None in all_response_means[iToy] ]#Remove any toys that had bad fits (in the pt range)
    for iToy in range(0, nReplica+1):
      if iToy in badToys:
        continue
      toy_hist = ROOT.TH1F(out_sys_name+"_"+str(iToy), out_sys_name+"_"+str(iToy), len(orig_hist_edges)-1, orig_hist_edges_array)
      all_response_means[iToy].reverse()
      for iBin in range(1, toy_hist.GetNbinsX()+1):
        toy_hist.SetBinContent(iBin, all_response_means[iToy][iBin-1])
      toy_hist.Write(toy_hist.GetName(), ROOT.TObject.kOverwrite)
    out_file_toys.Close()

  ## Add results to queue, needed to return information when multiprocessing
  if(result_queue):
    result_queue.put([sysVar_hist, rms_hist, sig_hist])
  print( "Exiting "+out_sys_name)

#Function for running fits on nominal data & MC distributions, and finding significance of ratio
def derive_nominal_significance(in_file_data_name, in_file_mc_name, min_pt=0, max_pt = 99999999, save_all_toys=False):

  print("Starting nominal fits")

  in_file_data = ROOT.TFile.Open( in_file_data_name, "READ" )
  in_file_mc = ROOT.TFile.Open( in_file_mc_name, "READ" )

  myFitter = JES_BalanceFitter.JES_BalanceFitter(1.6)
  myFitter.SetGaus()
  myFitter.SetFitColor(ROOT.kRed)
  myFitter.rebin = False

  #Get bootstrap object
  nominal_boot_name = "bootstrap_Nominal"
  boot_data = in_file_data.Get(nominal_boot_name)
  boot_mc = in_file_mc.Get(nominal_boot_name)

  nReplica = boot_data.GetNReplica()

  #Get all data and MC replicas (TH1Fs)
  hists_data = []
  hists_data.append( boot_data.GetNominal() )
  for iToy in range(0, nReplica):
    hists_data.append( boot_data.GetReplica(iToy) )

  hists_mc = []
  hists_mc.append( boot_mc.GetNominal() )
  for iToy in range(0, nReplica):
    hists_mc.append( boot_mc.GetReplica(iToy) )

  #Get original binning, and reverse them.  The loop will start from the highest pt bin!
  orig_binning = range(1,hists_data[0].GetNbinsX()+1)
  orig_binning = [iBin for iBin in orig_binning if hists_data[0].GetXaxis().GetBinUpEdge(iBin) <= max_pt]
  orig_binning = [iBin for iBin in orig_binning if hists_data[0].GetXaxis().GetBinLowEdge(iBin) >= min_pt]
  orig_binning.reverse()

  bins_to_combine = []
  bin_sigs = []
  bin_RMS = []

  # This will be 2D dictionary [iBin][iToy] of each data fit
  saved_nominal_ratios = {}

  if( save_all_toys ):
    all_response_means = []
    for iToy in range(0, nReplica+1): #One list of bins per toy
      all_response_means.append( [] )

  #For each pt bin
  for iBin in orig_binning:

    #Fit all MC distributions
    means_mc = []
    for iToy in range(len(hists_mc)):
      h_response_distribution = hists_mc[iToy].ProjectionY( "h_proj_mc_"+str(iToy)+"_"+str(iBin), iBin, iBin, "ed");
      fit_result = MJB_fit(myFitter, h_response_distribution)
      means_mc.append( myFitter.GetMean() )

    #Fit all data distributions
    means_data = []
    for iToy in range(len(hists_data)):
      h_response_distribution = hists_data[iToy].ProjectionY( "h_proj_data_"+str(iToy)+"_"+str(iBin), iBin, iBin, "ed");
      fit_result = MJB_fit(myFitter, h_response_distribution)
      means_data.append( myFitter.GetMean() )

    #Get all nominal+toy data/MC response ratios
    nominal_ratios = get_data_mc_ratios(means_data, means_mc)

    #Convert ratios to systematic variations
    nominal_toy_ratios = []
    nominal_ratio = -1

    for iToy, nom_ratio in nominal_ratios.items():
      if( save_all_toys ):
        all_response_means[iToy].append( nom_ratio )
      if iToy == 0:
        nominal_ratio = nom_ratio
      else:
        if( nom_ratio != None ):
          nominal_toy_ratios.append( nom_ratio )

    #Calculate RMS & significance
    nominal_rms = numpy.std(nominal_toy_ratios)
    mu = nominal_ratio / nominal_rms / nominal_rms
    sig = 1.0 / nominal_rms / nominal_rms
    final_significance = math.sqrt( abs(mu)/math.sqrt(sig) )

    bin_sigs.append( final_significance )
    bin_RMS.append( nominal_rms )

    saved_nominal_ratios[iBin] = nominal_ratios

  #Make RMS & significance histograms
  orig_hist_edges = list(hists_data[0].GetXaxis().GetXbins())
  orig_hist_edges = [edge for edge in orig_hist_edges if edge <= max_pt] #Edges below a pre-determined pt threshold
  orig_hist_edges = [edge for edge in orig_hist_edges if edge >= min_pt] #Edges above a pre-determined pt threshold
  orig_hist_edges_array = array.array('f', orig_hist_edges)

  in_file_data.Close()
  in_file_mc.Close()

  bin_sigs.reverse()
  bin_RMS.reverse()

  rms_name = nominal_boot_name.replace('bootstrap','RMS')
  rms_hist = ROOT.TH1F(rms_name, rms_name, len(orig_hist_edges)-1, orig_hist_edges_array)
  for iBin in range(1, rms_hist.GetNbinsX()+1):
    rms_hist.SetBinContent(iBin, bin_RMS[iBin-1])
  sig_name = nominal_boot_name.replace('bootstrap','sig')
  sig_hist = ROOT.TH1F(sig_name, sig_name, len(orig_hist_edges)-1, orig_hist_edges_array)
  for iBin in range(1, sig_hist.GetNbinsX()+1):
    sig_hist.SetBinContent(iBin, bin_sigs[iBin-1])

  #If saving all toys, but all their pt distributions into a single file in AllToyFits/
  if( save_all_toys ):
    out_dir = os.path.dirname(in_file_data_name)
    if not os.path.exists(out_dir+'/AllToyFits'):
      os.makedirs(out_dir+'/AllToyFits')
    out_file_toys = ROOT.TFile.Open(out_dir+"/AllToyFits/Nominal.root", "RECREATE")
    badToys = [iToy for iToy in range(0, nReplica+1) if None in all_response_means[iToy] ]#Remove any toys that had bad fits (in the pt range)
    for iToy in range(0, nReplica+1): #One list of bins per toy
      if iToy in badToys:
        continue
      all_response_means[iToy].reverse()
      toy_hist = ROOT.TH1F("Nominal_"+str(iToy), "Nominal_"+str(iToy), len(orig_hist_edges)-1, orig_hist_edges_array)
      print("Length is ", str(toy_hist.GetNbinsX()), "vs", len(all_response_means[iToy]))
      for iBin in range(1, toy_hist.GetNbinsX()+1):
        toy_hist.SetBinContent(iBin, all_response_means[iToy][iBin-1])
      toy_hist.Write(toy_hist.GetName(), ROOT.TObject.kOverwrite)
    out_file_toys.Close()

  #Return data (rms and sig hists) and all data & mc fits for use in systematic variations
  return rms_hist, sig_hist, means_data, means_mc

#Function for running fits on a nominal distribution and toys, and finding RMS of response for uncertainties
def derive_nominal_uncertainty(sample):

  print("Starting nominal uncertainty fits for sample", sample.histFile)

  myFitter = JES_BalanceFitter.JES_BalanceFitter(1.6)
  myFitter.SetGaus()
  myFitter.SetFitColor(ROOT.kRed)
  myFitter.rebin = False

  #Get bootstrap object
  in_file = ROOT.TFile.Open( sample.bootstrapFile, "READ" )
  nominal_boot_name = "bootstrap_Nominal"
  boot_object = in_file.Get(nominal_boot_name)

  nReplica = boot_object.GetNReplica()

  #Get all data and MC replicas (TH1Fs)
  hists = []
  hists.append( boot_object.GetNominal() )
  for iToy in range(0, nReplica):
    hists.append( boot_object.GetReplica(iToy) )

  bin_RMS = []
  #For each pt bin
  for iBin in range(1,hists[0].GetNbinsX()+1):
    lowEdge = hists[0].GetXaxis().GetBinLowEdge(iBin)
    upEdge = hists[0].GetXaxis().GetBinUpEdge(iBin)
    mean_fits = []
    for iToy in range(len(hists)):
      h_response_distribution = hists[iToy].ProjectionY( "h_proj_"+str(iToy)+"_"+str(iBin), iBin, iBin, "ed");
      fit_result = MJB_fit(myFitter, h_response_distribution)
      histMean = h_response_distribution.GetMean(1)
      fitMean = myFitter.GetMean()
      perError = abs(1-(histMean/(fitMean+1e-6)))
      #if(myFitter.GetMean()<0.5 or myFitter.GetMean()>1.5):
      if(perError > 0.1):
        saveBadFit(h_response_distribution, fit_result, fitMean, histMean, perError, myFitter.GetMeanError(), lowEdge, upEdge, iToy, sample.displayName)
        continue
      mean_fits.append( myFitter.GetMean() )

    nominal_response = mean_fits.pop(0)
    mean_fits = [fit for fit in mean_fits if fit != None]

    #Calculate RMS & significance
    this_rms = numpy.std(mean_fits)
    bin_RMS.append( this_rms )

  in_file.Close()
  #Make RMS hist and save to file
  out_file = ROOT.TFile.Open( sample.histFile, "UPDATE" )

  orig_hist_edges = list(hists[0].GetXaxis().GetXbins())
  orig_hist_edges_array = array.array('f', orig_hist_edges)

  rms_name = "Nominal_RMS"
  rms_hist = ROOT.TH1F(rms_name, rms_name, len(orig_hist_edges)-1, orig_hist_edges_array)
  for iBin in range(1, rms_hist.GetNbinsX()+1):
    rms_hist.SetBinContent(iBin, bin_RMS[iBin-1])

  rms_hist.Write( rms_hist.GetName(), ROOT.TObject.kOverwrite )
  out_file.Close()

#Run fits on nominal & systematic distributions, no toys!
def derive_balance(sample, rebin=False, nominal_only=False, toy_uncert=True):

  hist_to_fit = "recoilPt_PtBal"

  #Make directory for storing fits
  fitDir = sample.histFile.replace('.root', '_Plots/GaussianFits/')
  if not os.path.exists(fitDir):
    os.makedirs(fitDir)

  #Will save results to input files
  inFile = ROOT.TFile.Open( sample.histFile, "UPDATE" )

  dirList = [key.GetName() for key in inFile.GetListOfKeys() if "MJB" in key.GetName()] #List of directories
  if(nominal_only):
    dirList = [dir_name for dir_name in dirList if "MJB_Nominal" in dir_name]

  print("Running on file", sample.histFile)

  #Create fitting object and TCanvas
  myFitter = JES_BalanceFitter.JES_BalanceFitter(1.6)
  myFitter.SetGaus()
  myFitter.SetFitColor(ROOT.kRed)
  myFitter.rebin = False
  c1 = ROOT.TCanvas()

  #Get uncertainty for nominal result (derived from toys)
  if(toy_uncert):
    nominal_error_hist = inFile.Get("Nominal_RMS")

  #If we will rebin following bootstrap output
  if(rebin):
    rebin_file = ROOT.TFile.Open(sample.rebin_file, "READ")

  nominal_dir_name = [dir_name for dir_name in dirList if "MJB_Nominal" in dir_name]
  if len(nominal_dir_name) != 1 :
    print("Error, found", len(nominal_dir_name), "nominal directories, but expecting only 1: ", nominal_dir_name)
    exit(1)
  nominal_dir_name = nominal_dir_name[0]
  nominal_dir = inFile.Get( nominal_dir_name )


  print("Running over full set of directories:", dirList)
  for thisDirName in dirList:
    print("Start fitting on",thisDirName)

    histList = [hist_to_fit]
    thisDir = inFile.Get( thisDirName )


    if(rebin):
      rebin_hist = rebin_file.Get(thisDirName.replace("MJB", "sig", 1))
      if not rebin_hist:
        print("Could not find rebin histogram for", thisDirName.replace("MJB", "sig"), ", will continue for this directory without rebinning")

    thisDir.cd()
    ### For each histogram, calculate the profile and save it to the input file ###
    for histName in histList:
      this2DHist = thisDir.Get(histName)
    
      #Will also need to fit the nominal distribution in the exact same binning
      if( rebin ):
        nominal_2D_Hist = nominal_dir.Get(histName)

      xEdges = this2DHist.GetXaxis().GetXbins().GetArray()
      xEdges_list = list(this2DHist.GetXaxis().GetXbins())
      num_xBins = this2DHist.GetNbinsX()

      h_template = ROOT.TH1D("Template", "Template", num_xBins, xEdges);
      new_hist_name = "fitBalance"
      if(rebin and rebin_hist):
        new_hist_name += '_rebin'
        h_mean_nominal = h_template.Clone(new_hist_name+"_nominal");  h_mean_nominal.SetTitle(new_hist_name+"_nominal");

      h_mean   = h_template.Clone(new_hist_name);  h_mean.SetTitle(new_hist_name);
      h_redchi = h_template.Clone(new_hist_name+"_ReducedChi");  h_mean.SetTitle(new_hist_name+"_ReducedChi");
      h_median = h_template.Clone(new_hist_name+"_Median");  h_mean.SetTitle(new_hist_name+"_Median");
      h_width  = h_template.Clone(new_hist_name+"_Width");  h_mean.SetTitle(new_hist_name+"_Width");
      h_medianHist = h_template.Clone(new_hist_name+"_MedianHist");  h_mean.SetTitle(new_hist_name+"_MedianHist");

      bins_to_merge = []
      if(rebin and rebin_hist):
        bins_to_merge = get_bin_grouping(xEdges_list, rebin_hist)
      else:
        for iBin in range(1,num_xBins+1):
          bins_to_merge.append( [iBin] )


      #Loop over each pt bin
      for iBin, bin_range in enumerate(bins_to_merge):
        h_response_distribution = this2DHist.ProjectionY( "h_proj_"+str(iBin), bin_range[0], bin_range[-1], "ed");
        ptLow  = int( this2DHist.GetXaxis().GetBinLowEdge(bin_range[0]) )
        ptHigh = int( this2DHist.GetXaxis().GetBinUpEdge(bin_range[-1]) )

        if(h_response_distribution.GetEntries() < 30):
          print(sample.name, thisDirName, "bin", iBin, "(", ptLow, "->", ptHigh, ") has", h_response_distribution.GetEntries(), "entries")

        fit_result = MJB_fit(myFitter, h_response_distribution)
        mean = myFitter.GetMean()
        mean_err = myFitter.GetMeanError()
        #sigma = myFitter.GetSigma()
        #sigma_err = myFitter.GetSigmaError()
        #resolution_err = math.sqrt( (sigma_err/mean)**2 + (sigma*mean_err/(mean**2) )**2 )

        for iOldBin in range(bin_range[0], bin_range[-1]+1):
          h_mean.SetBinContent(iOldBin, mean)
          if toy_uncert and "Nominal" in thisDirName:
            h_mean.SetBinError(iOldBin, nominal_error_hist.GetBinContent(iOldBin) )
          else:
            h_mean.SetBinError(iOldBin, mean_err)
          h_redchi.SetBinContent( iOldBin, myFitter.GetChi2Ndof() )
          h_width.SetBinContent( iOldBin, myFitter.GetSigma() )

        #Save plots of the fits
        if( f_plot_fits ):
          h_response_distribution.GetXaxis().SetRangeUser(0, 2.0)
          h_response_distribution.SetMarkerSize(0.6)
          h_response_distribution.Draw()
          fit_result.Draw("same")

          AtlasStyle.myText( 0.6, 0.88, 0.8, "pt: {:} to {:}".format( ptLow, ptHigh ) )
          AtlasStyle.myText( 0.6, 0.84, 0.8, "Effective entries: {:}".format(int(h_response_distribution.GetEffectiveEntries())) )
          AtlasStyle.myText( 0.6, 0.80, 0.8, "Fit mean: {:.4f} +- {:.4f}".format(mean, mean_err) )
          AtlasStyle.myText( 0.6, 0.76, 0.8, "Hist mean: {:.4f}".format(h_response_distribution.GetMean()) )
          AtlasStyle.myText( 0.6, 0.72, 0.8, "Reduced chi2: {:.2f}".format(myFitter.GetChi2Ndof()) )

          c1.SaveAs(fitDir+'/'+thisDirName+'_Bin'+str(iBin)+'.png')
        
        if( rebin ):
          h_response_distribution_nominal = nominal_2D_Hist.ProjectionY( "h_proj_Nominal_"+str(iBin), bin_range[0], bin_range[-1], "ed");
          fit_result = MJB_fit(myFitter, h_response_distribution_nominal)
          mean = myFitter.GetMean()
          mean_err = myFitter.GetMeanError()
          for iOldBin in range(bin_range[0], bin_range[-1]+1):
            h_mean_nominal.SetBinContent(iOldBin, mean)
            h_mean_nominal.SetBinError(iOldBin, mean_err)

      h_mean.Write( h_mean.GetName(), ROOT.TObject.kOverwrite )
      h_redchi.Write( h_redchi.GetName(), ROOT.TObject.kOverwrite )
      h_width.Write( h_width.GetName(), ROOT.TObject.kOverwrite )
      if( rebin ):
        h_mean_nominal.Write( h_mean_nominal.GetName(), ROOT.TObject.kOverwrite )
      inFile.cd()


  inFile.Close()

def saveBadFit(response, fit, fitMean, histMean, perError, error, lowEdge, highEdge, iToy, sampleName):
  badDir= './gridOutput/badFits/'
  if not os.path.exists(badDir):
    os.makedirs(badDir)

  saveName = sampleName+'_Toy'+str(iToy)+'_pt'+str(lowEdge)+'_'+str(highEdge)+'.png'
  c1 = ROOT.TCanvas()

  response.Draw('p')
  fit.Draw('SAME')

  AtlasStyle.myText( 0.6, 0.88, 0.8, "Sample:  {:} - Toy {:}".format( sampleName, iToy ) )
  AtlasStyle.myText( 0.6, 0.84, 0.8, "pT:  {:} to {:}".format( lowEdge, highEdge ) )
  AtlasStyle.myText( 0.6, 0.80, 0.8, "Fit Mean: {:.4}".format( fitMean ) )
  AtlasStyle.myText( 0.6, 0.76, 0.8, "Hist Mean: {:.4}".format( histMean ) )
  AtlasStyle.myText( 0.6, 0.72, 0.8, "%-Error: {:.4}".format( perError ) )
  AtlasStyle.myText( 0.6, 0.68, 0.8, "Fit Error: {:.4}".format( error ) )

  c1.SaveAs(badDir+saveName)
