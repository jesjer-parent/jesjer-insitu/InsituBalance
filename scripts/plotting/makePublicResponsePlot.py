#Script for making the public response plot with proper formatting
import ROOT
import AtlasStyle
AtlasStyle.SetAtlasStyle()

def makePublicResponsePlot():

  sideBars = True
  is_logx = False

  #Default ATLAS style removes x-axis bars
  if( sideBars ):
    ROOT.gStyle.SetErrorX(0.5)

  jetType = "EMTopo"
  min_pt = 300
  max_pt = 3000

  min_response = 0.935
  max_response = 1.045
  min_ratio = 0.97
  max_ratio = 1.07


  #Change your hardcoded path to the input files
  input_dir = "///eos/home-e/evandewa/data/Jet_Cal/gridOutput/files/"

  #Order to be drawn on the plot
  file_names = ["Data.all.root", "Hw8.all.root", "PP8.all.root", "Shp.all.root", "Py8.all.root"]
  sample_names = ["Data", "Herwig", "Powheg+Pythia8", "Sherpa", "Pythia8"]

  colors = [ROOT.kBlack, ROOT.kGreen-5, ROOT.kMagenta, ROOT.kBlue, ROOT.kRed]
  marker_styles = [20, 21, 33, 23, 22]

  c1 = ROOT.TCanvas("MJB", "MJB", 600, 600)
  is_logy = False
  pad1, pad2, zeroLine, oneLine, ratioLine1, ratioLine2 = getRatioObjects(c1, is_logx, is_logy)
  pad1.cd()

  hists = []
  for iF, file_name in enumerate(file_names):

    input_file = ROOT.TFile.Open(input_dir+file_name, "READ")
    hist = input_file.Get("MJB_Nominal/fitBalance")
    hist.SetDirectory(0)
    hists.append(hist)
    input_file.Close()

    hist.SetMarkerSize(0.7)
    hist.SetLineWidth(1)
    hist.GetYaxis().SetTitle("#it{R}_{MJB}, #LT #it{p}_{T}^{lead jet}/#it{p}_{T}^{ref} #GT")
    hist.GetXaxis().SetTitle("#it{p}_{T}^{ref} [GeV]")

    hist.SetMarkerColor(colors[iF])
    hist.SetLineColor(colors[iF])
    hist.SetMarkerStyle(marker_styles[iF])
    hist.GetXaxis().SetRangeUser(min_pt, max_pt)
    hist.SetMaximum(max_response)
    hist.SetMinimum(min_response)

    hist.GetYaxis().SetTitleSize( 0.065 )
    hist.GetYaxis().SetTitleOffset( 1.06 )
    hist.GetYaxis().SetLabelSize( 0.06 )
    hist.GetYaxis().SetTickLength(0.02)

    if(iF==0):
      hist.DrawCopy("p")
    else:
      hist.DrawCopy("p same")

#  oneLine.Draw("same")

  #Configure legend
  leg = ROOT.TLegend(0.58, 0.79, 0.94, 0.96,"")
  leg.SetFillStyle(0)
  leg.SetEntrySeparation(0.0001)
#  leg.SetTextSize(0.05);
  leg.SetTextFont(42);

  #Dedicated ordering of legend
  leg.AddEntry(hists[0], sample_names[0], "p")#Data
  leg.AddEntry(hists[4], sample_names[4], "p")#Pythia
  leg.AddEntry(hists[3], sample_names[3], "p")#Sherpa
  leg.AddEntry(hists[2], sample_names[2], "p")#PP8
  leg.AddEntry(hists[1], sample_names[1], "p")#Herwig

  #Draw ratio plots in 2nd pad
  pad2.cd()
  for iH, hist in enumerate(hists):
    if iH == 0:
      continue
    hist.Divide(hists[0])

    #Configure
    hist.SetMinimum(min_ratio)
    hist.SetMaximum(max_ratio)
    hist.GetYaxis().SetTitle("MC / Data")
    hist.GetYaxis().SetTitleSize( 0.12 )
    hist.GetYaxis().SetTitleOffset( 0.55 )
    hist.GetYaxis().SetLabelSize( 0.11 )
    hist.GetYaxis().SetNdivisions(503)
    hist.GetYaxis().SetTickLength(0.03)

    hist.GetXaxis().SetLabelSize( 0.12 )
    hist.GetXaxis().SetTitleSize( 0.12 )
    hist.GetXaxis().SetTitleOffset( 1.25 )
    hist.GetXaxis().SetTickLength(0.07)

    if( iH == 1 ):
      hist.DrawCopy("p")
      ratioLine1.Draw("same")
      ratioLine2.Draw("same")
      hist.DrawCopy("p same")
    else:
      hist.DrawCopy("p same")

  c1.cd()
  leg.Draw("same")
  ### Draw text to plot ###
  textSize = 0.8
  yTop = 0.93
  AtlasStyle.ATLAS_LABEL(0.172,yTop, textSize, "   Internal")
  AtlasStyle.myText(0.172,yTop-0.04, textSize, "#sqrt{#it{s}} = 13 TeV, 80 fb^{-1}, multijet")
  AtlasStyle.myText(0.172,yTop-0.08, textSize, "Anti-#it{k_{t} R} = 1.0 (LCTopo+"+jetType+")")
  AtlasStyle.myText(0.172,yTop-0.12, textSize, "|#eta^{lead jet}| < 0.8")

  outName = "MJB_draft"
  if( not sideBars ):
    outName += "_noXbars"
  if( is_logx ):
    outName += "_logX"

  c1.SaveAs(outName+".pdf")

#### Get Ratio Objects ####
def getRatioObjects(c0, logX, logY):
  pad1 = ROOT.TPad("pad1","pad1",0,0.36,1,0.99)
  pad2 = ROOT.TPad("pad2","pad2",0,0.01,1,0.35)
  pad1.Draw()
  pad2.Draw()
  pad1.SetBottomMargin(0)
  pad1.SetTopMargin(0.01)
  pad2.SetTopMargin(0.05)
  pad1.SetRightMargin(0.05)
  pad2.SetRightMargin(0.05)
  pad1.SetLeftMargin(0.15)
  pad2.SetLeftMargin(0.15)
  pad2.SetBottomMargin(.33)
  zeroLine = ROOT.TF1("zl0", "0", -50000, 50000 )
  zeroLine.SetTitle("")
  zeroLine.SetLineWidth(1)
  zeroLine.SetLineStyle(7)
  zeroLine.SetLineColor(ROOT.kBlack)
  oneLine = ROOT.TF1("ol0", "1.0", -50000, 50000 )
  oneLine.SetTitle("")
  oneLine.SetLineWidth(1)
  oneLine.SetLineStyle(7)
  oneLine.SetLineColor(ROOT.kBlack)
  ratioLine1 = ROOT.TF1("ol0", "1.0", -50000, 50000 )
  ratioLine1.SetTitle("")
  ratioLine1.SetLineWidth(1)
  ratioLine1.SetLineStyle(7)
  ratioLine1.SetLineColor(ROOT.kBlack)
  ratioLine2 = ROOT.TF1("ol0", "1.02", -50000, 50000 )
  ratioLine2.SetTitle("")
  ratioLine2.SetLineWidth(1)
  ratioLine2.SetLineStyle(7)
  ratioLine2.SetLineColor(ROOT.kBlack)

  if logX:
    pad1.SetLogx()
    pad2.SetLogx()
  if logY:
    pad1.SetLogy()

  return pad1, pad2, zeroLine, oneLine, ratioLine1, ratioLine2


if __name__ == "__main__":
  makePublicResponsePlot()
