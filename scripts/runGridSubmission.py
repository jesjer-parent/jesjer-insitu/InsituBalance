#!/usr/bin/python

#####################################
# A script for MJB grid submission
# Submit grid jobs using text files with lists of containers
# For questions contact Jeff.Dandoy@cern.ch
#####################################


import os, math, sys
from time import strftime

def runGrid():
  ### User Options ###
  test = False # does not run the jobs
  
  jetType = "EMTopo"

  #config_name = "source/InsituBalance/data/config_MJB_"+jetType+".py"
  #config_name = "source/InsituBalance/data/config_MJB_"+jetType+year+".py"
  config_name = "source/InsituBalance/data/config_MJB_smallR.py"
  #config_name = "source/InsituBalance/data/config_MJB_largeR.py"

  #config_name = "source/InsituBalance/data/config_Gjet.py"
  #config_name = "source/InsituBalance/data/config_Zee.py"
  #config_name = "source/InsituBalance/data/config_Zmm.py"

  #extraTag = jetType+"_BS3_GRID4_broke" # Extra output tag for all files
  extraTag = "_smallR_"+jetType
  #extraTag = "_largeR_"+jetType+"_SoftDrop"

  #nGBPerJob = -1
  #If running on containers with many files, this will limit number of jobs
  nGBPerJob = 25
  
  timestamp = strftime("_%d%m%y")

  if not test:
    if not os.path.exists("../gridOutput"):
      os.system("mkdir ../gridOutput")
    if not os.path.exists("../gridOutput/gridJobs"):
      os.system("mkdir ../gridOutput/gridJobs")


  #### Driver options ####
  runType = 'grid'   #CERN grid
  #runType = 'local'
  #runType = 'condor' #Uchicago Condor

  ## Set this only for group production submissions ##
  production_name = ""
  #production_name = "phys-exotics"
  #production_name = "perf-jets"

  samples = {
#%%%%%%%%%%%%%%%%%%%%% R21 G+jet %%%%%%%%%%%%%%%%%%%%%%%%%%%%#

#"GjPy8_mc16a" : "mc16_13TeV.423*.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP*JETM4*_r9364_p3600",
#"GjShL_mc16a" : "mc16_13TeV.361*.Sherpa_CT10_SinglePhotonPt*JETM4*_r9364_p3600",
#"GjShN_mc16a" : "mc16_13TeV.3645*.Sherpa_222_NNPDF30NNLO_SinglePhoton_pty_*JETM4*_r9364_p3596",

#"GjPy8_mc16d" : "mc16_13TeV.423*.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP*JETM4*_r10201_p3600",
#"GjShL_mc16d" : "mc16_13TeV.361*.Sherpa_CT10_SinglePhotonPt*JETM4*_r10201_p3600",
#"GjShN_mc16d" : "mc16_13TeV.3645*.Sherpa_222_NNPDF30NNLO_SinglePhoton_pty_*JETM4*_r10201_p3596",

#"GjData17"    : "data17_13TeV.period*.physics_Main.PhysCont.DAOD_JETM4.grp17_v01_p3601",
#"GjData16"    : "data16_13TeV.period*.physics_Main.PhysCont.DAOD_JETM4.grp16_v01_p3601",
#"GjData15"    : "data15_13TeV.period*.physics_Main.PhysCont.DAOD_JETM4.grp15_v01_p3601",

#%%%%%%%%%%%%%%%%%%%%% R21 Z+jet %%%%%%%%%%%%%%%%%%%%%%%%%%%%#

#"ZeePP8_mc16a"  : "mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.deriv.DAOD_JETM3.e3601_s3126_r9364_p3600",
#"ZeeShp_mc16a"  : "mc16_13TeV.364*.Sherpa_221_*_Zee*JETM3*_r9364_p3600",

#"ZeePP8_mc16d"  : "mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.deriv.DAOD_JETM3.e3601_s3126_r10201_p3600",
#"ZeeShp_mc16d"  : "mc16_13TeV.364*.Sherpa_221_*_Zee*JETM3*_r10201_p3600",

#"ZmmPP8_mc16a"  : "mc16_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.deriv.DAOD_JETM3.e3601_s3126_r9364_p3600",
#"ZmmShp_mc16a"  : "mc16_13TeV.364*.Sherpa_221_*_Zmumu*JETM3*_r9364_p3600",

#"ZmmPP8_mc16c"  : "mc16_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.deriv.DAOD_JETM3.e3601_s3126_r10201_p3600",
#"ZmmShp_mc16c"  : "mc16_13TeV.364*.Sherpa_221_*_Zmumu*JETM3*_r10201_p3600",

#"ZjData17"      : "data17_13TeV.period*.physics_Main.PhysCont.DAOD_JETM3.grp17_v01_p3601",
#"ZjData16"      : "data16_13TeV.period*.physics_Main.PhysCont.DAOD_JETM3.grp16_v01_p3601",
#"ZjData15"      : "data15_13TeV.period*.physics_Main.PhysCont.DAOD_JETM3.grp15_v01_p3601",

#%%%%%%%%%%%%%%%%% R21 Multijet Balance %%%%%%%%%%%%%%%%%%%%%#
# "QCD_Py8_mc16a"  : "mc16_13TeV.3610*.Pythia8*JZ*W.*JETM1.*_r9364_r9315_p3600",
# "QCD_Hw8_mc16a"  : "mc16_13TeV.3644*.Herwig7EvtGen_H7UE_NNPDF30nlo_jetjet_JZ*.deriv.DAOD_JETM1.*_r9364_r9315_p3600",
# "QCD_Shp_mc16a"  : "mc16_13TeV.4261*.Sherpa_CT10_jets_JZ*.*JETM1.*_r9364_r9315_p3600",
# "QCD_PP8_mc16a"  : "mc16_13TeV.4260*.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ*.*JETM1.*_r9364_r9315_p3600",

# "QCD_Py8_mc16d"  : "mc16_13TeV.3610*.Pythia8*JZ*W.*JETM1.*_r10201_r10210_p3600",
# "QCD_Hw8_mc16d"  : "mc16_13TeV.3644*.Herwig7EvtGen_H7UE_NNPDF30nlo_jetjet_JZ*.deriv.DAOD_JETM1.*_r10201_r10210_p3600",
# "QCD_Shp_mc16d"  : "mc16_13TeV.4261*.Sherpa_CT10_jets_JZ*.*JETM1.*_r10201_r10210_p3600",
# "QCD_PP8_mc16d"  : "mc16_13TeV.4260*.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ*.*JETM1.*_r10201_r10210_p3600",

#"QCD_Py8_mc16e"  : "mc16_13TeV.3647*.Pythia8*JZ*W.*JETM1.*_r10724_r10726_p3749",
# "QCD_Hw8_mc16e"  : "",
#"QCD_Shp_mc16e"  : "mc16_13TeV.4261*.Sherpa_CT10_jets_JZ*.*JETM1.*_r10724_r10726_p3712",
# "QCD_PP8_mc16e"  : "",

#"QCD_Data15"     : "data15_13TeV.period*.physics_Main.PhysCont.DAOD_JETM1.grp15_*p3601",
#"QCD_Data16"     : "data16_13TeV.period*.physics_Main.PhysCont.DAOD_JETM1.grp16_*p3601",
#"QCD_Data17"     : "data17_13TeV.period*.physics_Main.PhysCont.DAOD_JETM1.grp17_*p3601",
#"QCD_Data18"     : "data18_13TeV.period*.physics_Main.PhysCont.DAOD_JETM1.grp18_*p3704",
}

  files = []
  outputTags = []

  for sampleName, sample in samples.iteritems():

    output_tag = sampleName + extraTag + timestamp
    submit_dir = "../gridOutput/gridJobs/submitDir_"+output_tag

    ## Configure submission driver ##
    driverCommand = ''
    if runType == 'grid':
      driverCommand = 'prun --optGridOutputSampleName='
      #driverCommand = 'prun --optSubmitFlags="--forceStaged" --optGridOutputSampleName='
      #driverCommand = 'prun --optSubmitFlags="--skipScout --excludedSite=ANALY_CERN_SHORT,ANALY_BNL_SHORT" --optGridOutputSampleName='
      if len(production_name) > 0:
        #driverCommand = ' prun --optSubmitFlags="--memory=5120 --official --skipScout" --optGridOutputSampleName='
        driverCommand = 'prun --optSubmitFlags="--official" --optGridOutputSampleName='
        #driverCommand = 'prun --optSubmitFlags="--official --memory=3240" --optGridOutputSampleName='
        driverCommand += 'group.'+production_name
      else:
        driverCommand += 'user.%nickname%'
      driverCommand += '.%in:name[2]%.'+output_tag
      if( nGBPerJob > 0):
        driverCommand += ' --optGridNGBPerJob '+str(nGBPerJob)
      # if( nFilesPerJob > 0):
      #   driverCommand += ' --optGridNFilesPerJob '+str(nGBPerJob)
    elif runType == 'condor':
      driverCommand = ' condor --optFilesPerWorker 10 --optBatchWait'
    elif runType == 'local':
      driverCommand = ' direct'


    command = 'xAH_run.py'
    if runType == 'grid':
      command += ' --inputRucio '

    if 'sampleLists' in sample:
      command += ' --inputList'
    command += ' --files '+sample
    command += ' --config '+config_name
    command += ' --force --submitDir '+submit_dir
    command += ' '+driverCommand
    #command += ' --optGridNFilesPerJob 1'
    # command += ' --optGridNJobs 700'

    print command
    if not test: os.system(command)

  #  if not "Sherpa" in file_in:
  #    command += ' --oneJobPerFile'

if __name__ == "__main__":
  runGrid()

#  #Code can be edited to run in a loop like below, allowing more automation when running full results
#  for jetType in ["EM", "PF"]:
#    for year in ["17", "16", "15"]:
#      runGrid(jetType, year)
