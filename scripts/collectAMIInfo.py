###########################################################
# collectAMIInfo.py                                       #
# A short script for collecting cross section and filter  #
# efficiency information for a list of samples from AMI.  #
# For more information contact Jeff.Dandoy@cern.ch        #
###########################################################

import os, argparse
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--file", dest='file', default="CS.txt",
     help="File including container names")
args = parser.parse_args()

## If output file already exists, remove it ##
if os.path.isfile( "XS_"+os.path.basename(args.file) ):
  os.remove("XS_"+os.path.basename(args.file))

with open( args.file, 'r') as inputContainers :
  for container in inputContainers:
    if container.startswith('#'):
      continue

    ## Correct name of container to look at EVNT gen version
    originalName = container.rstrip()
    container = container.split('.')
    containerName = container[0]+'.'+container[1]+'.'+container[2]+'.evgen.EVNT.'
    containerTag = container[-1].rstrip().split('_')[0]

    containerName += containerTag+'/'

    ## Setup Python AMI Client
    try:
      import pyAMI.client
      import pyAMI.atlas.api as AtlasAPI
      import unicodedata
    except ImportError:
      raise ImportError('Unable to import pyAMI, requires setupATLAS + lsetup pyAMI')
      exit(1)
    client = pyAMI.client.Client('atlas')
    AtlasAPI.init()

    ## collect relevant dataset information from AMI client
    try:
      print containerName
      datasetInfo = AtlasAPI.get_dataset_info(client, containerName)[0]
    except:
      print "Failed, will try with", originalName
      datasetInfo = AtlasAPI.get_dataset_info(client, originalName)[0]
    datasetNumber = int(datasetInfo['datasetNumber'])
    try: #Because of diboson samples
      crossSection_mean = float(datasetInfo['crossSection_mean'])
      crossSection_unit = datasetInfo['crossSection_unit']
    except:
      print datasetInfo
      crossSection_mean = float(datasetInfo['crossSection'])
      crossSection_unit = None

    try: #Because of diboson samples
      GenFiltEff_mean = float(datasetInfo['approx_GenFiltEff'])
    except:
      GenFiltEff_mean = float(datasetInfo['GenFiltEff_mean'])

    totalEvents = int(datasetInfo['totalEvents'])


    ## Set all cross section info to inverse fb
    if not crossSection_unit:
      crossSection_mean = crossSection_mean*1e3
    elif "nano" in crossSection_unit:
      crossSection_mean = crossSection_mean*1e3
    elif "pico" in crossSection_unit:
      crossSection_mean = crossSection_mean
    elif "femto" in crossSection_unit:
      crossSection_mean = crossSection_mean/1e3

    RelUncertainty = 1.
    datasetName = originalName.split('.')[2] 
    kFactor = 1.
    ## Write out results to file
    with open("XS_"+os.path.basename(args.file), 'a') as g:
      print >> g, datasetNumber, datasetName, '{0:.4E}'.format(crossSection_mean), '{0}'.format(kFactor), '{0}'.format(GenFiltEff_mean), '{0}'.format(RelUncertainty)

# DSID  Physics short               xsec  k-fac eff   rel. unc.

    ## Remove temporary text file
#    os.system("rm tmp.txt")
