#!/usr/bin/env python

############################################################
# Script to histogram trigger yields for the InsituBalance
# package, as a first step to deriving trigger pt thresholds.
#
# author: jeff.dandoy AT cern.ch
############################################################

import math, time, glob, argparse

parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot")

parser.add_argument("--mc", dest='mc', action='store_true', default=False, help="Running on mc.")

parser.add_argument("--file", dest='file', default="", help="Input file name when running over a single file. Has priority over dir+tag.")
parser.add_argument("--dir", dest='dir', default="", help="Directory containing input files, used if --file is not set.")
parser.add_argument("--tag", dest='tag', default="", help="Tag for selecting input files in --dir.  Used if --file is not set.")
parser.add_argument("--tree", dest='treeName', default="outTree_Nominal", help="Name of TTree to get events from.")

parser.add_argument("--outDir", dest='outDir', default="", help="Directory for output files.  Defaults to TriggerPlots/ in the input file directory.")
parser.add_argument("--outName", dest='outName', default="Study", help="Tag to append to output name.")

parser.add_argument("--nevents", dest='nevents', type=int, default = -1, help="Maximum number of events to run over.")
parser.add_argument("--triggers", dest='triggers', default="HLT_j420,HLT_j360,HLT_j260", help="Comma separated list of triggers to use.  Must be in order, from highest pt to lowest.")
args = parser.parse_args()

import ROOT, array, sys, os

#Types of trigger efficiency calculations.  Use unprescaled for leading unprescaled trigger,
#prescaled for supporting triggers in data, and unbiased for true turn on in MC.
efficiency_types = ["trigonly", "unbiased", "wrong", "trigdecision", "emulate"]

#List of L1 triggers seeded by each HLT trigger
L1_list = {'HLT_j420':'L1_J100', 'HLT_j380':'L1_J100', 'HLT_j360':'L1_J100', 'HLT_j260':'L1_J75', 'HLT_j175':'L1_J50', 'HLT_j110':'L1_J30', 'HLT_j85':'L1_J20'}

# Run over events in the TTrees to derive trigger turn-on curves
def deriveTriggerHists():
    if len(args.file) > 0:
      fileNames = [args.file]
    else:
      fileNames = glob.glob( args.dir+'/*'+args.tag+'*.root' )

    args.triggers = args.triggers.split(',')

    #Make the output directory & configure file names
    if len(args.outDir) == 0 :
      args.outDir = os.path.dirname( fileNames[0] )+"/triggerPlots/"

    outName = args.outDir+'/TriggerHists_'+args.outName+'.root'

    if not os.path.exists( args.outDir ):
      os.makedirs( args.outDir )

    # 2D lists of histograms with format trigger:type
    h_recoilpt_numer = []
    h_recoilpt_denom = []
    h_jetpt_numer = []
    h_jetpt_denom = []


    outFile = ROOT.TFile.Open(outName, "RECREATE")
    #loop over each trigger
    for trigger in args.triggers:
      #If this is the last trigger, skip it, as we have no reference trigger for it!
      if trigger == args.triggers[-1]:
        continue

      h_recoilpt_numer.append( [] )
      h_jetpt_numer.append( [] )
      h_recoilpt_denom.append( [] )
      h_jetpt_denom.append( [] )

      #Create histograms
      for thisType in efficiency_types:
        # numerator for recoilpt
        h_recoilpt_numer[-1].append( ROOT.TH1F(thisType+"_recoilpt_numer_"+trigger, "h_recoilpt_numer", 200, 0, 1000) )
        # numerator jetpt
        h_jetpt_numer[-1].append( ROOT.TH1F(thisType+"_jetpt_numer_"+trigger, "h_jetpt_numer", 200, 0, 1000) )
        # denominator for recoilpt
        h_recoilpt_denom[-1].append( ROOT.TH1F(thisType+"_recoilpt_denom_"+trigger, "h_recoilpt_denom", 200, 0, 1000) )
        # denominator for jetpt
        h_jetpt_denom[-1].append( ROOT.TH1F(thisType+"_jetpt_denom_"+trigger, "h_jetpt_denom", 200, 0, 1000) )



    totalEntries = 0
    totalCount = 0
    for fileName in fileNames:

      inFile = ROOT.TFile.Open(fileName, "READ")
      tree = inFile.Get(args.treeName)
      print fileName
      totalEntries += tree.GetEntries()
      inFile.Close()

    print "Running over a total of ", totalEntries, "events"

    #Run over each file
    for fileName in fileNames:

      inFile = ROOT.TFile.Open(fileName, "READ")
      tree = inFile.Get(args.treeName)
      numEntries = tree.GetEntries()


      #Weight by cutflow if using MC
      cutflowWeight = 1
      if args.mc:
        for key in inFile.GetListOfKeys():
          if "cutflow" in key.GetName() and not "weight" in key.GetName():
            hist = inFile.Get(key.GetName())
            cutflowWeight = 1./hist.GetBinContent(1)
      print "Cutflow weight is ", cutflowWeight


      #Set branches
      tree.SetBranchStatus('*', 0)
      jet_pt =   ROOT.std.vector('float')()
      tree.SetBranchStatus( "jet_pt", 1)
      tree.SetBranchAddress( "jet_pt", jet_pt)
      recoilPt = array.array('f',[0])
      tree.SetBranchStatus( "recoilPt", 1)
      tree.SetBranchAddress( "recoilPt", recoilPt)
      passedTriggers =   ROOT.std.vector('string')()
      tree.SetBranchStatus( "passedTriggers", 1)
      tree.SetBranchAddress( "passedTriggers", passedTriggers)
      weight =   array.array('f', [0])
      tree.SetBranchStatus( "weight", 1)
      tree.SetBranchAddress( "weight", weight)
      triggerPrescales =   ROOT.std.vector('float')()
      tree.SetBranchStatus( "triggerPrescales", 1)
      tree.SetBranchAddress( "triggerPrescales", triggerPrescales)

      isPassBitsNames =   ROOT.std.vector('string')()
      tree.SetBranchStatus( "isPassBitsNames", 1)
      tree.SetBranchAddress( "isPassBitsNames", isPassBitsNames)
      isPassBits =   ROOT.std.vector('int')()
      tree.SetBranchStatus( "isPassBits", 1)
      tree.SetBranchAddress( "isPassBits", isPassBits)

      max_HLT_jet_pt = array.array('f', [0])
      tree.SetBranchStatus( "max_HLT_jet_pt", 1)
      tree.SetBranchAddress( "max_HLT_jet_pt", max_HLT_jet_pt)
      max_L1_jet_pt =  array.array('f', [0])
      tree.SetBranchStatus( "max_L1_jet_pt", 1)
      tree.SetBranchAddress( "max_L1_jet_pt", max_L1_jet_pt)


      print "Running over ", tree.GetEntries(), "entries"
      count = 0
      while tree.GetEntry(count):
        count += 1
        totalCount += 1

        #If we are running over a set number of events
        if (args.nevents > -1) and (count > args.nevents):
          break

        #Print the status and the expected time to finish
        if totalCount%1e5 == 0:  print "Event ", totalCount, ". It's been", (time.time() - startTime)/60. , "minutes.  Event rate is ", totalCount/(time.time()-startTime), " events per second.  We need about ", (totalEntries-totalCount)*(time.time()-startTime)/totalCount/60., " more minutes."

        #Look at each trigger
        for iT, trigger in enumerate(args.triggers):

          #Skip last trigger
          if trigger == args.triggers[-1]:
            continue
          lower_trigger = args.triggers[iT+1]

          if not args.mc:
            pT = list(passedTriggers)
            weight[0] = 1.
            cutflowWeight = 1.
          for iType, thisType in enumerate(efficiency_types):

            # Correct biased - for prescaled triggers
            # numerator is all events passing this trigger
            # denominator is all events passing this trigger or the next lowest trigger
            if thisType == "wrong":
              if trigger in passedTriggers or lower_trigger in passedTriggers:
                h_recoilpt_denom[iT][iType].Fill( recoilPt[0], weight[0]*cutflowWeight)
                h_jetpt_denom[iT][iType].Fill( jet_pt[0], weight[0]*cutflowWeight)
              if trigger in passedTriggers:
                h_recoilpt_numer[iT][iType].Fill( recoilPt[0], weight[0]*cutflowWeight )
                h_jetpt_numer[iT][iType].Fill( jet_pt[0], weight[0]*cutflowWeight )

            # unbiased - This is for MC when trigger decision is perfect
            # numerator is all events passing this trigger
            # denominator is all events passing any trigger
            elif thisType == "unbiased":
              h_recoilpt_denom[iT][iType].Fill( recoilPt[0], weight[0]*cutflowWeight )
              h_jetpt_denom[iT][iType].Fill( jet_pt[0], weight[0]*cutflowWeight )
              if trigger in passedTriggers:
                h_recoilpt_numer[iT][iType].Fill( recoilPt[0], weight[0]*cutflowWeight )
                h_jetpt_numer[iT][iType].Fill( jet_pt[0], weight[0]*cutflowWeight )

            # Unprescaled method - for when trigger in question is unprescaled
            # numerator is events passing this trigger and the next lowest trigger
            # denominator is events passing the next lowest trigger
            elif thisType == "trigonly":
              if lower_trigger in passedTriggers:
                h_recoilpt_denom[iT][iType].Fill( recoilPt[0], weight[0]*cutflowWeight)
                h_jetpt_denom[iT][iType].Fill( jet_pt[0], weight[0]*cutflowWeight)
              if trigger in passedTriggers and lower_trigger in passedTriggers:
                h_recoilpt_numer[iT][iType].Fill( recoilPt[0], weight[0]*cutflowWeight )
                h_jetpt_numer[iT][iType].Fill( jet_pt[0], weight[0]*cutflowWeight )

            elif thisType == "trigdecision":
              trigger_L1 = L1_list[trigger]
              list_isPassBitsNames = list(isPassBitsNames)
              prescale_bit_index = list_isPassBitsNames.index(trigger)
              passthrough_decision = 0x4 & isPassBits.at( prescale_bit_index )

              if (lower_trigger in passedTriggers) and (trigger_L1 in passedTriggers) and not passthrough_decision:
                h_recoilpt_denom[iT][iType].Fill( recoilPt[0], weight[0]*cutflowWeight)
                h_jetpt_denom[iT][iType].Fill( jet_pt[0], weight[0]*cutflowWeight)

                #Nested in denominator
                if( trigger in passedTriggers ):
                  h_recoilpt_numer[iT][iType].Fill( recoilPt[0], weight[0]*cutflowWeight)
                  h_jetpt_numer[iT][iType].Fill( jet_pt[0], weight[0]*cutflowWeight )

            elif thisType == "emulate":
              #If L1 and HLT trigger jets were not found, this is an odd event, skip it
              if( lower_trigger in passedTriggers and max_HLT_jet_pt > 0 and max_L1_jet_pt > 0):
                h_recoilpt_denom[iT][iType].Fill( recoilPt[0], weight[0]*cutflowWeight)
                h_jetpt_denom[iT][iType].Fill( jet_pt[0], weight[0]*cutflowWeight)

                #Nested in denominator
                HLT_cut = int(trigger.replace('HLT_j',''))
                L1_cut = int(L1_list[trigger].replace('L1_J',''))
                if( max_L1_jet_pt[0] >= L1_cut and max_HLT_jet_pt[0] >= HLT_cut ):
                  h_recoilpt_numer[iT][iType].Fill( recoilPt[0], weight[0]*cutflowWeight)
                  h_jetpt_numer[iT][iType].Fill( jet_pt[0], weight[0]*cutflowWeight )

      inFile.Close()

    #Loop over all histograms and set their x-axis minimum to be the pt of the next lowest pt trigger.
    #Below this both triggers are similarly unefficient, so the calculation becomes less meaningful
    for iT, trigger in enumerate(args.triggers):
      #If this is the last trigger, skip it, as we have no reference trigger for it!
      if trigger == args.triggers[-1]:
        continue

      lower_trigger_pt = int(args.triggers[iT+1].replace('HLT_j',''))

      for iTy, thisType in enumerate(efficiency_types):
        h_recoilpt_numer[iT][iTy].GetXaxis().SetRangeUser(lower_trigger_pt, 1000)
        h_jetpt_numer[iT][iTy].GetXaxis().SetRangeUser(lower_trigger_pt, 1000)
        h_recoilpt_denom[iT][iTy].GetXaxis().SetRangeUser(lower_trigger_pt, 1000)
        h_jetpt_denom[iT][iTy].GetXaxis().SetRangeUser(lower_trigger_pt, 1000)

    outFile.Write()
    outFile.Close()

if __name__ == "__main__":

  startTime = time.time()


  deriveTriggerHists()
  print "Finished creating turn-on curve histograms from TTree"

