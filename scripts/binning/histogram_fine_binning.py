import array, os, sys
import math
import time, glob
import argparse


parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot")
parser.add_argument("--tree", dest='tree', default="outTree_Nominal", help="Input TTree name")
parser.add_argument("--outName", dest='outName', default="", help="Tag to append to output name.")
parser.add_argument("--outDir", dest='outDir', default="", help="Directory of output file.")
parser.add_argument("--file", dest='file', default="", help="Input file name")
parser.add_argument("--dir", dest='dir', default="", help="Directory for input files, if --file not used.")
parser.add_argument("--tag", dest='tag', default="", help="Substring tag for input files, if --file not used.")
args = parser.parse_args()

import ROOT

####################### User defined variables ###############################
if( 'Data15' in args.file ):
  triggers = ["HLT_j360","HLT_j260","HLT_j175", "HLT_j110"]
  trigger_pts = [500       ,400       ,300       , 200]
elif( 'Data16' in args.file ):
  triggers = ["HLT_j380","HLT_j260","HLT_j175", "HLT_j110"]
  trigger_pts = [500       ,400       ,300       ,200]
elif( 'Data17' in args.file ):
  triggers = ["HLT_j420", "HLT_j360","HLT_j260","HLT_j175", "HLT_j110"]
  trigger_pts = [550       ,500       ,400       ,300       , 200]
elif( 'Data18' in args.file ):
  triggers = ["HLT_j360","HLT_j260","HLT_j175", "HLT_j110"]
  trigger_pts = [500       ,400       ,300       , 200]

print "Using triggers", triggers
print "Using trigger_pts", trigger_pts

### Make output file ###
if not args.outDir:
  args.outDir = os.path.dirname( args.file )
if not os.path.exists( args.outDir ):
  os.makedirs( args.outDir )

outFile = ROOT.TFile.Open(args.outDir+'/FineBinningHist_'+args.outName+".root", "RECREATE")
#Recoil pt vs subleading jet pt
h_finept = ROOT.TH2F("finept","finept",300,0,3000., 300, 0, 3000)
h_finept.GetXaxis().SetTitle("Recoil p_{T} (GeV)")
h_finept.GetYaxis().SetTitle("2nd jet p_{T} (GeV)")

h_finept_tightAsym = ROOT.TH2F("finept_tightAsym","finept_tightAsym",300,0,3000., 300, 0, 3000)
h_finept_tightAsym.GetXaxis().SetTitle("Recoil p_{T} (GeV)")
h_finept_tightAsym.GetYaxis().SetTitle("2nd jet p_{T} (GeV)")

### Find input files
if len(args.file) > 0:
  fileNames = [args.file]
else:
  fileNames = glob.glob( args.dir+'/*'+args.tag+'*.root' )


totalEntries = 0
totalCount = 0
for fileName in fileNames:

  inFile = ROOT.TFile.Open(fileName, "READ")
  tree = inFile.Get(args.tree)
  print fileName
  totalEntries += tree.GetEntries()
  inFile.Close()



### Loop over input files and connect branches ###
for fileName in fileNames:
  inFile = ROOT.TFile.Open(fileName, "READ")
  tree = inFile.Get(args.tree)

  numEntries = tree.GetEntries()
  tree.SetBranchStatus('*', 0)
  recoil_pt = array.array('f',[0])
  tree.SetBranchStatus( "recoilPt", 1)
  tree.SetBranchAddress( "recoilPt", recoil_pt)
  #weight = array.array('f',[0])
  #tree.SetBranchStatus( "weight", 1)
  #tree.SetBranchAddress( "weight", weight)
  passedTriggers =   ROOT.std.vector('string')()
  tree.SetBranchStatus( "passedTriggers", 1)
  tree.SetBranchAddress( "passedTriggers", passedTriggers)
  jet_pt =   ROOT.std.vector('float')()
  tree.SetBranchStatus( "jet_pt", 1)
  tree.SetBranchAddress( "jet_pt", jet_pt)

  startTime = time.time()

  ## Loop over input TTree ##
  count = 0
  print "Running on " , numEntries, "entries"
  while tree.GetEntry(count):
    count += 1
    totalCount += 1
    if totalCount%1e5 == 0:  print "Event ", totalCount, ". It's been", (time.time() - startTime)/60. , "minutes.  Event rate is ", totalCount/(time.time()-startTime), " events per second.  We need about ", (totalEntries-totalCount)*(time.time()-startTime)/totalCount/60., " more minutes."


    # For each trigger we're using
    for iT, trigger_pt in enumerate(trigger_pts):
      #If it passes the trigger efficiency cut
      if recoil_pt[0] > trigger_pt:
        #Check the one trigger for this pt range
        if triggers[iT] in passedTriggers:
          h_finept.Fill( recoil_pt[0], jet_pt[1] )

          if(  (jet_pt[1] / recoil_pt[0]) <= 0.7 ):
            h_finept_tightAsym.Fill( recoil_pt[0], jet_pt[1] )
        break #only check triggers once

  inFile.Close()

outFile.Write()
outFile.Close()

