#!/usr/bin/env python

############################################################
# Script to run histogram_trigger_yields.py over many files
# in parallel and combine the output, reducing the amount of
# time it requires to derive efficiency turn-on curves.
#
# author: jeff.dandoy AT cern.ch
############################################################

import math, time, glob, argparse, subprocess

parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot")
parser.add_argument("--ncores", dest='ncores', default="4", type=int, help="Number of parallel jobs to run.")

parser.add_argument("--mc", dest='mc', action='store_true', default=False, help="Running on mc.")

parser.add_argument("--dir", dest='dir', default="", help="Directory containing input files, used if --file is not set.")
parser.add_argument("--tag", dest='tag', default="", help="Tag for selecting input files in --dir.  Used if --file is not set.")
parser.add_argument("--tree", dest='tree', default="outTree_Nominal", help="Name of TTree to get events from.")

parser.add_argument("--outDir", dest='outDir', default="", help="Directory for output files.  Defaults to TriggerPlots/ in the input file directory.")
parser.add_argument("--outName", dest='outName', default="Study", help="Tag to append to output name.")

parser.add_argument("--nevents", dest='nevents', type=int, default = -1, help="Maximum number of events to run over.")
parser.add_argument("--triggers", dest='triggers', default="HLT_j420,HLT_j360,HLT_j260", help="Comma separated list of triggers to use.  Must be in order, from highest pt to lowest.")
args = parser.parse_args()

import ROOT, array, sys, os


# Run over events in the TTrees to derive trigger turn-on curves
def run_deriveTriggerHists():

    fileNames = glob.glob( args.dir+'/*'+args.tag+'*.root' )

    script_path = os.path.dirname(os.path.realpath(__file__))

    #Make the output directory & configure file names
    if len(args.outDir) == 0 :
      args.outDir = os.path.dirname( fileNames[0] )+"/triggerPlots/"

    if not os.path.exists('logs/'):
      os.makedirs('logs/')

    pids, logFiles = [], []
    #Build the python command for each file
    for fileName in fileNames:
      baseFileName = fileName.split('/')[-1].replace('.root','')
      command = "python "+script_path+'/histogram_trigger_yields.py'
      if args.mc:
        command += ' --mc'
      command += ' --file '+fileName
      command += ' --tree '+args.tree
      command += ' --outDir '+args.outDir
      command += ' --outName '+args.outName+'_TMP_'+baseFileName
      command += ' --triggers '+args.triggers
      command += ' --nevents '+str(args.nevents)

      logFile = 'logs/histogram_trigger_yields_{0}.log'.format(baseFileName)

      if len(pids) >= args.ncores:
        wait_completion(pids, logFiles)
      print command
      res = submit_local_job(command, logFile)
      pids.append(res[0])
      logFiles.append(res[1])

    # Wait for all jobs to finish
    wait_all(pids, logFiles)
    for f in logFiles:
      f.close()

    # Now combine output files into 1
    os.system('hadd -f '+args.outDir+'/TriggerHists_'+args.outName+'.root '+args.outDir+'/TriggerHists_*'+args.outName+'*TMP*.root')
    os.system('rm '+args.outDir+'/TriggerHists_*'+args.outName+'*TMP*.root')


def submit_local_job(exec_sequence, logfilename):
  output_f=open(logfilename, 'w')
  pid = subprocess.Popen(exec_sequence, shell=True, stderr=output_f, stdout=output_f)
  time.sleep(0.5)  #Wait to prevent opening / closing of several files

  return pid, output_f

def wait_completion(pids, logFiles):
  print """Wait until the completion of one of the launched jobs"""
  while True:
    for pid in pids:
      if pid.poll() is not None:
        print "\nProcess", pid.pid, "has completed"
        logFiles.pop(pids.index(pid)).close()  #remove logfile from list and close it
        pids.remove(pid)

        return
    print ".",
    sys.stdout.flush()
    time.sleep(0.5) # wait before retrying

def wait_all(pids, logFiles):
  print """Wait until the completion of all launched jobs"""
  while len(pids)>0:
    wait_completion(pids, logFiles)
  print "All jobs finished!"

if __name__ == "__main__":

  run_deriveTriggerHists()
  print "Finished creating turn-on curve histograms from all input files"

