#!/usr/bin/env python

############################################################
# Script to derive and fit trigger efficiency turn-on curves
# for the InsituBalance package, allowing the fully efficient
# pt threshold on the recoil object to be chosen.
# Take the output of histogram_trigger_yields.py as input.
#
# author: jeff.dandoy AT cern.ch
############################################################

import math, time, glob, argparse

parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot")
parser.add_argument("--file", dest='file', default="", help="Input file name when running over a single file. Has priority over dir.")
parser.add_argument("--dir", dest='dir', default="", help="Input path for multiple files.")

args = parser.parse_args()

import ROOT, array, sys, os
sys.path.insert(0, os.path.dirname(os.path.realpath(__file__))+'/../plotting/')
import AtlasStyle
AtlasStyle.SetAtlasStyle()

efficiency_types = ["trigdecision", "emulate"]
#efficiency_types = ["trigonly", "wrong", "unbiased", "trigdecision", "emulate"]
variables = ["recoilpt","jetpt"]
colors = [ROOT.kOrange-3, ROOT.kAzure-7, ROOT.kGreen-5, ROOT.kMagenta-5, ROOT.kGray+1, ROOT.kRed, ROOT.kCyan-3, ROOT.kViolet]

import re

def atof(text):
    try:
        retval = float(text)
    except ValueError:
        retval = text
    return retval

#Sort strings by found integers
def natural_keys(text):
    return [ atof(c) for c in re.split(r'[+-]?([0-9]+(?:[.][0-9]*)?|[.][0-9]+)', text) ]

def get_and_fit_curves(fileName):

  inFile = ROOT.TFile.Open(fileName, "READ")

  #Get list of all triggers used from histogram names
  #Hist name is of format like: prescaled_recoilpt_numer_HLT_j360
  trigger_list = []
  inputHists = []
  for key in inFile.GetListOfKeys():
    name = key.GetName()
    trigger_name = '_'.join(name.split('_')[3:])
    trigger_list.append( trigger_name )

  #Get only unique triggers:
  trigger_list = list(set(trigger_list))

  #Sort trigger list
  trigger_list.sort(key = natural_keys )

  hists = {}
  fits = {}
  pts = {}
  for var in variables:
    hists[var] = {}
    fits[var] = {}
    pts[var] = {}
    for eff in efficiency_types:
      hists[var][eff] = {}
      fits[var][eff] = {}
      pts[var][eff] = {}

      for trig in trigger_list:

        h_numer = inFile.Get( eff+"_"+var+"_numer_"+trig )
        h_denom = inFile.Get( eff+"_"+var+"_denom_"+trig )

        minBin = h_numer.GetXaxis().GetFirst()
        minPt = h_numer.GetXaxis().GetBinLowEdge( minBin )
        maxPt = 300+int(trig.replace('HLT_j',''))
        print("For trigger", trig, " min pt ", minPt, ", max pt ", maxPt)

        #Set bin content below region of interest to zero
        #Otherwise plot looks busy for lower-pt triggers
        for iBin in range(1, minBin):
          h_numer.SetBinContent(iBin, 0)
          h_denom.SetBinContent(iBin, 0)

        thisTEff = ROOT.TEfficiency(h_numer, h_denom)
        thisTEff.SetMarkerSize( 0.5 )

        thisFit = ROOT.TF1( 'Fit_'+eff+'_'+var+'_'+trig, EffFit, minPt, maxPt, 3 )
        thisFit.SetParameters(0.8, 300, 50) #Values from a low pt trigger turn-on, just to have a good starting point
        thisFit.SetParLimits(0, 0., 1.) #Maximum efficiency, prescale_1 * prescaled_2, must be b/w 0 and 1
        thisTEff.Fit(thisFit)
        thisEfficiencyPt = getEfficiencyPoint( thisFit ) #Get pt of full efficiency

        #Save results
        hists[var][eff][trig] = thisTEff
        fits[var][eff][trig]  = thisFit
        pts[var][eff][trig]   = thisEfficiencyPt

  return hists, fits, pts, trigger_list


def plot_oneFile_trigComparison(hists, fits, pts, fileTag, trigger_list, outDir):

  for var in variables:
    for eff in efficiency_types:
      c1 = ROOT.TCanvas()
      leg = ROOT.TLegend(0.5, 0.6, 0.9, 0.9)
      leg.SetHeader("Trigger: p_{T} eff. thresh (GeV)","C");

      hist_list = hists[var][eff]
      fit_list = fits[var][eff]
      pt_list = pts[var][eff]

      iT = 0
      for trig_name, hist in hist_list.items():

        print(hist)
        leg.AddEntry( hist, trig_name+': '+str(int(pt_list[trig_name])), "l")

        # Scale hist & fits up to 1, so that different triggers can be plotted together?
        hist.SetTitle(";"+var+" (GeV); Efficiency")
        hist.SetLineColor( colors[iT] )
        hist.SetMarkerColor( colors[iT] )
        if iT == 0:
          hist.Draw("")
          ROOT.gPad.Update()
          graph = hist.GetPaintedGraph()
          graph.SetMaximum(2)
          graph.GetXaxis().SetLimits(0,700)
          ROOT.gPad.Update()
        else:
          hist.Draw("same")
          graph = hist.GetPaintedGraph()
          ROOT.gPad.Update()

        fit_list[trig_name].SetLineColor( colors[iT] )
        fit_list[trig_name].Draw("same")
        iT += 1


      AtlasStyle.ATLAS_LABEL(0.2,0.85, 0.05,"    Internal")
      AtlasStyle.myText(0.2,0.79,0.05, "#sqrt{s} = 13 TeV")

      leg.Draw("same")

      plotName = outDir+'/TrigComparison_'+fileTag+'_'+eff+'_'+var+'.png'
      c1.SaveAs(plotName);
      c1.Clear();

def plot_oneTrigger_fileComparison(hists, fits, pts, fileTags, trigger_list, outDir):

  for var in variables:
    for eff in efficiency_types:
      for trig_name in trigger_list:

        c1 = ROOT.TCanvas()
        leg = ROOT.TLegend(0.5, 0.6, 0.9, 0.9)
        leg.SetHeader("Trigger:p_{T} eff. thresh (GeV)","C");

        iF = 0
        for fileTag in fileTags:

          #Not all triggers are available for each file, so skip if missing
          if trig_name not in hists[fileTag][var][eff]:
            continue

          hist = hists[fileTag][var][eff][trig_name]
          fit  = fits[fileTag][var][eff][trig_name]
          pt   = pts[fileTag][var][eff][trig_name]

          leg.AddEntry( hist, fileTag+': '+str(int(pt)), "l")

          hist.SetTitle(";"+var+" (GeV); Efficiency")
          hist.SetLineColor( colors[iF] )
          hist.SetMarkerColor( colors[iF] )
          if iF == 0:
            hist.Draw("")
            ROOT.gPad.Update()
            graph = hist.GetPaintedGraph()
            graph.SetMaximum(2)
            ROOT.gPad.Update()
          else:
            hist.Draw("same")
            graph = hist.GetPaintedGraph()
            ROOT.gPad.Update()

          fit.SetLineColor( colors[iF] )
          fit.Draw("same")
          iF += 1



        AtlasStyle.ATLAS_LABEL(0.2,0.85, 0.05,"    Internal")
        AtlasStyle.myText(0.2,0.79,0.05, "#sqrt{s} = 13 TeV")

        leg.Draw("same")

        plotName = outDir+'/FileComparison_'+eff+'_'+trig_name+'_'+var+'.png'

        c1.SaveAs(plotName);
        c1.Clear();


#Get point at which curve is 99% efficient
def getEfficiencyPoint( EfficiencyFit ):

  maxEff = EfficiencyFit.GetParameter(0)
  print("Maximum efficiency is at ", maxEff)
  return int(math.ceil(EfficiencyFit.GetX( 0.99*maxEff )))

#Fit function for pt efficiency curve
def EffFit(x, p):

  #Assumes trigger efficiency goes from zero to 1, does not work for prescaled method!
  #return (1/2.)*(1+ROOT.TMath.Erf( (x[0]-p[0])/(math.sqrt(2)*p[1]) ) )

  #Allows maximum trigger efficiency to float, necessary for prescaled method!
  #Max value = prescale_1 * prescaled_2
  return (p[0]/2.)*(1+ROOT.TMath.Erf( (x[0]-p[1])/(math.sqrt(2)*p[2]) ) )

if __name__ == "__main__":

  hists = {}
  fits = {}
  pts = {}
  fileList = []
  fileTags = []
  trigger_list = []

  if len(args.file) > 0:
    fileList = [args.file]
  else:
    fileList = glob.glob(args.dir+'/TriggerHists_*.root')

  print(fileList)
  outDir = os.path.dirname(fileList[0])

  for fileName in fileList:
    print("Deriving hists for file", fileName)
    fileTag = fileName.split('/')[-1].replace('.root','').split('_')[-1]
    fileTags.append( fileTag )
    this_hists, this_fits, this_pts, this_triggers = get_and_fit_curves(fileName)
    hists[fileTag] = this_hists
    fits[fileTag] = this_fits
    pts[fileTag] = this_pts
    trigger_list += this_triggers

  trigger_list = list(set(trigger_list))
  print("Trigger list is ", trigger_list)

  for fileTag in fileTags:
    plot_oneFile_trigComparison(hists[fileTag], fits[fileTag], pts[fileTag], fileTag, trigger_list, outDir)

  print("File list is ", fileTags)
  if len(fileTags) > 1:
    plot_oneTrigger_fileComparison(hists, fits, pts, fileTags, trigger_list, outDir)

  print("Finished creating, fitting, and plotting turn-on curves")

