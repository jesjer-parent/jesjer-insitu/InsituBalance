import array, os, sys
import math
import time, glob
import argparse


parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot")
parser.add_argument("--file", dest='file', default="", help="Input file name")
parser.add_argument("--deriveBinning", dest='deriveBinning', action='store_true', default=False, help="Run the binning finder with full MJB statistics")
parser.add_argument("--iterativeCutoff", dest='iterativeCutoff', action='store_true', default=False, help="Check statistics for various 2nd jet pt thresholds.")
args = parser.parse_args()

import ROOT
sys.path.insert(0, '../plotting/')
import AtlasStyle
AtlasStyle.SetAtlasStyle()

####################### User defined variables ###############################

## For calcBinning, the initial edges to use ##
#initial_bin_edges = [200,250,300,350,400,450,500,550,600,650,700,750,800,850,900,950,1000,1050,1100,1150,1200,1300,1400,1500,1750,2000,2250,2500,2750,3000]
#initial_bin_edges = [300,1050,1100,1150,1200,1300,1500,1700,2000,2300]
#initial_bin_edges = [300, 1700]
#The threshold nevents for accepting a bin threshold
#Errors & numEvents : 1% = 10000, 2% = 2500, 3% = 1111, 4% = 625, 5% = 400

#hist_name = "finept"
hist_name = "finept_tightAsym"

#Requires at least 200 events per bin when using full statistics
initial_bin_edges = [200,250,300,350,400,450,500,550,600,650,700,750,800,850,900,950,1000,1050,1100,1150,1200,1250,1300,1350,1400,1450,1500,1600,1700,1800,1900,2000,2250,2500,2750,3000]
final_per_bin_yield = 200
min_bin_size = 10 #5*10 = 50 GeV

## For getIterativeHist, the final binEdges and different subleading jet cutoffs to use ##
iterative_pts = [950,1200,1400,1600,1800,1900,2000,None]



inputFile = ROOT.TFile.Open( args.file, "READ" )
h_finept = inputFile.Get(hist_name)

if args.deriveBinning:
  
  ## Get binning of full stats MJB ##  
  h_pt_1D = h_finept.ProjectionX("proj_allEvents") 
  
  numEvents = [] #Number of events in each bin
  
  if len(initial_bin_edges) < 2:
    print("Error, need at least 2 bins to begin with")
  
  for iBin in range(1, len(initial_bin_edges) ):
    thisNumEvents = 0
    thisLowEdge = initial_bin_edges[iBin-1]
    thisUpEdge = initial_bin_edges[iBin]
    thisLowBin = h_pt_1D.GetXaxis().FindBin( thisLowEdge+0.1)
    thisUpBin = h_pt_1D.GetXaxis().FindBin( thisUpEdge-0.1 )
    for iB in range(thisLowBin, thisUpBin+1):
      thisNumEvents += h_pt_1D.GetBinContent(iB)
    numEvents.append( thisNumEvents )
    
  ## Calculate the new bin edges ##
  thisNumEvents = 0. # nevents currently in this bin
  currentBin = h_pt_1D.GetXaxis().FindBin( initial_bin_edges[-1] ) #First bin to include
  lastBin = h_pt_1D.FindLastBinAbove(0)
  numBinsAdded = 0  # number of bins added, must be a multiple of min_bin_size
  
  # while we haven't passed the last bin
  while( currentBin <= lastBin ):
    thisNumEvents += h_pt_1D.GetBinContent( currentBin )
    numBinsAdded += 1
  
  
    ## If we're at the last bin, add it to the edge
    if currentBin == lastBin:
      initial_bin_edges.append( h_pt_1D.GetXaxis().GetBinUpEdge( currentBin ) )
      numEvents.append( thisNumEvents )
      thisNumEvents = 0.
      numBinsAdded = 0
      currentBin += 1
  
    #if the bin width is wider than the maximum, permanently double the min_bin_size:
    elif (numBinsAdded > min_bin_size):
      min_bin_size = min_bin_size*2
      currentBin += 1
  
    # if we have enough events in this bin, and the bin width is appropriate
    elif (thisNumEvents >= final_per_bin_yield and numBinsAdded == min_bin_size ):
      initial_bin_edges.append( h_pt_1D.GetXaxis().GetBinUpEdge( currentBin ) )
      numEvents.append( thisNumEvents )
      thisNumEvents = 0.
      numBinsAdded = 0
      currentBin += 1
    
    #if we don't have enough events yet
    else:
      currentBin += 1
    
  print("Bin widths and statistics for full MJB event yield:")
  for iBin in range(1,len(initial_bin_edges)):
    print(initial_bin_edges[iBin-1],"->",initial_bin_edges[iBin],":", numEvents[iBin-1])

if args.iterativeCutoff:
  outDir = os.path.dirname( args.file )+"/"

  #Get bin yields for all pt cutoffs
  iterative_hists = {}
  for cutoff in iterative_pts:
    
    if cutoff == None:
      y_last_bin = h_finept.GetNbinsY()
    else:
      y_last_bin = h_finept.GetYaxis().FindBin(cutoff)-1
    h_pt_1D = h_finept.ProjectionX("proj_"+str(y_last_bin), 1, y_last_bin) 
    print("Cutoff", cutoff, "gives", y_last_bin)
    
    h_iter = ROOT.TH1F("h_iter_"+str(cutoff), "h_iter_"+str(cutoff), len(initial_bin_edges)-1, array.array('f', initial_bin_edges) )
    for iBin in range(1, h_iter.GetNbinsX()+1 ):
      thisNumEvents = 0
      thisLowEdge = h_iter.GetXaxis().GetBinLowEdge(iBin)
      thisUpEdge = h_iter.GetXaxis().GetBinUpEdge(iBin)

      thisLowBin = h_pt_1D.GetXaxis().FindBin( thisLowEdge+0.1)
      thisUpBin = h_pt_1D.GetXaxis().FindBin( thisUpEdge-0.1 )
      for iB_sub in range(thisLowBin, thisUpBin+1):
        thisNumEvents += h_pt_1D.GetBinContent(iB_sub)
      h_iter.SetBinContent(iBin, thisNumEvents)
      print("Setting bin content", thisNumEvents)

      iterative_hists[cutoff] = h_iter

  ######## Plot Histogram of bin cutoffs ##########
#  hist.GetXaxis().SetRangeUser(200, 2800)
#  hist.SetMinimum(1)
#  hist.SetBinContent(0, 0)
#  hist.SetBinError(0, 0)

  colors = [ROOT.kSpring-6, ROOT.kRed, ROOT.kBlue, ROOT.kGreen, ROOT.kCyan, ROOT.kOrange, ROOT.kViolet, ROOT.kBlack]
  c1 = ROOT.TCanvas()
  leg = ROOT.TLegend(0.7, 0.6, 0.88, 0.9)
  iCut = 0
  for cutoff in iterative_pts:
    h_iter = iterative_hists[cutoff]
    h_iter.GetXaxis().SetTitle("Recoil p_{T}")
    h_iter.GetYaxis().SetTitle("Events")
    h_iter.SetLineColor(colors[iCut])
    h_iter.SetMinimum(1)
    
    if iCut == 0:
      h_iter.Draw()
    else:
      h_iter.Draw("same")
    leg.AddEntry( h_iter, str(cutoff)+" (GeV)", "l")



    iCut += 1

  c1.SetLogy()
  leg.Draw("same")
  AtlasStyle.ATLAS_LABEL(0.4,0.85, 0.04,"Internal ")
  AtlasStyle.myText(0.4,0.79,0.04, "#sqrt{s} = 13 TeV, ~80 fb^{-1}")
  c1.SaveAs(outDir+"/IterativeBinning.png")

######################### plotIter ##################################
#if (args.plotIter):
#
#  ## Get Histograms ##
#  inDir = os.path.dirname( args.fileName )+"/"
#  if not "binningPlots" in inDir:
#    inDir += "binningPlots/"
#  inFile =  ROOT.TFile.Open( inDir+"IterativeBinningHist.root", "READ" )
#
#  vh_iterative = []
#  for cutoff in iterativeCutoffs:
#    vh_iterative.append( inFile.Get("Iteration_"+str(cutoff)) )
#
#  ### Draw Histograms ###
#  colors = [ROOT.kRed, ROOT.kBlue, ROOT.kGreen, ROOT.kCyan, ROOT.kOrange, ROOT.kViolet, ROOT.kPink-7, ROOT.kSpring-7]
#
#  c1 = ROOT.TCanvas()
#  leg = ROOT.TLegend(0.6, 0.6, 0.88, 0.9)
#  for iH, hist in enumerate(vh_iterative):
#    hist.GetXaxis().SetTitle("Recoil p_{T}")
#    hist.GetYaxis().SetTitle("Events")
#    hist.SetLineColor(colors[iH])
#    hist.SetMaximum( hist.GetMaximum()**(3./2) )
#    if iH == 0:
#      hist.Draw()
#    else:
#      hist.Draw("same")
#    leg.AddEntry( hist, hist.GetTitle(), "l")
#
#
#  AtlasStyle.ATLAS_LABEL(0.2,0.9, 1,"  Internal")
#  AtlasStyle.myText(0.2,0.84,1, "#sqrt{s} = 13 TeV, ~27 fb^{-1}")
#  
#  leg.Draw("same")
# 
#  c1.SetLogy()
#  c1.SaveAs(inDir+"IterativeBinning.png")
