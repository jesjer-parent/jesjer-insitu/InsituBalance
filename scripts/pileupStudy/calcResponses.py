from __future__ import print_function
import ROOT
import os, sys, glob

sys.path.insert(0, os.path.dirname(os.path.realpath(__file__))+'/../../external_packages/JES_ResponseFitter/scripts/')

import JES_BalanceFitter, AtlasStyle
import array

AtlasStyle.SetAtlasStyle()
ROOT.gROOT.SetBatch(True)

def MJB_fit(myFitter, h_response_distribution, low_threshold=1/8., high_threshold=1/8.):
  maxContent = h_response_distribution.GetBinContent(h_response_distribution.GetMaximumBin())
  fitLow = h_response_distribution.GetXaxis().GetBinLowEdge( h_response_distribution.FindFirstBinAbove(maxContent*low_threshold)-1 )
  fitHigh = h_response_distribution.GetXaxis().GetBinUpEdge( h_response_distribution.FindLastBinAbove(maxContent*high_threshold)+1 )
  fit_result = myFitter.Fit(h_response_distribution, fitLow, fitHigh )
  return fit_result

def deriveResponses(inFileName, hist, muBinning, fitDir, ptBin):
    inFile = ROOT.TFile.Open(inFileName, 'UPDATE')

    myFitter = JES_BalanceFitter.JES_BalanceFitter(1.6)
    myFitter.SetGaus()
    myFitter.SetFitColor(ROOT.kRed)
    myFitter.rebin = False

    this2Dhist = inFile.Get(hist)
    balHist = ROOT.TH1F('h_response_'+hist, '', len(muBinning)-1, array.array('d', muBinning))

    for iBin in range(len(muBinning)-1):
        lowBin = this2Dhist.GetXaxis().FindBin(muBinning[iBin])
        highBin = this2Dhist.GetXaxis().FindBin(muBinning[iBin+1])
        
        h_response_distribution = this2Dhist.ProjectionY( "h_proj_"+str(iBin), lowBin, highBin, "ed")
        fit_result = MJB_fit(myFitter, h_response_distribution)

        c1 = ROOT.TCanvas()
        h_response_distribution.GetXaxis().SetRangeUser(0, 2.0)
        h_response_distribution.SetMarkerStyle(20)
        h_response_distribution.SetMarkerSize(0.8)
        h_response_distribution.Draw('p')
        fit_result.Draw('SAME')

        ptList = hist.split('_')
        AtlasStyle.myText( 0.6, 0.88, 0.8, 'pT: {:} to {:}'.format(int(ptList[0]), int(ptList[1])) )
        AtlasStyle.myText( 0.6, 0.84, 0.8, '<#mu>: {:} to {:}'.format(muBinning[iBin], muBinning[iBin+1]) )
        AtlasStyle.myText( 0.6, 0.80, 0.8, "Effective entries: {:}".format(int(h_response_distribution.GetEffectiveEntries())) )
        AtlasStyle.myText( 0.6, 0.76, 0.8, 'Mean: {:.4} +- {:.4}'.format(myFitter.GetMean(), myFitter.GetMeanError()) )

        saveName = fitDir+'PileupStudy_ptBin'+str(ptBin)+'_muBin'+str(iBin)+'.png'
        c1.SaveAs(saveName)

        balHist.SetBinContent(iBin+1, myFitter.GetMean())
        balHist.SetBinError(iBin+1, myFitter.GetMeanError())
    
    balHist.Write(balHist.GetName(), ROOT.TObject.kOverwrite)
    inFile.Close()

def createResponsePlots(baseDir, sample, muBinning):
    inFileName = glob.glob(baseDir+'PileupFiles/*'+sample+'*.root')

    inFile = ROOT.TFile.Open(inFileName[0], 'READ')
    histList = [thisHist.GetName() for thisHist in inFile.GetListOfKeys()]
    inFile.Close()

    ptBin = 0
    for hist in histList:
      if 'h_' in hist:
        continue
      fitDir = baseDir+'PileupFiles/'+sample+'.all_Plots/GaussianFits/'
      if not os.path.exists(fitDir):
        os.makedirs(fitDir)
        
      deriveResponses(inFileName[0], hist, muBinning[hist], fitDir, ptBin)
      ptBin+=1
