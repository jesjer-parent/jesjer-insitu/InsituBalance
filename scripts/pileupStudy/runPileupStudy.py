import ROOT, os
import setupStudy, calcResponses, makePileupResponsePlots

def runStudy(baseDir, samples, ptBinning):
    print('> Deriving Mu Binning')
    if not os.path.exists(baseDir+'PileupFiles/'):
        os.makedirs(baseDir+'PileupFiles/')
    muBinning = setupStudy.deriveBinning(baseDir, samples[0], ptBinning)

    print('> Combining Files')
    for sample in samples:
        fileDir = baseDir+'PileupFiles/'
        if not os.path.exists(fileDir):
            os.makedirs(fileDir)
            
        if 'Data' in sample:
            sample+='.all'
            testName = fileDir+sample+'_pileup_hist.root'
        else:
            testName = fileDir+sample+'.all_pileup_hist.root'
            
        if not os.path.exists(testName):
            setupStudy.createPileup2DHists(baseDir, sample, ptBinning)
            setupStudy.haddFiles(baseDir+'PileupFiles/', sample)
        else:
            print('>>> '+sample+' files have already been combined')

    print('> Fitting Responses')
    for sample in samples:
        calcResponses.createResponsePlots(baseDir, sample, muBinning)

    if not os.path.exists(baseDir+'PileupPlots/'):
        os.makedirs(baseDir+'PileupPlots/')
    makePileupResponsePlots.makePileupResponsePlot(baseDir)

if __name__=='__main__':
    baseDir = './gridOutput/'
    ptBinning = [200, 500, 1000, 3000]
    samples = ['Data', 'Py8', 'Shp', 'Hw8', 'PP8']

    if not 'Data' in samples[0]:
        print('DATA MUST BE FIRST ENTRY IN LIST')
        exit(1)
        
    runStudy(baseDir, samples, ptBinning)
