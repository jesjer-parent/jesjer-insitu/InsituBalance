import ROOT, glob
import AtlasStyle
AtlasStyle.SetAtlasStyle()

def makePileupResponsePlot(baseDir):
    sideBars = True
    is_logx = False

    #Default ATLAS style removes x-axis bars
    if( sideBars ):
        ROOT.gStyle.SetErrorX(0.5)

    jetType = "EMTopo"
    
    min_response = 0.9401
    max_response = 1.086
    min_ratio = 0.99
    max_ratio = 1.11
    
    inputDir = baseDir+"PileupFiles/"
    outputDir = baseDir+"PileupPlots/"

    fileNames = glob.glob(inputDir+"*hist*.root")
    sampleNames = ["Data", "Hw8", "PP8", "Py8", "Shp"]

    colors = [ROOT.kBlack, ROOT.kGreen-5, ROOT.kMagenta, ROOT.kRed, ROOT.kBlue]
    marker_styles = [20, 21, 33, 22, 23]

    #Get Hist names
    inFile = ROOT.TFile.Open(fileNames[0], 'READ')
    histList = [thisHist.GetName() for thisHist in inFile.GetListOfKeys() if 'h_response' in thisHist.GetName()]
    inFile.Close()

    for thisHist in histList:
        c1 = ROOT.TCanvas("MJB", "MJB", 600, 600)
        is_logy = False
        pad1, pad2, zeroLine, oneLine, ratioLine1, ratioLine2 = getRatioObjects(c1, is_logx, is_logy)
        pad1.cd()
        
        splitName = thisHist.split('_')
        min_pt = splitName[-2]
        max_pt = splitName[-1]

        hists = []
        for iS, sample in enumerate(sampleNames):
            inputFile = ROOT.TFile.Open(fileNames[iS], "READ")
            hist = inputFile.Get(thisHist)
            hist.SetDirectory(0)
            hists.append(hist)
            inputFile.Close()
            
            min_mu = 0
            max_mu = int(hist.GetXaxis().GetBinUpEdge(5))

            hist.SetMarkerSize(0.7)
            hist.SetLineWidth(1)
            hist.GetYaxis().SetTitle("#it{R}_{MJB}, #LT #it{p}_{T}^{lead jet}/#it{p}_{T}^{ref} #GT")
            hist.GetXaxis().SetTitle("<#mu>")
            
            hist.SetMarkerColor(colors[iS])
            hist.SetLineColor(colors[iS])
            hist.SetMarkerStyle(marker_styles[iS])
            hist.GetXaxis().SetRangeUser(min_mu, max_mu)
            hist.SetMaximum(max_response)
            hist.SetMinimum(min_response)
            
            hist.GetYaxis().SetTitleSize( 0.065 )
            hist.GetYaxis().SetTitleOffset( 1.06 )
            hist.GetYaxis().SetLabelSize( 0.06 )
            hist.GetYaxis().SetTickLength(0.02)
            
            if(iS==0):
                hist.DrawCopy("p")
            else:
                hist.DrawCopy("p same")
        
        #Configure legend
        leg = ROOT.TLegend(0.58, 0.79, 0.94, 0.96,"")
        leg.SetFillStyle(0)
        leg.SetEntrySeparation(0.0001)
        #leg.SetTextSize(0.05);
        leg.SetTextFont(42);

        #Dedicated ordering of legend
        leg.AddEntry(hists[0], sampleNames[0], "p")#Data
        leg.AddEntry(hists[3], sampleNames[3], "p")#Pythia
        leg.AddEntry(hists[4], sampleNames[4], "p")#Sherpa
        leg.AddEntry(hists[2], sampleNames[2], "p")#PP8
        leg.AddEntry(hists[1], sampleNames[1], "p")#Herwig

        #Draw ratio plots in 2nd pad
        pad2.cd()
        for iH, hist in enumerate(hists):
            if iH == 0:
                continue
            hist.Divide(hists[0])

            #Configure
            hist.SetMinimum(min_ratio)
            hist.SetMaximum(max_ratio)
            hist.GetYaxis().SetTitle("MC / Data")
            hist.GetYaxis().SetTitleSize( 0.12 )
            hist.GetYaxis().SetTitleOffset( 0.55 )
            hist.GetYaxis().SetLabelSize( 0.11 )
            hist.GetYaxis().SetNdivisions(503)
            hist.GetYaxis().SetTickLength(0.03)
            
            hist.GetXaxis().SetLabelSize( 0.12 )
            hist.GetXaxis().SetTitleSize( 0.12 )
            hist.GetXaxis().SetTitleOffset( 1.25 )
            hist.GetXaxis().SetTickLength(0.07)
            
            if( iH == 1 ):
                hist.DrawCopy("p")
                ratioLine1.Draw("same")
                ratioLine2.Draw("same")
                hist.DrawCopy("p same")
            else:
                hist.DrawCopy("p same")

        c1.cd()
        leg.Draw("same")
        ### Draw text to plot ###
        textSize = 0.8
        yTop = 0.93
        AtlasStyle.ATLAS_LABEL(0.172,yTop, textSize, "   Internal")
        AtlasStyle.myText(0.172,yTop-0.04, textSize, "#sqrt{#it{s}} = 13 TeV, 36 fb^{-1}, multijet")
        AtlasStyle.myText(0.172,yTop-0.08, textSize, "Anti-#it{k_{t} R} = 1.0 ("+jetType+"+JES)")
        AtlasStyle.myText(0.172,yTop-0.12, textSize, "|#eta^{lead jet}| < 0.8, "+min_pt+" #leq p_{T}^{ref} < "+max_pt)

        outName = "PileupStudy_"+str(min_pt)+'_'+str(max_pt)
        if( not sideBars ):
            outName += "_noXbars"
        if( is_logx ):
            outName += "_logX"

        c1.SaveAs(outputDir+outName+".pdf")


#### Get Ratio Objects ####
def getRatioObjects(c0, logX, logY):
    pad1 = ROOT.TPad("pad1","pad1",0,0.36,1,0.99)
    pad2 = ROOT.TPad("pad2","pad2",0,0.01,1,0.35)
    pad1.Draw()
    pad2.Draw()
    pad1.SetBottomMargin(0)
    pad1.SetTopMargin(0.01)
    pad2.SetTopMargin(0.05)
    pad1.SetRightMargin(0.05)
    pad2.SetRightMargin(0.05)
    pad1.SetLeftMargin(0.15)
    pad2.SetLeftMargin(0.15)
    pad2.SetBottomMargin(.33)
    zeroLine = ROOT.TF1("zl0", "0", -50000, 50000 )
    zeroLine.SetTitle("")
    zeroLine.SetLineWidth(1)
    zeroLine.SetLineStyle(7)
    zeroLine.SetLineColor(ROOT.kBlack)
    oneLine = ROOT.TF1("ol0", "1.0", -50000, 50000 )
    oneLine.SetTitle("")
    oneLine.SetLineWidth(1)
    oneLine.SetLineStyle(7)
    oneLine.SetLineColor(ROOT.kBlack)
    ratioLine1 = ROOT.TF1("ol0", "1.0", -50000, 50000 )
    ratioLine1.SetTitle("")
    ratioLine1.SetLineWidth(1)
    ratioLine1.SetLineStyle(7)
    ratioLine1.SetLineColor(ROOT.kBlack)
    ratioLine2 = ROOT.TF1("ol0", "1.05", -50000, 50000 )
    ratioLine2.SetTitle("")
    ratioLine2.SetLineWidth(1)
    ratioLine2.SetLineStyle(7)
    ratioLine2.SetLineColor(ROOT.kBlack)
    
    if logX:
        pad1.SetLogx()
        pad2.SetLogx()
    if logY:
        pad1.SetLogy()
            
    return pad1, pad2, zeroLine, oneLine, ratioLine1, ratioLine2

if __name__ == "__main__":
    baseDir = './gridOutput/'
    makePileupResponsePlot(baseDir)
