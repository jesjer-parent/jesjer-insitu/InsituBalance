import ROOT, glob, os, array

def retrieveBins(binFile):
    muBinning = {}
    inFile = ROOT.TFile.Open(binFile, 'READ')
    ptBins = [thisHist.GetName() for thisHist in inFile.GetListOfKeys()]

    for ptRange in ptBins:
        thisBinning = [0]
        hist = inFile.Get(ptRange)
        xAxis = hist.GetXaxis()
        nBins = xAxis.GetNbins()
        for bin in range(1, nBins+1):
            thisBinning.append(xAxis.GetBinUpEdge(bin))
        muBinning[ptRange] = thisBinning
    
    return muBinning

def calcMuBinning(baseDir, inFileName, ptBins):
    muBinning = {}
    mus = [[], [], []]

    inFile = ROOT.TFile.Open(inFileName, 'READ')
    myTree = inFile.Get('outTree_Nominal')
    for event in myTree:
        pt = event.recoilPt
        mu = event.averageInteractionsPerCrossing
        if pt>=ptBins[0] and pt<ptBins[1]:
            mus[0].append(mu)
        elif pt>=ptBins[1] and pt<ptBins[2]:
            mus[1].append(mu)
        elif pt>=ptBins[2] and pt<ptBins[3]:
            mus[2].append(mu)

    for iBin,thisMu in enumerate(mus):
        thisBinning = [0]
        thisMu.sort()
        numPerBin = int(len(thisMu)/5)
        for i in range(1, 5):
            val = round(thisMu[i*numPerBin], 2)
            thisBinning.append(val)

        thisBinning.append( int(thisMu[-1])+1 )
        
        ptStr = str(ptBins[iBin])+'_'+str(ptBins[iBin+1])
        #Create a file of empty histograms to store the mu Bins for rerunning
        binFile = ROOT.TFile.Open(baseDir+'PileupFiles/binFile.root', 'UPDATE')
        binHist = ROOT.TH1D(ptStr, '', len(thisBinning)-1, array.array('d', thisBinning))
        binFile.Write()
        binFile.Close()

        muBinning[ptStr] = thisBinning        

    inFile.Close()
    return muBinning

def deriveBinning(baseDir, dataName, ptBinning):
    muBinning = {}

    if os.path.exists(baseDir+'PileupFiles/binFile.root'):
        muBinning = retrieveBins(baseDir+'PileupFiles/binFile.root')
        
    else:
        if not os.path.exists(baseDir+'tree/'+dataName+'.all.root'):
            command = 'hadd '+baseDir+'tree/'+dataName+'.all.root '+baseDir+'tree/*'+dataName+'*'
            os.system(command)

        print('>>> Done adding Data files together')
        inFileName = baseDir+'tree/'+dataName+'.all.root'            
        muBinning = calcMuBinning(baseDir, inFileName, ptBinning)

    return muBinning

def write2DhistFile(fileName, outName, ptBinning, weight):
    inFile = ROOT.TFile.Open(fileName, 'READ')
    outFile = ROOT.TFile(outName, 'UPDATE')
    
    hist1 = ROOT.TH2F(str(ptBinning[0])+'_'+str(ptBinning[1]), ';<#mu>;pT Balance', 9000, 0, 90, 200, 0, 2)
    hist2 = ROOT.TH2F(str(ptBinning[1])+'_'+str(ptBinning[2]), ';<#mu>;pT Balance', 9000, 0, 90, 200, 0, 2)
    hist3 = ROOT.TH2F(str(ptBinning[2])+'_'+str(ptBinning[3]), ';<#mu>;pT Balance', 9000, 0, 90, 200, 0, 2)
    
    myTree = inFile.Get('outTree_Nominal')
    for event in myTree:
        pt = event.recoilPt
        mu = event.averageInteractionsPerCrossing
        bal = event.ptBal

        if pt>=ptBinning[0] and pt<ptBinning[1]:
            hist1.Fill(mu, bal, weight)
        elif pt>=ptBinning[1] and pt<ptBinning[2]:
            hist2.Fill(mu, bal, weight)
        elif pt>=ptBinning[2] and pt<ptBinning[3]:
            hist3.Fill(mu, bal, weight)

    hist1.Write(hist1.GetName(), ROOT.TObject.kOverwrite)
    hist2.Write(hist2.GetName(), ROOT.TObject.kOverwrite)
    hist3.Write(hist3.GetName(), ROOT.TObject.kOverwrite)
    outFile.Close()

    inFile.Close()

def calcWeight(fileName, sample):
    inFile = ROOT.TFile.Open(fileName, 'READ')
    weight=1.0
    if not 'Data' in sample:
        thisCutflow = inFile.Get("MetaData_EventCount")
        if 'Py8' in sample:
            numEvents = thisCutflow.GetBinContent(1)
        else:
            numEvents = thisCutflow.GetBinContent(3)  #sumOfWeights
        weight = 1./numEvents
    inFile.Close()
    return weight

def createPileup2DHists(baseDir, sample, ptBinning):
    allFileNames = glob.glob(baseDir+'tree/*'+sample+'*')
    print('>>> Working on '+sample)
    for fileName in allFileNames:
        print('>>>>> '+fileName)
        outName = fileName.replace('tree', 'PileupFiles')
        outName = outName.replace('.root', '_scaled.root')

        weight = calcWeight(fileName, sample)
        write2DhistFile(fileName, outName, ptBinning, weight)
        
def haddFiles(inDir, sample):
    addOn = '.all_pileup_hist.root '
    if 'all' in sample:
        addOn = '_pileup_hist.root '
        
    command = 'hadd '+inDir+sample+addOn+inDir+'*'+sample+'*scaled*'
    os.system(command)
    
    rmCommand = 'rm '+inDir+'*'+sample+'*scaled*'
    os.system(rmCommand)
