#!/usr/bin/env python

###########################################################
# reweightTrees.py                                        #
# A short script to reweight TTrees by their initial      #
# event number.  Allows for directly combining TTrees.    #
# For more information contact Jeff.Dandoy@cern.ch        #
###########################################################

import os
import glob, array, argparse, math
#put argparse before ROOT call.  This allows for argparse help options to be printed properly (otherwise pyroot hijacks --help) and allows -b option to be forwarded to pyroot
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--file", dest='file', default="", help="Input file")
parser.add_argument("--treeName", dest='treeName', default="nominal", help="Name of trees to be reweighted")
parser.add_argument("--outName", dest='outName', default="", help="Name of output ttree")

args = parser.parse_args()


from ROOT import *

def deriveFlavorComposition():

  print "Reweighting file ", args.file

  inFile = TFile.Open(args.file, "READ")
  if( not inFile):
    print "Error, could not get input file, exiting!"
    exit(1)

  metaHist = inFile.Get("MetaData_EventCount")
  if( not metaHist):
    print "Error, could not get meta data hist, exiting!"
    exit(1)

  tree = inFile.Get( args.treeName )
  if( not tree ):
    print "Warning, cuold not get input tree, it may not exist as there are no events, returning!"
    return

  pt_binning = [0, 20, 30, 40, 50, 60, 80, 100, 150, 200, 300, 400, 500, 600, 800, 1000, 2000, 3000, 5000, 7000, 10000]
  eta_binning = [0, 0.2, 0.7, 1.3, 1.8, 2.5, 3.2]
  pt_array = array.array('f', pt_binning)
  eta_array = array.array('f', eta_binning)

  scaleFactor = 1./metaHist.GetBinContent(1)

  print scaleFactor
  print tree.GetName()

  if not os.path.exists("FlavorCompositionHistograms"):
    os.makedirs("FlavorCompositionHistograms")
  outFileName = args.file.replace(".root","")+"_"+args.treeName+"_flavorComposition.root"
  outFileName = outFileName.split('/')[-1]
  outFile = TFile.Open("FlavorCompositionHistograms/"+outFileName, "RECREATE")

  tree.SetBranchStatus("*", 0)

  weight = array.array('f', [0])
  tree.SetBranchStatus("weight", 1)
  tree.SetBranchAddress("weight", weight)
  njet = array.array('I', [0])
  tree.SetBranchStatus("njet", 1)
  tree.SetBranchAddress("njet", njet)

  jet_pt = std.vector('float')()
  tree.SetBranchStatus("jet_pt", 1)
  tree.SetBranchAddress("jet_pt", jet_pt)
  jet_eta = std.vector('float')()
  tree.SetBranchStatus("jet_eta", 1)
  tree.SetBranchAddress("jet_eta", jet_eta)
  jet_PartonTruthLabelID = std.vector('float')()
  tree.SetBranchStatus("jet_PartonTruthLabelID", 1)
  tree.SetBranchAddress("jet_PartonTruthLabelID", jet_PartonTruthLabelID)

  i = 0
  print tree.GetEntries(), " entries !!"
  gluon_yield = TH2F("gluon_yield", "gluon_yield; Jet p_{T}; Jet #eta", len(pt_array)-1, pt_array, len(eta_array)-1, eta_array)
  quark_yield = TH2F("quark_yield", "quark_yield; Jet p_{T}; Jet #eta", len(pt_array)-1, pt_array, len(eta_array)-1, eta_array)
  gluon_AntiKt4EMTopo = TH2F("gluon_AntiKt4EMTopo", "gluon_AntiKt4EMTopo; Jet p_{T}; Jet #eta", len(pt_array)-1, pt_array, len(eta_array)-1, eta_array)
  quark_AntiKt4EMTopo = TH2F("quark_AntiKt4EMTopo", "quark_AntiKt4EMTopo; Jet p_{T}; Jet #eta", len(pt_array)-1, pt_array, len(eta_array)-1, eta_array)
  gluon_AntiKt4EMTopo.SetDirectory(outFile)
  quark_AntiKt4EMTopo.SetDirectory(outFile)
  gluon_Njet = {}
  quark_Njet = {}
  for iJ in range(3,11):
    tmp_g = TH2F("gluon_AntiKt4EMTopo_nJets"+str(iJ), "gluon_AntiKt4EMTopo_nJets"+str(iJ)+"; Jet p_{T}; Jet #eta", len(pt_array)-1, pt_array, len(eta_array)-1, eta_array)
    tmp_q = TH2F("quark_AntiKt4EMTopo_nJets"+str(iJ), "quark_AntiKt4EMTopo_nJets"+str(iJ)+"; Jet p_{T}; Jet #eta", len(pt_array)-1, pt_array, len(eta_array)-1, eta_array)
    gluon_Njet[iJ] = tmp_g
    quark_Njet[iJ] = tmp_q
#  gluonFraction_AntiKt4EMTopo_nJets0 = ROOT.TH2F("gluonFraction_AntiKt4EMTopo_nJets0", "gluonFraction_AntiKt4EMTopo_nJets0; Jet p_{T}; Jet #eta", 70, 0, 7000
  while tree.GetEntry(i):
    i += 1

    if(i%10000==0):
      print "Event",i


    if jet_pt.size() <= 0:
      continue

    for iJ in range(0, jet_pt.size()):
      if jet_PartonTruthLabelID.at(iJ) == 21:
        gluon_AntiKt4EMTopo.Fill( jet_pt.at(iJ), jet_eta.at(iJ), weight[0] * scaleFactor )
        gluon_yield.Fill( jet_pt.at(iJ), jet_eta.at(iJ), 1 )
        if( njet[0] <= 10 ):
          gluon_Njet[njet[0]].Fill( jet_pt.at(iJ), jet_eta.at(iJ), weight[0] * scaleFactor )
      elif (jet_PartonTruthLabelID.at(iJ) >= 1 and jet_PartonTruthLabelID.at(iJ) <=4) :
        quark_AntiKt4EMTopo.Fill( jet_pt.at(iJ), jet_eta.at(iJ), weight[0] * scaleFactor )
        quark_yield.Fill( jet_pt.at(iJ), jet_eta.at(iJ), 1 )
        if( njet[0] <= 10 ):
          quark_Njet[njet[0]].Fill( jet_pt.at(iJ), jet_eta.at(iJ), weight[0] * scaleFactor )




  outFile.Write()
  outFile.Close()


if __name__ == "__main__":
  deriveFlavorComposition()
  print "Finished deriveFlavorComposition()"
