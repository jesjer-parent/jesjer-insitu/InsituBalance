import ROOT
import os
import AtlasStyle


def computeFlavorComposition():

  in_files = ["Py8_EM", "Hw7_EM"]

  #Get list of all histograms from the first file (the nominal one)
  f_MC_nominal = ROOT.TFile.Open(in_files[0]+'.root', "READ")
  hist_list = []
  for key in f_MC_nominal.GetListOfKeys():
    hist_name = key.GetName()
    if "AntiKt4EMTopo" in hist_name:
      hist_list.append( hist_name.replace("gluon_","").replace("quark_","") )
  f_MC_nominal.Close()
  hist_list = list(set(hist_list))
  hist_list = [hist for hist in hist_list if not "nJets" in hist] #Remove nJets hist as no nJet dependence is seen
  print("Will run over the following histograms:", hist_list)

  c1 = ROOT.TCanvas()
  c1.SetRightMargin(0.15)
  c1.SetLeftMargin(0.10)

  #Load each input file
  file_list = []
  for in_file in in_files:
    this_file = ROOT.TFile.Open(in_file+'.root', 'READ')
    file_list.append( this_file )

  #Where to save the gluo composition & error histograms
  outFile = ROOT.TFile.Open("FinalFlavorComposition.root", "RECREATE")

  #Loop over each input file & calculate the gluon composition histogram
  final_hists = []
  for hist_name in hist_list:
    print(hist_name)
    for iF, in_file in enumerate(in_files):
      #Get the quark and gluon histogram
      gluon_nominal = file_list[iF].Get( "gluon_"+hist_name )
      quark_nominal = file_list[iF].Get( "quark_"+hist_name )
    
      #Calculate gluon / quark+gluon
      quark_nominal.Add(gluon_nominal)
      gluon_nominal.Divide(quark_nominal)
      gluon_nominal.SetMaximum(1)
      gluon_nominal.GetYaxis().SetTitleOffset(1)
      gluon_nominal.Draw("colz")
      c1.SetLogx()
      c1.SaveAs(in_file+'_'+hist_name+".png")

      #Only save the non nJets one for calculating the difference
      if not "nJets" in hist_name:
        if( "Py8" in in_file ):
          #gluon_nominal.SetName("gluonFraction_AntiKt4EMPFlow")
          gluon_nominal.SetName("gluonFraction_AntiKt4EMTopo")
          gluon_nominal.Write()
        gluon_nominal.SetName( in_file )
        final_hists.append( gluon_nominal )

  #Loop over all the composition histograms and get the bin-by-bin difference
  for i in range(len(final_hists)):
    for j in range(i+1, len(final_hists)):
      new_hist = final_hists[i].Clone()
      #loop over all bins
      for iBinX in range(0, new_hist.GetNbinsX()+1):
        for iBinY in range(0, new_hist.GetNbinsY()+1):
          if(new_hist.GetBinContent(iBinX, iBinY) > 0 and final_hists[j].GetBinContent(iBinX, iBinY) > 0):
            new_hist.SetBinContent( iBinX, iBinY, new_hist.GetBinContent(iBinX, iBinY) - final_hists[j].GetBinContent(iBinX, iBinY) )
          else:
            new_hist.SetBinContent( iBinX, iBinY, 0 )
      #Set zero bins to nearest neighbors
      for iBinX in range(0, new_hist.GetNbinsX()+1):
        for iBinY in range(0, new_hist.GetNbinsY()+1):
          if(new_hist.GetBinContent(iBinX, iBinY) == 0):
            new_hist.SetBinContent( iBinX, iBinY, new_hist.GetBinContent(iBinX-1, iBinY) )

      new_hist.SetMaximum(0.08)
      new_hist.SetMinimum(-0.08)
      new_hist.Smooth()
      new_hist.Draw("colz")
      name = "compare_"+in_files[i]+'_vs_'+in_files[j]+'.png'
      c1.SaveAs( name )

      #new_hist.SetName("gluonFractionError_AntiKt4EMPFlow")
      new_hist.SetName("gluonFractionError_AntiKt4EMTopo")
      new_hist.Write()

  outFile.Close()





#---------------------------------------------------------------------------------
if __name__ == "__main__":

#  parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
#  parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot.")
#  parser.add_argument("--outDir", dest='outDir', default="", help="Output directory")
#  parser.add_argument("--path", dest='path', required=True, default="", help="Path of input files")
#  parser.add_argument("--jetType", dest='jetType', required=True, default="AntiKt4EMTopo", help="Type of jet inputs, determining histogram names.  Like AntiKt4EMTopo.")
#  args = parser.parse_args()

  AtlasStyle.SetAtlasStyle()
  ROOT.gStyle.SetOptStat(0)
  ROOT.gErrorIgnoreLevel = ROOT.kWarning
  # Always do batch mode!
  ROOT.gROOT.SetBatch(True)

  computeFlavorComposition()
  print("Done computeFlavorComposition") 
