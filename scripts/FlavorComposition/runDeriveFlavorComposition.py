#!/usr/bin/python

##############################################################
# runLocalPlotter.py                                         #
##############################################################
# Submit PlotMinitree jobs locally on a folder of TTrees     #
##############################################################
# Jeff.Dandoy and Nedaa.Asbah                                #
##############################################################

import os, math, sys, glob, subprocess, time, shutil
import argparse
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--path", dest='path', default="./",
     help="Path to the directory containing the input TTrees")
parser.add_argument("--ncores", dest='ncores', default=5,
     type=int, help="Number of parallel jobs ")
args = parser.parse_args()

import re
import datetime



def main():
  test = False # does not run the jobs

  logTag = time.strftime("_%Y%m%d")

  files = glob.glob(args.path+'/*Py8*.root')

  if not os.path.exists('logs/DFC_'+logTag):
    os.makedirs('logs/DFC_'+logTag)

  ver = ""

  treeNames = ["outTree_Nominal"] 

  nJobs = len(treeNames)*len(files)
  iJobs = 0

  startTime = datetime.datetime.now()
  
  pids, logFiles = [], []
  ## Submit histogramming jobs ##
  for treeName in treeNames:
    for file in files:
      iJobs += 1
      if( iJobs%(nJobs/1)==0 ):
        timeDiff = datetime.datetime.now()-startTime
        minutes = timeDiff.seconds/60.
        percentage = float(iJobs)/nJobs*100
        print '\033[94m' + 'We are {0:.2f}% of the way (of {1} jobs) there after {2:.2f} minutes.  We predict it will take {3:.2f} more minutes'.format(percentage, nJobs, minutes, minutes*(100/percentage-1)) + '\033[0m'

      if len(pids) >= args.ncores:
        wait_completion(pids, logFiles)
  
      logFile='logs/DFC_{0}/{1}_{2}.log'.format(logTag,os.path.basename(file).replace('.root',''),treeName)
      submit_dir = 'gridOutput/localJobs/'+logFile
  
      command = 'python deriveFlavorComposition.py --file '+file+' --treeName '+treeName

      print command
  
      if not test:
        res = submit_local_job(command, logFile)
        pids.append(res[0])
        logFiles.append(res[1])

  wait_all(pids, logFiles)
  for f in logFiles:
    f.close()



  ## Now collect output ##
#
#  if not test:
#    os.system('hadd -f '+args.path+'/Combined.root '+args.path+'/*_reweighted.root')
#    os.system('rm '+args.path+'/*_reweighted.root')


def submit_local_job(exec_sequence, logfilename):
  output_f=open(logfilename, 'w')
  pid = subprocess.Popen(exec_sequence, shell=True, stderr=output_f, stdout=output_f)
  time.sleep(0.5)  #Wait to prevent opening / closing of several files

  return pid, output_f

def wait_completion(pids, logFiles):
  print """Wait until the completion of one of the launched jobs"""
  while True:
    for pid in pids:
      if pid.poll() is not None:
        print "\nProcess", pid.pid, "has completed"
        logFiles.pop(pids.index(pid)).close()  #remove logfile from list and close it
        pids.remove(pid)

        return
    print ".",
    sys.stdout.flush()
    time.sleep(0.5) # wait before retrying

def wait_all(pids, logFiles):
  print """Wait until the completion of all launched jobs"""
  while len(pids)>0:
    wait_completion(pids, logFiles)
  print "All jobs finished!"

if __name__ == "__main__":
    main()
