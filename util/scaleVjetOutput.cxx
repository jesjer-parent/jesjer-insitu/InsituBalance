/////////////////////////////////////////////////////////////////
//Program for rescaling and combining V+jet histogram & bootstrap
//output from the MPF framework.  Rescales according to sum of 
//weighted events in a retrieved histogram.
//
//author: Jeff Dandoy
/////////////////////////////////////////////////////////////////


#include <vector>
#include <iostream>
#include <string>

//For directory searching
#include <dirent.h>
#include <sys/stat.h>

#include <TFile.h>
#include <TKey.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>

#include "BootstrapGenerator/TH2DBootstrap.h"

using namespace std;

int main(int argc, char *argv[])
{

  //Histogram that stores the sum of weights information
  std::string cutflow_name = "hSumWeights";
  int weight_bin_index = 1;  //Bin containing weight information

  /////////// Retrieve arguments //////////////////////////
  std::string input_path = "";       //path to input root files
  std::string input_tag = "";        //string for selecting files
  std::string output_file_name = ""; //output file name

  //Put arguements in a vector
  std::vector< std::string> options;
  for(int ii=1; ii < argc; ++ii){
    options.push_back( argv[ii] );
  }

  int iArg = 0;
  while(iArg < argc-1) {
    //Print help menu
    if (options.at(iArg).compare("-h") == 0) {
      std::cout << std::endl
           << " scaleVjetOutput : Scale hists and bootstraps to MC sum of weighted events" << std::endl
           << std::endl
           << " Optional arguments:" << std::endl
           << "  -h                Prints this menu" << std::endl
           << "  --path            Path to the input files" << std::endl
           << "  --tag             String for wildcarding input file names" << std::endl
           << "  --outName         Name of output file with combined results" << std::endl
           << std::endl;
      exit(1);
    //Retrive path
    } else if (options.at(iArg).compare("--path") == 0) {
       char next_char = options.at(iArg+1)[0];
       if (iArg+1 == argc || next_char == '-' ) {
         std::cout << " --path should be followed by a directory path" << std::endl;
         return 1;
       } else {
         input_path = options.at(iArg+1);
         iArg += 2;
       }
    //Retrive tag
    } else if (options.at(iArg).compare("--tag") == 0) {
       char next_char = options.at(iArg+1)[0];
       if (iArg+1 == argc || next_char == '-' ) {
         std::cout << " --tag should be followed by a string" << std::endl;
         return 1;
       } else {
         input_tag = options.at(iArg+1);
         iArg += 2;
       }
    //Retrive output name
    } else if (options.at(iArg).compare("--outName") == 0) {
       char next_char = options.at(iArg+1)[0];
       if (iArg+1 == argc || next_char == '-' ) {
         std::cout << " --outName should be followed by a ROOT filename" << std::endl;
         return 1;
       } else {
         output_file_name = options.at(iArg+1);
         iArg += 2;
       }
    }else{
      std::cout << "Couldn't understand argument " << options.at(iArg) << std::endl;
      return 1;
    }
  }//while arguments

  if( input_path.empty() ){
    std::cerr << "Need to specify --path to files" << std::endl;
    exit(1);
  }
  if( output_file_name.empty() ){
    std::cerr << "Need to specify --outName to name new file" << std::endl;
    exit(1);
  }

  //Get list of input files
  std::vector< std::string > input_file_names;
  DIR *dir;
  class dirent *ent;
  class stat st;

  dir = opendir(input_path.c_str());
  while ((ent = readdir(dir)) != NULL) {
      const string file_name = ent->d_name;
      const string full_file_name = input_path + "/" + file_name;

      if ( (file_name[0] == '.') || (stat(full_file_name.c_str(), &st) == -1) )
          continue;

      const bool is_directory = (st.st_mode & S_IFDIR) != 0;
      if (is_directory)
          continue;

      //Skip if tag is not in filename
      if( file_name.find(input_tag) == std::string::npos )
        continue;

      input_file_names.push_back(full_file_name);
  }
  closedir(dir);

  if( input_file_names.size() <= 0 ){
    std::cerr << "Could not find any files with input tag " << input_tag << " in path " << input_path << std::endl;
    exit(1);
  }

  std::cout << "Combining files: \n" ;
  for( unsigned int iF = 0; iF < input_file_names.size(); ++iF ){
    std::cout << "  " << input_file_names.at(iF) << "\n";
  }
  std::cout << std::endl;

  //Open input files
  std::vector<TFile*> input_files;
  for( unsigned int iF = 0; iF < input_file_names.size(); ++iF ){
    TFile* inFile = TFile::Open(input_file_names.at(iF).c_str(), "READ");
    input_files.push_back( inFile );
  }

  //Calculate total sum of weights
  double sum_of_weights = 0.0;
  for( unsigned int iF = 0; iF < input_files.size(); ++iF ){
    TH1F* sum_hist = (TH1F*) input_files.at(iF)->Get(cutflow_name.c_str());
    sum_of_weights += sum_hist->GetBinContent(weight_bin_index);
  }
  double scale_factor = 1./sum_of_weights;


  //Create output file
  TFile *output = TFile::Open( output_file_name.c_str(), "RECREATE");

  //Loop over each key in first file & combine
  TIter next(input_files.at(0)->GetListOfKeys());
  TKey *key;
  while ((key = (TKey*)next() )){
    TObject* obj = key->ReadObj();
    std::string obj_name = key->GetName();

    if( obj->InheritsFrom(TH1::Class()) ){
      TH1* hist = (TH1*) obj;
      hist->Scale( scale_factor );
      //Loop over other files
      for(unsigned int iF=1; iF < input_files.size(); ++iF ){
        TH1* hist_to_add = (TH1*) input_files.at(iF)->Get(obj_name.c_str());
        hist_to_add->Scale( scale_factor );
        hist->Add( hist_to_add );
        delete hist_to_add;
      }//For each file
    }//TH1
    
    else if( obj->InheritsFrom(TH1Bootstrap::Class()) ){
      TH1Bootstrap* boot = (TH1Bootstrap*) obj;
      boot->Scale( scale_factor );
      //Loop over other files
      for(unsigned int iF=1; iF < input_files.size(); ++iF ){
        TH1Bootstrap* boot_to_add = (TH1Bootstrap*) input_files.at(iF)->Get(obj_name.c_str());
        boot_to_add->Scale( scale_factor );
        boot->Add( boot_to_add );
        delete boot_to_add;
      }//For each file
    }//TH1

    //Write object to new file
    output->cd();
    obj->Write( key->GetName() );

  }// for each key

  //Close files
  output->Close();
  for(unsigned int iF=0; iF < input_files.size(); ++iF ){
    input_files.at(iF)->Close();
  }

  std::cout << "Finished combining inputs into " << output_file_name << std::endl;
  return 0;
}
